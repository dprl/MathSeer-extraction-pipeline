## 0.3.1 (2024-11-26)

### Fix

- scaling for coordinates to improve SVG visualization

## 0.3.0 (2024-11-26)

### Feat

- svg rendering improve, doing feature commit to sync docker container version

## 0.2.1 (2024-01-30)

### Fix

- added versions to files

## 0.2.0 (2024-01-30)

### Feat

- **server**: added endpoint get_version

## 0.1.0 (2024-01-30)

### BREAKING CHANGE

- added versioning support

### Feat

- Update latest changes from qdgga repo
- Add logs folder in ScanSSD
- Delete notebook
- Correct requirements
- Create input lg files form image
- Add image operations from lpga
- Add qdgga src code and create formula img inputs
- Fix multi gpu device error
- Update paths and docs
- Update paths and docs
- Add MultiGPU code
- Fix symbolscraper path and add batch size arg
- Uncomment main pipeline
- Add requirements
- Add try except block
- Remove unused code
- Remove old paths
- Remove old paths
- Remove old paths
- Add more logs
- Add more logs
- Add output directories
- Add proper paths in pipeline
- Add directory paths in settings
- Add inputs directory
- Add parser arguments

### Fix

- Add bin folder for SymbolScraper
- Remove breakpoint in conversion
