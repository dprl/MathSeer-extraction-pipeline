# Optimized dataloader (recurrent) train and test

```bash
git clone git@gitlab.com:dprl/graphics-extraction.git
cd graphics-extraction
```

## Generate input data for parser

```bash
git checkout smiles_based_eval_graphs
make conda-remove &&  make
make generate-chem-small
```

### Convert generated files to cdxml/svg (Optional)
```bash
make convert_lg2nx
make convert_lg2nx-test
```

### Visualize generated cdxml/svgs (Optional)
```bash
make visualize-svgs
make visualize-svgs-test
```

## Training and testing Parser
```bash
cd modules/qdgga-parser
git checkout chem-recurrent-uneven-sync
make conda-remove &&  make
```

### Training (recurrent)
**Using multi GPUs**
```bash
make train-chem-small
```

**Using single GPU**

```bash
make train-chem-small-single
```

### Testing (recurrent)
**Using multi GPUs**
```bash
make test-chem-small
```

**Using single GPU**

```bash
make test-chem-small-single
```
