# Dataset Generation and Metrics

## Generate ground truth data (primitive extraction + alignment):

**Note: To turn off visualization → use `-v 0`**

- Pubchem Random 5k dataset:
    - Full set (takes 4-5 hours): `make generate-chem-5k-new && ./bin/combine-chunks -i modules/lgap-parser/data/generated-dataset/chem/train_pubchem_5k_newprim/`
    - Current errors set (around 500 errors):
    `./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/lgap-parser/data/pubchem_5k_newprim_exact_matches_errors.csv -id 0 -sm 1 -o modules/lgap-parser/data/generated-dataset/chem/pubchem_5k_newprim_errors -lgs 0`
    - Other manual sets:
        - Create subsets of `modules/chemscraper/data/pubchem_5k_random/pubchem_random_5k.csv` and pass it to the `--data_file` argument above
- Other small sets (Debugging):
    - `make generate-chem-small-vis`OR
    - Small train set (22 molecules): `./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/indigo_pubchem_small.csv -id 1 -sm 3 -o modules/lgap-parser/data/generated-dataset/chem/train -lgs 0`
    - Small test set (17 molecules): `./generate-visual-lg -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/indigo_pubchem_small_test.csv -id 1 -sm 3 -o modules/lgap-parser/data/generated-dataset/chem/test -lgs 0`

### Primitive Error and visualization outputs:

- The error csv is generated at the output directory as `prim_errors.csv`. 
Examples: `modules/lgap-parser/data/generated-dataset/chem/train_pubchem_5k_newprim/combined/prim_errors.csv` , `modules/lgap-parser/data/generated-dataset/chem/pubchem_5k_newprim_errors/prim_errors.csv`
- The visualizations (primitives, skeletons, etc.) are generated inside `images`
Examples: `modules/lgap-parser/data/generated-dataset/chem/train_pubchem_5k_newprim/combined/images/` , `modules/lgap-parser/data/generated-dataset/chem/train_pubchem_5k_newprim_errors/images/`

## Evaluation of generated ground truth data

### Evaluation (SMILES based)

**Note: Change paths as per your sets and output directories that you used in the commands above**

- For evaluation in terms of how many are completely correct (gives the original SMILES back) -- which is what we need for correct ground truths:
`./visual-postprocess-eval-smiles -c modules/chemscraper/metrics/configs_datasets/indigo_pubchem_small.ini -l modules/lgap-parser/data/generated-dataset/chem/train_pubchem_5k_newprim/combined/lgs/line -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_5k_newprim/combined/smiles_eval -i modules/chemscraper/data/pubchem_5k_random/pubchem_random_5k.csv -id 1 -sm 3 -svg 1 -v 1`

### Filtering errors and corrects

`./bin/filter-exact-matches -i modules/lgap-parser/data/generated-dataset/chem/train_pubchem_5k_newprim/combined/smiles_eval/smiles_meta_indigo_pubchem_small_BORN_DIGITAL.csv -o modules/lgap-parser/data/generated-dataset/chem/train_pubchem_5k_newprim/combined/train_filtered.csv -e 1`

### Visualization tool (Streamlit)

**Note: Change paths as per your sets and output directories that you used in the commands above**

- For visualizing inputs, converted visual graph and output SVGs (useful for checking what went wrong):
`./bin/viz-svgs -pi modules/lgap-parser/data/generated-dataset/chem/train_pubchem_5k_newprim/combined/images/ -s modules/lgap-parser/data/generated-dataset/chem/train_pubchem_5k_newprim/combined/smiles_eval/svg_cdxmls/svgs -g modules/lgap-parser/data/generated-dataset/chem/train_pubchem_5k_newprim/combined/smiles_eval/graphs -f modules/lgap-parser/data/generated-dataset/chem/train_pubchem_5k_newprim/combined/train_filtered_errors.txt`
Change input csvs to change the dataset you want to test.

## Reference scores (current status)

The actual training set we generate for training is `modules/chemscraper/data/pubchem_5k_random/pubchem_random_5k.csv`, which has 5000 molecules. But it may be slow and not possible to generate data for this at once, so there are chunks available as well in the same folder. You may use those when you are ready to test on a larger scale or you may take subsets of those chunks if you want to test more variations of molecules.

**For reference:** the small train (22) and small test (17) had 100% exact SMILES matches. 
**For the larger train set**: there were 4606 exact SMILES matches out of 5000 molecules.
