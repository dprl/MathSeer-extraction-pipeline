import math
import csv
import traceback
import io, json
# RZ: Add debug operations
from src.utils.debug_fns import *
import src.utils.debug_fns as debug_fns
dTimer = DebugTimer("ChemScraper data generation pipeline")
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.

import argparse
from itertools import repeat, chain
from operator import itemgetter
import os
import os.path as osp
import time
import logging
import sys
import math
from configparser import ConfigParser, ExtendedInterpolation
from shapely.validation import make_valid
import random

import cv2
import networkx as nx
import numpy as np
import pandas as pd
from scipy.spatial import distance
from tqdm import tqdm
from matplotlib import cm

from modules.protables.ProTables import read_sscraper_json, read_sscraper_xml, \
    obj_filter_values, filter_bb_list, region_intersection
from modules.chemscraper.extract_mst import ExtractMST
from src.constants import GEN_DATA_DIR, PARALLEL_TOLERANCE, OUTPUT_DIR
from src.convert_pdf_to_image import convert_pdfs, convert2image_pymupdf_paths
from src.dataset import TrainDataset, chunked_iterable
from src.graph_construction import (create_nodes, find_nearest_neighbors,
                                    get_ccs)
from src.file_io import generate_lg
from src.symbolscraper import process_sscraper
from src.tokenizer import get_tokenizer
from src.utils.utils import (do_overlap, get_closest_bboxes, imshow,
                                   visualize_annotations, visualize_symbols)

from modules.chemscraper.parser_v2 import Parser_v2
from modules.chemscraper import nx_merge
from modules.utils.dprl_map_reduce import map_concat, map_reduce, combine_dict_pair
from src.extract_lines import get_label_map, preprocess_image, get_label_map_geometric_primitives
from src.extract_primitives_utils import GeometryHandler
from shapely.geometry import LineString, Polygon, Point
from collections import defaultdict
from modules.chemscraper.nx_debug_fns import shapedraw, draw_points, str_remove_suffix, shapesdraw
from src.demo_utils import display_vector, display_pixel, display_pixel_overlaps_poly

from modules.chemscraper.nx_debug_fns import gdraw, draw_graph
from modules.pipeline.pipeline_run_chemx import process_sscraper_one_pdf
from modules.protables.ProTables import read_sscraper_json_from_io
INTER_THRESHOLD = 0.6
dTimer.qcheck("Python module load")

from multiprocessing import Value

# HACK: Global variables for map-reduce progress
CURR_COUNT=Value('i',0)
MAX_COUNT=None

def get_args():
    parser = argparse.ArgumentParser()
    # parser.add_argument('--data_path', type=str, default="modules/chemscraper/data")
    parser.add_argument('--data_file', type=str,
                        default="modules/chemscraper/data/synthetic/indigo_pubchem_small.csv")
    parser.add_argument('--dynamic_indigo', action='store_false')
    parser.add_argument('--formats', type=str, default="chartok_coords,edges")
    parser.add_argument('--coords_file', type=str, default="aux_file")
    parser.add_argument('--pseudo_coords', action='store_true')
    # parser.add_argument('--save_path', type=str, default=GEN_DATA_DIR)
    # parser.add_argument('--sscraper_path', type=str, default=SSCRAPER_DIR)
    parser.add_argument('--save_image', action='store_false')
    parser.add_argument('--shuffle_nodes', action='store_true')
    parser.add_argument('--vocab_file', type=str,
                        default="modules/generate_data/src/vocab/vocab_chars.json")
    parser.add_argument('--coord_bins', type=int, default=64)
    parser.add_argument('--sep_xy', action='store_false')
    parser.add_argument('--mask_ratio', type=float, default=0)
    parser.add_argument('--continuous_coords', action='store_true')

    # parser.add_argument('-ma', '--mol_augment', action='store_false')
    # parser.add_argument('-d', '--default_option', action='store_false')
    parser.add_argument('-ma', '--mol_augment', type=int, default=0)
    parser.add_argument('-mar', '--mol_augment_rare', type=int, default=0)

    parser.add_argument('-d', '--default_option', type=int, default=1)
    parser.add_argument('-an', '--atom_numbers', type=int, default=0)

    parser.add_argument('-o', '--output_dir', type=str,
                        default=os.path.join(GEN_DATA_DIR, "indigo"))
    # parser.add_argument('-i', '--in_file', type=str, default='test.smi')
    parser.add_argument('--include_condensed', action='store_true')
    parser.add_argument('-r', '--render_pdf', type=int, default=1, 
                        help="Render pdf molecule")
    parser.add_argument( "-c", "--convert_pdfs", type=int, default=1,
                        help="Convert input pdfs to images")
    parser.add_argument( "-v", "--visualize", type=int, default=0, 
                        help="Visualize annotations and graphs")
    parser.add_argument( "-p", "--parallel", type=int, default=1, 
                        help="Parallel flag")
    parser.add_argument( "-pl", "--parallel_lg", type=int, default=1, 
                        help="Parallel flag for lg generation")
    parser.add_argument("-t", "--thresholds",
                        default="modules/chemscraper/data/thresholds-indigo.ini",
                        type=str)
    parser.add_argument("-id", "--id_index_in_file", default=1, type=int)
    parser.add_argument("-sm", "--smiles_index_in_file", default=3, type=int)

    parser.add_argument( "-lgs", "--lg_suffix", type=int, default=0, 
                        help="Same directory for lg files for both modes (pdf\
                        and lines), mode denoted by suffix")
    
    parser.add_argument("-w", "--num_workers", default=-1, type=int)   
    parser.add_argument("-cs", "--chunk_size", type=int)   
    parser.add_argument('--suffix', '-s', type=str, default="")
    parser.add_argument('--dpi', default=300, type=int,
                        help='Default DPI for converting PDF to images')
    parser.add_argument( "--debug", type=int, default=0, 
                        help="Visualize missing primitives")

    # parser.add_argument('-o', '--output_path', type=str,
    #                     default="../outputs/molscribe/")
    args = parser.parse_args()
    thresholds = ConfigParser(interpolation=ExtendedInterpolation())
    thresholds.read(args.thresholds)
    args.thresholds = thresholds
    args.formats = args.formats.split(',')
    args.save_path = args.output_dir
    args.warning_images_path = os.path.join(args.save_path, "images", "prim_warnings")
    args.sscraper_path = os.path.join(args.save_path, "sscraper")

    os.makedirs(args.warning_images_path, exist_ok=True)
    return args


def create_polygon(points, width):
    # Create the original points as numpy arrays for vector operations
    point1 = points[0]
    point2 = points[1]

    # Direction vector from point1 to point2
    direction = point2 - point1

    # Perpendicular vector (normal)
    normal = np.array([-direction[1], direction[0]])

    # Normalize the vectors
    direction_unit = direction / np.linalg.norm(direction)
    normal_unit = normal / np.linalg.norm(normal)

    # Scale the vectors
    lengthening = np.ceil(direction_unit * width)
    widening = np.ceil(normal_unit * width)

    # Calculate the four corners of the polygon
    polygon = np.array([point1 - lengthening + widening, 
                        point1 - lengthening - widening,
                        point2 + lengthening - widening,
                        point2 + lengthening + widening]).clip(0)
    return Polygon(polygon)

def get_line_prims(image, inverted_image, L, filename, args):
    # Extract line primitives and map pdf graphics to these primitives    
    #label_map, label_map_new, line_dict, new_label_dict, lsuperpositions = get_label_map(image)
    # dTimer.qcheck("Start")
    save_path = os.path.join(args.save_path, 'images')
    line_dict, contours = get_label_map_geometric_primitives(image, inverted_image, filename,
                                                                   save_path=save_path,
                                                                   visualize=args.visualize)
    # dTimer.qcheck("Get label map")
    # handler = GeometryHandler()      
    # label_map = handler.line2labelmap(line_dict, image.shape)
    # display_pixel(label_map)
    
    # Sort left-right top-down
    sorted_nodes = sorted(sorted(L.nodes(data=True), key=lambda x:x[1]['box'][1]), 
                          key=lambda x:x[1]['box'][0])
    pdf2lineids = defaultdict(list)
    line_ids_added = {}
    pdf_intersections = defaultdict(dict)
    prim_id_to_node_counter = {}
    inter_dict = {}
    prim_ids = set()
    for node_counter, (_, data) in enumerate(sorted_nodes):
        obj_id = data["obj_id"]
        shape_obj_dict = data["prim_shape_obj_dict"]
        # shape_dict = data["prim_shape_dict"]
        for prim_id in obj_id:
            prim_id_to_node_counter[prim_id] = node_counter
            prim_ids.add(prim_id)
            shape_obj = shape_obj_dict[prim_id]

            if not shape_obj.is_valid:
                print(f"{filename}: Invalid shape object: (node_counter, obj_id, prim_id) = "
                      f"{(node_counter, obj_id, prim_id)}")

                shape_obj = shape_obj.buffer(0)
                if not shape_obj.is_valid:
                    print(f"{filename}: Could not fix shape object: (node_counter, obj_id, prim_id) = "
                          f"{(node_counter, obj_id, prim_id)}")
                    continue
                else:
                    print(f"{filename}: Fixed shape object: (node_counter, obj_id, prim_id) = "
                          f"{(node_counter, obj_id, prim_id)}")

            # if prim_id == 48:
            #     import pdb; pdb.set_trace()
            #     shapesdraw([shape_obj] + [LineString(x) for x in line_dict.values()])
            # shape = shape_dict[prim_id]
            for (new_id, line) in sorted(line_dict.items(), key=itemgetter(0)):
                # Calculate the intersection
                line = LineString(line)
                intersection = shape_obj.intersection(line)
                percent = intersection.length / line.length
                pdf_intersections[prim_id][new_id] = percent
                # print(prim_id, new_id, percent)
                # if shape_obj.buffer(4).contains(line):
                # if prim_id == 48 and new_id == 9:
                #     import pdb; pdb.set_trace()
                    
                if percent > INTER_THRESHOLD:
                    # If already assigned line id matches another pdf prim object
                    # If complete overlapp already found, skip (inter==1)
                    if new_id in line_ids_added: 
                        if max(inter_dict[new_id]) == 1:
                            continue
                        inter_dict[new_id].append(percent)
                        if percent == max(inter_dict[new_id]):
                            for idx, prev_prim_id in enumerate(line_ids_added[new_id]):
                                if new_id in pdf2lineids[prev_prim_id]:
                                    pdf2lineids[prev_prim_id].remove(new_id)
                                    # line_ids_added[new_id].remove(prev_prim_id)
                            pdf2lineids[prim_id].append(new_id)
                        line_ids_added[new_id].append(prim_id)
                        # print(f"{filename}: line {new_id} repeated: {line_ids_added[new_id]}")
                    else:
                        pdf2lineids[prim_id].append(new_id)
                        line_ids_added[new_id] = [prim_id]
                        inter_dict[new_id] = [percent]
    
    # dTimer.qcheck("Map pdf graphics to line primitives")
    left_lines = line_dict.keys() - line_ids_added.keys()
    left_lines_list = list(left_lines)
    # left_lines_copy = left_lines.copy()
    empty_pdf_prims = set([k for k, v in pdf2lineids.items() if not len(v)])
    left_pdf_prims = prim_ids - pdf2lineids.keys()
    left_pdf_prims = left_pdf_prims.union(empty_pdf_prims)
    left_pdf_prims_list = list(left_pdf_prims)
    # left_pdf_prims_copy = left_pdf_prims.copy()

    # Add maximum overlapping left out line prim to left out pdf prims
    for prim_id in left_pdf_prims_list:
        line_id = max(pdf_intersections[prim_id].items(), key=itemgetter(1))[0]
        if line_id in left_lines_list:
            pdf2lineids[prim_id] = [line_id]
            left_pdf_prims_list.remove(prim_id)            
            left_lines_list.remove(line_id)

    line_ids_all = [line_id for line_id in pdf2lineids.values()]
    line_ids_all = list(chain(*line_ids_all))

    prim_warnings = {
                    filename: {"missing_pdf_prim": False, "missing_line_prim": False,
                              "missing_pdf_prim_ids": left_pdf_prims_list,
                              "missing_line_prim_ids": left_lines_list,
                              "missing_pdf_prim_locations": [],
                              "missing_line_prim_locations": []
                              }
                   }
    # print("------------START---------------------------")
    assert len(line_ids_all) == len(set(line_ids_all)), f"{filename}: Repeated primitives!!"
    # if new_label_dict:
    #     print(filename, ":Split lines : \n", new_label_dict) 
    debug_img = None
    if args.debug and (left_lines_list or left_pdf_prims_list):
        debug_img = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
        # debug_img = np.ones((image.shape[0], image.shape[1], 3),
        #                     dtype=np.uint8) * 255

    if left_lines_list:
        # print("\n", filename, ":Remaining line ids: ", left_lines_list) 
        prim_warnings[filename]["missing_line_prim"] = True
        left_points = []
        for left_line_id in left_lines_list:
            points = line_dict[left_line_id]
            if args.debug:
                fill_color(debug_img, points)
            end_points = tuple(map(tuple, points[[0, -1]]))
            left_points.append(end_points)
        prim_warnings[filename]["missing_line_prim_locations"] = left_points

    if left_pdf_prims_list:
        # print("\n********************************************")
        # print(filename, ": Remaining pdf ids: ", left_pdf_prims_list)
        # print("********************************************")
        prim_warnings[filename]["missing_pdf_prim"] = True
        left_points = []
        for left_pdf_id in left_pdf_prims_list:
            node_counter = prim_id_to_node_counter[left_pdf_id]
            shape = sorted_nodes[node_counter][1]["prim_shape_obj_dict"][left_pdf_id]
            points = shape.bounds
            left_points.append( ((points[0], points[1]), (points[2], points[3])) )
            if args.debug:
                if shape.geom_type == "LineString":
                    all_points = np.array(shape.coords).astype(np.int32)
                else:
                    all_points = np.array(shape.exterior.coords).astype(np.int32)
                all_points = all_points.reshape((-1, 1, 2))
                debug_img = cv2.polylines(debug_img, [all_points],
                                          True, (255, 0, 0), 3)
        prim_warnings[filename]["missing_pdf_prim_locations"] = left_points
    # print("------------END---------------------------\n\n")
    if debug_img is not None:
        cv2.imwrite(os.path.join(args.warning_images_path, filename + ".png"),
                    debug_img)
    # dTimer.check("Check missing primitives")
    return pdf2lineids, line_dict, contours, prim_warnings

def fill_color(img_rgb, points):
    #red = np.array([255, 0, 0])
    red = np.array([0, 0, 255])
    x_coords = points[:, 1]
    y_coords = points[:, 0]
    img_rgb[x_coords, y_coords, 0] = red[0]
    img_rgb[x_coords, y_coords, 1] = red[1]
    img_rgb[x_coords, y_coords, 2] = red[2]

def generate_train_data(params):
    global CURR_COUNT, MAX_COUNT
    prim_warnings = {}
    try:
        # dTimer.check("Start")
        filename = f"NO_FILENAME_ERROR_{random.randint(0, 99999)}"
        # args, filename, image, pdf_buffer = params
        args, train_dataset, idx = params

        filename, _, image, _, _, pdf_buffer = train_dataset[idx]
        if image is None:
            return []

        sscraper_info = run_sscraper_buffer((filename, pdf_buffer, args))[0]

        thresholds_dict = {
            "REMOVE_ALPHA":args.thresholds.getfloat("thresholds", "REMOVE_ALPHA"),
            "NEG_CHARGE_Y_POSITION":args.thresholds.getfloat("thresholds", "NEG_CHARGE_Y_POSITION"),
            "NEG_CHARGE_LENGTH_TOLERANCE":args.thresholds.getfloat("thresholds", "NEG_CHARGE_LENGTH_TOLERANCE"),
            "Z_TOLERANCE":args.thresholds.getfloat("thresholds", "Z_TOLERANCE"),
            "CLOSE_NONPARALLEL_ALPHA":args.thresholds.getfloat("thresholds", "CLOSE_NONPARALLEL_ALPHA"),
            "CLOSE_CHAR_LINE_ALPHA":args.thresholds.getfloat("thresholds", "CLOSE_CHAR_LINE_ALPHA"),

            "LONGEST_LENGTHS_DIFF_RATIO":args.thresholds.getfloat("thresholds", "LONGEST_LENGTHS_DIFF_RATIO"),
            "PARALLEL_TOLERANCE":args.thresholds.getfloat("thresholds", "PARALLEL_TOLERANCE"),        
            "COS_PRUNE":args.thresholds.getfloat("thresholds", "COS_PRUNE"),
            "HASHED_LENGTH_DIFF_THRESHOLD":args.thresholds.getfloat("thresholds", "HASHED_LENGTH_DIFF_THRESHOLD"),
        }
        extractor = ExtractMST(thresholds_dict)
        parser = Parser_v2(thresholds_dict)

        ##############################################
        # Combine SymbolScraper + ScanSSD information
        ##############################################
        # Bounding boxes for SymbolScraper PDF characters and words

        # imshow(image)
        # dTimer.check("Reading json")

        # Remove outer rectangles
        (num_pages, page_sizes, pageCharBBs, pageWordBBs, \
        pageGraphicBBs_original, pageGeometryBBs, pageCharGeometryBBs) = sscraper_info
        
        objTypeFilter = obj_filter_values( ["rectangle"] )
        pageGraphicBBs = filter_bb_list(objTypeFilter, pageGraphicBBs_original)

        # Combine lines and characters
        # AKS: Only 1 page per pdf currently
        pageCharGraphicBBs = pageCharBBs[0] + pageGraphicBBs[0]
        # Sort by Left-right, top-down
        pageCharGraphicBBs.sort(key=itemgetter(0, 1))
        pageCharGeometryBBs = filter_bb_list(objTypeFilter, pageCharGeometryBBs)
        pageCharGeometryBBs = pageCharGeometryBBs[0]
        pageCharGeometryBBs.sort(key=itemgetter(0, 1))

        # dTimer.check("Sorting and initial ops")
        if args.visualize:
            image_char_graphic_annotated = visualize_annotations(image, pageCharGraphicBBs)
            if args.save_image:
                path = os.path.join(args.save_path, 'images')
                cv2.imwrite(os.path.join(path, f'{filename}_annotated_char_graphic.png'), 
                            image_char_graphic_annotated)


            image_char_geometry_annotated = visualize_annotations(image, pageCharGeometryBBs)
            if args.save_image:
                path = os.path.join(args.save_path, 'images')
                cv2.imwrite(os.path.join(path, f'{filename}_annotated_char_geometry.png'),
                            image_char_geometry_annotated)

        # dTimer.check("Visualize annotations")
        image = preprocess_image(image)
        inverted_image = 255 - image
        # dTimer.check("Preprocess image")

        region_bbs = [[(0, 0, *image.shape[1::-1])]]
        regionProTable, _ = region_intersection(region_bbs, 'FR', [pageCharGeometryBBs], 
                                                sort_obj_types=False)
        regions = regionProTable[0][0][1]
        graphics = pageGraphicBBs_original[0]

        # dTimer.check("Region intersections")
        prim_msts, prim_graphs, _, _, g_dict = extractor.extract_mst_poly(regionProTable)
        # P=0, R=0, since only 1 graph per page and only 1 page per pdf
        mst_graph = prim_msts[0][0]
        in_graph = prim_graphs[0][0]
        # dTimer.check("MST extraction")
        
        G, mst_graph = parser.close_edges(mst_graph.copy(), in_graph)
        modified_graph = G.copy()

        new_edges = filter(lambda edge: no_shared_neighboords(edge,modified_graph) and \
                           similar_angles(modified_graph.nodes()[edge[0]]["angle"],modified_graph.nodes()[edge[1]]["angle"]) and \
                            coolinear(modified_graph.nodes()[edge[0]]["shape_obj"],modified_graph.nodes()[edge[1]]["shape_obj"]) and\
                            modified_graph.nodes()[edge[0]]["lbl_cls"] == "Ln" and \
                                modified_graph.nodes()[edge[1]]["lbl_cls"] == "Ln", modified_graph.edges())
        new_graph = nx.Graph()
        new_graph.add_nodes_from(modified_graph.nodes(data=True))
        new_graph.add_edges_from(new_edges)
        coolinear_groups = [new_graph.subgraph(c).copy() for c in nx.connected_components(new_graph) if new_graph.subgraph(c).copy().number_of_nodes()>1]
        recreated_graph = None
        for group in coolinear_groups:
            nodes = sorted(list(group.nodes()))
            master_node = nodes[0]
            for index in range(1, len(nodes)):
                new_data = update_node_data_pair(modified_graph.nodes(data=True)[nodes[0]], 
                                                 modified_graph.nodes(data=True)[nodes[index]])
                recreated_graph = nx.contracted_nodes(modified_graph, master_node, nodes[index],copy=True)
                nx.set_node_attributes(recreated_graph, {master_node:new_data})                
        if recreated_graph is not None:
            self_loops = list(nx.selfloop_edges(recreated_graph))
            recreated_graph.remove_edges_from(self_loops)
            G = recreated_graph
        
        # import pdb;pdb.set_trace()
        # H = parser.merge_chars(mst_graph.copy(), G)
        # Only annotation numbers have been contracted after close_edges
        J = expand_number_annotations(G)
        # H = parser.merge_parallel_lines( H )
        # I = parser.annotate_and_merge_brackets(G)
        # J = parser.fixing_wedge( I )
        H = split_hashed_wedges(J, regions, extractor, graphics, g_dict)
        # gdraw(H)

        K = annotate_bonds(H)
        # L = expand_characters(K, G)
        L = annotate_prims(inverted_image, K)

        # dTimer.check("Post-processing graph algorithms")
        # gdraw(L, hide_enum=True)
        # gdraw(L, hide_enum=True, pdf_obj=os.path.join(args.save_path, "graphs",
        #                                               filename + "_2"))
        # draw_graph(L)
        if args.visualize:
            draw_graph(L, save_path=args.save_path, file_name=filename, save=True,
                    title=filename)
        # dTimer.check("Draw and save graph")

        # ------------------------------------------------------------------------------
        # Extract line primitives and map pdf graphics to these primitives
        pdf2lineids, line_dict, contours, prim_warnings = get_line_prims(image, inverted_image, L, filename, args)
        # dTimer.check("Extract line primitives")
        
        # Generate LG file
        # lg_pdf = generate_lg(args, filename, L, None, None, None, mode="pdf")
        lg_line = generate_lg(args, filename, L, pdf2lineids, line_dict, mode="line")
        # lgs = {"pdf": lg_pdf, "line": lg_line}
        lgs = {"pdf": None, "line": lg_line}
        if args.lg_suffix:
            lg_paths = {"pdf": os.path.join(args.save_path, 'lgs', "both",
                                            f"{filename}_pdf.lg"),
                        "line": os.path.join(args.save_path, 'lgs', "both",
                                            f"{filename}_line.lg")}
        else:
            lg_paths = {
                # "pdf": os.path.join(args.save_path, 'lgs', "pdf", 
                #                             f"{filename}.lg"), 
                        "line": os.path.join(args.save_path, 'lgs', "line",
                                            f"{filename}.lg")}

        for mode, lg_path in lg_paths.items():
            with open(lg_path, "w") as f:
                f.write(lgs[mode])
        # dTimer.check("Generate LG file")
        # # import pdb; pdb.set_trace()
        # # TODO: Use molscribe information to check
    except Exception as e:
        path_errors = os.path.join(args.save_path, 'errors')
        os.makedirs(path_errors, exist_ok=True)
        traceback_string = traceback.print_exc()
        with open(os.path.join(path_errors,f"{filename}.json"),"w+") as f:
            data_error = {
                "filename":filename,
                "Exception":str(e),
                "Traceback":str(traceback_string)
            }
            f.write(json.dumps(data_error, indent=4))      
    # HACK: Track progress using global thread-locked variable CURR_COUNT
    with CURR_COUNT.get_lock():
        CURR_COUNT.value += 1
        percentage = CURR_COUNT.value / MAX_COUNT 
        if CURR_COUNT.value < MAX_COUNT:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} molecules generated [ {percentage:3.2%} ]                    \r', end='')
        else:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} molecules generated [ {percentage:3.2%} ]                    \n\n')
        #time.sleep(0.25) # for debugging
    return prim_warnings
    
def split_hashed_wedges(G, regions, extractor, graphics, g_dict):

    hash_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'HW')
    graph_view = nx.subgraph_view(G, filter_node=hash_selector)

    count = 0
    hashed_nodes = list(graph_view.nodes(data=True))
    for node_id, data in hashed_nodes:
        obj_id = data["obj_id"][0]
        hashed_object = regions[obj_id]
        obj_type, obj_label, lbl_type, obj_properties = extractor.get_obj_attrs(hashed_object)

        neis = list(G.neighbors(node_id))

        # Unmerge Hashed wedges to constituent lines
        nodes_list = []
        for graphic_obj_id in hashed_object[-1]:
            graphic_obj = graphics[graphic_obj_id]
            graphic_obj_type, graphic_obj_label,\
            graphic_lbl_type, graphic_obj_properties = extractor.get_obj_attrs(graphic_obj)
            new_node_id = len(regions) + count
            node_name, node_dict, g_dict = extractor.create_node(graphic_obj_properties,
                                                         graphic_obj_label, graphic_obj_type, new_node_id,
                                                         graphic_lbl_type, g_dict)
            add_node = True
            # If a line in the hased wedge intersects with other lines, skip that
            # line, as it causes problem with aligning with overmerged
            # line primitives
            for nei in neis:
                if node_dict["shape_obj"].intersects(G.nodes[nei]["shape_obj"]):
                    add_node = False
                    break
            if add_node:
                nodes_list.append((node_name, node_dict))
                count += 1

        G.add_nodes_from(nodes_list)
        nodes_list = sorted(nodes_list, key=lambda x:tuple(x[1]["box"][:2]))
        node_end1, node_end2 = nodes_list[0], nodes_list[-1]
        
        G.add_edges_from((nodes_list[i][0], nodes_list[i+1][0]) for i in range(len(nodes_list) - 1))
        
        neis = list(G.neighbors(node_id))
        for nei in neis:
            distance1 = node_end1[1]["shape_obj"].distance(G.nodes[nei]["shape_obj"]) 
            distance2 = node_end2[1]["shape_obj"].distance(G.nodes[nei]["shape_obj"]) 
            if distance1 < distance2:
                distance = distance1
                closest_node = node_end1[0]
            else:
                distance = distance2
                closest_node = node_end2[0]

            G.add_edge(closest_node, nei, weight=distance, parallel=False,
                       ln_intersect=False)

        G.remove_node(node_id)
    return G

def expand_number_annotations(G):
    annotation_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'Annotation')
    H = nx.subgraph_view(G, filter_node=annotation_selector).copy()
    for node, data in H.nodes(data=True):
        if 'contraction_label' in data:
            G.add_nodes_from(data['contraction'].items())
            G.add_edges_from(zip(repeat(node), data['contraction'].keys()))
    return G




def expand_characters(G, M):
    G_org = G.copy()
    char_selector = nx_merge.node_attrs_match( G, 'lbl_cls', 'Char', 'Charge')
    H = nx.subgraph_view(G, filter_node=char_selector).copy()
    node_data = H.nodes(data=True)
    nx.set_edge_attributes(G, "CONNECTED", 'label')
    label_dict = {}

    for node, data in node_data:
        if 'contraction_label' in data:
            # if data['lbl_cls'] == "Charge":
            #     del data['pos']
            # G.nodes[node]['lbl_type'] = str_remove_suffix(node)
            # G.nodes[node]['contraction_label'] = str_remove_suffix(node)
            contract_nodes = list(data["contraction"].items())
            G.add_nodes_from(contract_nodes)
            contract_nodes.append((node, data))
            contract_nodes_ids = list(map(itemgetter(0), contract_nodes))

            G.remove_edges_from(G_org.edges(node))
            for contract_node in contract_nodes_ids:
                for p_node, c_node, edge_data in M.edges(contract_node, data=True):
                    if c_node not in G.nodes():
                        c_node = G.graph["removed_nodes_parent"][c_node]
                    G.add_edge(p_node, c_node, weight=edge_data['weight'])
                    if p_node in contract_nodes_ids and c_node in contract_nodes_ids:
                        # G[edge[0]][edge[1]]['label'] = 'CONCATENATED'
                        G[p_node][c_node]['label'] = 'CONNECTED'
                    else:
                        G[p_node][c_node]['label'] = 'CONNECTED'
    # draw_graph(G)
    # import pdb; pdb.set_trace()
    return G

def annotate_bonds(G):
    G = G.copy()
    line_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'Ln' )
    H = nx.subgraph_view(G, filter_node=line_selector)

    ln_type = None
    for node in H.nodes():
        if 'contraction_label' in G.nodes[node]:
            lbl = G.nodes[node]['contraction_label']
            lbl = lbl.split('||')[1].strip(" ")
                
            if lbl == '2':
                ln_type = 'Double'
            elif lbl == '3':
                ln_type = 'Triple'
        else:
            ln_type = 'Single'
        if ln_type is None:
            ln_type = "UNKNOWN"
        G.nodes[node]["contraction_label"] = ln_type
    return G

def get_contours(image, polygon, box):
    poly_box = np.array(polygon.bounds)
    min_x, min_y = np.floor(poly_box[:2]).astype(int)
    max_x, max_y = np.ceil(poly_box[2:]).astype(int)

    poly_box = np.array((min_x, min_y, max_x, max_y))
    
    cropped_img = image[min_y:max_y+1, min_x:max_x+1]

    diff = box - poly_box

    # Create a mask image of the same size as the original image, initialized to black
    mask = np.zeros(cropped_img.shape[:2], dtype=np.uint8)

    # Draw the polygon on the mask (fill with white)
    points = (np.array(polygon.exterior.coords) - np.array([min_x, min_y])).astype(np.int32)
    cv2.fillPoly(mask, [points], (255))

    # Apply the mask to the original image
    cropped_img = cv2.bitwise_and(cropped_img, cropped_img, mask=mask)

    if diff[0] > 0:
        cropped_img[:, :diff[0]] = 0
    if diff[1] > 0:
        cropped_img[:diff[1], :] = 0
    if diff[2] < 0:
        cropped_img[:, -1:diff[2]:-1] = 0
    if diff[3] < 0:
        cropped_img[-1:diff[3]:-1, :] = 0
        
    # contours, _ = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours, _ = cv2.findContours(cropped_img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    original_contours = [(contour + np.array([[min_x, min_y]])).tolist()
                         for contour in contours]

    # import pdb; pdb.set_trace()
    # img_test = np.zeros_like(cropped_img)
    # cv2.drawContours(img_test, contours, contourIdx=-1, color=255,
    #                  thickness=-1)

    return original_contours

def annotate_prims(image, G):
    G = G.copy()
    # line_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'Ln' )
    # H = nx.subgraph_view(G, filter_node=line_selector)
    linewidths_all = nx.get_node_attributes(G, "lineWidth")
    linewidths_all = list(filter(lambda x: not isinstance(x, list), 
                                 linewidths_all.values()))
    linewidths_mean = sum(linewidths_all) / len(linewidths_all)

    for node, data in G.nodes(data=True):
        dict_contracted_lines = data.get('contraction', None)
        box_dict = {}
        shape_dict = {}
        shape_obj_dict = {}
        contours_dict = {}
        own_box = data["box"]
        own_box[:2] = list(map(math.floor, own_box[:2]))
        own_box[2:] = list(map(math.ceil, own_box[2:]))
        own_obj_id = data["obj_id"][0]
        box_dict[own_obj_id] = own_box
        shape = data['shape'] 
        # For hashed wedge to avoid intersection errors later,
        shape_obj = data['shape_obj']

        if "HW" in node:
            points = data["points"]
            # TODO: The actual bbox can be constructed using the points below
            # but, it will not miss parts of the wedge lines since the points do
            # not consider the line width. Requires more complex logic to
            # incorporate line width to expand the polygon, so use the
            # rectangular bbox for now
            # points = [*points[:2], *points[-1:-3:-1]]
            points = [[own_box[0], own_box[1]], [own_box[0], own_box[3]], 
                      [own_box[2], own_box[3]], [own_box[2], own_box[1]]]
            shape_obj = Polygon(points)
            shape = "polygon"
            data["shape"] = shape

        if shape == "linestring":
            points = np.array(shape_obj.coords)
            if len(points) != 2:
                continue
            linewidth = data["lineWidth"]
            if isinstance(linewidth, list):
                if len(linewidth):
                    linewidth = linewidth[0]
                else:
                    linewidth = linewidths_mean
            linewidth = math.ceil(linewidth)

            shape_obj = create_polygon(points, linewidth)
            shape = "polygon"
            data["shape"] = shape

        shape_obj_dict[own_obj_id] = shape_obj
        shape_dict[own_obj_id] = shape

        contours = get_contours(image, shape_obj, own_box)
        contours_dict[own_obj_id] = contours

        if dict_contracted_lines and data["lbl_cls"] != "Char" and \
                data["lbl_cls"] != "Charge" and data["lbl_cls"] != "Annotation":
            boxes = []
            obj_ids = []
            boxes.append(own_box)
            obj_ids.append(own_obj_id)

            for prop in dict_contracted_lines.values():
                box = prop['box']
                obj_id = prop['obj_id'][0]
                box[:2] = list(map(math.floor, box[:2]))
                box[2:] = list(map(math.ceil, box[2:]))
                box_dict[obj_id] = box
                prop_shape_obj = prop['shape_obj']
                prop_shape = prop['shape']
                if prop_shape == "linestring":
                    points = np.array(prop_shape_obj.coords)
                    if len(points) != 2:
                        continue
                    linewidth = math.ceil(prop["lineWidth"])
                    prop_shape_obj = create_polygon(points, linewidth)
                    prop_shape = "polygon"
                    prop["shape"] = prop_shape

                shape_obj_dict[obj_id] = prop_shape_obj
                shape_dict[obj_id] = prop_shape
                contours = get_contours(image, prop_shape_obj, box)
                contours_dict[obj_id] = contours

                boxes.append(box)
                obj_ids.append(obj_id)

            boxes = np.array(boxes)
            new_box = boxes.min(axis=0)[:2].tolist()
            new_box.extend(boxes.max(axis=0)[2:].tolist())
            data['combined_box'] = new_box
            data['obj_id'] = obj_ids
        data['prim_box_dict'] = box_dict
        data['prim_shape_dict'] = shape_dict
        data['prim_shape_obj_dict'] = shape_obj_dict
        data['prim_contours_dict'] = contours_dict
    return G

def get_chemdraw_data(args):
    train_files = args.data_file.split(',')
    train_df = pd.concat([pd.read_csv(file) for file in train_files])
    train_df.fillna("", inplace=True)
    logging.debug(f'Total number of molecules: {len(train_df)}')
    tokenizer = get_tokenizer(args)
    return train_df, tokenizer

def run_sscraper_buffer(params):
    filename, pdf_io, args = params
    try:
        pdf_io = io.BytesIO(pdf_io)
        json_content = process_sscraper_one_pdf(filename, pdf_io)
        json_dict = json.loads(json_content)
        sccraper_payload = \
                read_sscraper_json_from_io(io.BytesIO(json_content), "server", 
                                           osp.join( args.save_path, "images", filename), 
                                           extract_graphics=True, 
                                           parallel=False, png_ext="PNG",
                                           doc_dir_mode=False, width_adjustment=0.15, 
                                           remove_manual_lines=False)
        return [sccraper_payload]
    except Exception as e:
        print(e)
        print(traceback.print_exc())
        return [[]]

def write_smiles_csv(train_dataset, output_file):
    with open(output_file, 'w',  newline='') as file:
        writer = csv.writer(file)
    
        # Write the header row
        writer.writerow(('filename', 'smiles'))
        
        # Write data rows
        for row in zip(train_dataset.filenames, train_dataset.augmented_smiles):
            writer.writerow(row)

def read_sscraper(params):
    args, filename = params
    sscraper_out_dir = osp.join(args.save_path, "sscraper")
    try:  # ML: UGLY HACK if scanssd doesnt make a pro for a document for any reason this blows up
        # sccraper_payload_xml = read_sscraper_xml( sscraper_out_dir, filename,
        #                                     osp.join( args.save_path, "images",
        #                                              filename) ,
        #                                     extract_graphics=True)
        sccraper_payload = read_sscraper_json( sscraper_out_dir, filename,
                                              osp.join( args.save_path, "images", filename) ,
                                              extract_graphics=True, parallel=False,
                                              png_ext="PNG", doc_dir_mode=False,
                                              width_adjustment=0.15, remove_manual_lines=False)

        # (num_pages, page_sizes, pageCharBBs, pageWordBBs, \
        #  pageGraphicBBs, pageGeometryBBs, pageCharGeometryBBs) = sccraper_payload
        
        # (_, _, pageCharBBs_xml, pageWordBBs_xml, pageGraphicBBs_xml) = sccraper_payload_xml
    except FileNotFoundError as e:
        logging.warning(f"could not find a png")
        logging.warning(e)
    except Exception as e:
        logging.warning(e)
        print(traceback.print_exc())
        logging.warning(f"could not find sscraper file: {sscraper_out_dir}/{filename}")
        return
    return [sccraper_payload]

def similar_angles(a, b):
    if type(a) == type([]) or type(b) == type([]):
        return False            
    return abs(a-b)<10

def coolinear(l1, l2):            
    p1 = l1.interpolate(l1.project(Point(l2.coords[0])))
    p2 = l1.interpolate(l1.project(Point(l2.coords[1])))
    projected_line = LineString([p1,p2])
    # import pdb;pdb.set_trace()
    return projected_line.length==0

def update_node_data_pair(data1,data2):            
    dist_matrix = distance.cdist(np.array(data1["verts"]),np.array(data2["verts"]))
    index = np.where(dist_matrix == dist_matrix.max())

    new_verts = [data1["verts"][index[0].item()], data2["verts"][index[1].item()]]
    new_shape_obj = LineString(new_verts)
    new_length = new_shape_obj.length
    p1 = Point(new_verts[0])
    p2 = Point(new_verts[1])
    new_midpoint = ((p1.x+p2.x)/2, (p1.y+p2.y)/2)
    new_angle = (data1["angle"]+data2["angle"])/2
    new_slope = (data1["slope"]+data2["slope"])/2

    BOXES = np.array(data1["box"]+data2["box"]).reshape((2,-1))
    new_box = [BOXES[:,0].min(), BOXES[:,1].min(), BOXES[:,2].max(), BOXES[:,3].max()]

    data1["angle"] = new_angle
    data1["box"] = new_box
    data1["length"] = new_length
    data1["linestring"] = new_shape_obj
    data1["midpoint"] = new_midpoint
    data1["points"] = new_verts
    data1["shape_obj"] = new_shape_obj
    data1["slope"] = new_slope
    data1["verts"] = new_verts
    
    return data1            

def no_shared_neighboords(edge,G):
    nei1 = G.neighbors(edge[0])
    nei2 = G.neighbors(edge[1])

    return len(set(nei1).intersection(set(nei2))) == 0

def main():
    global CURR_COUNT, MAX_COUNT
    args = get_args()
    num_warnings = 0

    # Initiate logging and handle command line arguments.
    log_dir = os.path.join(OUTPUT_DIR, "logs")
    os.makedirs(log_dir, exist_ok=True)
    filepath = os.path.join(log_dir, "mol_info_" +
                            str(round(time.time())) + ".log")
    print("Logging pipeline to:\n  " + osp.abspath(filepath))
    logging.basicConfig(
        filename=filepath, filemode="w",
        format="%(process)d - %(asctime)s - %(message)s",
        datefmt="%d-%b-%y %H:%M:%S", level=logging.DEBUG,
    )
    consoleHandler = logging.StreamHandler(sys.stdout)
    # Ignore warnings from these modules
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("matplotlib").setLevel(logging.WARNING)
    logging.getLogger("PIL").setLevel(logging.WARNING)
    logging.getLogger().addHandler(consoleHandler)

    # Create dataset from the input csv
    logging.debug(f'\n [Reading CSV data from {args.data_file} ...]')
    train_df, tokenizer = get_chemdraw_data(args)
    dTimer.qcheck("Read CSV data")

    logging.debug(f'\n [Constructing dataset object]')
    train_dataset = TrainDataset(args, train_df, tokenizer, split='train', 
                                 dynamic_indigo=args.dynamic_indigo,
                                 suffix=args.suffix)
    dTimer.qcheck("Construct dataset object")

    logging.debug(f'\n [Rendering PDF molecules from SMILES to {os.path.join(args.save_path, "pdf")}]')
    # Render pdfs for molecules
    # train_dataset.render_molecules(save_molecule=False)

    # Creating output lg directories if does not exist
    if args.lg_suffix:
        lg_path = os.path.join(args.save_path, 'lgs', "both")
        os.makedirs(lg_path, exist_ok=True)
    else:
        pdf_path = os.path.join(args.save_path, 'lgs', "pdf")
        line_path = os.path.join(args.save_path, 'lgs', "line")
        os.makedirs(pdf_path, exist_ok=True)
        os.makedirs(line_path, exist_ok=True)
    prim_warnings = {}
    MAX_COUNT = len(train_dataset)
    chunk_size = getattr(args, "chunk_size", None) or len(train_dataset)
    prim_warnings_all = {}
    for i, chunk in enumerate(chunked_iterable(range(len(train_dataset)), chunk_size)):  # Iterate over chunks sequentially
        print(f'>>> Processing chunk {i+1} / {math.ceil(len(train_dataset)/chunk_size)} ...\n')
        train_dataset.render_molecules(save_molecule=args.render_pdf,
                                       parallel=args.parallel,
                                       chunk=chunk)
        dTimer.qcheck("Rendering pdf molecules")

        # import pdb; pdb.set_trace()
        # Convert pdfs to images
        if args.convert_pdfs:
            # convert_pdfs(args.save_path)
            image_path = os.path.join(args.save_path, "images")
            pdf_path = os.path.join(args.save_path, "pdf")
            logging.debug(f'\n [Converting PDF pages to PNG images: ]')
            convert2image_pymupdf_paths(pdf_path, image_path,
                                        selected_files=set([train_dataset.filenames[i]
                                                           for i in chunk]),
                                        dpi=args.dpi, extension="PNG",
                                        parallelize_pdf=True,
                                        num_workers=args.num_workers)
        dTimer.qcheck(f"Pdf to png conversion: {os.path.join(args.save_path, 'images')}")
        logging.debug(f'\n [Generating training data (labeled graphs) to '
                      f'{os.path.join(args.save_path, "lgs")} ... ]')

        if args.parallel_lg:
            input_list = [(args, train_dataset, idx) for idx in range(len(chunk))]
            prim_warnings = map_reduce(generate_train_data, combine_dict_pair, input_list, cpu_count=args.num_workers)
        else:
            for idx in chunk:
                try:
                    filename, _, image, _, _, pdf_buffer = train_dataset[idx]
                    prim_warnings.update(generate_train_data((args, filename, image, pdf_buffer)))
                except Exception as e:
                    import traceback
                    traceback.print_exc()
                    logging.debug(f"Error in filename: {train_dataset.filenames[idx]}")
                    logging.debug(e)
        prim_warnings_all.update(prim_warnings)

    smiles_out_file = os.path.join(args.save_path, f"smiles_input{args.suffix}.csv")
    write_smiles_csv(train_dataset, smiles_out_file)

    if prim_warnings:
        prim_warnings_df = pd.DataFrame.from_dict(prim_warnings_all, orient='index')
        # Filter only errors
        prim_warnings_df = prim_warnings_df[prim_warnings_df.any(axis=1)]
        prim_warnings_df.reset_index(inplace=True)
        column_order = ["index", "missing_line_prim", "missing_pdf_prim",
                        "missing_line_prim_ids", "missing_line_prim_locations",
                        "missing_pdf_prim_ids", "missing_pdf_prim_locations"]
        prim_warnings_df = prim_warnings_df[column_order]
        prim_warnings_df.rename(
            columns={
                "index": "Filename",
                "missing_line_prim": "Excluded line primitive",
                "missing_pdf_prim": "Undersegmentation/Excluded PDF primitive",
                "missing_line_prim_ids": "Excluded line primitive ids",
                "missing_line_prim_locations": "Excluded line primitive locations",
                "missing_pdf_prim_ids": "Undersegmented/excluded PDF primitive ids",
                "missing_pdf_prim_locations": "Undersegmented/excluded PDF primitive locations",
                },
            inplace=True)
        warning_file = os.path.join(args.save_path, f"prim_warnings{args.suffix}.tsv")
        prim_warnings_df.to_csv(warning_file, sep='\t')
        num_warnings = len(prim_warnings_df)
        logging.debug(f'\n Primitive warning info generated at {warning_file}')

    logging.debug(f' Total warnings in primitive extraction before postprocessing = {num_warnings}/{len(train_dataset)}')
    logging.debug(f' Note that most of these warnings are resolved in postprocessing\n\n')
    dTimer.qcheck(f"Train data creation (labeled graphs): parallel ="
                  f" {bool(args.parallel)}, visualize={bool(args.visualize)}: {os.path.join(args.save_path, 'lgs')}")
    logging.debug(dTimer)

if __name__ == "__main__":
    main()

