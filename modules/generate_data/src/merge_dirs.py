import os
import shutil
import argparse
from pathlib import Path
from tqdm import tqdm
from itertools import repeat
from src.utils.debug_fns import *
import src.utils.debug_fns as debug_fns
from modules.utils.dprl_map_reduce import map_concat
dTimer = DebugTimer("ChemScraper data generation pipeline")
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.

dTimer = DebugTimer("Test")

from multiprocessing import Value

# HACK: Global variables for map-reduce progress
CURR_COUNT=Value('i',0)
MAX_COUNT=None

def copy_new(params):
    global CURR_COUNT, MAX_COUNT
    src, dest, item = params
    s = os.path.join(src, item)
    d = os.path.join(dest, item)
    try:
        if os.path.isdir(s):
            if os.path.exists(d):
                for file in os.listdir(s):
                    src_file = os.path.join(s, file)
                    dest_file = os.path.join(d, file)
                    # if os.path.isdir(src_file):
                    #     shutil.copytree(src_file, dest_file, dirs_exist_ok=True)  # Copy subdirectories
                    # else:
                        # shutil.copy2(src_file, dest_file)  # Copy files
                    shutil.copy2(src_file, dest_file)  # Copy files
            else:
                shutil.copytree(s, d)
        else:
            shutil.copy2(s, d)
    except FileNotFoundError:
        print(f"Warning: File or directory not found: {s}, skipping.")

    # HACK: Track progress using global thread-locked variable CURR_COUNT
    with CURR_COUNT.get_lock():
        CURR_COUNT.value += 1
        percentage = CURR_COUNT.value / MAX_COUNT 
        if CURR_COUNT.value < MAX_COUNT:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} files copied [ {percentage:3.2%} ]                    \r', end='')
        else:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} files copied [ {percentage:3.2%} ]                    \n\n')
        #time.sleep(0.25) # for debugging
    return []

def copy_directory_contents(src, dest):
    """Copy contents from src to dest, creating directories as needed."""
    global CURR_COUNT, MAX_COUNT
    print(f">>Copying {src} to {dest}")
    if not os.path.exists(dest):
        # os.makedirs(dest, exist_ok=True)
        shutil.copytree(src, dest)
        print(f">>Copying complete\n")
        return

    # os.makedirs(dest, exist_ok=True)
    items = os.listdir(src)
    MAX_COUNT = len(items)
    # dTimer.check(f"Copying {src}")

    params = list(zip(repeat(src), repeat(dest), items))
    map_concat(copy_new, params)
    # dTimer.check(f"Parallel copy of {src}")
    # for item in tqdm(items, desc=f"Copying {src}", total=len(items)):
    #     s = os.path.join(src, item)
    #     d = os.path.join(dest, item)
    #     try:
    #         if os.path.isdir(s):
    #             shutil.copytree(s, d)
    #         else:
    #             shutil.copy2(s, d)
    #     except FileNotFoundError:
    #         print(f"Warning: File or directory not found: {s}, skipping.")

    # dTimer.check(f"Series copy of {src}")
    CURR_COUNT.value = 0

def copy_files(src_dir, dest_dir, file_patterns):
    """Copy specific files matching patterns from src_dir to dest_dir."""
    if os.path.exists(src_dir):
        os.makedirs(dest_dir, exist_ok=True)
        files = [f for f in os.listdir(src_dir) if any(f.endswith(pattern) for pattern in file_patterns)]
        for file in tqdm(files, desc=f">>Copying files from {src_dir} to {dest_dir}:", total=len(files)):
            try:
                shutil.copy(os.path.join(src_dir, file), os.path.join(dest_dir, file))
            except FileNotFoundError:
                print(f"Warning: File not found: {file}, skipping.")

def merge_directories(input_dirs, output_dir):
    """Merge specified contents from multiple input directories into a single output directory."""
    os.makedirs(output_dir, exist_ok=True)
    
    # Define folder and file paths to copy
    folders_to_copy = [
        ("combined/images", "images"),
        ("combined/lgs/line", "lgs"),
        ("combined/graphs", "graphs"),
        ("combined/pdf", "pdf"),
        ("combined/errors", "errors")
    ]
    file_patterns = [".tsv", ".csv"]
    
    
    for input_dir in input_dirs:
        print(f"Processing directory: {input_dir}")
        input_path = Path(input_dir)
        
        # Copy specific folders
        for src_subpath, dest_subpath in folders_to_copy:
            src = input_path / src_subpath
            dest = Path(output_dir) / dest_subpath
            
            if os.path.exists(src):
                copy_directory_contents(src, dest)
        
        # Copy csv and tsv files
        copy_files(input_path / "combined", output_dir, file_patterns)
        
        # Copy smiles_eval* folders
        for sub_dir in input_path.iterdir():
            if sub_dir.is_dir() and sub_dir.name.startswith("smiles_eval"):
                copy_directory_contents(sub_dir, Path(output_dir) / sub_dir.name)

        print(f"\nFinished processing directory: {input_dir}")
        print("=" * 50)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Merge multiple dataset directories into one consolidated output.")
    parser.add_argument("output_dir", type=str, help="Path to the output directory.")
    parser.add_argument("input_dirs", nargs='+', type=str, help="List of input directories to merge.")
    
    args = parser.parse_args()
    merge_directories(args.input_dirs, args.output_dir)
    
    print(f"Merging complete. Output stored in: {args.output_dir}")
    print("=" * 50)
    print("=" * 50)
