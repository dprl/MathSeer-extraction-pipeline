################################################################
# Taken from this web page: https://pymupdf.readthedocs.io/en/latest/recipes-multiprocessing.html
################################################################

from __future__ import print_function, division
import sys
import os
import time
from multiprocessing import Pool, cpu_count
import fitz

def render_page(vector):
    """Render a page range of a document.

    Notes:
        The PyMuPDF document cannot be part of the argument, because that
        cannot be pickled. So we are being passed in just its filename.
        This is no performance issue, because we are a separate process and
        need to open the document anyway.
        Any page-specific function can be processed here - rendering is just
        an example - text extraction might be another.
        The work must however be self-contained: no inter-process communication
        or synchronization is possible with this design.
        Care must also be taken with which parameters are contained in the
        argument, because it will be passed in via pickling by the Pool class.
        So any large objects will increase the overall duration.
    Args:
        vector: a list containing required parameters.
    """
    # recreate the arguments
    idx = vector[0]  # this is the segment number we have to process
    cpu = vector[1]  # number of CPUs
    filename = vector[2]  # document filename

    # RZ mod:
    #mat = vector[3]  # the matrix for rendering
    dpi = vector[3]
    output_path = vector[4]
    extension = vector[5]
    pdf_name = os.path.basename(os.path.splitext(filename)[0])
    doc = fitz.open(filename)  # open the document
    num_pages = doc.page_count  # get number of pages

    # pages per segment: make sure that cpu * seg_size >= num_pages!
    seg_size = int(num_pages / cpu + 1)
    seg_from = idx * seg_size  # our first page number
    seg_to = min(seg_from + seg_size, num_pages)  # last page number

    for i in range(seg_from, seg_to):  # work through our page segment
        page = doc[i]
        # page.get_text("rawdict")  # use any page-related type of work here, eg
        #pix = page.get_pixmap(alpha=False, matrix=mat)  # original
        pix = page.get_pixmap(alpha=False, dpi=dpi)

        # store away the result somewhere ...
        output_png = os.path.join( output_path, f"{pdf_name}.{extension}" )
        # print(output_png)
        pix.save( output_png )
        #pix.save( "%i.png" % (i+1))

    #print("Processed page numbers %i through %i" % (seg_from, seg_to - 1))
