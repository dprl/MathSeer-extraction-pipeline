import cv2
import matplotlib.pyplot as plt
import numpy as np
import matplotlib

def imshow(image, title=None, ncols=2, figsize=(15, 10), cmap='gray'): 
    '''
    Visualize an image or a set of images using subplot

    Parameters
    ----------
    image : numpy.ndarray or list of numpy.ndarray
             Image array or the collection of image arrays to be displayed
    title : str ot list of str, optional, default: None
            The title or collection of titles corresponding to the images
    ncols: int, optional, default: 2
           Number of columns for subplot
    figsize: (float, float), optional, default: (12, 15)
             width, height in inches for single image

    Returns
    -------
    None
    '''
    backend = matplotlib.get_backend()
    if backend == "agg":
        matplotlib.use('TkAgg')

    if not isinstance(image, list):
        plt.figure(figsize=figsize)
        plt.imshow(image, cmap=cmap)
        if title:
            plt.title(title, fontsize=30)
    else:
        plt.figure(figsize=figsize)
        # calculate the number of rows
        nrows = int(np.ceil((len(image)) / ncols))
        for i, img in enumerate(image):
            plt.subplot(nrows, ncols, i+1)
            plt.imshow(img, cmap=cmap)
            if title:
                plt.title(title[i], fontsize=50)
    plt.show(block=False)
    plt.pause(0.001)

def visualize_annotations(image, chars_lines, rect_thickness=2, circ_thickness=8):
    image = image.copy()
    color = (0, 165, 255)
    # import pdb; pdb.set_trace()
    
    # import pdb; pdb.set_trace()
    for char in chars_lines:
        if (char[5] == "G" or char[5] == "GC") and char[6].get("isLine", False):
            if len(char[6]["points"]) > 2:  # HACK
                points1 = char[6]["points"][0]
                points2 = char[6]["points"][1]
            else:
                points1, points2 = char[6]["points"]
            points1 = map(int, points1)
            points2 = map(int, points2)
            image = cv2.circle(image, tuple(points1), radius=0,
                             color=color, thickness=circ_thickness)
            image = cv2.circle(image, tuple(points2), radius=0,
                             color=color, thickness=circ_thickness)
        ul_x, ul_y, br_x, br_y = char[:4]
        image = cv2.rectangle(image, (ul_x, ul_y), (br_x, br_y), color, rect_thickness)
    return image

def visualize_symbols(image, sscraper_coords, symbols):
    sym_bbox = {}
    image = image.copy()
    thickness1 = 3
    for sym, chars in symbols.items():
        coords = sscraper_coords[chars]
        # Color symbols with 2 primitives differently
        if len(coords) > 1:
            color = (255, 0, 0)
        else:
            color = (0, 165, 255)
        point1 = tuple(coords[:, :2].min(axis=0))
        point2 = tuple(coords[:, 2:].max(axis=0))
        image = cv2.rectangle(image, point1, point2, color, thickness1)
        sym_bbox[sym] = [*point1, *point2]
    return image, sym_bbox

def do_overlap(bbox1, bbox2):
    ul1_x, ul1_y, br1_x, br1_y = bbox1
    ul2_x, ul2_y, br2_x, br2_y = bbox2

    # if rectangle has area 0, no overlap
    if ul1_x == br1_x or ul1_y == br1_y or br2_x == ul2_x or ul2_y == br2_y:
        return False
     
    # If one rectangle is on left side of other
    if ul1_x > br2_x or ul2_x > br1_x:
        return False
 
    # If one rectangle is above other
    if br1_y < ul2_y or br2_y < ul1_y:
        return False
 
    return True

def get_closest_bboxes(points, bounding_boxes):
    """
    Get distances of bboxes and closest bboxes for each point
    """
    # # Example list of bounding boxes (format: [x_min, y_min, x_max, y_max])
    # bounding_boxes = np.array([[0, 0, 10, 10], [5, 5, 15, 15], [20, 20, 30, 30]])
    
    # # Example list of points (format: [x, y])
    # points = np.array([[3, 3], [10, 10]])
    
    # Extract x and y coordinates for points and bounding boxes
    # point_x, point_y = points[:, 0], points[:, 1]
    bbox_x_min, bbox_y_min = bounding_boxes[:, 0], bounding_boxes[:, 1]
    bbox_x_max, bbox_y_max = bounding_boxes[:, 2], bounding_boxes[:, 3]
    
    # Calculate closest point distances
    closest_distances = np.zeros((len(points), len(bounding_boxes)))
    for i, point in enumerate(points):
        closest_x = np.maximum(bbox_x_min, np.minimum(point[0], bbox_x_max))
        closest_y = np.maximum(bbox_y_min, np.minimum(point[1], bbox_y_max))
        dx = point[0] - closest_x
        dy = point[1] - closest_y
        closest_distance = np.sqrt(dx**2 + dy**2)
        closest_distances[i] = closest_distance

    # Get the indices of bounding boxes sorted by closest point distance
    closest_bbox_indices_per_point = np.argsort(closest_distances, axis=1)
    
    # print("Bounding boxes:")
    # print(bounding_boxes)
    # print("Points:")
    # print(points)
    # print("Closest bounding box indices for each point:")
    # print(closest_distances)
    # print(closest_bbox_indices_per_point)
    return closest_distances, closest_bbox_indices_per_point
