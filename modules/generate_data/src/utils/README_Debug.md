# README\_Debug -- CSCI 335: Assignment 2 (Fall 2023)


## Debugging Library (`src/debug.py`)

A debugging library created for the course has been provided in `src/debug.py`. 
**Students are expected to use this library to develop your
implementation** in small, carefully tested steps.

The provided code imports the main `Debug` class object as `db`
using:

```
	from debug import Debug as db
```

With this import, commands in the library all begin with `db.` (e.g., `db.check()`, `db.pcheck()`). A timer for recording times to reach different parts of the program is also started immediately upon importing the library (see below for details).

## Check Messages and Running Tests

Two main functions to print a message and check a variable/expression's value and optionally run a test case are:

```
	db.check(msg, expression [, boolean test])
``` 

and to generate the same output but **adding a pause that asks the user to press
a key** before continuing:

```
	db.pcheck(msg, expression [, boolean test])
```

These functions allow you to define a string message, an expression to view,
and an optional boolean test (that is run as a Python assertion, with pass/fail
reported). 

* *Message (first two arguments)*: allows viewing a message along with a variable or data structure (e.g.,
  an int, list or dict, which can be defined in-place). The message can be any string, and the expression may be anything that can be passed to the `print()` command in Python.

* *Test case (optional third argument)*: a message is
  printed from the first two argument, but an additional test in the third argument
  is run, with success or
  failure reported.
    * By default, failed tests (i.e., boolean tests that return false) produce
      a failed test message, but do not stop the program. 
    * `db.exit_on_fail()` will force the program to stop upon the first failed
      test, `db.no_exit_on_fail()` returns to the default behavior without
      stopping on failed tests.

* *Message Formatting Functions.* There are additional functions to control formatting in messages. These include:

	* Adding a newline
before the expression (2nd argument) is printed (`db.ncheck`, `db.npcheck`)
	* Adding a debug label before the
message (`db.dcheck`, `db.dpcheck`)
	* Adding both a debug label *and* a newline before printing the expression (2nd argument) (`db.dncheck`, `db.dnpcheck`)


## Timing Parts of Your Program

On importing the debug library, a timer is started. Commands may be used to
record labels associated with time stamps in the timer (similar to a stopwatch
with an option to record multiple times).

Key operations:

* `db.time(<str>)` records timestamps with a label
* `db.ptime(<str>)` will record a timestamp and then pause. Two timestamps are created: one before the pause using the passed string, and one after the pause labeled `( pause)`.
* `db.show_times()` displays all recorded checkpoints along with total time for all checkpoints. 
* `db.vtime(<str>)` ('verbose' timestamp) can be used to output timing information at the command line as well as update the timing object.

The timer is helpful for capturing timing data directly within a program.

 
## Controlling Debug Messages and Viewing the Log 
 
**Turning messages off.** To turn off all debug messages,
`db.msg_off()`.  When messages are turned off, no messages or tests are run or
recorded. Timing information is still collected and can be reported even when messages are turned off. `db.msg_on()` will turn messages on again.

**Checking the log.** You can view current timing and generated messages using
the `db.dump()` command, or save this to a file using `db.dump(<file-name>)`.
The log shows information on the debugger state, timing information, and then
*all* `check`-style commands that were run for the program.

