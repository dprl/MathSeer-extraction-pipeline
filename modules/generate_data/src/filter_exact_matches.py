import os
import csv
import argparse
import pandas as pd

# Custom function to parse command-line arguments
def parse_args():
    parser = argparse.ArgumentParser(description='Filter files')
    parser.add_argument('--filelist', '-f', help='File list to select', type=str,
                        default="")
    parser.add_argument('--input_csv', '-i', help='Path to the input csv',
                        default="")
    parser.add_argument('--output_csv', '-o', help='Path to the output csv',
                        default="")
    parser.add_argument('--error', '-e', type=int, help='Generate error file or not', default=0)
    args = parser.parse_args()
    if not args.output_csv:
        args.output_csv = os.path.join(os.path.splitext(args.input_csv)[0] + "_exact_matches.csv")

    return args

def main():
    args = parse_args()
    # Step 1: Load the list of image IDs from the text file
    filelist_file = args.filelist
    selected_files = set()
    if filelist_file:
        with open(filelist_file, "r") as f:
            selected_files = set(filter(None, f.read().strip("").split("\n")))

        num_filtered_files = len(selected_files)

        # Step 2: Filter the CSV file based on the loaded list of image IDs
        with open(args.input_csv, 'r', newline='') as infile, \
                open(args.output_csv, 'w', newline='') as outfile:
            reader = csv.DictReader(infile, delimiter=',')
            writer = csv.DictWriter(outfile, fieldnames=reader.fieldnames, delimiter=',')
            writer.writeheader()
            for row in reader:
                if selected_files:
                    if row['image_id'] in selected_files:
                        writer.writerow(row)
                else:
                    writer.writerow(row)
    else:
        dataset = pd.read_csv(args.input_csv)
        # just_exact_matches = concatenated[["LONGEST_LENGTHS_DIFF_RATIO","NEG_CHARGE_Y_POSITION","NEG_CHARGE_LENGTH_TOLERANCE","levenshtein_normalized"]]
        just_exact_matches = dataset[dataset.levenshtein == 0]
        final = just_exact_matches[["Unnamed: 0", "ground_truth"]]
        final.columns = ["ID", "SMILES"]        
        with open(os.path.splitext(args.output_csv)[0] + ".txt", "w") as f:
          f.write("\n".join(final.ID.astype("string")).strip())
        final.to_csv(args.output_csv, index=False)
        num_filtered_files = len(final.index)

        if args.error:
            error_csv = os.path.splitext(args.output_csv)[0] + "_errors.csv"
            errors = dataset[dataset.levenshtein != 0]
            final = errors[["Unnamed: 0", "ground_truth"]]
            final.columns = ["ID", "SMILES"]        
            with open(os.path.splitext(error_csv)[0] + ".txt", "w") as f:
              f.write("\n".join(final.ID.astype("string")).strip())
            final.to_csv(error_csv, index=False)
            num_errors = len(final.index)


    print(f">> Filtering complete. \n  The filtered CSV is saved at:\n   {args.output_csv}")
    print(f">> Total formulas after filtering: {num_filtered_files}.")
    if args.error:
        print(f">> Total formulas with errors: {num_errors}.")

if __name__ == '__main__':
    main()
