import os
from operator import itemgetter
from modules.generate_data.src.file_io import points2contours
from modules.generate_data.src.extract_lines import get_label_map_geometric_primitives
from modules.generate_data.src.extract_primitives_utils import get_images_processed
import argparse
import pathlib
from glob import glob
from itertools import repeat
from modules.utils.dprl_map_reduce import map_concat
from tqdm import tqdm
import traceback
import json

import numpy as np
import cv2
from modules.chemscraper.utils.viz_utils import imshow
from modules.generate_data.src.demo_utils import display_pixel


from multiprocessing import Value

# HACK: Global variables for map-reduce progress
CURR_COUNT=Value('i',0)
MAX_COUNT=None

def generate_lg_test(filename, line_dict, mode="line"):    
    outString = f"# IUD, {filename}\n"

    outString += f"# [ OBJECTS ]\n"
    outString += f"#    Objects (O): {len(line_dict)}\n"
    outString += "### Format: O, objId, class, 1.0 (weight), [ primitiveId list ]\n"
    prefix = "O, "
    middle = ", 1.0, "
    
    for key in sorted(line_dict.keys()):
        obj_id = str(key)
        label = "_"
        outString += prefix + "sym_" + obj_id + ", " + label + middle + obj_id + "\n"


    outString += "\n# [PRIMITIVE FEATURES]\n"
    prefix = "#contours, "
    # Write #cc i.e. connected components' box coordinates
    for line_id, points in sorted(line_dict.items(),
                                      key=itemgetter(0)):
        name = str(line_id)
        old_contours = points2contours(points)
        for contour in old_contours:
            contours_str = ", ".join(str(coordinate) for point_list in contour
                                  for point in point_list for coordinate in point)
            outString += prefix + name + f", {contours_str}" + "\n"

    return outString


def generate_test_data(params):
    global CURR_COUNT, MAX_COUNT
    try:
        img_path, args = params
        out_path = args.output_folder_lgs
        visualize = args.visualize
        filename = str(pathlib.Path(os.path.basename(img_path)).with_suffix(""))
        
        ## READ Image from the dataset
        inverted_image, image = get_images_processed(img_path)
        if args.invert:
            image, inverted_image = inverted_image, image
        # import pdb;pdb.set_trace()    
        line_dict, new_label_dict, contours = get_label_map_geometric_primitives(image, inverted_image, filename,
                                                                    save_path=os.path.join(out_path,"images"),
                                                                    visualize=visualize)
        if not args.prim_only:
            lg_content = generate_lg_test(filename, line_dict, contours)
            with open(os.path.join(out_path,"lgs",f"{filename}.lg"),"w+") as f:
                f.write(lg_content)

    except Exception as e:
        path_errors = os.path.join(out_path, 'errors')
        os.makedirs(path_errors, exist_ok=True)        
        traceback_string = traceback.format_exc()
        with open(os.path.join(path_errors,f"{filename}.json"),"w+") as f:
            data_error = {
                "filename":filename,
                "Exception":str(e),
                "Traceback":str(traceback_string)
            }
            f.write(json.dumps(data_error, indent=4))      

    # HACK: Track progress using global thread-locked variable CURR_COUNT
    with CURR_COUNT.get_lock():
        CURR_COUNT.value += 1
        percentage = CURR_COUNT.value / MAX_COUNT 
        if CURR_COUNT.value < MAX_COUNT:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} molecules processed [ {percentage:3.2%} ]                    \r', end='')
        else:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} molecules processed [ {percentage:3.2%} ]                    \n\n')
        #time.sleep(0.25) # for debugging
    return []


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_folder_images",
                        #default="modules/chemscraper/metrics/test_config.ini",
                        type=str)
    parser.add_argument("-o", "--output_folder_lgs",
                        #default="modules/chemscraper/data/thresholds-indigo.ini",
                        type=str)
    parser.add_argument("-e", "--image_extension",
                        default="png",
                        type=str)    
    parser.add_argument("-v", "--visualize",
                        default=0,
                        type=int)    
    parser.add_argument("-p", "--parallel",                        
                        default=1,
                        type=int)    
    parser.add_argument('--filelist', '-f', 
                        help='File list to select', type=str,
                        default="")
    parser.add_argument("-po", "--prim_only",                        
                        default=0,
                        type=int)    
    parser.add_argument("-in", "--invert",                        
                        default=0,
                        type=int)    
    parser.add_argument("-w", "--num_workers", default=-1, type=int)   

    parsed_args = parser.parse_args()
    return parsed_args

def line2labelmap(line_dict, shape) -> np.array:
    label_map = np.zeros(shape, dtype=int)
    for line_id, points in line_dict.items():
        label_map[points[:, 1], points[:, 0]] = line_id+1
    return label_map

#display_pixel(line2labelmap(line_dict,(500,1000)))
def get_lg_from_image(_image, id=None, visualize=False, visualize_dir=""):
    # image = image.astype(np.float32)
    image = cv2.cvtColor(_image, cv2.COLOR_BGR2GRAY)
    #0.7*255 = 178.5 aprox 178
    _, img_in = cv2.threshold(image, 178, 255, cv2.THRESH_BINARY)

    # img_in_inverted = 1. - img_in    
    img_in_inverted = 255-img_in  
    
    if (img_in_inverted == 0).all() or (img_in_inverted == 255).all():
        line_dict = {}
    else:
        line_dict, new_label_dict = get_label_map_geometric_primitives(img_in, img_in_inverted, 
                                                                       filename=id,
                                                                       save_path=visualize_dir,
                                                                       visualize=visualize)
    lg_content = generate_lg_test("not_used", line_dict)    
    return lg_content, img_in, img_in_inverted

def main():    
    global CURR_COUNT, MAX_COUNT
    args = get_args()
    in_path = args.input_folder_images
    out_path = args.output_folder_lgs
    img_extension = args.image_extension
    visualize = args.visualize
    parallel = args.parallel

    filelist_file = args.filelist
    selected_files = set()
    if filelist_file:
        with open(filelist_file, "r") as f:
            selected_files = set(filter(None, f.read().strip("").split("\n")))


    print(f"\nGenerating test lgs from {in_path} to {out_path}\n")    
    os.makedirs(os.path.join(out_path,"lgs"), exist_ok=True)
    if visualize:
        os.makedirs(os.path.join(out_path,"images"), exist_ok=True)
    
    filenames = glob(os.path.join(in_path,f"*.{img_extension}"))
    
    if selected_files:
        filenames = list(filter(lambda x: os.path.basename(x).split(".")[0] in
                            selected_files, filenames))
    
    MAX_COUNT = len(filenames)
    if parallel:
        input_list = list(zip(filenames, repeat(args)))
        _ = map_concat(generate_test_data, input_list, stages=10,
                       cpu_count=args.num_workers)
    else:
        for file_path in tqdm(filenames):
            _ = generate_test_data((file_path, args))

if __name__ == "__main__":
    main()
