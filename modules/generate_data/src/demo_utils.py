# Display image
import cv2
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from PIL import ImageDraw, Image
from matplotlib import cm
# import matplotlib
# matplotlib.use("Agg")

# import sys
# root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
# sys.path.append(root_dir)
# Dataset

# from pylena.scribo import LSuperposition, VSegment
from typing import List


def display_vector(img_in: np.ndarray, vsegments: List, on_image: bool, figsize=(15, 10)) -> None:
    """
    Display the image with the vectorial output
    :param img_in: The input image
    :param img_label:  The pixel label mqp
    :param on_image (Optional, default: False): If the display should only be on the input image or not
    """
    plt.figure(figsize=figsize)
    img_rgb = np.ones(
        (img_in.shape[0], img_in.shape[1], 3), dtype=np.uint8) * 255

    if on_image:
        img_rgb[:, :, 0] = img_in
        img_rgb[:, :, 1] = img_in
        img_rgb[:, :, 2] = img_in

    colors = np.random.randint(
        low=0, high=255, size=(len(vsegments), 3)).tolist()

    for vsegment_index, vsegment in enumerate(vsegments):
        color = tuple(colors[vsegment_index])
        cv2.line(img_rgb, (vsegment.x0, vsegment.y0),
                 (vsegment.x1, vsegment.y1), color)

    plt.imshow(img_rgb)

    plt.xticks([])
    plt.yticks([])
    # plt.show()
    plt.show(block=False)
    plt.pause(0.001)

def display_pixel(img_label: np.ndarray, img_in: np.ndarray = None,
                  superposition_only: bool = False, figsize=(15, 10), 
                  save_path=None, label_one_white=False) -> None:
    """
    Display the image with the pixel output
    :param img_label:  The pixel label map
    :param img_in (Optional, default: None): The input image
    :param superposition_only (Optional, default: False): If the display should only be the superpositions
    :param figsize: The figure size for plotting
    :param save_path: The path to save the output image, if provided
    """
    backend = matplotlib.get_backend()
    if backend == "agg":
        matplotlib.use('TkAgg')

    plt.figure(figsize=figsize)
    img_rgb = np.ones((img_label.shape[0], img_label.shape[1], 3), dtype=np.uint8) * 255

    if img_in is not None:
        img_rgb[:, :, :] = np.repeat(img_in[:, :, np.newaxis], 3, axis=2)

    # Assign white/red to label 1
    if label_one_white:
        img_rgb[img_label == 1] = [255, 255, 255]
    else:
        img_rgb[img_label == 1] = [255, 0, 0]

    if not superposition_only:
        # Assign random colors to labels starting from 2
        unique_labels = np.unique(img_label)
        unique_labels = unique_labels[~np.isin(unique_labels, [0, 1])]
        random_colors = np.random.randint(0, 255, size=(len(unique_labels), 3), dtype=np.uint8)
        label_mask = np.isin(img_label, unique_labels)
        img_rgb[label_mask] = random_colors[np.searchsorted(unique_labels, img_label[label_mask])]

    plt.imshow(img_rgb)
    
    # Display or save plot as requested
    if save_path:
        plt.savefig(save_path)
        plt.close()
    else:
        plt.show(block=False)
        plt.pause(0.001)


def display_pixel_old(img_label: np.ndarray, img_in: np.ndarray = None,
                  superposition_only: bool = False, figsize=(15, 10), 
                  save_path=None) -> None:
    """
    Display the image with the pixel output
    :param img_label:  The pixel label mqp
    :param img_in (Optional, default: None): The input image
    :param superposition_only (Optional, default: False): If the display should only be the superpositions
    """
    backend = matplotlib.get_backend()
    if backend == "agg":
        matplotlib.use('TkAgg')

    plt.figure(figsize=figsize)
    img_rgb = np.ones(
        (img_label.shape[0], img_label.shape[1], 3), dtype=np.uint8) * 255        

    if img_in is not None:
        img_rgb[:, :, 0] = img_in
        img_rgb[:, :, 1] = img_in
        img_rgb[:, :, 2] = img_in
        

    #red = np.array([255, 0, 0])
    red = np.array([255, 0, 0, 0])
    white = np.array([255, 255, 255, 255])
    # img_rgb[img_label == 1, 0] = red[0]
    # img_rgb[img_label == 1, 1] = red[1]
    # img_rgb[img_label == 1, 2] = red[2]

    img_rgb[img_label == 1, 0] = white[0]
    img_rgb[img_label == 1, 1] = white[1]
    img_rgb[img_label == 1, 2] = white[2]
    

    if not superposition_only:
        # np.random.seed(2)
        max_label = np.max(img_label)
        color = np.random.randint(low=0, high=255, size=(max_label - 1, 3))
        for i in range(2, max_label+1):
            img_rgb[img_label == i, 0] = color[i - 2, 0]
            img_rgb[img_label == i, 1] = color[i - 2, 1]
            img_rgb[img_label == i, 2] = color[i - 2, 2]            

    plt.imshow(img_rgb)
    # Display or save plot as requested
    if not save_path:
        # plt.xticks([])
        # plt.yticks([])
        # plt.show()
        plt.show(block=False)
        plt.pause(0.001)
        # plt.show(block=False)
        # plt.pause(0.001)
    else:
        plt.savefig( save_path, format="pdf")
        plt.close()


def display_pixel_overlaps_poly(img_label: np.ndarray, img_in: np.ndarray = None, superposition_only: bool = False, figsize=(15, 10), polygons=None) -> None:
    """
    Display the image with the pixel output
    :param img_label:  The pixel label mqp
    :param img_in (Optional, default: None): The input image
    :param superposition_only (Optional, default: False): If the display should only be the superpositions
    """
    plt.figure(figsize=figsize)
    img_rgb = np.ones(
        (img_label.shape[0], img_label.shape[1], 3), dtype=np.uint8) * 255

    if img_in is not None:
        img_rgb[:, :, 0] = img_in
        img_rgb[:, :, 1] = img_in
        img_rgb[:, :, 2] = img_in

    red = np.array([255, 0, 0])
    img_rgb[img_label == 1, 0] = red[0]
    img_rgb[img_label == 1, 1] = red[1]
    img_rgb[img_label == 1, 2] = red[2]

    if not superposition_only:
        # np.random.seed(2)
        max_label = np.max(img_label)
        color = np.random.randint(low=0, high=255, size=(max_label - 1, 3))
        for i in range(2, max_label+1):
            img_rgb[img_label == i, 0] = color[i - 2, 0]
            img_rgb[img_label == i, 1] = color[i - 2, 1]
            img_rgb[img_label == i, 2] = color[i - 2, 2]
    #polygons
    img2 = Image.fromarray(np.uint8(cm.gist_earth(img_label)*255))
    img_label_normal = Image.fromarray(np.uint8(cm.gist_earth(img_label)*255))
    
    draw = ImageDraw.Draw(img2)
    
    for poly in polygons:
        xx, yy = poly.exterior.coords.xy
        pinga = list(poly.exterior.coords)
        x = list(map(int, list(xx)))
        y = list(map(int, list(yy)))
        
        #draw.polygon(pinga, fill = "wheat")
        draw.polygon(pinga)
    import pdb;pdb.set_trace()
    img3 = Image.blend(img_label_normal, img2, 0.5)
    
    plt.imshow(img_rgb)

    plt.xticks([])
    plt.yticks([])
    # plt.show()
    plt.show(block=False)
    plt.pause(0.001)
