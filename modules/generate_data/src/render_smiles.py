import io
import traceback
import argparse
import cv2
import random
import re
import string
import numpy as np
import os
from rdkit import Chem
from rdkit.Chem.Draw import rdMolDraw2D
import cairosvg
        

from modules.chemscraper.utils.indigo import Indigo
from modules.chemscraper.utils.indigo.renderer import IndigoRenderer

from .chemistry import normalize_nodes
from .constants import RGROUP_SYMBOLS, SUBSTITUTIONS, ELEMENTS, COLORS

from .indigo_methods import *
# from src.utils.utils import imshow
cv2.setNumThreads(1)

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-ma', '--mol_augment', action='store_false')
    parser.add_argument('-d', '--default_option', action='store_false')
    parser.add_argument('-o', '--out_file', type=str, default='out')
    parser.add_argument('-i', '--in_file', type=str, default='test.smi')
    parser.add_argument('-e', '--ext', type=str, default='png', choices=['pdf', 'png'])
    parser.add_argument('-s', '--save_image', action='store_true')
    args = parser.parse_args()
    return args

def get_graph(mol, shuffle_nodes=False, pseudo_coords=False):
    mol.layout()
    coords, symbols = [], []
    index_map = {}
    atoms = [atom for atom in mol.iterateAtoms()]
    if shuffle_nodes:
        random.shuffle(atoms)
    for i, atom in enumerate(atoms):
        if pseudo_coords:
            x, y, z = atom.xyz()
        else:
            x, y = atom.coords()
        coords.append([x, y])
        symbols.append(atom.symbol())
        index_map[atom.index()] = i
    # if pseudo_coords:
    #     coords = normalize_nodes(np.array(coords))
    #     h, w, _ = image.shape
    #     coords[:, 0] = coords[:, 0] * w
    #     coords[:, 1] = coords[:, 1] * h
    n = len(symbols)
    edges = np.zeros((n, n), dtype=int)
    for bond in mol.iterateBonds():
        s = index_map[bond.source().index()]
        t = index_map[bond.destination().index()]
        # 1/2/3/4 : single/double/triple/aromatic
        edges[s, t] = bond.bondOrder()
        edges[t, s] = bond.bondOrder()
        if bond.bondStereo() in [5, 6]:
            edges[s, t] = bond.bondStereo()
            edges[t, s] = 11 - bond.bondStereo()
    graph = {
        'coords': coords,
        'symbols': symbols,
        'edges': edges,
        'num_atoms': len(symbols)
    }
    return graph

def generate_output_smiles(indigo, mol):
    # TODO: if using mol.canonicalSmiles(), explicit H will be removed
    smiles = mol.smiles()
    mol = indigo.loadMolecule(smiles)
    if '*' in smiles:
        part_a, part_b = smiles.split(' ', maxsplit=1)
        part_b = re.search(r'\$.*\$', part_b).group(0)[1:-1]
        symbols = [t for t in part_b.split(';') if len(t) > 0]
        output = ''
        cnt = 0
        for i, c in enumerate(part_a):
            if c != '*':
                output += c
            else:
                output += f'[{symbols[cnt]}]'
                cnt += 1
        return mol, output
    else:
        if ' ' in smiles:
            # special cases with extension
            smiles = smiles.split(' ')[0]
        return mol, smiles

def render_indigo_image(smiles, out_dir, filename, ext, save_molecule=False,
                        shuffle_nodes=False, pseudo_coords=False, args=None, seed=0):
    indigo = Indigo()
    random.seed(seed)
    renderer = IndigoRenderer(indigo)
    indigo.setOption('render-output-format', ext)
    indigo.setOption('render-background-color', '1,1,1')
    # indigo.setOption("render-background-color", "0, 0, 0")
    # indigo.setOption("render-base-color", "1, 1, 1")
    indigo.setOption('render-stereo-style', 'none')
    # indigo.setOption('render-label-mode', 'hetero')
    indigo.setOption('render-label-mode', 'terminal-hetero')
    indigo.setOption('render-font-family', 'Arial')
    indigo.setOption('render-implicit-hydrogens-visible', True)
    indigo.setOption('render-relative-thickness', 1.0)
    if not args.default_option:
        thickness = random.uniform(0.5, 1.5)  # limit the sum of the following two parameters to be smaller than 4
        indigo.setOption('render-relative-thickness', thickness)
        indigo.setOption('render-bond-line-width', random.uniform(1, 4 - thickness))
        if random.random() < 0.5:
            indigo.setOption('render-font-family', random.choice(['Arial', 'Times', 
                                                                  'Courier', 'Helvetica']))
        indigo.setOption('render-label-mode', random.choice(['hetero', 'terminal-hetero']))
        indigo.setOption('render-implicit-hydrogens-visible', random.choice([True, False]))
        # if random.random() < 0.1:
        #     indigo.setOption('render-stereo-style', 'old')
        # if random.random() < 0.2:
        #     indigo.setOption('render-atom-ids-visible', True)

    try:
        mol = indigo.loadMolecule(smiles)
        smiles = mol.canonicalSmiles()
        if args.mol_augment:
            if random.random() < 0.5:
                indigo.setOption('render-font-family', random.choice(['Arial', 'Times', 
                                                                      'Courier', 'Helvetica']))
            # if random.random() < INDIGO_DEARMOTIZE_PROB:
            #     mol.dearomatize()
            # else:
            #     mol.aromatize()
            # add_comment(indigo)
            mol = add_explicit_hydrogen(indigo, mol)
            mol = add_rgroup(indigo, mol, smiles)
            # if args.include_condensed:
            mol = add_rand_condensed(indigo, mol)
            mol = add_functional_group(indigo, mol, True)
            # mol = add_color(indigo, mol)
        if args.mol_augment_rare:
            if random.random() < 0.5:
                indigo.setOption('render-font-family', random.choice(['Arial', 'Times', 
                                                                      'Courier', 'Helvetica']))
            mol = add_rare_atoms(indigo, mol)

        mol, smiles = generate_output_smiles(indigo, mol)
        pdf_buffer = renderer.renderToBuffer(mol)

        indigo.setOption('render-background-color', '0,0,0')
        indigo.setOption("render-base-color", "1, 1, 1")
        if save_molecule:
            renderer.renderToFile(mol, os.path.join(out_dir, filename + "." + ext))

        # img = np.repeat(np.expand_dims(img, 2), 3, axis=2)  # expand to RGB
        graph = get_graph(mol, shuffle_nodes, pseudo_coords)

        # Return non-inverted pdf object for symbol scraper
        # indigo.setOption('render-output-format', ext)
        # mol = indigo.loadMolecule(smiles)
        # renderer.renderToFile(mol, os.path.join(out_dir, filename + "." + ext))

        indigo.setOption('render-output-format', "png")
        buf = renderer.renderToBuffer(mol)
        img = cv2.imdecode(np.asarray(bytearray(buf), dtype=np.uint8), 1)  # decode buffer to image
        success = True
        # imshow(img)
        # import pdb; pdb.set_trace()
        
    except Exception as e:
        print (e)
        print(traceback.print_exc())
        print(filename)
        img = np.array([[[255., 255., 255.]] * 10] * 10).astype(np.float32)
        graph = {}
        success = False
        pdf_buffer = []

    return img, pdf_buffer, smiles, graph, success


SVG_FILTER = re.compile(r'<path(?!(?:[^>]*\bclass\s*=)).*?>')
def render_rdkit_image(smiles, out_dir, filename, ext, save_molecule,
                       args, seed, scale_factor=80, save_svg=False):
    random.seed(seed)
    white = (1, 1, 1)
    black = (0, 0, 0)

    try:
        # Load the molecule from SMILES (octane)
        mol = Chem.MolFromSmiles(smiles)  # SMILES for octane

        # indigo.setOption('render-implicit-hydrogens-visible', True)
        # mol = Chem.AddHs(mol)
        
        # Compute 2D coordinates for the molecule
        Chem.rdDepictor.Compute2DCoords(mol)

        # Prepare the molecule for drawing (optional, makes it ready for 2D rendering)
        rdMolDraw2D.PrepareMolForDrawing(mol)
        
        # Get the conformer to access atom positions
        conf = mol.GetConformer()
        
        # Determine the bounding box (min/max x, y) for the molecule
        min_x, min_y = float('inf'), float('inf')
        max_x, max_y = float('-inf'), float('-inf')
        
        for atom in mol.GetAtoms():
            pos = conf.GetAtomPosition(atom.GetIdx())
            min_x = min(min_x, pos.x)
            min_y = min(min_y, pos.y)
            max_x = max(max_x, pos.x)
            max_y = max(max_y, pos.y)
        
        # Calculate width and height based on the bounding box (in atomic units)
        width = max_x - min_x
        height = max_y - min_y
        
        # Set scale (use points per atomic unit for proper rendering, e.g., 40 points per unit)
        scale_factor = 80  # This scales the atomic units to pixels
        
        # Compute canvas size in pixels based on molecule dimensions and scale
        canvas_width = int(width * scale_factor)
        canvas_height = int(height * scale_factor)

        # ===================== Create PDF buffer with NORMAL colors =====================
        # Create an RDKit drawing canvas for PDF output with normal colors (white background)
        drawer_pdf_normal = rdMolDraw2D.MolDraw2DSVG(canvas_width, canvas_height, noFreetype=True)

        rdMolDraw2D.SetMonochromeMode(drawer_pdf_normal, black, white)
        
        drawer_pdf_normal.drawOptions().bondLineWidth = 3  # Bond thickness
        
        # Enable atom indices (optional, show atom numbers in black)
        drawer_pdf_normal.drawOptions().addAtomIndices = True
        # drawer_pdf_normal.drawOptions().highlightColour = (0, 0, 0)  # Black for highlighted elements (like atom indices)
        
        # Draw the molecule on the normal PDF canvas
        drawer_pdf_normal.DrawMolecule(mol)
        
        # Finish the normal PDF drawing
        drawer_pdf_normal.FinishDrawing()

        # Get the SVG content
        svg_content_normal = drawer_pdf_normal.GetDrawingText()
        svg_content_normal_filtered = SVG_FILTER.sub('', svg_content_normal)
        if save_svg:
            with open(os.path.join(out_dir, filename + "_rdkit.svg"), "w") as svg_file:
                svg_file.write(svg_content_normal)
            with open(os.path.join(out_dir, filename + "_rdkit_filtered.svg"), "w") as svg_file:
                svg_file.write(svg_content_normal_filtered)
        
        # Convert SVG to PDF using CairoSVG and store in an in-memory buffer
        pdf_buffer = io.BytesIO()
        cairosvg.svg2pdf(bytestring=svg_content_normal_filtered.encode(), write_to=pdf_buffer)
        # if save_molecule:
        #     with open(os.path.join(out_dir, filename + "_original.pdf"), "wb") as pdf_file:
        #         pdf_file.write(pdf_buffer.getvalue())  # Write buffer content to the file

        # pdf_buffer.seek(0)  # Reset the buffer's pointer to the start
        
        # ===================== Create PDF output with INVERTED colors =====================
        # Create an RDKit drawing canvas for PDF output
        drawer_pdf_inverted = rdMolDraw2D.MolDraw2DSVG(canvas_width, canvas_height, noFreetype=True)
        # drawer_pdf_inverted = rdMolDraw2D.MolDraw2DSVG(canvas_width, canvas_height)
        
        # Invert colors: Set background to black and elements to white
        rdMolDraw2D.SetMonochromeMode(drawer_pdf_inverted, white, black)
        ## OR set colors manually
        # drawer_pdf_inverted.drawOptions().setBackgroundColour(black)
        # drawer_pdf_inverted.drawOptions().setAtomColour((1, 1, 1))  # White atoms
        # Set all atom colors to white by iterating over the palette for all atomic numbers
        # atom_palette = {}
        # for atomic_num in range(1, 119):  # Cover all known elements in the periodic table
        #     atom_palette[atomic_num] = white
        # drawer_pdf_inverted.drawOptions().setAtomPalette(atom_palette)
        # drawer_pdf_inverted.drawOptions().setAnnotationColour(white)

        drawer_pdf_inverted.drawOptions().bondLineWidth = 3  # Increase bond width for better visibility
        
        # Enable atom indices (optional, show atom numbers in white)
        drawer_pdf_inverted.drawOptions().addAtomIndices = True
        # drawer_pdf_inverted.drawOptions().highlightColour = (1, 1, 1)  # White for highlighted elements (like atom indices)
        # drawer.drawOptions().fontSize = random.uniform(0.5, 1.0)
        # drawer.SetFontSize(500)
        
        # Draw the molecule on the inverted PDF canvas
        drawer_pdf_inverted.DrawMolecule(mol)
        
        # Finish the inverted PDF drawing
        drawer_pdf_inverted.FinishDrawing()
        
        # Get the SVG content
        svg_content = drawer_pdf_inverted.GetDrawingText()
        svg_content_filtered = SVG_FILTER.sub('', svg_content)
        # with open(os.path.join(out_dir, filename + "_inverted.svg"), "w") as svg_file:
        #     svg_file.write(svg_content)
        
        # Convert SVG to PDF using CairoSVG and 
        # Save the inverted PDF to a file
        cairosvg.svg2pdf(bytestring=svg_content_filtered.encode(), 
                         write_to=os.path.join(out_dir, filename + ".pdf"))
        pdf_value = pdf_buffer.getvalue()
        success = True

    except Exception as e:
        # print (e)
        # print(traceback.print_exc())
        print(f"RDKit Rendering error: {filename}")
        img = np.array([[[255., 255., 255.]] * 10] * 10).astype(np.float32)
        graph = {}
        success = False
        pdf_value = []
    return pdf_value, smiles, success


if __name__ == "__main__":
    args = get_args()
    with open(args.in_file, "r") as f:
        smiles = f.read().strip()
    print(smiles)
    # smiles = "CC(CN(O)C=O)C(=O)N[C@H](C(=O)N(C)C)C(C)(C)C"
    # smiles = "COC1=CC=NC(C(=O)NC2=CC=CC(OC3=CC=C(C)C=C3)=C2)=C1O"
    # generate_indigo_image(smiles)
    # generate_molecule(smiles, out=args.out_file, ext=args.ext,
    #                       mol_augment=args.mol_augment,
    #                       default_option=args.default_option,
    #                       save_image=args.save_image)
    
