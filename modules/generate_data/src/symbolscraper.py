import subprocess
from subprocess import DEVNULL, STDOUT
import os.path as osp
import os
import time
import logging
import requests
from tqdm import tqdm
from src.constants import ROOT_DIR


def process_sscraper(input_dir, output_dir, ):
    # Start docker container service
    os.chdir(ROOT_DIR)
    if not osp.isdir(output_dir):
        os.makedirs(output_dir)

    pbar = tqdm(total=len(os.listdir(input_dir)), desc="  Processing")
    try:
        for f in os.listdir(input_dir):
            xml_content = sync_call(os.path.join(input_dir, f))
            if xml_content is not None:
                save_to_out_dir(xml_content, output_dir, f)
            pbar.update(1)
    except Exception as e:
        print(e)
        # proc.terminate()

    pbar.close()
    # proc.terminate()

def sync_call(pdf_file: str):
    filename = os.path.basename(pdf_file)
    # print(f'Filename for request: {filename}')
    # r = "http://localhost:7002/process-pdf?writePageBorderBBOX=true&writePoints=true"
    r = ("http://localhost:7002/explain-geometry?processors=ExplainGeometry&"
         "writePageBorderBBOX=true&writePoints=true&writeFontSize=true")

    file = {"pdf": (filename, open(pdf_file, 'rb'), "application/pdf")}
    # print('Posting request')
    resp = requests.post(r, files=file)
    # print('Returned request')
    if resp.status_code != 200:
        # print("request failed!")
        return None
    return resp.content


def save_to_out_dir(content, output_dir, f):
    f = os.path.join(output_dir, f)
    f = os.path.splitext(f)[0] + ".json"
    # f = os.path.splitext(f)[0] + ".xml"
    with open(f, "wb+") as content_file:
        content_file.write(content)


if __name__ == '__main__':
    process_sscraper('/home/mattlangsenkamp/Documents/dprl/MathSeer-extraction-pipeline/inputs/A00_3007/pdf',
                     'fake_dir')
