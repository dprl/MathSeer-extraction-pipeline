import time
import math
import numpy as np
import cv2
# from shapely.geometry import Polygon, Point
from shapely.geometry import LineString, Point
from shapely.ops import triangulate
from operator import itemgetter
from collections import defaultdict
from scipy.ndimage import label
from modules.utils.dprl_map_reduce import map_concat
import pandas as pd
# from pylena.scribo import line_detector, LSuperposition, e_segdet_process_traversal_mode
# import pylena

from modules.chemscraper.nx_debug_fns import shapedraw, draw_points, shapesdraw
# from src.demo_utils import display_vector, display_pixel
# from src.utils.utils import imshow
from skimage import measure
from shapely.validation import make_valid
from scipy.spatial import distance
from shapely.strtree import STRtree
from modules.generate_data.src.extract_primitives_utils import get_new_components
from modules.generate_data.src.old_extract_primitives_utils import old_get_new_components


axis_map = {'horizontal': 0, 'vertical': 1}
sort_order_map = {'horizontal': itemgetter(0, 1), 'vertical': itemgetter(1, 0)}
SUP_AREA_THRESHOLD = 0.15
# AREA = math.inf
AREA = 1000

def find_boundaries(black_pixel_indices, orientation='vertical'):
    """
    Finds the extreme points (leftmost, rightmost, topmost, bottommost) of a single connected component using black pixel indices.

    Args:
    black_pixel_indices: A 2D NumPy array with 2 columns, where each row represents the (x, y) coordinates of a black pixel.

    Returns:
    A dictionary containing four lists of coordinates:
      * "top_points": A list of (x, y) coordinates of all points on the top boundary.
      * "bottom_points": A list of (x, y) coordinates of all points on the bottom boundary.
      * "left_points": A list of (x, y) coordinates of all points on the left boundary.
      * "right_points": A list of (x, y) coordinates of all points on the right boundary.
    """

    # Extract x and y coordinates from the indices
    black_xs, black_ys = black_pixel_indices.T

    # Find bounding values
    min_x, max_x = min(black_xs), max(black_xs)
    min_y, max_y = min(black_ys), max(black_ys)

    # Filter points based on boundaries
    if orientation == 'vertical':
        top_points = [(x, max_y) for x, y in zip(black_xs, black_ys) if y == max_y]
        bottom_points = [(x, min_y) for x, y in zip(black_xs, black_ys) if y == min_y]
        return top_points, bottom_points
    else:
        left_points = [(min_x, y) for x, y in zip(black_xs, black_ys) if x == min_x]
        right_points = [(max_x, y) for x, y in zip(black_xs, black_ys) if x == max_x]
        return left_points, right_points

def find_connected_components(indices):
    """
    Find connected components from a list of pixel indices using an efficient method.
    
    :param indices: List of (row, column) indices of pixels.
    :param shape: Tuple representing the shape of the 2D space (image).
    :return: List of lists, where each sublist contains the indices of a connected component.
    """
    # Convert indices list to a NumPy array
    np_indices = np.array(indices)

    # Create a blank image
    # Determine the bounding box of the points
    y_min, x_min = np.min(np_indices, axis=0)
    y_max, x_max = np.max(np_indices, axis=0)
    
    # Create an image of the size of the bounding box
    width, height = x_max - x_min + 1, y_max - y_min + 1
    array = np.zeros((height, width), dtype=np.uint8)

    # Adjust points to the new coordinate system
    np_indices = np_indices - np.array([y_min, x_min])

    # Set the pixels using advanced indexing
    array[np_indices[:, 0], np_indices[:, 1]] = 1

    # Find connected components
    labeled_array, num_features = label(array)

    # Extract indices for each connected component
    components = [np.column_stack(np.where(labeled_array == i)) for i in range(1, num_features + 1)]
    components = [comp + np.array([y_min, x_min]) for comp in components]

    return components

def are_touching(group1, group2):
    """
    Check if two groups of pixels are touching each other using NumPy for efficient computation.

    :param group1: List of (row, col) indices for the first group.
    :param group2: List of (row, col) indices for the second group.
    :return: Boolean indicating whether the two groups are touching.
    """
    # Convert groups to NumPy arrays
    np_group1 = np.array(group1)
    np_group2 = np.array(group2)

    # Define neighbor offsets for 8-connectivity
    neighbors_offsets = np.array([[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]])

    # Calculate all neighbors for group1
    neighbors = np_group1[:, np.newaxis, :] + neighbors_offsets

    # Check if any neighbor is in group2
    return np.any(np.isin(neighbors.reshape(-1, 2), np_group2).all(axis=1))


def find_touching_points(group1, group2):
    """
    Finds the actual points in group1 touching points in group2, including diagonals.

    Args:
    group1: A 2D array of shape (N1, 2) containing the coordinates of points in group 1.
    group2: A 2D array of shape (N2, 2) containing the coordinates of points in group 2.

    Returns:
    A 2D array of shape (N_touching, 2) containing the coordinates of the touching points in group1.
    """
    group1 = np.array(group1)
    group2 = np.array(group2)

    # Create a distance matrix between all points in group1 and group2.
    dist_mat = np.linalg.norm(group1[:, None] - group2[None, :], axis=2)

    # Find the minimum distance for each point in group1.
    min_dist_group1 = np.min(dist_mat, axis=1)

    # Find the indices of points in group1 that are touching or diagonally touching.
    touching_indices = np.where((min_dist_group1 == 1) | (min_dist_group1 == np.sqrt(2)))[0]

    # Return the actual touching points using the indices.
    return group1[touching_indices]

## NOT USED
def superposed2ccs(label_map):
    superpos_masks = label_map * (label_map==1)
    # imshow(superpos_masks, cmap='gray')
    superpos_ccs = measure.label(superpos_masks)
    superpos_ccs[superpos_ccs != 0] += label_map.max()
    superpos_indices = np.argwhere(label_map == 1)
    label_map[superpos_indices[:, 0], superpos_indices[:, 1]] = superpos_ccs[superpos_indices[:, 0], superpos_indices[:, 1]]
    return label_map
    
def relabel_segmented_ids(array):
    unique_ids = np.unique(array)
    max_id = unique_ids.max()
    
    # Mapping from old IDs to new consecutive IDs
    id_mapping = {old_id: new_id for new_id, old_id in enumerate(unique_ids) if old_id != new_id}

    # Create a new array with the relabeled IDs
    new_array = np.copy(array)
    for old_id, new_id in id_mapping.items():
        new_array[array == old_id] = new_id
    return new_array, id_mapping

def get_line(label_map):
    line_dict = defaultdict(list)

    orientations_dict = {}
    unique_labels = np.unique(label_map)[2:] # Ignore the 0 and 1 labels
    
    mixed_indices = np.argwhere(np.isin(label_map, unique_labels))

    # Create a dict
    for idx in mixed_indices:
        lbl = label_map[idx[0], idx[1]]
        line_dict[lbl].append([idx[1], idx[0]])
    
    # Delete Labels with only One Point
    del_keys = []
    for k in sorted(line_dict.keys()):
        if len(line_dict[k]) <= 1:
            del_keys.append(k)
    for k in del_keys:
        del line_dict[k]
    
    # Convert all indices to numpy and find orientation; then sort by LR-TD/TD-LR
    for k in sorted(line_dict.keys()):
        line_dict[k] = np.array(line_dict[k]).reshape((-1,2))
        diff = line_dict[k].max(axis=0) - line_dict[k].min(axis=0)
        orientation = 'horizontal' if diff[0] > diff[1] else 'vertical'
        sort_order = sort_order_map[orientation]
        line_dict[k] = np.array(sorted(line_dict[k], key=sort_order))
        orientations_dict[k] = orientation
    return line_dict, orientations_dict

def split_lines_superpos(label_map, line_dict, orientations_dict, 
                         lsuperpositions):
    label_map = label_map.copy()
    new_label = max(line_dict.keys()) + 1
    new_label_dict = defaultdict(list)

    # for unique_label in unique_labels[-1:]:
    for unique_label, points in sorted(line_dict.items(), 
                                           key=itemgetter(0)):
        orientation = orientations_dict[unique_label]
        axis = axis_map[orientation]
        # lsuperpositions_filtered = list(filter(lambda x:x.label == unique_label, lsuperpositions))
        # yx = [[sup.y, sup.x] for sup in lsuperpositions_filtered]
        superpos = lsuperpositions.get(unique_label, np.array([]))
        # If no superposition, skip
        if not superpos.size:
            continue
        # For very large supsrpositions, no need to split
        if len(superpos) > SUP_AREA_THRESHOLD * len(points):
            continue
        components = find_connected_components(superpos[:, [1,0]])
        split_points = []
        # print(f"Boundary1:\n", boundary1)
        # print(f"\nBoundary2:\n", boundary2)
        boundary1, boundary2 = find_boundaries(points, orientation)
        for comp in components:
            comp[:, [0, 1]] = comp[:, [1,0]]
            # print()
            if are_touching(boundary1, comp) or are_touching(boundary2, comp):
                # print(f"Points in boundary touches superposition {i}")
                pass
            else:
                touching_points = find_touching_points(points, comp)
                if touching_points.size:
                    split_points.append(touching_points)
                # print(f"Need to split line {unique_label} at superposition {i}")
        # assert len(split_points)<=1, "More than 1 superposition touching"
        if not split_points:
            continue
        # split_points_i = split_points[0]
        split_points_simple = []
        # print(f"Split line {unique_label} into {len(split_points)+1} lines")
        for split_point in split_points:
            split_point = split_point[np.lexsort([split_point[:, axis]])]
            split_point = split_point[int(len(split_point)/2)]
            split_points_simple.append(split_point)
        split_points_simple = np.array(split_points_simple)
        split_indices = np.searchsorted(points[:, axis], split_points_simple[:, axis])
        split_lines = np.split(points, split_indices)

        if split_lines[0].size:
            line_dict[unique_label] = split_lines[0]
        not_assigned = True
        for split_line in split_lines[1:]:
            if not split_line.size:
                continue
            split_line_yx = split_line[:, [1, 0]]
            label_map[split_line_yx[:,0], split_line_yx[:,1]] = new_label
            if not_assigned and not split_lines[0].size:
                # line_dict[unique_label] = LineString(split_line)
                line_dict[unique_label] = split_line
                not_assigned = False
            else:
                # line_dict[new_label] = LineString(split_line)
                line_dict[new_label] = split_line
                orientations_dict[new_label] = orientations_dict[unique_label]
                new_label_dict[unique_label].append(new_label)
                new_label += 1
    return label_map, new_label_dict, line_dict, orientations_dict

def estimate_line_thickness(pixel_indices, orientation):
    if orientation == 'horizontal':
        # Sort and split based on unique x-coordinates
        # sorted_indices = pixel_indices[np.argsort(pixel_indices[:, 0])]
        _, split_indices = np.unique(pixel_indices[:, 0], return_index=True)
    elif orientation == 'vertical':
        # Sort and split based on unique y-coordinates
        # sorted_indices = pixel_indices[np.argsort(pixel_indices[:, 1])]
        _, split_indices = np.unique(pixel_indices[:, 1], return_index=True)
    else:
        raise ValueError("Orientation must be 'horizontal' or 'vertical'")

    # Calculate thickness of each line segment
    thicknesses = np.diff(np.append(split_indices, len(split_indices)))
    average_thickness = np.mean(thicknesses)
    return average_thickness

def split_lines(label_map, line_dict, orientations_dict, smooth_tolerance=30, skip=True):    
    label_map = label_map.copy()
    new_label = max(line_dict.keys()) + 1
    new_label_dict = defaultdict(list)

    # for unique_label in unique_labels[-1:]:
    for unique_label, points in sorted(line_dict.items(), 
                                           key=itemgetter(0)):
        linestring = LineString(points)
        orientation = orientations_dict[unique_label]
        # smooth_tolerance = estimate_line_thickness(points, orientation) * 1.5
        lines_s = linestring.simplify(smooth_tolerance)
        # shapedraw(lines_s)
        # print(lines_s.area)
        corners = np.array(lines_s.coords.xy).T
        if skip and (len(corners) > 10 or len(corners) < 3):
            continue
        
        axis = axis_map[orientation]
        split_indices = np.searchsorted(points[:, axis], corners[1:-1, axis])
        split_lines = np.split(points, split_indices)

        if split_lines[0].size:
            line_dict[unique_label] = split_lines[0]
        not_assigned = True
        for split_line in split_lines[1:]:
            if not split_line.size:
                continue
            split_line_yx = split_line[:, [1, 0]]
            label_map[split_line_yx[:,0], split_line_yx[:,1]] = new_label
            if not_assigned and not split_lines[0].size:
                line_dict[unique_label] = split_line
                not_assigned = False
            else:
                line_dict[new_label] = (split_line)
                orientations_dict[new_label] = orientations_dict[unique_label]
                new_label_dict[unique_label].append(new_label)
                new_label += 1
    return label_map, new_label_dict, line_dict, orientations_dict

def convert_superpos(lsuperpositions):
    # Assuming 'your_list' is your list of scribo.LSuperposition objects
    # Convert your list of objects to a list of tuples or dictionaries
    data = [(obj.label, obj.x, obj.y) for obj in lsuperpositions]
    # Create a DataFrame
    df = pd.DataFrame(data, columns=['label', 'x', 'y'])

    # Group by 'label' and aggregate the x and y values into lists, then convert to NumPy arrays
    coordinates_dict = {label: np.array(group[['x', 'y']]) 
                        for label, group in df.groupby('label')}
    return coordinates_dict

def annotate_superpos(line_dict, lsuperpositions, new_label_dict):
    for line_id, new_line_ids in new_label_dict.items():
        new_line_ids.append(line_id)
        sup = lsuperpositions.get(line_id, np.array([]))
        if not sup.size:
            continue
        components = find_connected_components(sup[:, [1,0]])
        for new_line_id in new_line_ids:
            points = line_dict[new_line_id]
            # Since Pylena may assign non-existent superposition for the new
            # line id (weird behaviour by Pylena)
            new_sup = []
            for comp in components:
                comp_xy = comp[:, [1,0]]
                if are_touching(comp_xy, points):
                    new_sup.append(comp_xy)

            # Concatenate all the components after the loop, which is more efficient
            new_sup = np.concatenate(new_sup) if new_sup else np.array([])
            # Assuming lsuperpositions is a dictionary
            lsuperpositions[new_line_id] = new_sup
    return lsuperpositions

def annotate_superpos_old(lsuperpositions, new_label_dict):
    lsuperpositions_filtered = list(filter(lambda x:x.label in new_label_dict, lsuperpositions))
    lsuperpositions_filtered = sorted(lsuperpositions_filtered, key=lambda x:x.label)
    for sup in lsuperpositions_filtered:
        x, y = sup.x, sup.y
        for new_id in new_label_dict[sup.label]:
            lsuperpositions.append(LSuperposition(new_id, x, y))
    lsuperpositions = sorted(lsuperpositions, key=lambda x:x.label)
    return lsuperpositions

def add_superpos(line_dict, lsuperpositions, orientations_dict, shape):
    label_map = np.zeros(shape, dtype=int)
    for line_id, points in line_dict.items():
        label_map[points[:, 1], points[:, 0]] = line_id
        superpos = lsuperpositions.get(line_id, np.array([]))
        if superpos.size:
            label_map[superpos[:, 1], superpos[:, 0]] = line_id
            lines_added = np.concatenate((points, superpos))
            orientation = orientations_dict[line_id]
            sort_order = sort_order_map[orientation]
            lines_added = np.array(sorted(lines_added, key=sort_order))
            line_dict[line_id] = lines_added
    return line_dict, label_map

def preprocess_image(img_in):
    img_in = cv2.cvtColor(img_in, cv2.COLOR_BGR2GRAY) / 255
    _, img_in = cv2.threshold(img_in, 0.7, 1, cv2.THRESH_BINARY)
    img_in = 1. - img_in
    img_in = (img_in*255).astype('uint8')
    return img_in

def relabel(line_dict, label_map_new, orientations_dict):
    old2new = {old_key: new_key for new_key, (old_key, _) in
                   enumerate(sorted(line_dict.items(), key=lambda x:tuple(x[1].min(axis=0))))}
    for old_key, new_key in old2new.items():
        points = line_dict[old_key]
        label_map_new[points[:, 1], points[:, 0]] = new_key + 1
    line_dict = {old2new[old_key]:line for (old_key, line) in line_dict.items()}
    orientations_dict = {old2new[old_key]:orientation for (old_key, orientation) in orientations_dict.items()}
    return line_dict, label_map_new, orientations_dict

def add_missing_lines(label_map, lsuperpositions, area=AREA):
    """
    Add missing lines in label map from extra superpositions
    """ 
    label_map = label_map.copy()
    
    unique_labels = np.unique(label_map)[2:]
    extra_superpos_keys = lsuperpositions.keys() - set(unique_labels)
    max_id = max(unique_labels)
    for key in extra_superpos_keys:
        superpos = lsuperpositions[key]
        # Remove these non existent superpositions
        lsuperpositions.pop(key)
        if len(superpos) < area:
            continue
        max_id += 1
        label_map[superpos[:, 1], superpos[:, 0]] = max_id
    return label_map

def get_label_map(image):
    
    #visualize_polygons(image)
    # label_map, lsuperpositions, _ = line_detector(image, mode='full', discontinuity_relative=0, blumi=180, 
    #                                         llumi=180, min_len=0, max_thickness=100000, verbose=False, 
    #                                         nb_values_to_keep=15, minimum_for_fusion=15, threshold_intersection=0.8, 
    #                                         remove_duplicates=True)
    
    label_map, lsuperpositions, _ = line_detector(image, minimum_for_fusion=30, default_sigma_thickness=40, max_thickness=100000)

    #display_pixel(label_map)
    #import pdb;pdb.set_trace()
    lsuperpositions = convert_superpos(lsuperpositions)
    label_map_new = add_missing_lines(label_map, lsuperpositions)

    # label_map_new = label_map.copy()
    line_dict, orientations_dict = get_line(label_map_new)
    label_map_new, new_label_dict, line_dict_new, orientations_dict = split_lines_superpos(label_map_new, line_dict, 
                                                                                  orientations_dict, lsuperpositions)
    label_map_new, new_label_dict2, line_dict_new, orientations_dict = split_lines(label_map_new, line_dict_new,
                                                                                   orientations_dict)
    new_label_dict.update(new_label_dict2)
    lsuperpositions = annotate_superpos(line_dict_new, lsuperpositions, new_label_dict)
    line_dict_new, label_map_new = add_superpos(line_dict_new, lsuperpositions, orientations_dict,
                             label_map.shape)
    line_dict_new, label_map_new, orientations_dict = relabel(line_dict_new, label_map_new, orientations_dict)

    #import pdb;pdb.set_trace()
    # Reduce primitives
    # label_dic_merged, polygons, orientations_dict_new = relabel_components_polygons(line_dict_new, orientations_dict, label_map.shape, label_map_new, lsuperpositions)
    # new_label_map_renamed = line2labelmap(label_dic_merged, label_map.shape) 

    
    # display_pixel(new_label_map_renamed)

    return label_map, label_map_new, line_dict_new, new_label_dict, lsuperpositions

def get_label_map_parallel(files):
    outputs = map_concat(get_label_map, files)
    label_maps = []
    lsuperpositions = []
    label_dicts = []
    for output in outputs:
        label_maps.append(output[0])
        lsuperpositions.append(output[1])
        label_dicts.append(output[2])
    return label_maps, lsuperpositions, label_dicts

def get_label_map_old(images):
    label_maps = []
    lsuperpositions_all = []
    label_dicts = []
    for i, image in enumerate(images):
        print(i, files[i] + "\n")
        label_map, lsuperpositions, vsegments = line_detector(image, mode='full', discontinuity_relative=0, blumi=180, 
                                                      llumi=180, min_len=0, max_thickness=100000, verbose=False, 
                                                        nb_values_to_keep=15,
                                                      minimum_for_fusion=15, threshold_intersection=0.8, remove_duplicates=True,
                                                             )
        label_map_new, new_label_dict = split_lines_superpos(label_map, lsuperpositions)
        label_map_new, new_label_dict2 = split_lines(label_map_new)
        new_label_dict.update(new_label_dict2)
        # label_map_ccs = superposed2ccs(label_map_new)
        lsuperpositions_updated = annotate_superpos(lsuperpositions, new_label_dict)  
        # label_map_final = relabel_segmented_ids(label_map_new)
        label_maps.append(label_map_new)
        lsuperpositions_all.append(lsuperpositions_updated)
        label_dicts.append(new_label_dict)
    return label_maps, lsuperpositions_all, label_dicts

def get_label_map_geometric_primitives(image, inverted_image, filename, save_path=None, visualize=False):
    # line_dict,new_label_dict = get_new_components(image)
    contours = []

    #TO VISUALIZE:
    # line_dict, contours = get_new_components(image, inverted_image, filename, save_path, visualize)
    line_dict, _ = old_get_new_components(image, inverted_image, filename, save_path, visualize)
    return line_dict, contours

