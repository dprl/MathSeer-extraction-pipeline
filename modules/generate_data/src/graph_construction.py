import os
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial import distance
from modules.chemscraper.nx_debug_fns import create_pos_from_box,\
str_remove_suffix

def create_nodes(stroke_set_id_list):
    expression_graph = nx.DiGraph()

    # add each of the stroke_set as a node in the graph
    for stk_id in stroke_set_id_list:
        expression_graph.add_node(stk_id)

    return expression_graph


def get_ccs(edges):
    G = nx.Graph()
    for p, c in edges:
        G.add_edge(p,c)
    components = nx.connected_components(G)
    return sorted(components)

def find_nearest_neighbors(G):
    # Extract bounding boxes as NumPy arrays
    bounding_boxes = np.array([data['box'] for _, data in G.nodes(data=True)])
    
    # Convert bounding boxes to a NumPy array
    # bounding_boxes = np.array(list(bbox_data.values()))
    
    # Calculate pairwise distances between bounding boxes
    distances = distance.cdist(bounding_boxes, bounding_boxes, metric='euclidean')
    
    # Fill diagonal elements with infinity to exclude self-distances
    np.fill_diagonal(distances, np.inf)
    
    # Find the nearest neighbor for each node
    # nearest_neighbors = {}

    nearest_neighbor_idx = np.argsort(distances, axis=1)
    nodes = list(G.nodes())
    nearest_neighbors = np.vectorize(lambda x: nodes[x])(nearest_neighbor_idx)
    # for i, node in enumerate(G.nodes()):
    #     nearest_neighbor_idx = np.argmin(distances[i])
    #     nearest_neighbor = list(G.nodes())[nearest_neighbor_idx]
    #     nearest_neighbors[node] = nearest_neighbor
    
    # Print the nearest neighbors
    # for node, neighbor in nearest_neighbors.items():
    #     print(f"Node {node} is nearest to Node {neighbor}")
    return nearest_neighbors, distances

