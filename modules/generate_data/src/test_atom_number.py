from modules.chemscraper.utils.indigo import Indigo
from modules.chemscraper.utils.indigo.renderer import IndigoRenderer

from rdkit import Chem
from rdkit.Chem.Draw import rdMolDraw2D
import cairosvg

# Load the molecule from SMILES (octane)
mol = Chem.MolFromSmiles("CCCCCCCC")  # SMILES for octane

# Compute 2D coordinates for the molecule
Chem.rdDepictor.Compute2DCoords(mol)

# Set up a 2D drawing object for SVG output (500x400 canvas)
drawer = rdMolDraw2D.MolDraw2DSVG(500, 400)

# Enable atom indices (atom numbers)
drawer.drawOptions().addAtomIndices = True

# Draw the molecule on the canvas
drawer.DrawMolecule(mol)

# Finish the drawing process
drawer.FinishDrawing()

# Get the SVG content
svg_content = drawer.GetDrawingText()
cairosvg.svg2pdf(bytestring=svg_content.encode(), 
                 write_to="octane_with_atom_numbers.pdf")

# TODO: Working , but only for PNG
# from rdkit import Chem
# from rdkit.Chem import Draw

# # Load molecule from SMILES
# mol = Chem.MolFromSmiles("CCCCCCCC")  # SMILES for octane

# # Add atom indices as atom labels
# for atom in mol.GetAtoms():
#     atom.SetProp('atomNote', str(atom.GetIdx()))  # Add atom index as label

# # Generate and save the molecule with atom numbers to PNG
# Draw.MolToFile(mol, "octane_rdkit_labeled.png", size=(500, 400), kekulize=True)


# from rdkit import Chem
# from rdkit.Chem import Draw

# # Load molecule from SMILES (octane)
# mol = Chem.MolFromSmiles("CCCCCCCC")  # SMILES for octane

# # Draw the molecule with atom numbers included
# img = Draw.MolToImage(mol, size=(500, 400), kekulize=True, includeAtomNumbers=True)

# # Save the image to a PNG file
# img.save("octane_with_atom_numbers.png")

# TODO: Working for PNG
# from rdkit import Chem
# from rdkit.Chem import Draw
# from rdkit.Chem.Draw import rdMolDraw2D

# # Load the molecule from SMILES (octane)
# mol = Chem.MolFromSmiles("CCCCCCCC")  # SMILES for octane

# # Create a 2D drawing of the molecule
# drawer = rdMolDraw2D.MolDraw2DCairo(500, 400)  # Create a canvas (500x400)
# drawer.drawOptions().addAtomIndices = True  # Show atom indices on the molecule

# # Prepare the molecule for drawing
# rdMolDraw2D.PrepareAndDrawMolecule(drawer, mol)

# # Finish drawing and save to a file
# drawer.FinishDrawing()

# # Write the image to a file (PNG)
# with open("octane_with_atom_numbers.png", "wb") as f:
#     f.write(drawer.GetDrawingText())

