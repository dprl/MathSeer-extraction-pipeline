import os
import time
import torch
import cv2
import imagesize
from operator import itemgetter
from collections import defaultdict, Counter
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from itertools import repeat, islice
from torch.utils.data import Dataset
from src.constants import FORMAT_INFO
from src.render_smiles import render_indigo_image, render_rdkit_image
from modules.utils.dprl_map_reduce import map_concat
from tqdm import tqdm

from multiprocessing import Value

# HACK: Global variables for map-reduce progress
CURR_COUNT=Value('i',0)
MAX_COUNT=None


class TrainDataset(Dataset):
    def __init__(self, args, df, tokenizer, split='train', 
                 dynamic_indigo=False, suffix=""):
        super().__init__()
        self.df = df
        self.chunk_size = getattr(args, "chunk_size", len(self.df))
        self.args = args
        self.tokenizer = tokenizer
        self.suffix = suffix
        self.molecules = defaultdict(list)
        self.molecules_info = []
        os.makedirs(self.args.save_path, exist_ok=True)
        # if 'file_path' in df.columns:
        #     self.file_paths = df['file_path'].values
        #     if not self.file_paths[0].startswith(args.data_path):
        #         self.file_paths = [os.path.join(args.data_path, path) for path in df['file_path']]
        self.smiles = df.iloc[:, args.smiles_index_in_file].values
        self.get_smiles_dist()
        # self.smiles = df['SMILES'].values if 'SMILES' in df.columns else None
        self.formats = args.formats
        self.labelled = (split == 'train')
        # Filenames
        # import pdb; pdb.set_trace()
        self.filenames = df.iloc[:,args.id_index_in_file].apply(str).values + args.suffix
        if self.labelled:
            self.labels = {}
            for format_ in self.formats:
                if format_ in ['atomtok', 'inchi']:
                    field = FORMAT_INFO[format_]['name']
                    if field in df.columns:
                        self.labels[format_] = df[field].values

        # self.transform = get_transforms(args.input_size,
        #                                 augment=(self.labelled and args.augment))
        # self.fix_transform = A.Compose([A.Transpose(p=1), A.VerticalFlip(p=1)])
        self.dynamic_indigo = (dynamic_indigo and split == 'train')
        if self.labelled and not dynamic_indigo and args.coords_file is not None:
            if args.coords_file == 'aux_file':
                self.coords_df = df
                self.pseudo_coords = True
            else:
                self.coords_df = pd.read_csv(args.coords_file)
                self.pseudo_coords = False
        else:
            self.coords_df = None
            self.pseudo_coords = args.pseudo_coords

        self.pdf_path = os.path.join(self.args.save_path, 'pdf')
        os.makedirs(self.pdf_path, exist_ok=True)
        self.augmented_smiles = []

    def get_smiles_dist(self, write=True):
        combined_string = ''.join(self.smiles)  # Combine all strings into one
        self.smiles_dist = Counter(combined_string)  # Count each character

        smiles_dist_mc = self.smiles_dist.most_common()
        total = sum(self.smiles_dist.values())
        if write:
            # Writing the sorted frequency data to a readable text file
            with open(os.path.join(self.args.save_path,
                                   f"symbols_distribution{self.suffix}.csv"), 'w') as file:
                file.write("S.N.,Symbol,Count,Percentage\n")
                for i, (char, count) in enumerate(smiles_dist_mc, start=1):
                    file.write(f"{i},{char},{count},{count/total*100:.2f}\n")
                file.write(f"*,Total,{total},100.00")
        # self.plot_smiles_dist()

    def plot_smiles_dist(self, xlabel="SMILES Characters", dataset="Pubchem"):
        # Plotting the frequency distribution
        # Sort the frequency by decreasing order
        sorted_frequency = Counter(self.smiles_dist).most_common()

        # Extracting characters and counts for plotting
        sorted_characters = list(map(itemgetter(0), sorted_frequency))
        sorted_counts = list(map(itemgetter(1), sorted_frequency))

        plt.figure(figsize=(15, 8))
        plt.bar(sorted_characters, sorted_counts, color='skyblue')
        plt.xlabel(xlabel)
        plt.ylabel('Frequency')
        plt.title(f'{xlabel} Frequency Distribution - {dataset}')
        plt.show()

    def __len__(self):
        return len(self.df)

    def __getitem__(self, idx):
        try:
            # import pdb; pdb.set_trace()
            return self.getitem(idx)
        except Exception as e:
            with open(os.path.join(self.args.save_path, f'error_dataset_{int(time.time())}.log'), 'w') as f:
                f.write(str(e))
            raise e

    def render_molecules(self, save_molecule=True, format='pdf', parallel=True,
                         chunk=None):
        # begin = time.time()
        global CURR_COUNT, MAX_COUNT
        # import pdb; pdb.set_trace()
        # AKS: Parallelize across molecules
        MAX_COUNT = len(self)
        indices = range(MAX_COUNT)
        self.molecules_info = []
        if parallel:
            input_list = [
                (self.smiles[i], self.pdf_path, self.filenames[i], format, 
                 save_molecule, self.args.shuffle_nodes, self.pseudo_coords, self.args, i)
                for i in chunk
            ]
            # Each molecule_info item is a list of image, smiles, graph, success
            molecule_info = map_concat(generate_molecule, input_list,
                                       cpu_count=self.args.num_workers)
            self.molecules_info.extend(molecule_info)
        else:
            # import pdb; pdb.set_trace()
            for idx in chunk:
                filename = self.filenames[idx]
                params = [self.smiles[idx], self.pdf_path, filename, format, 
                        save_molecule, self.args.shuffle_nodes,
                        self.pseudo_coords, self.args, idx]
                                    
                molecule_info = generate_molecule(params)
                self.molecules_info.append(molecule_info[0])
        self.augmented_smiles.extend(list(map(itemgetter(2), self.molecules_info)))

    def getitem(self, idx):
        ref = {}
        # filename = self.filenames[idx]
        # begin = time.time()

        # image = self.molecules["image"][idx]
        # smiles = self.molecules["smiles"][idx]
        # graph = self.molecules["graph"][idx]
        # success = self.molecules["success"][idx]

        image, pdf_buffer, smiles, graph, success, filename = self.molecules_info[idx]
        # raw_image = image
        # end = time.time()
        # if idx < 30 and self.args.save_image:
        img_path = os.path.join(self.args.save_path, 'images')
        img_file = os.path.join(img_path, f"{filename}.PNG")
        if self.args.save_image:
            # path = os.path.join(self.args.save_path, 'images')
            # os.makedirs(path, exist_ok=True)
            # cv2.imwrite(os.path.join(path, f'{idx}.png'), image)
            pass
        if not success:
            return filename, None, None, {}, {}, []
        # image, coords = self.image_transform(image, graph['coords'], renormalize=self.pseudo_coords)
        # pp = pprint.PrettyPrinter(indent=4)
        # pp.pprint(graph)
        # print(graph)
        # imshow(image)
        ms_png_height, ms_png_width, _ = image.shape
        try:
            image_converted = cv2.imread(img_file).copy()
            ss_png_width, ss_png_height = imagesize.get(img_file)

            if graph:
                coords = np.array(graph['coords'])
                coords[:, 0] *= ss_png_height / ms_png_height
                coords[:, 1] *= ss_png_width / ms_png_width
                graph['coords'] = coords
                coords = coords.round().astype(int)

                color = (0, 255, 0)
                thickness = 25

                image_annotated = image_converted.copy()
                for x, y in coords:
                    cv2.line(image_annotated, (x, y), (x, y), color, thickness)
        except Exception as e:
            print(f"Error in converting image for {filename}")
            # print(e)
            image_converted = None
            image_annotated = None
            coords = None

        # import pdb; pdb.set_trace()
        # if self.args.save_image:
        #     cv2.imwrite(os.path.join(img_path, f'{filename}_annotated_MS.png'),
                        # image_annotated)
        # imshow(image)
        # graph['coords'] = coords
        # ref['time'] = end - begin
        if 'atomtok' in self.formats:
            max_len = FORMAT_INFO['atomtok']['max_len']
            label = self.tokenizer['atomtok'].text_to_sequence(smiles, tokenized=False)
            ref['atomtok'] = torch.LongTensor(label[:max_len])
        if graph:
            if 'edges' in self.formats and 'atomtok_coords' not in self.formats and 'chartok_coords' not in self.formats:
                ref['edges'] = torch.tensor(graph['edges'])
            if 'atomtok_coords' in self.formats:
                self._process_atomtok_coords(idx, ref, smiles, graph['coords'], graph['edges'],
                                             mask_ratio=self.args.mask_ratio)
            if 'chartok_coords' in self.formats:
                self._process_chartok_coords(idx, ref, smiles, graph['coords'], graph['edges'],
                                             mask_ratio=self.args.mask_ratio)
        return filename, image, image_converted, graph, ref, pdf_buffer

    def _process_chartok_coords(self, idx, ref, smiles, coords=None, edges=None, mask_ratio=0):
        max_len = FORMAT_INFO['chartok_coords']['max_len']
        tokenizer = self.tokenizer['chartok_coords']
        if smiles is None or type(smiles) is not str:
            smiles = ""
        label, indices = tokenizer.smiles_to_sequence(smiles, coords, mask_ratio=mask_ratio)
        ref['chartok_coords'] = torch.LongTensor(label[:max_len])
        indices = [i for i in indices if i < max_len]
        ref['atom_indices'] = torch.LongTensor(indices)
        if tokenizer.continuous_coords:
            if coords is not None:
                ref['coords'] = torch.tensor(coords)
            else:
                ref['coords'] = torch.ones(len(indices), 2) * -1.
        if edges is not None:
            ref['edges'] = torch.tensor(edges)[:len(indices), :len(indices)]
        else:
            if 'edges' in self.df.columns:
                edge_list = eval(self.df.loc[idx, 'edges'])
                n = len(indices)
                edges = torch.zeros((n, n), dtype=torch.long)
                for u, v, t in edge_list:
                    if u < n and v < n:
                        if t <= 4:
                            edges[u, v] = t
                            edges[v, u] = t
                        else:
                            edges[u, v] = t
                            edges[v, u] = 11 - t
                ref['edges'] = edges
            else:
                ref['edges'] = torch.ones(len(indices), len(indices), dtype=torch.long) * (-100)


def generate_molecule(params):
    smiles, out_dir, filename, ext, save_molecule, shuffle_nodes,\
            pseudo_coords, args, seed = params
    if args.atom_numbers:
        pdf_buffer, smiles, success = \
            render_rdkit_image(smiles, out_dir, filename, ext, save_molecule,
                                args, seed)
        img = np.array([[[255., 255., 255.]] * 10] * 10).astype(np.float32)
        graph = {}
        
    else:
        img, pdf_buffer, smiles, graph, success = \
            render_indigo_image(smiles, out_dir, filename, ext, save_molecule,
                                shuffle_nodes, pseudo_coords, args, seed)
    with CURR_COUNT.get_lock():
        CURR_COUNT.value += 1
        percentage = CURR_COUNT.value / MAX_COUNT 
        if CURR_COUNT.value < MAX_COUNT:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} molecules rendered [ {percentage:3.2%} ]                    \r', end='')
        else:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} molecules rendered [ {percentage:3.2%} ]                    \n\n')


    return [[img, pdf_buffer, smiles, graph, success, filename]]

def chunked_iterable(iterable, chunk_size):
    """Yield chunks of `chunk_size` from `iterable` (Python 3.7 compatible)"""
    it = iter(iterable)
    while True:
        chunk = list(islice(it, chunk_size))  # Explicit assignment
        if not chunk:  # If chunk is empty, exit loop
            break
        yield chunk
