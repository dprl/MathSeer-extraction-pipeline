import os


def make_node_port_load_balancer(service_name, services, namespace, exposed_port) -> str:
    return node_port_load_balancer_template.format(
        namespace=namespace,
        service_name=service_name,
        exposed_port=exposed_port,
        servers=format_with_spaces(services, 8))


def create_node_port_load_balancers(
        sscraper_services,
        sscraper_port,
        scanssd_services,
        scanssd_port,
        qdgga_services,
        qdgga_port,
        namespace,
        spec_dir):
    with open(os.path.join(spec_dir, f'sscaper_nodeport_{namespace}.yaml'), 'w+') as ssc_f:
        ssc_f.write(make_node_port_load_balancer(
            service_name='sscraper',
            services=sscraper_services,
            namespace=namespace,
            exposed_port=sscraper_port
        ))
    '''with open(os.path.join(spec_dir, f'scanssd_nodeport_{namespace}.yaml')) as ssc_f:
        ssc_f.write(make_node_port_load_balancer(
            service_name='scanssd',
            services=scanssd_services,
            namespace=namespace,
            exposed_port=scanssd_port
        ))
    with open(os.path.join(spec_dir, f'qdgga_nodeport_{namespace}.yaml')) as ssc_f:
        ssc_f.write(make_node_port_load_balancer(
            service_name='qdgga',
            services=qdgga_services,
            namespace=namespace,
            exposed_port=qdgga_port
        ))'''


def format_with_spaces(servers, space_n):
    t_servers = map(lambda server: " "*space_n + "server "+ server+":7002;", servers)
    return os.linesep.join(t_servers)


node_port_load_balancer_template="""apiVersion: v1
kind: ConfigMap
metadata:
  name: nginx-{namespace}-{service_name}
  namespace: {namespace}
data:
  nginx.conf: |
    user nginx;
    worker_processes  1;
    events {{
      worker_connections  10240;
    }}
    http {{
      server {{
        listen 8889;
        client_max_body_size 0;
        proxy_read_timeout 1200s;
        location / {{
          proxy_pass http://scanssd/;
        }}  
      }}
      upstream scanssd {{
        least_conn;
{servers}
      }}
    }}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-{namespace}-{service_name}
  namespace: {namespace}
spec:
  selector:
    matchLabels:
      app: nginx-{namespace}-{service_name}
  replicas: 1
  template:
    metadata:
      labels:
        app: nginx-{namespace}-{service_name}
    spec:
      containers:
      - name: nginx-{namespace}-{service_name}
        image: nginx
        ports:
        - containerPort: 8889
        volumeMounts:
            - name: nginx-{namespace}-{service_name}
              mountPath: /etc/nginx/nginx.conf
              subPath: nginx.conf
              readOnly: true
      volumes:
      - name: nginx-{namespace}-{service_name}
        configMap:
          name: nginx-{namespace}-{service_name}
          items:
            - key: nginx.conf
              path: nginx.conf

---
apiVersion: v1
kind: Service
metadata:
  name: nginx-{namespace}-{service_name}
  namespace: {namespace}
spec:
  type: NodePort
  ports:
  - port: 8889
    protocol: TCP
    targetPort: 8889
    nodePort: {exposed_port}
  selector:
    app: nginx-{namespace}-{service_name}
"""

if __name__ == '__main__':
    f = open('ttt.conf', 'w+')
    f.write(node_port_load_balancer_template.format(
        namespace='msp',
        service_name='sscraper',
        exposed_port='30008',
        servers=format_with_spaces(['bob', 'doug'], 8)))
    f.close()