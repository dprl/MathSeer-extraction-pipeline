import os


def sscraper_service(server, namespace, i, exposed_port) -> str:
    return sscraper_service_template.format(i=i, namespace=namespace, server=server, exposed_port=exposed_port)


def create_and_save_sscraper_services(servers, namespace ,spec_dir):
    os.makedirs(spec_dir, exist_ok=True)
    services = []
    for i, s in enumerate(servers):
        key = ''
        assert len(s.keys()) > 0
        for k in s.keys():
            key = k

        service_file = os.path.join(spec_dir, f"{key}_sscraper_{namespace}.yml")
        services.append(f"sscraper-{i}")
        service_spec = sscraper_service(key, namespace, i, s[key]['ports'])
        with open(service_file, 'w+') as f:
            f.write(service_spec)
    return services


sscraper_service_template = """apiVersion: v1
kind: Service
metadata:
  name: sscraper-{i}
  namespace: {namespace}
spec:
  selector:
    app: sscraper-{i}
  type: NodePort
  ports:
  - port: 7002
    protocol: TCP
    targetPort: 7002
    nodePort: {exposed_port}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  namespace: {namespace}
  name: sscraper-{i}
spec:
  replicas: 1
  selector:
    matchLabels:
      app: sscraper-{i}
  template:
    metadata:
      labels:
        namespace: {namespace}
        app: sscraper-{i}
    spec:
      nodeName: {server}
      containers:
      - name: sscraper-{i}
        image: "dprl/symbolscraper-server:latest"
        imagePullPolicy: Always
        ports:
        - containerPort: 7002
"""


if __name__ == '__main__':
    servers_ = ['doug', 'bob']
    namespace_ = 'msp'
    spec_dir_ = 'infra_v1'
    create_and_save_sscraper_services(servers_, namespace_, spec_dir_)

