'''
----------ChemScraper Pipeline Init----------
Author: Ayush Kumar Shah (as1211@rit.edu)
Version: June 2024
DPRL, RIT
'''
from typing import Dict
import math

# RZ: Add debug operations
from modules.protables.debug_fns import *
import modules.protables.debug_fns as debug_fns
dTimer = DebugTimer("ChemScraper pipeline")
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.

# Main import list
import json
import os
import numpy as np
import subprocess
import argparse
import requests
import time
from glob import glob
from pdf2image import convert_from_path
from multiprocessing import Pool
import gc
from bs4 import BeautifulSoup as bsoup
import re
from configparser import ConfigParser, ExtendedInterpolation
from modules.protables.ProTables import read_region_csv, region_intersection,\
    read_sscraper_json, region_intersection_with_visual, \
    read_sscraper_json_from_io, grow_for_contents, merge_pro_tables,\
    transform_pr_table, region_bb_list, apply_obj_within_region,\
    separate_visual_regions, crop_contents, obj_keep_types, filter_pr_table,\
    write_it_pro_tsv, sort_page_regions
from modules.protables.util import get_cc_objects_original, get_cc_objects_original_v2
# , show_pro_table
# from modules.chemscraper.one_pdf_test import extract_graphics_pdf
from modules.chemscraper.extract_mst import ExtractMST
from modules.chemscraper.utils.process_mst import post_process_mst
from modules.chemscraper.utils.viz_utils import imshow
from modules.chemscraper.graph_conversions.CDXML_Converter import visual2cdxml
from modules.chemscraper.eval.metrics import gen_alt_metrics
from glob import glob

from Levenshtein import distance
from rdkit import Chem
from rdkit.Chem import rdmolfiles
import tempfile
import cv2
import json

# RZ additions
from pprint import pprint
from modules.chemscraper import parser_v2
dTimer.qcheck("Python module load")

from multiprocessing import Pool, cpu_count
from modules.pipeline.utils.pymupdf_render import render_page
# from modules.chemscraper.nx_debug_fns import gdraw
from io import BytesIO

from modules.chemscraper.nx_debug_fns import gdraw
from modules.chemscraper.nx_debug_fns import gdescr, vgdescr

# RZ: Debug -- YOLO region detections overlapping; method to repair
from modules.chemscraper.utils.merging import merge_page_boxes

from modules.generate_data.src.images_to_lg import get_lg_from_image

import copy
import subprocess

from modules.chemscraper.utils.lgeval_to_nx import lg_eval_to_nx
import networkx as nx

from modules.utils.dprl_map_reduce import map_reduce, combine_dict_pair
import multiprocessing as mp
################################################################
# SymbolScraper
################################################################


# ------------ Call SymbolScraper -----------------
def process_sscraper(args):
    xmls = {}
    tot_pdfs = len(os.listdir(args.in_pdf_dir))
    for idx, pdf in enumerate(os.listdir(args.in_pdf_dir)):
        print(f'Processing Symbol Scraper for PDF: {pdf: >15}, {idx+1: >4}/{tot_pdfs}')
        pdf_name = pdf.split('.pdf')[0]
        xml_content = sync_call(os.path.join(args.in_pdf_dir, pdf))
        if xml_content is not None:
            xmls[pdf_name] = xml_content
        else:
            print(f'Error Processing Symbol Scraper for PDF: {pdf}')        
    return xmls

def process_sscraper_one_pdf(id, pdf_file):
    request_url = ("http://localhost:7002/explain-geometry?processors=ExplainGeometry&"
         "writePageBorderBBOX=true&writePoints=true&writeFontSize=true")            
    file = {"pdf": (id, pdf_file, "application/pdf")}
    resp = requests.post(request_url, files=file)
    if resp.status_code != 200:
        print(f'Error Processing Symbol Scraper for PDF id: {id}')
        return None    
    graphics_content = resp.content    
    return graphics_content
    

# ------------ SymbolScraper request --------------
def sync_call(pdf_file: str):
    filename = os.path.basename(pdf_file)
    print(f'Filename for request: {filename}')

    # r = "http://localhost:7002/process-pdf?writePageBorderBBOX=true&writePoints=true"
    r = ("http://localhost:7002/explain-geometry?processors=ExplainGeometry&"
         "writePageBorderBBOX=true&writePoints=true&writeFontSize=true")

    file = {"pdf": (filename, open(pdf_file, 'rb'), "application/pdf")}
    print('Posting request')
    resp = requests.post(r, files=file)
    print('Returned request')
    if resp.status_code != 200:
        print("request failed!")
        return None
    return resp.content


# Saves SymbolScraper output in JSON format
# NOTE: Currently sequential
def save_json(pdf_xmls, out_xml_dir):
    for pdf in pdf_xmls.keys():
        # save_path = os.path.join(out_xml_dir, pdf+'.xml')
        save_path = os.path.join(out_xml_dir, pdf+'.json')
        with open(save_path, 'wb+') as f:
            f.write(pdf_xmls[pdf])
        f.close()


#wrapper for parallel, 
def get_lg_from_image_parallel(data):
    id, _image, visualize, visualize_dir = data
    lg_content, _, _ = get_lg_from_image(_image, id, visualize, visualize_dir)
    return {id:lg_content}

def parse_images_from_visual(list_images, thresholds, lgap_server_name="localhost", parallel=True,
                             visualize=False, visualize_dir=""):
    molecules_ids = ""    

    print("* Creating inputs (primitive extraction) for visual parser")
    content_for_parallel = []    
    lgs_dict = {}
    for val in list_images:
        id_molecule = val["id"]
        molecules_ids += f"{id_molecule}\n"
        if not parallel:
            lgs_dict = combine_dict_pair(lgs_dict,
                                         get_lg_from_image_parallel((id_molecule, val["img"],
                                                                     visualize, visualize_dir)))
        else:
            content_for_parallel.append((id_molecule, val["img"], visualize, visualize_dir))
        
    if parallel:
        #a third of the processes available    
        number_of_processes = math.ceil(mp.cpu_count()*0.75)         
        lgs_dict = map_reduce(get_lg_from_image_parallel, combine_dict_pair, content_for_parallel, cpu_count=number_of_processes)
                        
    print("* Input creation completed for visual parser")
    dTimer.qcheck("Creating inputs for visual parser")
    
    # headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}    
    headers = {'Content-type': 'application/json', 'Accept': 'application/json'}
    # payload = json.dumps({"molecules_ids":molecules_ids,"lgs":all_lgs})
    payload = json.dumps(lgs_dict)
    
    url = f"http://{lgap_server_name}:8007/test"
    
    resp = requests.post(url, data=payload, headers=headers)
    # lgs_predicted = resp.text.split("<end lg>")
    lgs_predicted = resp.json()
    print("* Number of LGs parsed: ", len(lgs_predicted))
            
    for molecule in list_images:
        id_molecule = molecule["id"]            
        if id_molecule not in lgs_predicted:
            print(f"Warning: No LG predicted for molecule {id_molecule}")
            molecule["graph"] = nx.Graph()
            continue
        else:
            lg_string = bytes(lgs_predicted[id_molecule], "utf-8").decode("unicode_escape")
        path_lg = os.path.join("placeholder", f"{id_molecule}.lg")  

        try:
            graph = lg_eval_to_nx((path_lg, False, "", thresholds,True, lg_string,
                                   molecule["bbox"], None))
            molecule["graph"] = graph[id_molecule][0][0]
        except:
            # import traceback; print(traceback.format_exc())
            # import pdb; pdb.set_trace()
            print("Warning: Could not postprocess LG to CDXML for molecule", id_molecule)
            molecule["graph"] = nx.Graph()
    return list_images


################################################################
# Generating PDF Pages Images
################################################################

# Generate PDF images for PDF pages
def pdf_to_png( quadruples ):
    # Filenames are without paths (e.g., a.pdf, not a/b/c.pdf)
    ( pdf_dir, output_dir, pdf_files, dpi ) = quadruples

    # Each file will have individual page images written to disk.
    for pdf_file in pdf_files: 
        pdf_name = pdf_file.split(".pdf")[0]

        # Set up output path for document.
        output_path = os.path.join( output_dir, pdf_name )
        if not os.path.exists( output_path ):
            os.makedirs(output_path)

        # Read PDF, write 1 PNG per page to output directory for that PDF doc
        pages = convert_from_path( os.path.join(pdf_dir, pdf_file), dpi )
        for i in range(len(pages)):
            pages[i].save( os.path.join(output_path,  str(i + 1) + ".png"), "PNG")

        # Clean up.
        del pages
        gc.collect()

def convert2image(args, img_dir):
    pdf_files = os.listdir(args.in_pdf_dir)
    n_processes = int(os.cpu_count()/2)
    n = int(len(pdf_files) / n_processes)

    if n < 1.0:
        n = 1
        n_processes = len(pdf_files)

    chunks = [ pdf_files[x:x+n] for x in range(0, len(pdf_files), n) ]
    num_chunks = len(chunks)
    infiles = [ args.in_pdf_dir ] * num_chunks
    outfiles  = [ img_dir ] * num_chunks
    dpis = [ args.dpi ] * num_chunks
    chunk_tuples = list(zip( infiles, outfiles, chunks, dpis ) )

    # Create a process pool and execute.
    pool = Pool( processes = n_processes )
    pool.imap( pdf_to_png, chunk_tuples )
    pool.close()
    pool.join()

def convert2image_pymupdf(args, img_dir):
    # RZ: Adding faster image conversion
    # Original method still in this file as `convert2image`
    pdf_files = os.listdir(args.in_pdf_dir)
    cpu = cpu_count()

    for pdf_file in pdf_files:
        pdf_name = pdf_file.split(".pdf")[0]
        filename = os.path.join( args.in_pdf_dir, pdf_file )

        # Set up output path for document.
        output_path = os.path.join( img_dir, pdf_name )
        if not os.path.exists( output_path ):
            os.makedirs(output_path)

        # Taken from multiprocessing example in PyMuPDF docs: 
        #    https://pymupdf.readthedocs.io/en/latest/recipes-multiprocessing.html
        #vectors = [(i, cpu, filename, mat) for i in range(cpu)]
        vectors = [(i, cpu, filename, args.dpi, output_path ) for i in range(cpu)]
        with Pool() as pool:
            pool.map( render_page, vectors, 1)  # Start processes, passing each a vector


################################################################
# Detection and Detection Visualization
################################################################

def visualize_detection(img_dir, out_dir, csv_dir, pdf_name='O\'Brien Tables'):
    pdf_img_dir = img_dir
    pdf_out_dir = out_dir #os.path.join(out_dir,pdf_name)
    pdf_csv_dir = csv_dir

    # Change here because this is where the visualize annotations script lives.
    subprocess.call(
        [
            "python3", "modules/chemscraper/visualize_annotations.py",
            "--img_dir", pdf_img_dir,
            "--out_dir", pdf_out_dir,
            "--math_dir", pdf_csv_dir
        ]
    )

def new_vis_tsv(img_dir, out_dir, tsv_dir, suffix=''):
    pdf_img_dir = img_dir
    pdf_out_dir = out_dir #os.path.join(out_dir,pdf_name)
    pdf_tsv_dir = tsv_dir

    subprocess.call(
        [
            "python3", "modules/chemscraper/visualize_annotations.py",
            "--img_dir", pdf_img_dir,
            "--out_dir", pdf_out_dir,
            "--tsv_dir", pdf_tsv_dir,
            "--suffix", suffix,
        ]
    )

def process_yolo(img_dir, output_dir):
    # Earlier YOLO v4
    yolo_module_dir = 'modules/scaled-yolov4'
    yolo_module_dir = os.path.abspath(yolo_module_dir)
    yolo_img_dir = os.path.abspath(img_dir)
    yolo_output_dir = os.path.abspath(output_dir)
    cur_dir = os.getcwd()
    os.chdir(yolo_module_dir)
    
    print("yolo_img_dir",yolo_img_dir)
    print("yolo_output_dir",yolo_output_dir)
    print("img_dir",img_dir)
    print("output_dir",output_dir)

    subprocess.run(["python", "docker_detection.py", "--input_dir", yolo_img_dir, "--output_dir", yolo_output_dir, "--model", "chem", "--multi_dir"] )
    os.chdir(cur_dir)


# YOLO b8 (Newer) Detector
def proccess_yolov8(img_dir, output_dir):
    print("[START YOLOV8]")
    endpoint = "http://localhost:8005/yolo_from_images"    
    
    yolo_img_dir = os.path.abspath(img_dir)
    yolo_output_dir = os.path.abspath(output_dir)
    
    for (A, _, _filenames) in os.walk(yolo_img_dir):
        array_files = []        
        pdf_name = os.path.basename(A)
        for i in _filenames:            
            f = open(f"{A}/{i}", 'rb')
            array_files.append(('images',(i,f,'image/png')))
        if len(array_files) != 0:
            files_sorted = sorted(array_files, key=lambda x: int(os.path.basename(x[1][0])[:-4]))        
            resp = requests.post(endpoint, files=files_sorted, timeout=None)
            if resp.status_code != 200:
                print(f"Request failed! Yolov8 to: {endpoint}. File: {pdf_name}")
                print("resp.reason",resp.reason)
            else:                        
                if not os.path.exists(yolo_output_dir):
                    os.makedirs(yolo_output_dir)

                data = json.loads(resp.content.decode())
                csv_data = data["csv"]            
                with open(f'{yolo_output_dir}/{pdf_name}.csv', 'w') as csv_file:                
                    csv_file.write(csv_data) 
            map(lambda x: x[1][1].close(),array_files)
            
    print("[END YOLOV8]")
    
################################################################
# Combining PDF Symbols and YOLO Region Detections
################################################################

def process_combine_one_file(json_content: BytesIO, page_size_map, bbox,
                             filename, thresholds):
    # RZ: **Used when processing a single molecule (e.g., for parser evaluation)

    # Check if both XMLS and PDF Detections are available for ALL
    # xml_files = glob(os.path.join(out_xml_dir, "*.xml"))
    
    all_pdf_msts = {}
    all_pdf_page_region_bboxes = {}
    all_page_region_mst_stats= {}


    # RZ addition
    fileRegionProTable = {}
    metrics = {}
    
    # RZ: Separating graphics intersection and parsing stages
    # Extract SymbolScraper Information    
    _, _, _, _,  _, _, pageCharGeometryBBs = \
            read_sscraper_json_from_io(json_content, "server", None, extract_graphics=True,
                page_size_map= page_size_map, parallel=False)


    # Extract YOLO detections
    all_chem_bbs = bbox  # list of lists of tuples

    regionProTable, metrics = region_intersection(all_chem_bbs, 'FR', pageCharGeometryBBs, 
        metrics=metrics, filename=filename, sort_obj_types=False)
    
    fileRegionProTable[ filename ] = regionProTable    

    # dTimer.qcheck("Merging SymbolScraper PDF with YOLO regions")

    # Parsing graphic objects in polygons/shapes to visual graph
    # THIS IS PER XML FILE (multiple detected molecules)
    for filename, regionProTable in fileRegionProTable.items():
        mst_generator = ExtractMST(thresholds)
        page_region_msts, page_region_ingraphs, page_region_bboxes, page_region_mst_stats, _ = \
            mst_generator.extract_mst_poly(regionProTable)
        
        # RZ: Neat trick -- sum will concatenate a list of lists, by 'adding' this to an empty list.
        parser = parser_v2.Parser_v2(thresholds=thresholds)
        pp_page_region_msts = parser.post_process_msts(page_region_msts,
                                                            page_region_ingraphs,
                                                            page_region_bboxes,
                                                            None,
                                                            debug=False, 
                                                            parallel=False)

        # all_pdf_msts[filename] = page_region_msts
        all_pdf_msts[filename] = pp_page_region_msts
        all_pdf_page_region_bboxes[filename] = page_region_bboxes
        all_page_region_mst_stats[filename] = page_region_mst_stats
    

    return all_pdf_msts, all_pdf_page_region_bboxes, all_page_region_mst_stats



def process_combine(args, out_xml_dir, det_dir, img_dir, vis_pad=0.1,
                    thresholds=None):
    # Check if both XMLS and PDF Detections are available for ALL
    # xml_files = glob(os.path.join(out_xml_dir, "*.xml"))
    json_files = glob(os.path.join(out_xml_dir, "*.json"))
    num_csvs = len(glob(os.path.join(det_dir, "*.csv")))
    num_jsons = len(json_files)

    # assert num_jsons == num_csvs, f" Error - Found {num_jsons} Symbol Scraper JSONs but {num_csvs} YOLO detect CSVs"
    
    all_pdf_msts = {}
    all_pdf_page_region_bboxes = {}
    # RZ addition
    fileRegionProTable_digital = {}
    fileRegionProTable = {}
    metrics = {}

    graphs_from_visual = {}
    all_mapping = {}


    # RZ: Separating graphics intersection and parsing stages
    for json_file in json_files:
        filename = os.path.basename(os.path.splitext(json_file)[0])
        file_path_csv = os.path.join(det_dir, filename + ".csv")
        pdf_img_dir = os.path.join(img_dir, filename)

        # Extract SymbolScraper Information
        num_pages, page_sizes, _, pageWordBBs,  _, _, pageCharGeometryBBs = read_sscraper_json(out_xml_dir, filename,
                                                                    pdf_img_dir, extract_graphics=True)
        dTimer.qcheck("Reading Symbol Scraper Info (JSON)")

        non_blank = len(list(filter(lambda x: len(x) > 0, pageCharGeometryBBs)))
        check('PDF File SymbolScraper JSON ', os.path.basename(json_file))
        check('         YOLO CSV (regions)', os.path.basename(file_path_csv))
        check('* PDF pages',num_pages)
        check('*     non-blank', non_blank)

        # RZ record metrics
        metrics[ filename ] = {}
        metrics[ filename ][ 'PDF-pages' ] = num_pages
        metrics[ filename ][ 'PDF-non-blank' ] = non_blank
        
        # Extract YOLO detections
        all_chem_bbs = read_region_csv(file_path_csv, num_pages)  # list of lists of tuples
        check('* Initial YOLO detections', sum(map(len, all_chem_bbs)))

        # RZ (Feb 2024): Debug -- check for overlapping YOLO dections
        # Doing this here to leave YOLO results as-is.
        merged_chem_bbs = list( map(merge_page_boxes, enumerate(all_chem_bbs)) )
        num_regions = sum(map(len, merged_chem_bbs))
        metrics[ filename ][ 'YOLO-detections' ] = num_regions

        dTimer.qcheck("Reading YOLO regions (CSV)")

        # AKS: Bring back math code to grow regions to hold words
        # Grow regions to hold words.
        # TODO: region gorwing not working since word bboxes are malformed (very bad)
        # regionWordPROTable, metrics = \
        #     region_intersection(merged_chem_bbs, 'FR', pageWordBBs,
        #                         metrics=metrics, filename=filename,
        #                         sort_obj_types=False)
        # grownRegionWordPROTable = transform_pr_table(
        #     grow_for_contents(page_sizes), None, regionWordPROTable)
        # # regionWordBBs = region_bb_list(regionWordPROTable)
        # regionWordBBs = region_bb_list(grownRegionWordPROTable)

        print(f'\n [ Creating merged ProTables: SymbolScraper and YOLO ... ]')
        # Merge formula regions from YOLO and primitives from SymbolScraper
        regionCharGeometryPROTable, metrics = \
            region_intersection(merged_chem_bbs, 'FR', pageCharGeometryBBs,
                                metrics=metrics, filename=filename, 
                                sort_obj_types=False)
        # dTimer.check("Region intersection")
        regionBBs = region_bb_list(regionCharGeometryPROTable)
        # dTimer.check("Region bb list")
        # regionCharGeometryWordPROTable = \
        #     merge_pro_tables( grownRegionWordPROTable, regionCharGeometryPROTable  )

        ##################################
        # Get CCs for generated regions
        ##################################
        ccGrownProTable, page_imgs = get_cc_objects_original_v2(pdf_img_dir, regionBBs, "FR") 
        # dTimer.check("Get ccs")
        # Merge CCs, chars and graphics into one complete PRO table
        mergedTable = merge_pro_tables(regionCharGeometryPROTable,
                                       ccGrownProTable)
        # dTimer.check("Merge protables")

        # Filter out objects larger than the formula regions
        filteredPROTable = apply_obj_within_region(mergedTable)
        # dTimer.check("Filter large objects")
        # AKS: Grow the region based on objects (CCs, symbol scraper objects)
        grownFilteredPROTable = transform_pr_table(grow_for_contents(page_sizes), 
                                                   None, filteredPROTable)
        # dTimer.check("Grow regions")

        objTypeFilter = obj_keep_types( [ 'S', 'GC', 'G' ] )
        regionProTable = filter_pr_table( None, objTypeFilter, grownFilteredPROTable )
        # dTimer.check("Filter object entries")

        regionProTable = sort_page_regions(regionProTable, sort_obj_types=False)
        dTimer.qcheck("Creating merged ProTables: SymbolScraper and YOLO ...")

        # print(f'\n [ Separating born-digital vs. visual regions ... ]')
        # regionProTable, visual_imgs, metrics, mapping = region_intersection_with_visual(merged_chem_bbs, 'FR', pageCharGeometryBBs, pdf_img_dir,
        #                           metrics=metrics, filename=filename, sort_obj_types=False,
        #                           vis_pad=vis_pad)
        visual_imgs, metrics, mapping, digital_table = separate_visual_regions(regionProTable, page_imgs,
                                                                               metrics, filename,
                                                                               args.viz_lgap,
                                                                               args.lgap_viz_dir)
        ##################################
        # Write to disk as TSV
        ##################################
        # If visualizing, save TSV for graphics/chars from PDF in formula regions
        if args.viz_dets: 
            write_it_pro_tsv( filename, args.in_pdf_dir, args.tsv_dir,
                ( {}, regionProTable ), page_sizes, suffix='_chem_regions')

        # dTimer.qcheck("Separating born-digital vs. visual regions")
        if len(visual_imgs):
            print(f'\n [ Parsing images using visual parser (LGAP) ]')
            check("* Number of visual regions", len(visual_imgs))
            msts_from_visual = parse_images_from_visual(visual_imgs, thresholds,
                                                        parallel = not args.np,
                                                        visualize=args.viz_lgap,
                                                        visualize_dir=os.path.join(args.lgap_viz_dir, 
                                                                                   filename))
            dTimer.qcheck("Parsing images to chemical graphs using visual parser (LGAP)")
        else:
            print(f'\n [ No visual regions detected ]')
            msts_from_visual = None

        graphs_from_visual[filename] = msts_from_visual
        all_mapping[filename] = mapping

        fileRegionProTable_digital[ filename ] = digital_table
        fileRegionProTable[ filename ] = regionProTable

    # Parsing graphic objects in polygons/shapes to visual graph
    # THIS IS PER XML FILE (multiple detected molecules)
    for filename, regionProTable in fileRegionProTable_digital.items():
    # for filename, regionProTable in fileRegionProTable.items():
        mst_generator = ExtractMST(args.thresholds)
        page_region_msts, page_region_ingraphs, page_region_bboxes, _, _ = \
            mst_generator.extract_mst_poly(regionProTable)
        
        # RZ: Neat trick -- sum will concatenate a list of lists, by 'adding' this to an empty list.
        check('* PDF pages with YOLO detections',len(page_region_msts))
        all_mst = sum( page_region_msts.values(), [] )
        check('* Initial MST count (all pages)', len(all_mst))
        print('')

        # RZ record metrics
        metrics[ filename ][ 'Pages-with-YOLO-detections' ] = len(page_region_msts)
        metrics[ filename ][ 'Initial-MST-count' ] = len(all_mst)

        
        if not args.v2:
            # Get the visual graphs from post-processed graphs
            pp_page_region_msts = post_process_mst(page_region_msts,
                                                   page_region_ingraphs,
                                                   page_region_bboxes, mode='pipeline')
        else:
            parser = parser_v2.Parser_v2(thresholds=args.thresholds)
            pp_page_region_msts = parser.post_process_msts(page_region_msts,
                                                              page_region_ingraphs,
                                                              page_region_bboxes,
                                                              img_dir,                                                              
                                                              debug=False,  # Set to true to see PDF graph visualizations
                                                              parallel= not args.np )



        #pcheck('Graph check', vgdescr( pp_page_region_msts[0][1] ))

        all_pdf_msts[filename] = pp_page_region_msts
        all_pdf_page_region_bboxes[filename] = page_region_bboxes

        counter_visual_regions = len(graphs_from_visual[filename] if graphs_from_visual[filename] else [])        
        counter = 0
        counter_digital = 0
        if graphs_from_visual[filename]:
            for page_num, regions in all_mapping[filename].items():                
                for region_num, type_region in regions.items():
                    if type_region == "visual":                    
                        all_pdf_msts[filename][page_num][region_num] = graphs_from_visual[filename][counter]["graph"]
                        all_pdf_page_region_bboxes[filename][page_num][region_num] = graphs_from_visual[filename][counter]["bbox"]
                        counter+=1
                    else:
                        counter_digital += 1
        else:
            counter_digital = num_regions

        check('* Number of Visual regions', counter)
        check('* Number of Born-digital regions', counter_digital)

    if counter_digital:
        dTimer.qcheck("Parsing PDF objects to chemical graphs using born-digital parser")    
    # print("-----------------")
    # print("MAPPING OF REGION TYPES")
    # pprint(all_mapping)
    # print("-----------------")

    # dTimer.qcheck("Parsing PDF objects to visual graphs")
    with open(os.path.join(os.path.dirname(img_dir), 
                           "visual_vs_digital_mapping_regions.json"), "w") as outfile:
        json.dump(all_mapping, outfile, indent=4)
    
    #graphs_from_visual
    metrics[filename].update({"Parsed-visual-regions": counter_visual_regions})

    return all_pdf_msts, all_pdf_page_region_bboxes, metrics

################################################################
# Generating molecule images, SMILES, and TSV files
################################################################

def gen_svg(cdxml_dir, out_path=None):
    print('\n [ RENDERING SVGs from CDXMLs (Molconvert) ]')
    ignore_dirs = ['cdxml', 'pages']
    all_dirs = os.listdir(cdxml_dir)

    # RZ: Only convert the _SVG annotated directories
    molecules_dirs = [pdf for pdf in all_dirs if pdf.split('_')[-1] not in ignore_dirs and pdf[-4:]=='_SVG']
    
    stderr_file_name = 'STDERR_Molconvert_SVG'
    print('* Redirecting standard error to:  ' +  stderr_file_name)
    
    for molecules_dir in molecules_dirs:
        pdf_cdxml_dir = os.path.join(cdxml_dir, molecules_dir)
        pattern = os.path.join(os.getcwd(), pdf_cdxml_dir, 'Page_*.cdxml')
        # Add escape character before spaces
        pattern = re.sub(r' ', r'\\ ', pattern)
        cdxmls = sorted(os.listdir(pdf_cdxml_dir))
        
        if out_path is None:
            pdf_svg_dir = os.path.join(cdxml_dir, molecules_dir, 'svg')
        else:
            pdf_svg_dir = out_path
        print('* CDXML dir: ', 
                os.path.join(pdf_cdxml_dir))
        print('  SVG dir: ', pdf_svg_dir )
        cdxmls = [f for f in cdxmls if f.endswith('.cdxml')]

        # Routing Java exceptions for font sizes in points to /dev/null
        err_file = open(stderr_file_name, 'w')
        # NOTE: Molconvert will only resize atoms based on bond length (atsiz)
        #       if font sizes cannot be interpreted (e.g., 'pt' in sizes)
        # svgs = subprocess.run('molconvert "svg:w1000,maxscale50" ' + pattern,
        
        svgs = subprocess.run('molconvert "svg:scale28" ' + pattern,        
                            shell=True, stdout=subprocess.PIPE, stderr=err_file)
        svg_string = svgs.stdout.decode('utf-8')
        err_file.close()

        pattern = r'(<\?xml version="1\.0"\?>.*?<\/svg\s*>)'
        pdf_svgs = re.findall(pattern, svg_string, re.DOTALL)
        if not len(pdf_svgs) or len(pdf_svgs) != len(cdxmls):
            # raise Exception('Error in SVG conversion. Please check CDXML Conversion')
            print("    Few CDXMLs can not be converted to SVG.")
            continue
        file2svg = zip(cdxmls,pdf_svgs)
        file2svg = dict(file2svg)

        os.makedirs(pdf_svg_dir, exist_ok=True)
        
        for key in sorted(list(file2svg.keys())):
            f = open(os.path.join(pdf_svg_dir, key.replace(".cdxml", ".svg")), 'w')
            svg = file2svg[key]
            f.write(svg)
            f.close()

    print('  SVG generation complete\n')
    
def gen_smiles(cdxml_dir, smi_dir, png, metrics=None, cdxmls=None):
    # Use Molconvert (DEFAULT/RECOMMENDED)
    if cdxmls is None:
        ignore_dirs = ['cdxml', 'pages']
        all_dirs = os.listdir(cdxml_dir)
        val_pdfs = [pdf for pdf in all_dirs if pdf.split('_')[-1] not in ignore_dirs]

        stderr_file_name = 'STDERR_Molconvert_SMILES'
        print('* Redirecting standard error to:  ' + stderr_file_name)
        
        for pdf_dir in val_pdfs:
            pdf_cdxml_dir = os.path.join(cdxml_dir,pdf_dir)
            pattern = os.path.join(os.getcwd(), pdf_cdxml_dir, 'Page_*.cdxml')
            
            cdxml_filenames = sorted(glob(pattern))
            grouped_cdxml_filenames = []
            stride = 50000 // len(cdxml_filenames[0])
            for i in range(stride, len(cdxml_filenames), stride):
                print(i-stride, i, sep=',')
                grouped_cdxml_filenames += [" ".join(cdxml_filenames[i-stride:i])]
            remaining = " ".join(cdxml_filenames[len(cdxml_filenames) // stride * stride:])
            if len(remaining) > 0:
                grouped_cdxml_filenames += [remaining]

            cdxmls = sorted(os.listdir(pdf_cdxml_dir))
            cdxmls = [os.path.join(pdf_cdxml_dir, cd) for cd in cdxmls]
            
            if len(grouped_cdxml_filenames) == 0:
                continue
            err_file = open(stderr_file_name, 'w')

            pdf_smiles = []
            for group in grouped_cdxml_filenames:
                smiles = subprocess.run('molconvert smiles ' + group,
                                        shell=True, stdout=subprocess.PIPE, stderr=err_file)
                pdf_smiles += smiles.stdout.decode('utf-8').split('\n')
            
            err_file.close()
            
            if not len(pdf_smiles):
                raise Exception('Error in SMILES conversion. Please check CDXML Conversion')            
            pdf_smiles = pdf_smiles[:-1]            
            file2smi = zip(cdxmls,pdf_smiles)
            file2smi = dict(file2smi) 
            check('* Files converted to SMILES', len(file2smi))

            # Now Write the SMILES
            pdf_smi_dir = os.path.join(smi_dir,pdf_dir)
            os.makedirs(pdf_smi_dir, exist_ok=True)
            f = open(os.path.join(pdf_smi_dir, 'smiles_out.txt'), 'w')
            for key in sorted(list(file2smi.keys())):
                smi = file2smi[key]
                f.write(key+', '+smi+'\n')
            f.close()

            pdf_smi_dir = os.path.join(cdxml_dir,pdf_dir)
            os.makedirs(pdf_smi_dir, exist_ok=True)
            f = open(os.path.join(pdf_smi_dir, 'smiles_out.txt'), 'w')
            for key in sorted(list(file2smi.keys())):
                smi = file2smi[key]
                f.write(smi+'\n')
            f.close()

    # Use RDKit SMILES conversion
    else:
        for pdf in cdxmls.keys():
            pdf_dir = os.path.join(smi_dir,pdf)
            os.makedirs(pdf_dir, exist_ok=True)

            smi_dict = {}
            pages = sorted(list(cdxmls[pdf].keys()))
            for page in pages:
                for idx_cd, cdxml in enumerate(cdxmls[pdf][page]):
                    f_name = os.path.join(smi_dir,pdf,'Page_'+str(page+1)+'_No'+str(idx_cd)+'.cdxml')
                    if cdxml != 'N/A':
                        rd_mol_it = rdmolfiles.MolsFromCDXML(cdxml, sanitize=False)
                        for rd_mol in rd_mol_it:
                            smi = Chem.MolToSmiles(rd_mol, canonical=True)
                            break
                        smi_dict[f_name] = smi
                    else:
                        smi_dict[f_name] = 'ERROR'
            
            # Write the smiles file:
            f = open(os.path.join(pdf_dir, 'smiles_out.txt'), 'w')
            for key in smi_dict.keys():
                smi = smi_dict[key]
                f.write(key+', '+smi+'\n')
            f.close()

    # RZ: By default this is no longer done.
    if cdxmls is None and png > 0:
        print(f' [NOW WRITING THE PNGS FOR THE CDXMLS]')
        for pdf in all_dirs:
            pdf_cdxmls = os.path.join(cdxml_dir,pdf)
            pdf_smi_dir = os.path.join(smi_dir,pdf)

            subprocess.run(['./modules/chemscraper/bin/cd_to_png.sh', pdf_cdxmls, pdf_smi_dir + "/pngs"])

    return metrics

def gen_tsv(smi_dir, frs, tsv_dir, metrics: Dict=None):
    root = os.getcwd()
    with open(os.path.join(root,".cz.json")) as f:
        config = json.load(f)      
    version = config["commitizen"]["version"]

    
    data_name = smi_dir.split('/')[1]
    smiles_count = 0
    with open(os.path.join(tsv_dir, 'ecfp_data.tsv'), 'w') as f:
        pdfs = os.listdir(smi_dir)
        
        for idx_pdf, pdf in enumerate(pdfs):
            if not (metrics is not None and pdf in metrics):
                continue
            in_pdf_basepath = os.path.abspath(os.path.join('inputs', data_name+'_inpdfs'))

            in_pdf_path = os.path.join(in_pdf_basepath, pdf+'.pdf')
            extras_str = ""
            
            # Read all the filenames and their CDXMLs
            smi_meta = []            
            with open(os.path.join(smi_dir, pdf, 'smiles_out.txt')) as smi_f:
                valid_smiles = 0
                for line in smi_f.readlines():
                    if line.split(',')[-1].lstrip().rstrip('\n') != 'ERROR':
                        valid_smiles += 1
                        file_n = os.path.basename(line.split(',')[0]).rstrip('.cdxml')
                        file_smi = line.split(',')[-1].lstrip().rstrip('\n')
                        smi_meta.append([file_n, file_smi])
                if metrics is not None:                    
                    metrics[ pdf ][ 'SMILES-output-strings' ] = valid_smiles
            smi_f.close()

            if metrics is not None:
                page_metrics = metrics.get(pdf, None)
                extras_str = \
                    f" detected={page_metrics['Initial-MST-count']} " \
                    f"parsed={page_metrics['MSTs-converted-to-CDXML']} " \
                    f"converted={page_metrics['SMILES-output-strings']} " \
                    f"version={version}"
            f.write('D\t'+str(idx_pdf+1)+'\t'+in_pdf_path + extras_str+'\n')

            uniq_pages = []
            for meta in smi_meta:
                pdf_page = int(meta[0].split('_')[1]) - 1
                uniq_pages.append(pdf_page)
            uniq_pages = list(set(uniq_pages))

            pdf_pages = {page:[] for page in uniq_pages}
            pdf_smiles = {page:[] for page in uniq_pages}
            for meta in smi_meta:
                pdf_page = int(meta[0].split('_')[1]) - 1
                pdf_pages[pdf_page].append(meta[0])
                pdf_smiles[pdf_page].append(meta[1])
            
            for page in sorted(pdf_pages.keys()):
                f.write('P\t'+str(page+1)+'\t'+str(1000)+'\t'+str(1000)+'\n')
                # Sort regions by top-left to bottom-right 
                bboxs = []
                smis = []
                for idx_page, page_file in enumerate(pdf_pages[page]):
                    # RZ: Skip non-cdxml files
                    #if page_file[-4:] == '.svg':
                    #    continue

                    pdf_page = page
                    mol_id = int(page_file.split('_')[-1].lstrip('No')) - 1
                    smi = pdf_smiles[page][idx_page] 
                    min_x, min_y, max_x, max_y = frs[pdf][pdf_page][mol_id]
                    bboxs.append([min_x, min_y, max_x, max_y])
                    smis.append(smi)
                smis = np.array(smis, dtype=object)
                bboxs = np.array(bboxs).reshape((-1,4))
                ind = np.lexsort((bboxs[:,0], bboxs[:,1]))
                bboxs = bboxs[ind]
                smis = smis[ind]
                for idx_page, box in enumerate(bboxs):
                    min_x, min_y, max_x, max_y = box
                    f.write('FR\t'+str(idx_page+1)+'\t'+str(min_x)+'\t'+str(min_y)+'\t'
                            +str(max_x)+'\t'+str(max_y)+'\n')
                    smi = smis[idx_page]
                    f.write('SMI\t'+str(idx_page+1)+'\t'+smi+'\t'+str(min_x)+'\t'+str(min_y)+'\t'
                            +str(max_x)+'\t'+str(max_y)+'\n')
                    smiles_count += 1

    check('  SMILES strings written', smiles_count)
    f.close()


################################################################
# Generating CDXML Files
################################################################

def gen_full_page_cdxml(msts, smi_dir, cdxml_dir):
    pdf_names = sorted(list(msts.keys()))
    # Generate CDXMLs for all PDFs parsed
    for pdf_name in pdf_names:
        pdf_msts = msts[pdf_name]
        smi_out_file = os.path.join(smi_dir,pdf_name,'smiles_out.txt')
        cd_files = []
        smiles = []
        
        with open(smi_out_file, 'r') as f:
            for line in f.readlines():
                cd_file = line.split(',')[0]
                smile = line.split(',')[-1].lstrip().rstrip('\n')
                if smile != 'ERROR':
                    cd_files.append(cd_file)
                    smiles.append(smile)
        f.close()

        # Get unique pages
        # int(file.split('/')[-1].split('_')[-1].lstrip('No').rstrip('.cdxml'))
        pages = []
        for file in cd_files:
            page_no = int(file.split('/')[-1].split('_')[1])
            if page_no not in pages:
                pages.append(page_no)
        pages = sorted(pages)
        valid_dict = {p-1:[] for p in pages}

        for file in cd_files:
            page_no = int(file.split('/')[-1].split('_')[1])
            mol_id = int(file.split('/')[-1].split('_')[-1].lstrip('No').rstrip('.cdxml'))
            valid_dict[page_no-1].append(mol_id-1)
        
        # Now write page level cdxml        
        visual2cdxml(pdf_msts, out_dir=cdxml_dir, valid_dict=valid_dict,
                    pdf_name=pdf_name)


################################################################
# Evaluation
################################################################

def gen_metrics(pred_smi_dir, regions, 
            source_cdxml_dir='modules/chemscraper/Table_cdxml_files/Table_cdxml_files',
            source_smi_dir='modules/chemscraper/Table_cdxml_files/molecule_smiles',
            eval_dir='outputs/chemxtest/eval_tsv'):
    all_tot_mols = 0
    tot_mols = 0
    exact_matches = 0
    tot_norm_score = 0
    tot_unnorm_score = 0
    all_source_lens = []
    source_cdxmls = sorted(os.listdir(source_cdxml_dir))
    source_smis = os.listdir(source_smi_dir)
    pdf_name = os.listdir(pred_smi_dir)[0]
    pred_smi_file = os.path.join(pred_smi_dir, pdf_name, 'smiles_out.txt')

    # Get the unique source table ID's
    unique_tables = []
    for smi_file in source_smis:
        table_id = int(smi_file.split('-')[0].lstrip('Tab'))
        if table_id not in unique_tables:
            unique_tables.append(table_id)
    # Create a dictionary to store the corresponding frs for each page
    matches = {p:[] for p in unique_tables}

    # Get all the pred smiles and strings
    pred_smi_paths = []
    pred_smi_strs = []
    with open(pred_smi_file, 'r') as f:
        for data_line in f.readlines():
            file_name = data_line.split(',')[0].split('/')[-1]
            smi_str = data_line.split(',')[1].lstrip().rstrip('\n')
            if smi_str != 'ERROR':
                pred_smi_paths.append(file_name)
                pred_smi_strs.append(smi_str)
    
    # Convert all the pred smiles to RDKit canonicalization
    can_pred_smis = []
    for idx_smi, smi_str in enumerate(pred_smi_strs):
        try:
            mol = Chem.MolFromSmiles(smi_str)
            can_smi = Chem.MolToSmiles(mol, canonical=True)
            can_pred_smis.append(can_smi)
        except:
            print(f'For cdxml: {pred_smi_paths[idx_smi]}')
            can_pred_smis.append(smi_str)
            
    for tab_id in sorted(unique_tables):
        table_id_smi_files = [smi_file for smi_file in source_smis if 
                          int(smi_file.split('-')[0].lstrip('Tab')) == tab_id]
        # Get all the source SMILES for the page
        tab_smis = []
        for file in sorted(table_id_smi_files):
            in_file = open(os.path.join(source_smi_dir,file), 'r')
            smi_string = in_file.read()
            in_file.close()
            tab_smis.append(smi_string.rstrip('\n'))
        
        # Canonicalize the source smis
        can_tab_smis = []
        for tab_smi in tab_smis:
            try:
                mol = Chem.MolFromSmiles(tab_smi)
                can_smi = Chem.MolToSmiles(mol, canonical=True)
                can_tab_smis.append(can_smi)
            except:
                can_tab_smis.append(tab_smi)

        # Get all the pred SMILES with its corresponding filenames for the page
        table_pred_smis = [can_pred_smis[idx_smi] for idx_smi, pred_smi_path in enumerate(pred_smi_paths) if 
                          int(pred_smi_path.split('_')[1]) == tab_id]
        table_pred_smi_filenames = [pred_smi_path for pred_smi_path in pred_smi_paths if 
                                    int(pred_smi_path.split('_')[1]) == tab_id]

        # Get the matches
        for page_idx, smi in enumerate(table_pred_smis):
            all_tot_mols += 1
            cur_unnorm_scores = []
            cur_unnorm_idxs = []
            for idx_tab_smi, tab_smi in enumerate(can_tab_smis):
                score = distance(tab_smi, smi)
                cur_unnorm_scores.append(score)
                cur_unnorm_idxs.append(idx_tab_smi)
            min_idx = cur_unnorm_scores.index(min(cur_unnorm_scores))
            min_score = min(cur_unnorm_scores)
            # Get the formula region of the smis
            # try:
            region = regions[pdf_name][tab_id-1][
                        int(table_pred_smi_filenames[page_idx].split('_')[2].lstrip('No').rstrip('.cdxml'))]
            # except:
            #     print(page_idx)
            #     print(tab_id)
            #     print(table_id_pred_smi_files)
            #     print(int(table_id_pred_smi_files[page_idx].split('_')[2].lstrip('No').rstrip('.smi')))
            #     print(regions[pdf_name][1])
            #     print(len(regions[pdf_name][2]))
            #     print(len(table_id_pred_smi_files))
            #     exit()

            if min_score == 0:
                exact_matches += 1
            if min_score <= 15: # Somewhat valid match, take it
                # Get the unnormalized scores
                norm_score = min_score/max(len(smi),len(can_tab_smis[min_idx]))
                matches[tab_id].append([can_tab_smis[min_idx], smi, region, min_score, norm_score])
                tot_mols += 1
                tot_unnorm_score += min_score
                tot_norm_score += norm_score
                all_source_lens.append(len(tab_smis[min_idx]))
            else: # No Source PDF match found
                matches[tab_id].append(['--', smi, region, len(smi), 1])
    
    # Now compute final metrics
    avg_norm_score = tot_norm_score/tot_mols
    avg_unnorm_score = tot_unnorm_score/tot_mols
    min_smi_len = min(all_source_lens)
    max_smi_len = max(all_source_lens)
    avg_smi_len = sum(all_source_lens)/len(all_source_lens)

    print(avg_norm_score)
    print(avg_unnorm_score)
    print(min_smi_len,max_smi_len,avg_smi_len)
    print(exact_matches/tot_mols)


    # Now write the final detections
    print(f"Eval Dir: {os.path.join(eval_dir,pdf_name+'_eval.tsv')}")
    with open(os.path.join(eval_dir,pdf_name+'_eval.tsv'), 'w') as f:
        f.write('PDF\t'+'1\t'+pdf_name+'\n')
        for page_id in sorted(matches.keys()):
            f.write('P\t'+str(page_id)+'\n')
            f.write('Source SMI\t'+'Pred SMI\t'+'PDF Region\t'+'Act. Leven.\t'+'Norm Leven.\n')
            # Order all the values by pdf region
            pdf_box = []
            for meta in matches[page_id]:
                pdf_box.append(meta[2])
            pdf_box = np.array(pdf_box).reshape((-1,4))
            sort_idx = np.lexsort((pdf_box[:,0],pdf_box[:,1]))
            np_meta = np.array(matches[page_id], dtype=object)
            np_meta = np_meta[sort_idx]

            for meta in np_meta:
                f.write(str(meta[0])+'\t'+str(meta[1])+'\t'+str(meta[2][0])+'\t'+str(meta[2][1])+
                                             '\t'+str(meta[2][2])+'\t'+str(meta[2][3])+'\t'+
                                             str(meta[3])+'\t'+str(meta[4])+'\n')
    f.close()

   
################################################################
# Main Pipeline
################################################################
def parse_args():
    parser = argparse.ArgumentParser(
        description='Pipeline Mode for Chem Diagram Extraction and Conversion')
    parser.add_argument(
        '--in_pdf_dir',
        default='inputs/chemxtest_inpdfs',
        type=str,
        help='Input Data Directory containing PDFs to be parsed'
    )
    parser.add_argument(
        '--output_dir',
        default='outputs',
        type=str,
        help='Input Data Directory containing PDFs to be parsed'
    )
    parser.add_argument(
        '--xml_dir',
        default='xmls',
        type=str,
        help='Output Data Directory name for saving SScraper XMLs'
    )
    parser.add_argument(
        '--img_dir',
        default='images',
        type=str,
        help='Outut Data Directory name for saving converted images'
    )
    parser.add_argument(
        '--yolo_dir',
        default='yolo_boxes',
        type=str,
        help='Outut Data Directory for detection data out from YOLO'
    )
    parser.add_argument(
        '--cdxml_dir',
        default='generated_cdxmls',
        type=str,
        help='Outut Data Directory for generated CDXMLs'
    )
    parser.add_argument(
        '--svg',
        action='store_true',
        help='Convert CDXML to SVG'
    )
    parser.add_argument(
        '--smi_dir',
        default='generated_smiles',
        type=str,
        help='Outut Data Directory for generated SMILES'
    )
    parser.add_argument(
        '--smi_pngs',
        default=0,
        type=int,
        help='Draw the pngs for the converted molecules'
    )
    parser.add_argument(
        '--tsv_dir',
        default='generated_tsv',
        type=str,
        help='Outut Data Directory for generated TSVs'
    )
    parser.add_argument(
        '--dpi',
        default=300,
        type=int,
        help='Default DPI for converting PDF to images'
    )
    parser.add_argument(
        '--cdxml_page',
        default=0,
        type=int,
        help='1: Create Full Page CDXMLs 0: Dont'
    )
    parser.add_argument(
        '--viz_dets',
        default=0,
        type=int,
        help='Visualize YOLO detections'
    )
    parser.add_argument(
        '--viz_lgap',
        default=0,
        type=int,
        help='Visualize LGAP input formulas'
    )
    parser.add_argument(
        '--viz_dir',
        default='yolo_viz',
        type=str,
        help='Outut Data Directory for generated CDXMLs'
    )
    parser.add_argument(
        '--eval',
        default=0,
        type=int,
        help='Evaluate base PDF with original CDXMLs (Only with source CDXML converted SMILES)'
    )
    parser.add_argument(
        '--eval_dir',
        default='eval_tsv',
        type=str,
        help='Evaluate base PDF with original CDXMLs (Only with source CDXML converted SMILES)'
    )

    # RZ Additions
    parser.add_argument(
        '--no_yolo',
        action='store_true',
        help='Skip YOLO detection'
    )
    parser.add_argument(
        '--no_ssc',
        action='store_true',
        help = 'Skip SymbolScraper'
    )
    parser.add_argument(
        '--no_smiles',
        action='store_true',
        help = 'Skip SMILES and final PRO table (TSV) output.'
    )
    parser.add_argument(
        '--parse_only',
        action='store_true',
        help = 'Skip YOLO, SymbolScraper, and SMILES/TSV generation.'
    )
    parser.add_argument(
        '--v2',
        action='store_true',
        help = 'Use V2 dev. molecule parser'
    )

    # AD: SMILES Conversion Flag
    parser.add_argument(
        '--rd_smiles',
        default=0,
        type=int,
        help='SMILES Conversion Method: 1-RDKit; 0-Molconvert'
    )
    parser.add_argument(
        '--vis_pad',
        default=0.2,
        type=float,
        help='Padding for Region Images for Visual Parser'
    )

    # RZ: Image generation flag (default using faster pymupdf)
    parser.add_argument(
        '--pdf2image',
        action="store_true",
        help = 'Use pdf2image to generate png for pages'
    )

    # RZ: Allow parallel execution to be turned off.
    parser.add_argument(
        '--np',
        action="store_true",
        help = "Turn off parallel execution in parser"
    )

    parser.add_argument(
        '-t', '--thresholds',
        default = "modules/chemscraper/data/thresholds-scott.ini",
        help = "Config file for thresholds to be used in parser"
    )

    parsed_args = parser.parse_args()
    thresholds = ConfigParser(interpolation=ExtendedInterpolation())
    thresholds.read(parsed_args.thresholds)
    
    thresholds_dict = {
        "REMOVE_ALPHA":thresholds.getfloat("thresholds", "REMOVE_ALPHA"),
        "NEG_CHARGE_Y_POSITION":thresholds.getfloat("thresholds", "NEG_CHARGE_Y_POSITION"),
        "NEG_CHARGE_LENGTH_TOLERANCE":thresholds.getfloat("thresholds", "NEG_CHARGE_LENGTH_TOLERANCE"),
        "Z_TOLERANCE":thresholds.getfloat("thresholds", "Z_TOLERANCE"),
        "CLOSE_NONPARALLEL_ALPHA":thresholds.getfloat("thresholds", "CLOSE_NONPARALLEL_ALPHA"),
        "CLOSE_CHAR_LINE_ALPHA":thresholds.getfloat("thresholds", "CLOSE_CHAR_LINE_ALPHA"),

        "LONGEST_LENGTHS_DIFF_RATIO":thresholds.getfloat("thresholds", "LONGEST_LENGTHS_DIFF_RATIO"),
        "PARALLEL_TOLERANCE":thresholds.getfloat("thresholds", "PARALLEL_TOLERANCE"),        
        "COS_PRUNE":thresholds.getfloat("thresholds", "COS_PRUNE"),
        "HASHED_LENGTH_DIFF_THRESHOLD":thresholds.getfloat("thresholds", "HASHED_LENGTH_DIFF_THRESHOLD"),
    }
    parsed_args.thresholds = thresholds_dict
    return parsed_args


def create_output_dirs( args ):
    # --------- Creating OUTPUT Directories ----------
    # Create main out directory for the given PDFs
    pdf_list_name = os.path.split(args.in_pdf_dir)
    pdf_list_name = pdf_list_name[-1] if pdf_list_name[-1] != '' else pdf_list_name[-2]
    # pdf_list_basename = os.path.basename(pdf_list_name).split('_')[0]
    # TODO: Why was it split at '_' before? Corrected now, but check if this is correct.
    pdf_list_basename = os.path.basename(pdf_list_name)
    out_dir = os.path.join(args.output_dir, pdf_list_basename)
    
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    
    # Create the SScraper out directory
    out_xml_dir = os.path.join(out_dir, args.xml_dir)
    if not os.path.exists(out_xml_dir):
        os.makedirs(out_xml_dir)
    
    # Create the directory for saving YOLO detections
    det_dir = os.path.join(out_dir, args.yolo_dir)
    if not os.path.exists(det_dir):
        os.makedirs(det_dir)
    
    # Create the directory for saving per PDF images
    img_dir = os.path.join(out_dir, args.img_dir)
    if not os.path.exists(img_dir):
        os.makedirs(img_dir)
    
    # Create the directory for saving the generated CDXMLs
    cdxml_out_dir = os.path.join(out_dir, args.cdxml_dir)
    if not os.path.exists(cdxml_out_dir):
        os.makedirs(cdxml_out_dir)
    
    # Create the directory for saving the generated SMILES
    smi_out_dir = os.path.join(out_dir, args.smi_dir)
    if not os.path.exists(smi_out_dir):
        os.makedirs(smi_out_dir)

    # Create the directory for saving the generated TSV
    tsv_out_dir = os.path.join(out_dir, args.tsv_dir)
    if not os.path.exists(tsv_out_dir):
        os.makedirs(tsv_out_dir)
    args.tsv_dir = tsv_out_dir

    # Create the visualization directory if required
    viz_out_dir = None
    combined_viz_out_dir = None
    if args.viz_dets:
        viz_out_dir = os.path.join(out_dir, args.viz_dir)
        if not os.path.exists(viz_out_dir):
            os.makedirs(viz_out_dir)
        combined_viz_out_dir = os.path.join(out_dir, "yolo_sscraper_viz")
        if not os.path.exists(combined_viz_out_dir):
            os.makedirs(combined_viz_out_dir)
    args.combined_viz_out_dir = combined_viz_out_dir
    
    lgap_viz_dir = ""
    if args.viz_lgap:
        lgap_viz_dir = os.path.join(out_dir, "lgap_viz")
        if not os.path.exists(lgap_viz_dir):
            os.makedirs(lgap_viz_dir, exist_ok=True)
    args.lgap_viz_dir = lgap_viz_dir

    # Create the directory for storing evaluation metrics
    eval_dir = None
    if args.eval:
        eval_dir = os.path.join(out_dir, args.eval_dir)
        if not os.path.exists(eval_dir):
            os.makedirs(eval_dir)

    return ( out_xml_dir, det_dir, img_dir, cdxml_out_dir, smi_out_dir,
            tsv_out_dir, viz_out_dir, eval_dir, args )

    

##################################
# Running the Pipeline
##################################

def process_pipeline(args):
    # --------- Creating OUTPUT Directories ----------
    ( out_xml_dir, det_dir, img_dir, cdxml_out_dir, smi_out_dir, \
     tsv_out_dir, viz_out_dir, eval_dir, args )  = create_output_dirs( args )
    
 
    ################################################################
    # SymbolScraper & PDF->PNG Image Generation + YOLO
    ################################################################
    # Flags `no_ssc`, `no_yolo`, `no_smiles` skip steps.
    # `parse_only` skips all of the associated steps.
    # CDXML is always generated for parse results.

    if not args.no_ssc and not args.parse_only:
        print('\n [ SYMBOL SCRAPER -- SERVICE PROCESSING PDFs... ]')
        start = time.time()
        pdf_xmls = process_sscraper(args)        
        save_json(pdf_xmls, out_xml_dir)
        print(f'SymScraper took: {time.time()-start} secs')
        dTimer.qcheck("SymbolScraper (PDF instruction extraction)")

    if not args.no_yolo and not args.parse_only:
        # Convert and Save PDF to Images
        print('\n [ PDF PAGE IMAGE GENERATION... ]')
        start = time.time()

        # RZ Aug 2023: Allow image 
        if args.pdf2image:
            # original, slower tool using pdf2image; writing files sequentially is largest delay
            convert2image(args, img_dir) 
        else:
            convert2image_pymupdf(args, img_dir) # RZ modification, Aug 2023
        print(f'Image conversion took: {time.time()-start} secs')
        dTimer.qcheck("PDF -> PNG image conversion")

        # Call YOLO to grab detections
        print('\n [ YOLOv8 MOLECULE DETECTION RUNNING ON PDF PAGE IMAGES... ]')
        start = time.time()        
        proccess_yolov8(img_dir, det_dir)
        #process_yolo(img_dir, det_dir)
        print(f'YOLO detections took: {time.time()-start} secs')
        dTimer.qcheck("YOLO molecule region detector")

    # Visualize YOLO detections
    if args.viz_dets:
        visualize_detection(img_dir=img_dir, out_dir=viz_out_dir, csv_dir=det_dir)
        dTimer.qcheck("YOLO molecule region visualization")

    ################################################################
    # Combine PDF Information with Formula Regions
    ################################################################
    # Combine symbol scraper and yolo detections 
    # AND parse to generate visual graphs
    print('\n [ COMBINING SYMBOL SCRAPER AND YOLO OUTPUTS... ]')
    start = time.time()
    all_pdf_msts, frs, metrics = process_combine(args, out_xml_dir, det_dir, img_dir, 
                                                 vis_pad=args.vis_pad, thresholds=args.thresholds)


    if args.viz_dets:
        new_vis_tsv(img_dir=img_dir, out_dir=args.combined_viz_out_dir, 
                    tsv_dir=args.tsv_dir, suffix='_chem_regions' )

    #print(f'Aligning SS and YOLO *and* parsing to produced visual graphs took: {time.time()-start} secs')

    # RZ: process_combine selects input AND parser; debug timer entries written in the function.
    #dTimer.qcheck("Combining SymbolScraper XML and YOLO regions")

    ################################################################
    # Parse Visual Graphs, Generate CDXML
    ################################################################
    print(f'\n [ CONVERTING VISUAL GRAPHS TO CDXMLS ({cdxml_out_dir})... ]')
    if not args.no_smiles:
        rd2sm = True
    else:
        rd2sm = False

    # SVG gen DEBUG: There are side-effects on graph scales after CDXML conversion
    # Make copy here if needed.
    msts_copy = None
    if args.svg:
        msts_copy = copy.deepcopy(all_pdf_msts)

    start = time.time()
    converter = visual2cdxml(dpr_vgraph_dict=all_pdf_msts, 
            out_dir=cdxml_out_dir, metrics=metrics, write_pages_out=args.cdxml_page)
    metrics = converter.metrics
    dpr_cdxml_dict = converter.dpr_cdxml_dict

    dTimer.qcheck("Converting visual graphs to CDXML (page + doc files: " + str(args.cdxml_page > 0) + ')')


    if not args.no_smiles and not args.parse_only:
        # Generate SMILES from CDXML
        print('\n [ CONVERTING CDXMLs to SMILES... ]')
        start = time.time()
        if not args.rd_smiles:
            gen_smiles(cdxml_dir=cdxml_out_dir, smi_dir=smi_out_dir, png=args.smi_pngs, 
                       metrics=metrics)
            dTimer.qcheck("CDXML -> SMILES translation (molconvert)")
        else:
            gen_smiles(cdxml_dir=cdxml_out_dir, smi_dir=smi_out_dir, png=args.smi_pngs, 
                       metrics=metrics, cdxmls=cdxmls)
            dTimer.qcheck("CDXML -> SMILES translation (RDKit)")
        print(f'  Converting CDXMLs to SMILES took: {time.time()-start} secs')
        

        # Now write the TSV for ECFP Work
        print('* Writing final TSV file for ECFP')
        start = time.time()
        gen_tsv(smi_dir=smi_out_dir, frs=frs, tsv_dir=tsv_out_dir, metrics=metrics)
        print(f'  Writing SMILES TSV took: {time.time()-start} secs')
        print(f'  TSV output directory: {tsv_out_dir}')
        dTimer.qcheck("TSV generation")


    #########################################################
    # Visualize detections, Generate Metrics if Requested
    #########################################################

    # Generate SVG
    if args.svg:
        # Create modified CDXML files -- SEE deepcopy above for msts_copy
        svg_converted = visual2cdxml(dpr_vgraph_dict=msts_copy, out_dir=cdxml_out_dir, svg=True)
        dTimer.qcheck("CDXML generation for molconvert SVG rendering")
        
        # in_dir_name = os.path.basename(args.in_pdf_dir.rstrip('/'))
        # cdxml_path = os.path.join(os.getcwd(), 'outputs', in_dir_name, args.cdxml_dir )
        cdxml_path = os.path.join(os.getcwd(), cdxml_out_dir)
        gen_svg( cdxml_path )
        dTimer.qcheck("molconvert SVG image generation")

    # Generate Levenshtein metrics for SMILES
    if args.eval:
        gen_alt_metrics(pred_smi_dir=smi_out_dir, regions=frs, eval_dir=eval_dir)
        print(f' [ WRITING EVAL METRICS TO: {eval_dir}... ]')
        dTimer.qcheck("SMILE evaluation metric and output")

    ##################################
    # Report times and metrics
    ##################################
    print("\n")
    print(">>----------\n")
    print(dTimer)
    print(">>----------\n")
    print("[ Metrics: ChemScraper pipeline ]")
    pprint(metrics)
    print(">>----------\n")



if __name__ == '__main__':
    args = parse_args()

    #check('args.no_ssc',args.no_ssc)
    #check('args.no_yolo',args.no_yolo)
    #check('args.no_smiles',args.no_smiles)
    #pcheck('args.parse_only',args.parse_only)

    process_pipeline(args)
