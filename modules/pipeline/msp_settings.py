################################################################
# msp_settings.py
#
# MathSeer Pipeline Configuration File
#
# Copyright (c) 2017-2021 Michael Condon, Kenny Davila, Mahshad
#   Mahdavi and Richard Zanibbi
################################################################

import os
import os.path as osp

################################################################
# REGOGNITION PARAMETERS
################################################################

# Recognition control flow
PIPE_SKIP_PREPARSE =   False #True
PIPE_COMBINE_IF_PREPARSE = True
PIPE_SKIP_PARSE = False
PIPE_COMBINE_ONLY = False
PIPE_PARSE_ONLY = False

# LGAP INPUT CREATION PARAMS
CC_OUTPUT_ONLY = False
ALIGN_CC_SYM = True
MIN_FINPUT = 2
MAX_FINPUT = 60

# ScanSSD
SCANSSD_CONF = "0.5"
SCANSSD_STRIDE = "0.75"
SCANSSD_CHEM_CONF = "0.9"
SCANSSD_CHEM_STRIDE = "1.0"

AREA_RATIO_LIM = 0.009
HEIGHT_RATIO_LIM = 0.1
RESIZE_PERCENT = 0.95

# Image processing
CC_GRAY_THRESHOLD=127           # grayscale threshold in [0,255]
INTERSECTION_THRESHOLD = 0.8    # Area coverage threshold, as percentage
INTERSECTION_THRESHOLD_THIN = 0.4    # Area coverage threshold, as percentage

# YOLO
YOLO_CONF = "0.2"
YOLO_IMG_SIZE = "896"
YOLO_NMS_IOU = "0.01"

################################################################
# DIRECTORY TEMPLATES AND FILES
################################################################

# Key System Directories
ROOT_DIR = osp.dirname( osp.dirname(osp.dirname(osp.abspath(__file__))) )
SRC_DIR = ROOT_DIR
TEST_INPUTS_DIR = osp.join(ROOT_DIR, "tests", "test_files")

SCANSSD_DIR = osp.join(ROOT_DIR, "modules/scanssd/src")
SCANSSD_DETECTION_DIR = osp.abspath(osp.join("%s", "scanssd"))
LGAP_DIR = osp.join(ROOT_DIR, "modules/lgap-parser")
SYMBOLS_DIR = osp.abspath(osp.join("%s", "sscraper"))
SSCRAPER_DIR = osp.join(ROOT_DIR, "symbolscraper-server")
YOLO_DIR = osp.join(ROOT_DIR, "modules/scaled-yolov4")
YOLO_DETECTION_DIR = osp.abspath(osp.join("%s", "scanssd", f"conf_{YOLO_CONF}"))

# Translation Map Files
LABELS_TRANS_CSV = os.path.abspath( osp.join(ROOT_DIR, "modules", "lgeval",
    "translate", "infty_to_crohme.csv") )

MATHML_MAP_DIR = os.path.abspath( osp.join(ROOT_DIR, "modules", "lgeval",
    "translate", "mathMLMap.csv") )

# Parse input/output directories
PARSE_OUT = "lgap-parser"
DETECT_EXP_IMG_OUT_DIR = osp.abspath(osp.join("%s", PARSE_OUT, "input", "png"))
PARSE_LG_INPUT_DIR = osp.abspath(osp.join("%s", PARSE_OUT, "input" ))

# New TSV inputs/outputs2
PARSE_TSV_INPUT_DIR = os.path.abspath( os.path.join("%s", PARSE_OUT, "input" ))
##################################
PARSE_TSV_OUT_DIR = osp.abspath(osp.join("%s", PARSE_OUT, "output" ))

# Visualization Directories
VDIR = "view"
HTML_OUT_DIR = osp.abspath(osp.join("%s", VDIR))
SCANSSD_VISUAL_DIR = osp.abspath(osp.join("%s", VDIR, "scanssd"))
YOLO_VISUAL_DIR = osp.abspath(osp.join("%s", VDIR, "scanssd"))
TSV_IN_VIS_DIR = osp.abspath(osp.join("%s", VDIR, "lgap-parser", "input"))
PARSE_DOT_OUT_DIR = osp.join("%s", VDIR, "lgap-parser", "output")
DETECT_EXP_SYM_IMG_OUT_DIR = osp.abspath(
    osp.join("%s", VDIR, "tsv")
)
SSCR_VIS_DIR = osp.abspath(osp.join("%s", VDIR, "sscraper"))



