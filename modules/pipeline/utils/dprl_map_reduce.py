###########################################################################
#
# dprl_map_reduce.py
#   - Minor decoration of the mr4mp parallel map reduce library
#
###########################################################################
# Created by: Richard Zanibbi, Aug 07 2023 11:17:53
###########################################################################

import multiprocessing as mp
import mr4mp 
import copy
from tqdm import tqdm

#from functool import partial, reduce
#from parts import parts


##################################
# Test functions
##################################

def combine_lists ( l1, l2 ):
    return l1 + l2

def combine_int_lists ( l1, l2 ):
    return sum([ l1, l2 ], [])

def map_int_list( in_list ):
    return [[ item + 1 for item in in_list ]]

def map_fn( in_dict ):
    # Return new dict created using references, avoid side effects
    mapped_dict = dict()

    # Simple increment
    for key in in_dict:
        mapped_dict[ key ] = in_dict[ key ] + 1

    return mapped_dict

##################################
# Dictionary reduce operation
##################################
def combine_dict_pair( d, e ):
    # NOTE: Update is an in-place operation returning None
    merged_dict = dict()
    merged_dict.update(d)
    merged_dict.update(e)

    return merged_dict

def combine_dict_tuple( d, e ):
    # NOTE: Merge first and second elements in dictionary *pairs*
    ( d1, d2 ) = d
    ( d3, d4 ) = e

    merged_dict_1 = dict()
    merged_dict_1.update( d1 )
    merged_dict_1.update( d3 )

    merged_dict_2 = dict() 
    merged_dict_2.update( d2 )
    merged_dict_2.update( d4 )

    return ( merged_dict_1, merged_dict_2 )

################################################################
# Main operations
################################################################
def map_concat( map_fn, in_list ):
    # Create process pool in map-reduce framework, number of processes in size
    # NOTE: map_fn MUST return a concatenatable (e.g., list) output.
    # Returns: Concatenation of map_fn outputs.
    p = mr4mp.pool( processes=mp.cpu_count() )

    # Progress bar if asked.
    r = p.mapconcat( map_fn, in_list )
    p.close()

    return r

def map_reduce( map_fn, reduce_fn, in_list ):
    # Create process pool in map-reduce framework
    # Match number of available physical processors
    p = mr4mp.pool( processes=mp.cpu_count() )
    r = p.mapreduce( map_fn, reduce_fn, in_list )
    p.close() 

    return r


# DEBUG: Invocation MUST be protected inside a 'main' block to avoid
# loading the multiprocessing library without side effects/state issues.
if __name__ == '__main__' :
    print( map_reduce( map_int_list, combine_int_lists, [ [1,2,3], [4,5,6] ] ) )
    print( map_reduce( map_int_list, combine_int_lists, [ [1,2,3], [2,3,4], [3,4,5],
                                                     [4,5,6], [5,6,7], [6,7,8],
                                                     [7,8,9], [8,9,10] ]))
    print( map_concat( map_int_list, [ [1,2,3], [2,3,4], [3,4,5],
                                         [4,5,6], [5,6,7], [6,7,8],
                                         [7,8,9], [8,9,10] ]))
    print( map_reduce( map_fn, combine_dict_pair, [ { 1: 2, 3: 4}, { 5:6, 7:8 } ] ) )
    # print( map_reduce( map_fn, combine_dict_tuple, [ { 1: 2, 3: 4}, { 5:6, 7:8 } ] ) )

