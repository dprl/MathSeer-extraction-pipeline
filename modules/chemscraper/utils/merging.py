# Forgive these imports, for some reason using sp.X was causing issues.
from shapely.strtree import STRtree
from shapely.geometry import box as Box  # Debugging
from shapely.ops import unary_union
import numpy as np


from modules.protables.debug_fns import *

def merge_page_boxes( id_coord_tuple ):
    (page_id, coord_tuple_list) = id_coord_tuple
    if len(coord_tuple_list) == 1:
        return coord_tuple_list

    # Wrapper to take lists of coordinate tuples
    coord_array = np.array( coord_tuple_list )
    (merges, box_dict) = merge_boxes( coord_array )
    
    all_boxes = box_dict.values()
    coord_list = [ tuple(map(int, box.bounds)) for box in all_boxes ]

    if merges > 0:
        check('>>> !! YOLO overlaps (page, #remaining, #removed)', (page_id + 1, len(box_dict), merges))
        #check('(input) coord_array\n', coord_array)
        #check('box_dict\n', box_dict)
        #check('cood_list\n', coord_list)

    return coord_list

def merge_boxes( np_coord_array ):
    # DEBUG: Handle case of no detections
    if len( np_coord_array ) < 2:    # < 2 rows
        return (0, {})   # empty 0 merges, empty dictionary

    boxes = list( map(Box, *np_coord_array.T) )

    # Two dictionaries (roughly inverse, between integer ids and objects/obj refs)
    box_dict = dict(enumerate(boxes))
    index_by_id = dict((id(b), i) for (i, b) in enumerate(boxes))

    # Query to identify intersections for each box
    intersect_dict = {}     # to record intersected ids

    # CHANGES for verion 1.8.5 (older, non-hashable objects)
    search_tree = STRtree(boxes)
    for (i, b) in enumerate(boxes):
        intersected_ids = [ index_by_id[ id(b) ] for b in search_tree.query(b)]
        if len(intersected_ids) > 1:
            intersect_dict[i] = set( intersected_ids )
        
    # Agglomerate intersections (transitive closure)
    intersected = set()
    for box_id in intersect_dict:
        intersected.add( box_id )
        
        next_set = intersect_dict[box_id]
        if len(next_set) > 1:
            for merge_id in next_set:
                if not merge_id == box_id:
                    intersect_dict[merge_id] = intersect_dict[merge_id].union( next_set )

    # Merge polygons
    merges = 0
    while len(intersected) > 0:
        # Store merged box in first id associated with merged region
        next_id = intersected.pop()
        next_set = intersect_dict[next_id]
        min_id = min(next_set)

        intersected = intersected - next_set  # remove merged ids from set

        # Merge boxes (polygons), store at popped id.
        polygons = [ box_dict[i] for i in next_set ]
        polygon_union = unary_union( polygons )
        merged_bb_tuple = polygon_union.bounds
        merged_bb = Box( *merged_bb_tuple )

        # Store merged box in location for first box given (which is spatially sorted)
        # Remove duplicate entries for overlapping boxes in output dict
        box_dict[min_id] = merged_bb
        for i in next_set - { min_id }:
            del(box_dict[i])
        
        merges += len(next_set) - 1  # add number of removed/merged boxes

    return (merges, box_dict)


if __name__ == '__main__':
    
    # numpy array IS an iterable in python
    # use to define bounding boxes compactly
    # by mapping box coordinates as box() arguments
    # - * REQUIRED to pass column of arguments to box()
    a = np.array([[0,0,1,1], [1.1,1.1,2,2], [0.5, 0.5, 1.5, 1.5], [3,3,5,5]])
    print( 'THE input coordinates:\n', a )
    print( '(merged boxes, final_boxes):\n', merge_boxes(a) )
