import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
import cv2
import os

def imshow(image, title=None, ncols=2, figsize=(15, 10), cmap='gray'): 
    '''
    Visualize an image or a set of images using subplot

    Parameters
    ----------
    image : numpy.ndarray or list of numpy.ndarray
             Image array or the collection of image arrays to be displayed
    title : str ot list of str, optional, default: None
            The title or collection of titles corresponding to the images
    ncols: int, optional, default: 2
           Number of columns for subplot
    figsize: (float, float), optional, default: (12, 15)
             width, height in inches for single image

    Returns
    -------
    None
    '''

    if not isinstance(image, list):
        plt.figure(figsize=figsize)
        plt.imshow(image, cmap=cmap)
        if title:
            plt.title(title, fontsize=30)
    else:
        plt.figure(figsize=figsize)
        # calculate the number of rows
        nrows = int(np.ceil((len(image)) / ncols))
        for i, img in enumerate(image):
            plt.subplot(nrows, ncols, i+1)
            plt.imshow(img, cmap=cmap)
            if title:
                plt.title(title[i], fontsize=50)
    plt.show(block=False)
    plt.pause(0.001)

def viz_translated_cd(pp_msts=None, cd_page_ids=None, cd_graph=None):
    if cd_graph == None:
        t_id = 2
        p_id = 1

        cd_page_ids = np.array(cd_page_ids)
        p_ids = np.where(cd_page_ids == t_id)[0]
        pp_msts = np.array(pp_msts, dtype=object)
        page_pp_msts = pp_msts[p_ids]
        cd_graph = page_pp_msts[p_id]
        act_idx = list(pp_msts).index(cd_graph)
        print(f'Act idx: {act_idx}')
        # exit(0)

        # print(cd_graph.edges(data=True))
        # exit(0)
    # cd_graph = nx.to_undirected(cd_graph)
    pos = nx.get_node_attributes(cd_graph, 'pos')
    fig = plt.figure()
    ax = fig.add_subplot(111)
    edge_labels = {}
    for edge in cd_graph.edges(data=True):
        # if edge[2]['type'] == 'Double':
        edge_labels[(edge[0], edge[1])] = edge[2]['type']
    nx.draw(cd_graph, pos, edge_color='blue', width=3, linewidths=3,
            node_size=500, node_color='pink', alpha=0.9, font_size=15,
            labels={node: node for node in cd_graph.nodes()}, ax=ax)
    nx.draw_networkx_edge_labels(cd_graph, pos, edge_labels=edge_labels, ax=ax)
    # ax.tick_params(left=True, bottom=True, labelleft=True, labelbottom=True)
    ax.set_ylim(ax.get_ylim()[::-1])
    plt.axis('on')
    plt.xticks()
    plt.yticks()
    plt.show()
    plt.close()


    ## Function to visualize the merges for MST graphs overlaid on the molecule
def view_intermediate_overlay(cd_graph, fr, p, img_dir):

    img = cv2.imread(os.path.join(img_dir, str(p+1)+'.png'),1)

    min_x, min_y, max_x, max_y = fr
    pad = 15
    mol_img = img[min_y-pad:max_y+pad, min_x-pad:max_x+pad]
    scale = 3
    width = int(mol_img.shape[1] * scale)
    height = int(mol_img.shape[0] * scale)
    dim = (width, height)
    mol_img = cv2.resize(mol_img, dim, interpolation=cv2.INTER_AREA)
    x_offset = min_x - pad
    y_offset = min_y - pad

    fig, ax1 = plt.subplots(1,1)
    ax1.imshow(mol_img)

    opacity = {'line': 0.6, 'centroid': 0.5}
    colors = {'general': '#ff0000', 'curve': '#fffb14', 'end_pt': '#0000ff', 
                'end_pt_leaf': '#00ff00'}
    # Draw the Merges for Polygon MST
    draw_merge_overlay(cd_graph, x_offset, y_offset, scale, ax1, colors, opacity)

    ax1.title.set_text('Determined Visual Graph Nodes from Translation')
    plt.axis('tight')
    # plt.axis('off')
    plt.show()
    plt.close()


def draw_merge_overlay(mst, x_off, y_off, scale, ax, colors, opacity):
    visited_nodes = []
    for edge in mst.edges(data=True):
        pt_e1 = list(mst.nodes[edge[0]]['pos'])
        pt_e2 = list(mst.nodes[edge[1]]['pos'])
        # Check whether any of the node is a leaf node and set color for each node
        if mst.degree[edge[0]] == 1:
            colr1 = colors['end_pt_leaf']
        else:
            colr1 = colors['end_pt']
        if mst.degree[edge[1]] == 1:
            colr2 = colors['end_pt_leaf']
        else:
            colr2 = colors['end_pt']
        # Scale the points and adjust with the offset for the molecule excerpt
        # from the original image
        pt_e1[0] = (pt_e1[0]-x_off)*scale
        pt_e1[1] = (pt_e1[1]-y_off)*scale
        pt_e2[0] = (pt_e2[0]-x_off)*scale 
        pt_e2[1] = (pt_e2[1]-y_off)*scale
        x = [pt_e1[0], pt_e2[0]]
        y = [pt_e1[1], pt_e2[1]]
        x1, y1 = [pt_e1[0]], [pt_e1[1]]
        x2, y2 = [pt_e2[0]], [pt_e2[1]]

        # Select line color based on node types
        if edge[0][:2] != 'C_' or \
            edge[1][:2] != 'C_':
            color = colors['curve']
        else:
            color = colors['general']
        # Select line thickness based on node types
        if edge[2]['type'] == 'Double':
            ln_width = 6
        else:
            ln_width = 2
        ax.plot(x,y, '-', color=color, linewidth=ln_width, alpha=opacity['line'])
        # Draw the blobs if not already visited
        if edge[0] not in visited_nodes:
            ax.plot(x1,y1, '.', color=colr1, markersize=25, alpha=opacity['centroid'])
            visited_nodes.append(edge[0])
        if edge[1] not in visited_nodes:
            ax.plot(x2,y2, '.', color=colr2, markersize=25, alpha=opacity['centroid'])
            visited_nodes.append(edge[1])
