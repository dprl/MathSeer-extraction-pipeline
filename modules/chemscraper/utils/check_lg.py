import os
import cv2
import matplotlib.pyplot as plt

def check_lg(lg_path, img):
    node_dict = {}
    node_loc = {}
    with open(lg_path, 'r') as f:
        for line in f:
            if line[0] == 'O':
                prim_id = int(line.split(',')[-1].strip())
                sym_id = line.split(',')[1].strip()
                node_dict[prim_id] = sym_id
            
            elif '#cc' in line:
                prim_id = int(line.split(',')[1].strip())
                x_min = int(line.split(',')[3].strip())
                y_min = int(line.split(',')[2].strip())
                x_max = int(line.split(',')[5].strip())
                y_max = int(line.split(',')[4].strip())
                node_loc[node_dict[prim_id]] = [x_min, y_min, x_max, y_max]
    f.close()
    for node in node_loc.keys():
        n_box = node_loc[node]
        if node[0] != 'L':
            txt = node.split('_')[0]
        else:
            txt = node
        img = cv2.rectangle(img, (n_box[0], n_box[1]), (n_box[2], n_box[3]),
                                (0,255,0), 2)
        img = cv2.putText(img, txt, (n_box[0],n_box[1]-10),
                                  cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255,0,0), 2)
    
    plt.imshow(img)
    plt.show()

def check_pdf_boxes(c_graphics, c_boxes):
    img = cv2.imread('data/images_300dpi/or100.09.tables/5.png', 1)
    for idx, c_box in enumerate(c_boxes):
        img = cv2.rectangle(img, (c_box[0], c_box[1]), (c_box[2], c_box[3]),
                                (0,255,0), 2)
        img = cv2.putText(img, c_graphics[idx], (c_box[0],c_box[1]-10),
                                  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255,0,0), 1)
    
    plt.imshow(img)
    plt.show()


def main():
    lg_dir = 'ini_qdgga_data/test/gt_lg'
    lg_file = 'Pg_5_Mol_11.lg'
    lg_path = os.path.join(lg_dir, lg_file)
    img_dir = 'ini_qdgga_data/test/images'
    img_path = os.path.join(img_dir, lg_file.split('.')[0]+'.png')
    img = cv2.imread(img_path, 1)
    check_lg(lg_path, img)


if __name__=='__main__':
    main()