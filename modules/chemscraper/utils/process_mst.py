# RZ
from debug_fns import *

import numpy as np
import networkx as nx
from modules.chemscraper.utils.graph_utils import sorted_order
from modules.chemscraper.utils.polygon_ops import PostProcess_Ops
from shapely.geometry import Polygon, Point, LineString
import copy
import math

def refactor_edges(full_gs):
    pp_gs = []
    for g in full_gs:
        order, _, _ = sorted_order(g)
        # Check if the extreme left graphics is not a line (i.e. switch edges)
        nodes = sorted(g.nodes())
        node1 = nodes[order[0]]
        if not node1.split('_')[0] == 'Ln':
            atom_cluster = []
            first_line = ''
            for o in order:
                # Keep storing the nodes until a line is reached 
                # (to find the atom cluster)
                n = nodes[o]
                if n.split('_')[0] != 'Ln':
                    atom_cluster.append(n)
                else:
                    first_line = n
                    break
            # Check if the last element is connected to a line (No Error)
            err = True
            right_node = atom_cluster[-1]
            r_edges = g.edges(right_node)
            for ed in r_edges:
                if ed[1].split('_')[0] == 'Ln':
                    err = False
                    break
            
            if err:
                # If no connection to a line update edges to connect to a line
                # Remove the line connection to the first node
                rmv_edges = [(atom_cluster[0], first_line), (first_line, atom_cluster[0])]
                add_edges = [(atom_cluster[-1], first_line), (first_line, atom_cluster[-1])]
                g.remove_edges_from(rmv_edges)
                g.add_edges_from(add_edges)
        pp_gs.append(g)
    return pp_gs


# Connect the closest nodes 
def post_process_mst(ss_poly_msts, in_g_poly, frs, mode='debug'):

    if mode == 'debug':
        pp_msts = []

        for idx, og_mst in enumerate(ss_poly_msts):
            og_mst_copy = copy.deepcopy(og_mst)
            in_og = in_g_poly[idx]
            # Create a list of "close" edges from the FC graph
            added_nodes = []
            for edge in in_og.edges(data=True):
                # Check if the edge already in MST, if not and close, add it
                if not og_mst.has_edge(edge[0],edge[1]) and edge[2]['weight'] <= 5.0:
                    og_mst_copy.add_edge(edge[0], edge[1], weight=edge[2]['weight'])
                    # added_nodes.extend((edge[0], edge[1]))
            pp_msts.append(og_mst_copy)
        visual_graphs = create_visual_graph(pp_msts, in_g_poly)

    # Pipeline mode
    else: 
        # RZ: pp_msts is another dictionary, with page as keys, list of MSTs as values
        pp_msts = {}
        for p in ss_poly_msts.keys():
            page_msts = ss_poly_msts[p]
            page_in_og = in_g_poly[p]
            page_new_msts = []
            for idx, mst in enumerate(page_msts):
                og_mst_copy = copy.deepcopy(mst)
                in_og = page_in_og[idx]

                # Create a list of "close" edges from the FC graph
                added_nodes = []
                for edge in in_og.edges(data=True):
                    # Check if the edge already in MST, if not, close and add it
                    if not og_mst_copy.has_edge(edge[0],edge[1]) and edge[2]['weight'] <= 6.0:
                        og_mst_copy.add_edge(edge[0], edge[1], weight=edge[2]['weight'])
                # Convert the MST to a directed graph and find Adj Matrix
                og_mst_copy_copy = copy.deepcopy(og_mst_copy)
                directed_mst = og_mst_copy.to_directed()
                adj_matrix = find_adj(og_mst_copy, p, idx)
                
                # try:
                with_hw_mst = find_hws(og_mst_copy_copy, adj_matrix, p, idx)
                # except IndexError:
                #     print(p, idx)
                #     print(mst.nodes())
                #     exit(0)
                page_new_msts.append([with_hw_mst, directed_mst, adj_matrix])
            pp_msts[p] = page_new_msts

    # RZ: Create visual graphs
    if mode == 'debug':
        visual_graphs = create_visual_graph(pp_msts, in_g_poly)

    # RZ: Pipeline
    else:
        visual_graphs = pipeline_create_visual_graph(pp_msts, in_g_poly, frs)

    return visual_graphs


def find_adj(mst, p, idx):
    line_signatures = ['Ln', 'Wave', 'Circle', 'SW', 'HW']
    # Find all the vertices of all the nodes
    node_vertices = {}
    for node in mst.nodes():
        # try:
        poly = mst.nodes[node]['poly']
        # except KeyError:
        #     print(mst.nodes(data=True))
        #     print(p, idx)
        #     exit(0)
        if node.split('_')[0] in line_signatures:
            char = False
        else:
            char = True
        
        
        min_rect = poly.minimum_rotated_rectangle
        # NOTE: Again, SScraper issue likely
        if isinstance(min_rect, LineString):
            x,y = min_rect.coords.xy
            node_vertices[node] = [[x[0],y[0]],[x[1],y[1]]]
        else:
            node_vertices[node] = find_vertices(min_rect, char=char)
        

    # Find adj list of all matrices    
    adj_list = {node:[[],[]] for node in mst.nodes()}
    for node in mst.nodes():
        node_vert1 = Point(node_vertices[node][0])
        node_vert2 = Point(node_vertices[node][1])
        nei_iter = mst.neighbors(node)
        for nei in nei_iter:
            nei_vert = Point(node_vertices[nei][0])
            vert1_dist = node_vert1.distance(nei_vert)
            vert2_dist = node_vert2.distance(nei_vert)
            if vert1_dist < vert2_dist:
                append_id = 0  # append to connecting to the left
            else:
                append_id = 1  # append to connecting to the right
            adj_list[node][append_id].append(nei)
    return adj_list



'''
From the minimum rectangle, find the vertices of the largest side,
Order the vertices top-down or left-right
'''
def find_vertices(shape, shape_obj):
    min_rect = shape_obj.minimum_rotated_rectangle
    if min_rect.geom_type == "LineString":
        shape = "linestring"
        shape_obj = min_rect

    if shape == "linestring":
        # Linestring
        linestring = shape_obj
        if min_rect.area != 0:
            print('polygon/linestring mismatch')
            # print(shape, shape_obj, min_rect, sep='\n')
            # sys.exit()
        # assert(min_rect.area == 0)
        coords = np.array(linestring.coords)
        # Sort left-right top-down
        coords = coords[np.lexsort((coords[:, 1], coords[:, 0]))]
        
        # TODO: maybe numpy array is better than list
        final_verts = coords[[0, -1]].tolist()
    elif (shape == "polygon"):
        # Polygon
        # TODO: Use numpy to improve this snippet
        x,y = min_rect.exterior.coords.xy
        edges = (Point(x[0], y[0]).distance(Point(x[1], y[1])), Point(x[1], y[1]).distance(Point(x[2], y[2])))
        max_edge_idx = edges.index(max(edges))
        vertices = [[x[max_edge_idx], y[max_edge_idx]],[x[max_edge_idx+1], y[max_edge_idx+1]]]
        found_x = False
        if vertices[0][0] < vertices[1][0]:
            final_verts = [vertices[0], vertices[1]]
            found_x = True
        elif vertices[0][0] > vertices[1][0]:
            final_verts = [vertices[1], vertices[0]]
            found_x = True
        
        if not found_x:
            if vertices[0][1] > vertices[1][1]:
                final_verts = [vertices[0], vertices[1]]
            elif vertices[0][1] < vertices[1][1]:
                final_verts = [vertices[1], vertices[0]]
    else:
        assert(False);

    return final_verts


def find_hws(mst, adj_mat, p, idx):
    all_nodes = list(mst.nodes())
    visited_start_nodes = []
    cur_lengths = []
    cur_nodes = []
    cur_verts = []
    cur_angles = []
    cur_order = 0 # 0: increasing; 1 decreasing
    found_hws = []

    ang_tol = 5 # Angle tolerance for parallel check
    len_thresh = 0.8 # Threshold for wedge lines to be increasing or decreasing
    
    cur_node = all_nodes[0]
    while len(visited_start_nodes) < len(all_nodes):
        
        if not len(cur_nodes):
            visited_start_nodes.append(cur_node)
        
        cur_node_name = cur_node.split('_')[0]
        if cur_node_name != 'Ln':
            cur_nodes = []
            cur_lengths = []
            cur_verts = []
            cur_angles = []
            nxt_found = False
            for node in all_nodes:
                if node not in visited_start_nodes:
                    cur_node = node
                    nxt_found = True
                    break
            if not nxt_found:
                break
        else:
            # if p == 2 and idx == 15:
            #     print(cur_nodes)
            #     print(cur_node)
            cur_node_poly = mst.nodes[cur_node]['poly']
            min_rect = cur_node_poly.minimum_rotated_rectangle
            cur_node_verts = find_vertices(min_rect, char=False)
            # Slope = y2-y1/x2-x1
            try:
                slope = (cur_node_verts[1][1] - cur_node_verts[0][1])/(
                            cur_node_verts[1][0] - cur_node_verts[0][0])
            except ZeroDivisionError:
                slope = (cur_node_verts[1][1] - cur_node_verts[0][1])/((
                            cur_node_verts[1][0] - cur_node_verts[0][0])+1)

            cur_angle = math.degrees(math.atan(slope))
            cur_length = Point(cur_node_verts[0]).distance(Point(cur_node_verts[1]))
            if not len(cur_nodes):
                cur_nodes.append(cur_node)
                cur_lengths.append(cur_length)
                cur_verts.append(cur_node_verts)
                cur_angles.append(cur_angle)

                # Find the next parallel increasing/decreasing line
                cur_node_neis = []
                frm_adj_mat = adj_mat[cur_node]
                for nei_block in frm_adj_mat:
                    cur_node_neis.extend(nei_block)
                
                # if p == 2 and idx == 15:
                #     print(cur_node_neis)
                found_nei = False
                for nei in cur_node_neis:
                    if nei.split('_')[0] == 'Ln':
                        nei_poly = mst.nodes[nei]['poly']
                        nei_min_rect = nei_poly.minimum_rotated_rectangle
                        nei_verts = find_vertices(nei_min_rect, char=False)
                        try:
                            nei_slope = (nei_verts[1][1] - nei_verts[0][1])/(
                                        nei_verts[1][0] - nei_verts[0][0])
                        except ZeroDivisionError:
                            nei_slope = (nei_verts[1][1] - nei_verts[0][1])/((
                                    nei_verts[1][0] - nei_verts[0][0])+1)
                        
                        nei_angle = math.degrees(math.atan(nei_slope))
                        nei_length = Point(nei_verts[0]).distance(Point(nei_verts[1]))
                        # if p == 2 and idx == 15:
                        #     print(f'Nei: {nei}')
                        #     print(f'Cur Slope: {cur_angles[0]}')
                        #     print(f'Nei Slope: {nei_slope}')
                        #     print(f'Cur len: {cur_lengths[0]}')
                        #     print(f'Nei len: {nei_length}')
                        len_ratio = min((nei_length/cur_lengths[0]), (cur_lengths[0]/nei_length))
                        if abs(nei_angle - cur_angles[0]) < ang_tol:  # Parallel
                            if len_ratio < len_thresh:  # Not equal length
                                found_nei = True
                                cur_node = nei
                                break
                if not found_nei:
                    cur_nodes = []
                    cur_lengths = []
                    cur_verts = []
                    cur_angles = []
                    nxt_found = False
                    for node in all_nodes:
                        if node not in visited_start_nodes:
                            cur_node = node
                            nxt_found = True
                            break
                    if not nxt_found:
                        break
            else:
                if len(cur_nodes) == 1: # Only 1 previous node added
                    prev_angle = cur_angles[0]
                    if abs(prev_angle - cur_angle) < ang_tol: # Slope equal (parallel)
                        prev_length = cur_lengths[0]
                        len_ratio = min((prev_length/cur_length), (cur_length/prev_length))
                        if len_ratio < len_thresh:
                            cur_nodes.append(cur_node)
                            cur_lengths.append(Point(cur_node_verts[0]).distance(Point(cur_node_verts[1])))
                            cur_verts.append(cur_node_verts)
                            cur_angles.append(cur_angle)
                            if prev_length > cur_length:
                                cur_order = 1 # Decreasing order
                            else:
                                cur_order = 0 # Increasing order
                            
                            # Find next node
                            cur_node_neis = []
                            frm_adj_mat = adj_mat[cur_node]
                            for nei_block in frm_adj_mat:
                                cur_node_neis.extend(nei_block)
                            
                            found_nei = False
                            for nei in cur_node_neis:
                                if nei.split('_')[0] == 'Ln' and nei not in cur_nodes:
                                    nei_poly = mst.nodes[nei]['poly']
                                    nei_min_rect = nei_poly.minimum_rotated_rectangle
                                    nei_verts = find_vertices(nei_min_rect, char=False)
                                    try:    
                                        nei_slope = (nei_verts[1][1] - nei_verts[0][1])/(
                                                    nei_verts[1][0] - nei_verts[0][0])
                                    except ZeroDivisionError:
                                        nei_slope = (nei_verts[1][1] - nei_verts[0][1])/((
                                                    nei_verts[1][0] - nei_verts[0][0])+1)
                                    nei_angle = math.degrees(math.atan(nei_slope))
                                    nei_length = Point(nei_verts[0]).distance(Point(nei_verts[1]))
                                    len_ratio = min((nei_length/cur_lengths[-1]), (cur_lengths[-1]/nei_length))
                                    if abs(nei_angle - cur_angles[-1]) < ang_tol: # Parallel
                                        if len_ratio < len_thresh: # Not equal length
                                            if cur_order == 0:
                                                if nei_length > cur_lengths[-1]:
                                                    found_nei = True
                                                    cur_node = nei
                                                    break
                                            else:
                                                if nei_length < cur_lengths[-1]:
                                                    found_nei = True
                                                    cur_node = nei
                                                    break
                            if not found_nei:
                                cur_nodes = []
                                cur_lengths = []
                                cur_verts = []
                                cur_angles = []
                                nxt_found = False
                                for node in all_nodes:
                                    if node not in visited_start_nodes:
                                        cur_node = node
                                        nxt_found = True
                                        break
                                if not nxt_found:
                                    break

                        else: # Equal length - Clean up data structures
                            cur_nodes = []
                            cur_lengths = []
                            cur_verts = []
                            cur_angles = []
                            nxt_found = False
                            for node in all_nodes:
                                if node not in visited_start_nodes:
                                    cur_node = node
                                    nxt_found = True
                                    break
                            if not nxt_found:
                                break
                    else: # Not parallel - Clean up data structures
                        cur_nodes = []
                        cur_lengths = []
                        cur_verts = []
                        cur_angles = []
                        nxt_found = False
                        for node in all_nodes:
                            if node not in visited_start_nodes:
                                cur_node = node
                                nxt_found = True
                                break
                        if not nxt_found:
                            break
                elif len(cur_nodes) >= 2:
                    cur_nodes.append(cur_node)
                    cur_lengths.append(Point(cur_node_verts[0]).distance(Point(cur_node_verts[1])))
                    cur_verts.append(cur_node_verts)
                    cur_angles.append(cur_angle)

                    # Find if any more neighs form a part of the wedge
                    cur_node_neis = []
                    frm_adj_mat = adj_mat[cur_node]
                    for nei_block in frm_adj_mat:
                        cur_node_neis.extend(nei_block)
                    
                    found_nei = False
                    for nei in cur_node_neis:
                        if nei.split('_')[0] == 'Ln' and nei not in cur_nodes:
                            nei_poly = mst.nodes[nei]['poly']
                            nei_min_rect = nei_poly.minimum_rotated_rectangle
                            nei_verts = find_vertices(nei_min_rect, char=False)
                            try:
                                nei_slope = (nei_verts[1][1] - nei_verts[0][1])/(
                                        nei_verts[1][0] - nei_verts[0][0])
                            except ZeroDivisionError:
                                nei_slope = (nei_verts[1][1] - nei_verts[0][1])/((
                                        nei_verts[1][0] - nei_verts[0][0])+1)
                            nei_angle = math.degrees(math.atan(nei_slope))
                            nei_length = Point(nei_verts[0]).distance(Point(nei_verts[1]))
                            len_ratio = min((nei_length/cur_lengths[-1]), (cur_lengths[-1]/nei_length))
                            if abs(nei_slope - cur_angles[-1]): # Parallel
                                if len_ratio < len_thresh: # Not equal length
                                    if cur_order == 0:
                                        if nei_length > cur_lengths[-1]:
                                            found_nei = True
                                            cur_node = nei
                                            break
                                    else:
                                        if nei_length < cur_lengths[-1]:
                                            found_nei = True
                                            cur_node = nei
                                            break
                    if not found_nei and len(cur_nodes) > 2: # Hash only if at least 3 lines
                        
                        hw_dict = {}
                        hw_dict['nodes'] = cur_nodes
                        hw_dict['lengths'] = cur_lengths
                        hw_dict['verts'] = cur_verts
                        hw_dict['order'] = cur_order
                        if not len(found_hws):
                            found_hws.append(hw_dict)
                        else: # Remove duplicates, take the largest one
                            for idx_hw, hw in enumerate(found_hws):
                                if len(set(hw['nodes']).intersection(set(cur_nodes))) >= 1:
                                    if len(hw['nodes']) < len(cur_nodes):
                                        found_hws.pop(idx_hw)
                                        found_hws.append(hw_dict)
                                        break

                        cur_nodes = []
                        cur_lengths = []
                        cur_verts = []
                        cur_angles = []
                        nxt_found = False
                        for node in all_nodes:
                            if node not in visited_start_nodes:
                                cur_node = node
                                nxt_found = True
                                break
                        if not nxt_found:
                            break
            
    # if p == 2 and idx == 15:
        # print(mst.nodes())
        # print(mst.edges())
        # print(found_hws)
        # print(p,idx)
        # print('Act End')
        # exit(0)
    # If HWedges found insert, delete as required
    if len(found_hws):
        for idx_hw, hw_dict in enumerate(found_hws):
            order = hw_dict['order']
            nodes = hw_dict['nodes']
            verts = hw_dict['verts']
            # Find the correct neighbor(s) to attach to
            if order == 0:
                touch_node = nodes[0] # Narrow end
                l_touch_node = nodes[-1] # Wide end
                small_vert1 = verts[0][0]
                small_vert2 = verts[0][1]
                large_vert1 = verts[-1][0]
                large_vert2 = verts[-1][1]
            else:
                touch_node = nodes[-1] # Narrow end
                l_touch_node = nodes[0] # Wide end
                small_vert1 = verts[-1][0]
                small_vert2 = verts[-1][1]
                large_vert1 = verts[0][0]
                large_vert2 = verts[0][1]

            l_nei = False
            for idx_node, node in enumerate(nodes):
                if node == touch_node:
                    # Remove all nodes 
                    touch_node_neis = []
                    nei_block = adj_mat[node]
                    for neis in nei_block:
                        for nei in neis:
                            if nei not in nodes:
                                touch_node_neis.append(nei)
                    # Find the closest neighbor touch point and build touch verts
                    try:
                        touch_nei = touch_node_neis[0]
                    except:
                        print(f'Neighbor for narrow end not found')
                        print(nodes)
                        # print(mst.nodes())
                        print(found_hws)
                        print(p, idx)
                        exit(0)
                    nei_poly = mst.nodes[touch_nei]['poly']
                    nei_min_rect = nei_poly.minimum_rotated_rectangle
                    nei_verts = find_vertices(nei_min_rect, char=False)
                    touch_nd_vert = Point(verts[idx_node][0])
                    nei_vert1, nei_vert2 = Point(nei_verts[0]), Point(nei_verts[1])
                    dist1 = touch_nd_vert.distance(nei_vert1)
                    dist2 = touch_nd_vert.distance(nei_vert2)
                    if dist1 < dist2:
                        act_small_vert = nei_verts[0]
                    else:
                        act_small_vert = nei_verts[1]
                    act_s_vert1 = act_small_vert
                    act_s_vert2 = [act_small_vert[0]+0.5, act_small_vert[1]+0.5]
                    mst.remove_node(node)
                
                elif node == l_touch_node: # If there are neighbors connect them (Wide End)
                    l_node_neis = []
                    nei_block = adj_mat[node]
                    for neis in nei_block:
                        for nei in neis:
                            if nei not in nodes:
                                l_node_neis.append(nei)
                    
                    if len(l_node_neis):
                        touch_nei = l_node_neis[0]
                        nei_poly = mst.nodes[touch_nei]['poly']
                        nei_min_rect = nei_poly.minimum_rotated_rectangle
                        nei_verts = find_vertices(nei_min_rect, char=False)
                        touch_nd_vert = Point(verts[idx_node][0])
                        nei_vert1, nei_vert2 = Point(nei_verts[0]), Point(nei_verts[1])
                        dist1 = touch_nd_vert.distance(nei_vert1)
                        dist2 = touch_nd_vert.distance(nei_vert2)
                        if dist1 < dist2:
                            act_l_vert = nei_verts[0]
                        else:
                            act_l_vert = nei_verts[1]
                        act_l_vert1 = act_l_vert
                        act_l_vert2 = [act_l_vert[0]+0.5, act_l_vert[1]+0.5]
                        mst.remove_node(node)
                        l_nei = True
                            
                else:
                    try:
                        mst.remove_node(node)
                    except nx.NetworkXError:
                        print(f'Cannot remove HW node {node}')
                        print(found_hws)
                        exit(0)

            # Now add the new node
            if l_nei:
                new_node_poly = Polygon([act_s_vert1, act_s_vert2, act_l_vert2, act_l_vert1])
            else:
                new_node_poly = Polygon([act_s_vert1, act_s_vert2, large_vert2, large_vert1])
            new_node_box = list(new_node_poly.bounds)
            mst.add_node('HW_'+str(idx_hw+1), poly=new_node_poly, box=new_node_box)
            # Add the narrow end edges
            for nei in touch_node_neis:
                mst.add_edge(nei, 'HW_'+str(idx_hw+1), weight=0.0)
            # Add the wide end edges
            if len(l_node_neis):
                for nei in l_node_neis:
                    mst.add_edge(nei, 'HW_'+str(idx_hw+1), weight=0.0)
            
    # if p == 2 and idx == 8:
    #     print(mst.nodes())
    #     print(mst.edges())
    #     # print(p,idx)
    #     exit(0)
    return mst



def create_visual_graph(pp_msts, in_g_poly, frs, p):
    ft_msts = []
    for idx, (mst, dir_mst, adj_matrix) in enumerate(pp_msts):
        try:
            cd_graph = nx.DiGraph()
            fc_g = in_g_poly[idx]
            fr = frs[idx]
            # Instantiate the operations class
            poly_ops = PostProcess_Ops(idx, p, mst, dir_mst, adj_matrix, fc_g, cd_graph)
            # Get the centroid for each node
            val = poly_ops.find_centroid()


            if not val:
                ft_msts.append('N/A')
                # if p == 0 and idx == 1:
                #     poly_ops.find_char_clusters()
                #     print(poly_ops.char_clusters)
                #     exit(0)
            else:
                line_signatures = ['Ln', 'Wave', 'Circle', 'SW']

                # Now find all the character clusters
                poly_ops.find_char_clusters()

                # Now find in/out character cluster, if any (At this point they are only connected to one line
                # if out of ring in the MST)
                poly_ops.in_ring_clusters(p, idx)

                # Now for all the out-of-ring characters find which lines it is connected to
                # These lines NOT a part of the main chain/ring
                poly_ops.find_cluster_lines()
                # if p == 2 and idx == 3:
                #     print(poly_ops.char_clusters)
                #     print(poly_ops.c_clus_bonds)

                # Now try to get all the floating lines in the MAIN chain
                # Corresponding to single/double/triple bond
                poly_ops.find_same_main_bond_lines()

                # if p == idx == 135:
                #     print(same_bond_lines)
                #     exit(0)

                # Strip the individual ID's from char clusters
                poly_ops.strip_id()

                # ID Duplicate clusters if two clusters with same string
                poly_ops.id_duplicate()

                if len(poly_ops.same_bond_lines) == 0:
                    # Find the vertices of the largest side of each poly (for CDXML encoding)
                    poly_ops.find_largest_side_vertices()

                    # Now get the different bonds
                    # bonds = [clusID_1, clusID_2, Number, Type]
                    poly_ops.char_only_bonds()

                    # Now create the CDXML graph
                    cd_graph = poly_ops.char_only_cdxml_graph()
                else:
                    # If there are bond lines that are in the main chain:
                    poly_ops.connect_mst_edges()

                    # Find the vertices of the largest side of each poly (for CDXML encoding)
                    poly_ops.find_largest_side_vertices()

                    # Now count the hidden carbons (main chain lines)
                    # Count the number of main lines and characters. Set the start as a line
                    # Not an in-chain character
                    poly_ops.find_main_lines()
                    poly_ops.compress_graph(p, idx)
                    poly_ops.find_new_adj()
                    # if idx == 25:
                    #     print(f'Ini Nodes: {poly_ops.mst.nodes()}')
                    #     print(f'Clus lines: {poly_ops.all_clus_lines}')
                    #     print(f'Main Lines: {poly_ops.main_lines}')
                    #     print(f'Same Bond Lines: {poly_ops.same_bond_lines}')

                    poly_ops.find_line_sets()
                    try:
                        cd_graph = poly_ops.build_pos_graph()
                    except:
                        cd_graph = 'N/A'
                    # if p == 2 and idx == 0:
                    #     print(poly_ops.main_sets)
                    #     print(poly_ops.mst.nodes())
                    #     print(poly_ops.same_bond_lines)
                    #     exit(0)
                    # # If one carbon line (2 Hidden carbons); only connect the clusters; get the graph
                    # if len(poly_ops.same_bond_lines) == 1:
                    #     cd_graph = poly_ops.one_main_line_graph()

                    # else:
                    #     # Find if a loop exists, if yes start from a line bond
                    #     poly_ops.find_loop()
                    #     # if p == 2 and idx == 1:
                    #     #     print(f'Loop: {poly_ops.loop}')
                    #     #     print(f'Valid Cycles: {poly_ops.valid_cycles}')
                    #     #     print(poly_ops.mst.edges())
                    #     #     exit(0)
                    #     # if idx == 45:
                    #     #     print(poly_ops.cycles)
                    #     #     exit(0)
                    #     # Now get the CDXML graph
                    #     # try:
                    #     # Rings Detected
                    #     if poly_ops.loop:
                    #         cd_graph = poly_ops.refined_processing(p, idx)
                    #     # No Rings Detected
                    #     else:
                    #         cd_graph = poly_ops.main_graph(p, idx)

                    #     # except KeyError:
                    #     #     print(f'Idx: {idx}')
                    #     #     print(poly_ops.mst.nodes())
                    #     #     exit(0)
                    # if p == 4 and idx == 1: #H:8,15
                    #     # print(cd_graph.edges(data=True))
                    #     viz_translated_cd(cd_graph=cd_graph)
                    # #     # view_intermediate_overlay(cd_graph, fr, p, 'outputs/chemxtest/images/or100.09.tables')
                    #     exit(0)

                ft_msts.append(cd_graph)
        except Exception as e:
            print(f"While processing graph, the following exception occurred {e}")

    return ft_msts


def pipeline_create_visual_graph(all_pdf_msts, all_pdf_in_g, frs):
    visual_graphs = {}
    
    for p in sorted(list(all_pdf_msts.keys())):
        page_msts = all_pdf_msts[p]
        page_in_g_poly = all_pdf_in_g[p]
        page_frs = frs[p]
        page_v_graphs = create_visual_graph(page_msts, page_in_g_poly,page_frs, p)
        visual_graphs[p] = page_v_graphs
        # ONLY TAKING 3 PAGES for TEMP TSV Generation
        # if p == 4:
        #     # for idx, (mst,_,_) in enumerate(page_msts):
        #     #     print(f'IDX: {idx}')
        #     #     print(mst.nodes())
        #     break
    return visual_graphs


