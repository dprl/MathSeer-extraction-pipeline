import re
from itertools import repeat, chain
import subprocess
import argparse
from modules.protables.ProTables import read_pro_lg
import os
import networkx as nx
import cv2
import numpy as np
import nx_merge
# from modules.chemscraper.parser_v2 import label_merged_chars, annotate_and_merge_brackets, fixing_line_pos, create_final_graph
from modules.chemscraper.parser_v2 import Parser_v2
from modules.chemscraper.geomtric_utils import GeometricToolbox
import math
import numpy as np
from shapely.geometry import Polygon,Point,LineString
from collections import defaultdict
from glob import glob
from modules.chemscraper.graph_conversions.CDXML_Converter import visual2cdxml
from modules.chemscraper.nx_debug_fns import shapedraw, gdraw, shapesdraw, draw_points, draw_graph
from modules.utils.dprl_map_reduce import map_reduce, combine_dict_pair, map_concat
import copy
from configparser import ConfigParser, ExtendedInterpolation
from modules.generate_data.src.utils.utils import imshow
from shapely.ops import nearest_points, unary_union
from shapely.affinity import translate, scale
import alphashape

def get_protable_from_single_lgeval(lg_eval_file_path, from_lg=False, lg_string=None):
    return read_pro_lg(os.path.dirname(lg_eval_file_path), [os.path.basename(lg_eval_file_path)], 
                       get_relations=True, from_string=from_lg, lg_list=[lg_string])            

def getTSVRegion( protable ):
    traces = []
    regionEntry = protable[0][1][0][0]
    offsetVector = np.array([ 0, 0, 0, 0 ]).astype(int)
    contour_offset = np.array([0, 0])
    
    primitiveIds = []
    classLabels = []
    bboxes = {}
    contours = {}
    symtags = []    

    objects = regionEntry[1]
    for i in range(len(objects)):            
        (oLabel, _ ) = objects[i][4:6]   

        try:
            objId = objects[i][6]
            symtags.append(objects[i][7])
        except:
            objId = int(i) 

        #  if traces present
        if len(objects[i]) >= 9:
            contour = objects[i][8]
            contour = [np.array(c) - contour_offset for c in contour]
            contours[ objId ] = contour
            traces.append(np.concatenate(contour).reshape(1, -1, 2))

        primitiveIds.append( objId )
        classLabels.append( oLabel )            

        ( bbminX, bbminY, bbmaxX, bbmaxY ) = objects[i][:4]
        bboxes[ objId ] = np.array( ( bbminY, bbminX, bbmaxY, bbmaxX ) )\
                .astype(int) - offsetVector

    return (primitiveIds, classLabels, bboxes, symtags, traces,
            contours)

def draw_contours(points):    
    points2 = np.concatenate(points)
    x_min, y_min = np.min(points2, axis=0)[0]
    x_max, y_max = np.max(points2, axis=0)[0]
    adjusted_points = [point - np.array([x_min, y_min]) for point in points]
 
    width, height = x_max - x_min + 1, y_max - y_min + 1
    image = np.zeros((height, width), dtype=np.uint8)
        
    cv2.drawContours(image, adjusted_points, contourIdx=-1, color=255,
                          thickness=-1)
    
    return np.argwhere(image == 255), image

def rename_relations(relations, sym2prims, symtags, ids, mapping_old_new):    
    LINES = ["HW","Ln","SW","Wave"]
    new_relations = []
    for edge in relations:
        node_from,node_to,label_edge = edge

        node_from = mapping_old_new[node_from]
        node_to = mapping_old_new[node_to]

        index_of_primitive0_from = ids.index(sym2prims[node_from][0])
        index_of_primitive0_to = ids.index(sym2prims[node_to][0])
        tag_node_from = symtags[index_of_primitive0_from].split("_")[0]
        tag_node_to = symtags[index_of_primitive0_to].split("_")[0]
        if not tag_node_from in LINES and  not tag_node_to in LINES:            
            new_relations.append((node_from,node_to,"CONCATENATED"))
        else:
            new_relations.append((node_from,node_to,label_edge))
    return new_relations

def get_midpoint_point( p1:Point, p2:Point) -> Point:
    return Point((p1.x+p2.x)/2, (p1.y+p2.y)/2)

def get_mid_line(line1:LineString, line2:LineString)->LineString:
    l1p1 = Point(line1.coords[0])
    l1p2 = Point(line1.coords[1])
    l2p1 = Point(line2.coords[0])
    l2p2 = Point(line2.coords[1])
    
    if l1p1.distance(l2p1) > l1p1.distance(l2p2):
        return LineString([get_midpoint_point(l1p1,l2p2),get_midpoint_point(l1p2,l2p1)])
    else:
        return LineString([get_midpoint_point(l1p1,l2p1),get_midpoint_point(l1p2,l2p2)]) 

def are_lines_similar_angles(line1:LineString, line2:LineString)->bool:
            
    l1p1 = Point(line1.coords[0])
    l1p2 = Point(line1.coords[1])
            
    l2p1 = Point(line2.coords[0])
    l2p2 = Point(line2.coords[1])

    if l1p1.distance(l2p1)<l1p1.distance(l2p2):
        l2_translated = translate(line2,xoff=l1p1.x-l2p1.x,yoff=l1p1.y-l2p1.y)
    else:
        l2_translated = translate(line2,xoff=l1p1.x-l2p2.x,yoff=l1p1.y-l2p2.y)

    l2_translated_p1 = Point(l2_translated.coords[0])
    l2_translated_p2 = Point(l2_translated.coords[1])
    
    if l2_translated_p1.distance(l1p1)>l2_translated_p2.distance(l1p1):
        reference_l2_translated = l2_translated_p1
    else:
        reference_l2_translated = l2_translated_p2
    
    third_side = LineString([reference_l2_translated, l1p2])
    a = line1.length
    b = l2_translated.length
    c = third_side.length      

    if a*b==0:            
        return False  
    
    args_cos = (a*a+b*b-c*c)/(2*a*b)
    if args_cos>=-1.0 and args_cos<=1.0:
        C = math.acos(args_cos)*180/math.pi
    else:
        #args_cos value might be calculated as 1.0000001 or similar when lines are colinear
        return True

    difference = C                
    return difference<15

def are_lines_coolinear(line1:LineString, line2:LineString)->bool:        
    return are_lines_similar_angles(line1, line2) and line1.distance(line2)==0

def merge_lines(line1:LineString, line2:LineString)->LineString:    

    l1p1 = Point(line1.coords[0])
    l1p2 = Point(line1.coords[1])
    l2p1 = Point(line2.coords[0])
    l2p2 = Point(line2.coords[1])

    if l1p1 == l2p1:    
        return LineString([l1p2,l2p2])
    elif l1p2 == l2p1:
        return LineString([l1p1,l2p2])
    elif l1p1 == l2p2:
        return LineString([l1p2,l2p1])            
    else:
        return LineString([l1p1,l2p1])

def get_lines_from_polygon_merged(points):
    all_lines = []
    for i,point in enumerate(points[:-1]):            
        point1 = point
        point2 = points[i+1]
        line = LineString([point1, point2])
        if i!=0 and are_lines_coolinear(line,all_lines[-1]):
            line = merge_lines(line,all_lines[-1])            
            all_lines[-1] = line
        else:
            all_lines.append(line)
    return all_lines

def get_verts(pixels):    
    pixels_inverted = pixels
    original = LineString(pixels_inverted)
    polygon_original = Polygon(pixels_inverted)
    convex = original.convex_hull

    if convex.geom_type == "LineString":
        assert len(convex.coords) == 2, "Has more than 2 points"
        return np.array(convex.coords).tolist(), convex, convex.length
    else:        
        simplified = polygon_original.simplify(20)        
        
        lines = []
        for i in range(len(simplified.exterior.coords)-1):
            lines.append(LineString([simplified.exterior.coords[i],simplified.exterior.coords[i+1]]))
        sorted_lines = sorted(lines, key=lambda x:-x.length)
        vertices_line = get_mid_line(sorted_lines[0], sorted_lines[1])
        
        extended = scale(vertices_line, xfact=4, yfact=4)
        
        intersection_line = extended.intersection(simplified.minimum_rotated_rectangle)
        
        polygon = convex.minimum_rotated_rectangle
        if Point(polygon.exterior.coords[0]).distance(Point(polygon.exterior.coords[1])) < Point(polygon.exterior.coords[0]).distance(Point(polygon.exterior.coords[3])):
            p1 = get_midpoint_point(Point(polygon.exterior.coords[0]),Point(polygon.exterior.coords[1]))
            p2 = get_midpoint_point(Point(polygon.exterior.coords[2]),Point(polygon.exterior.coords[3]))
        else:
            p1 = get_midpoint_point(Point(polygon.exterior.coords[0]),Point(polygon.exterior.coords[3]))
            p2 = get_midpoint_point(Point(polygon.exterior.coords[1]),Point(polygon.exterior.coords[2]))
        
        points = intersection_line

        midpoint = get_midpoint_point(p1,p2)
        shorter_line = LineString([get_midpoint_point(p1,midpoint),get_midpoint_point(midpoint,p2)])
        cd_length = points.length
        left = shorter_line.parallel_offset(cd_length / 2, 'left')
        right = shorter_line.parallel_offset(cd_length / 2, 'right')
        c1 = left.boundary.geoms[0]
        d1 = right.boundary.geoms[1]
        c2 = left.boundary.geoms[1]
        d2 = right.boundary.geoms[0]
        perpendicular1 = LineString([c1, d1])
        perpendicular2 = LineString([c2, d2]) 

        intersetion1 = perpendicular1.intersection(convex)
        intersetion2 = perpendicular2.intersection(convex)

        verts = None

        if intersetion1.length<intersetion2.length:
            if intersetion1.distance(Point(points.coords[0]))<intersetion1.distance(Point(points.coords[1])):
                verts= [points.coords[0],points.coords[1]]
            else:
                verts= [points.coords[1],points.coords[0]]
        else:
            if intersetion2.distance(Point(points.coords[0]))<intersetion2.distance(Point(points.coords[1])):
                verts= [points.coords[0],points.coords[1]]
            else:
                verts= [points.coords[1],points.coords[0]]
        
        return verts,intersection_line,intersection_line.length

            
def get_max_side_bbox(bbox_object):
    return max(abs(bbox_object[0]-bbox_object[2]),abs(bbox_object[1]-bbox_object[3]))

def get_tag_id_as_parser(tag):
    if tag == "Single" or tag == "Double" or tag == "Triple":
        return "Ln"
    else:
        return tag


def rename_symtags(sym2prims,class_by_symid):
    dict_labels = {symid: label for symid,label in class_by_symid}
    dict_counter = {}    
    ids = {}    
    
    for symid in sym2prims.keys():
        current_class = dict_labels[symid]
        if current_class in dict_counter:
            dict_counter[current_class]+=1
        else:
            dict_counter[current_class]=0
        ids[symid] = f"{current_class}_{dict_counter[current_class]}"
    new_symtags = []
    for symid,_ in class_by_symid:
        new_symtags.append(ids[symid])
    
    return new_symtags, ids


def get_angle(node_verts):
    if node_verts[1][0] == node_verts[0][0]:
        slope = float('inf') 
    else:
        slope = (node_verts[1][1] - node_verts[0][1]) / (node_verts[1][0] - node_verts[0][0])
    degrees = math.degrees(math.atan(slope))
    if degrees < 0:
        # if (90 - abs(degrees)) < parser.PARALLEL_TOLERANCE: # correcting arctan result
        #     degrees = abs(degrees)
        degrees = abs(degrees)    
    return degrees

#TODO:organize this
RATIO_TOLERANCE = 0.1
ANGLE_SIMILARITY_DIFFERENCE = 10
def are_lines_similar_angles(line1:LineString, line2:LineString)->bool:
            
    l1p1 = Point(line1.coords[0])
    l1p2 = Point(line1.coords[1])
            
    l2p1 = Point(line2.coords[0])
    l2p2 = Point(line2.coords[1])

    if l1p1.distance(l2p1)<l1p1.distance(l2p2):
        l2_translated = translate(line2,xoff=l1p1.x-l2p1.x,yoff=l1p1.y-l2p1.y)
    else:
        l2_translated = translate(line2,xoff=l1p1.x-l2p2.x,yoff=l1p1.y-l2p2.y)

    l2_translated_p1 = Point(l2_translated.coords[0])
    l2_translated_p2 = Point(l2_translated.coords[1])
    
    if l2_translated_p1.distance(l1p1)>l2_translated_p2.distance(l1p1):
        reference_l2_translated = l2_translated_p1
    else:
        reference_l2_translated = l2_translated_p2
    
    third_side = LineString([reference_l2_translated, l1p2])
    a = line1.length
    b = l2_translated.length
    c = third_side.length      

    if a*b==0:            
        return False  
    
    args_cos = (a*a+b*b-c*c)/(2*a*b)
    if args_cos>=-1.0 and args_cos<=1.0:
        C = math.acos(args_cos)*180/math.pi
    else:
        #args_cos value might be calculated as 1.0000001 or similar when lines are colinear
        return True

    difference = C                
    return difference<ANGLE_SIMILARITY_DIFFERENCE
def needs_split(skeleton:LineString,closest:LineString)->bool:
    length_ratio = min(skeleton.length, closest.length)/max(skeleton.length, closest.length)
    is_half_or_third = length_ratio>(0.33-RATIO_TOLERANCE) and length_ratio<(0.50+RATIO_TOLERANCE)

    p1 = skeleton.interpolate(skeleton.project(Point(closest.coords[0])))
    p2 = skeleton.interpolate(skeleton.project(Point(closest.coords[1])))

    projected_line = LineString([p1,p2])

    in_sight = max(projected_line.length, closest.length)>0 and \
        (min(projected_line.length, closest.length)/max(projected_line.length, closest.length))>=math.cos(ANGLE_SIMILARITY_DIFFERENCE*math.pi/180)

    are_crossing = skeleton.distance(closest)==0

    return are_lines_similar_angles(skeleton,closest) and is_half_or_third and in_sight and not are_crossing
def split_bond(to_split:LineString, reference: LineString):
    split_point1 = nearest_points(to_split, Point(reference.coords[0]))[0]
    
    split_point2 = nearest_points(to_split, Point(reference.coords[1]))[0]

    if split_point1.distance(Point(to_split.coords[0])) > split_point2.distance(Point(to_split.coords[0])):
        skeleton1 = LineString([split_point1, to_split.coords[1]])
        skeleton3 = LineString([split_point2, to_split.coords[0]])
    else:
        skeleton1 = LineString([split_point1, to_split.coords[0]])        
        skeleton3 = LineString([split_point2, to_split.coords[1]])
    skeleton2 = LineString([split_point1,split_point2])

    if skeleton1.length/to_split.length < RATIO_TOLERANCE:
        new_skeletons = [skeleton2, skeleton3]
    elif skeleton2.length/to_split.length < RATIO_TOLERANCE:
        new_skeletons = [skeleton1, skeleton3]
    elif skeleton3.length/to_split.length < RATIO_TOLERANCE:
        new_skeletons = [skeleton1, skeleton2]
    else:
        new_skeletons = [skeleton1, skeleton2, skeleton3]
    return new_skeletons
def get_line_midpoint(line:LineString)->Point:
    return get_midpoint_point(Point(line.coords[0]),Point(line.coords[1]))

def lg_eval_to_nx(args):
    file_path, visualize, visualize_path, thresholds, \
        from_lg_string, lg_string, yolo_bbox, svg_resolution = args
    if svg_resolution is None:
        svg_resolution = 720
    
    parser = Parser_v2(thresholds)
    geo = GeometricToolbox(thresholds)    

    pro, relations = get_protable_from_single_lgeval(file_path, from_lg=from_lg_string, lg_string=lg_string)
    
    (primitiveIds, classLabels, bboxes, symtags, _, contours) = getTSVRegion(pro)      
    sym2prims = defaultdict(list)
    [sym2prims[sym].append(primitiveIds[i]) for i, sym in enumerate(symtags)]
    
    
    class_by_symid = [(symtags[i], get_tag_id_as_parser(classLabels[i])) for i in range(len(symtags))]
    symtags, mapping_old_new = rename_symtags(sym2prims,class_by_symid)    
    sym2prims = defaultdict(list)
    [sym2prims[sym].append(primitiveIds[i]) for i, sym in enumerate(symtags)]
    relations = rename_relations(relations, sym2prims, symtags, primitiveIds, mapping_old_new)
    LINES = ["HW","Ln","SW","Wave"]
    info_mapping = []
    highest_side = 0    

    # nodes_to_remove = []
    for symbol_id, primitives in sym2prims.items():   
        box = np.array([bboxes[i] for i in primitives])
        box = [np.min(box[:,0]),np.min(box[:,1]),np.max(box[:,2]),np.max(box[:,3])]
        
        index_of_primitive0 = primitiveIds.index(primitives[0])      
        isLine = len(symtags[index_of_primitive0]) > 2 and symtags[index_of_primitive0].split("_")[0] in LINES   

        for i, id in enumerate(primitives):            
            if i == 0:
                flat_contours = contours[id]
            else:
                flat_contours.extend(contours[id])        
        # try:
        polygons = []
        just_points = None
        for countour_xd in flat_contours:                
            points = countour_xd.squeeze(1)
            if len(points) > 2:
                polygons.append(Polygon(points).buffer(2))                 
            if just_points is None:
                just_points = points
            else:
                just_points = np.concatenate((just_points, points))
        
        polygon = unary_union(polygons).buffer(-1)
        if polygon.geom_type == "MultiPolygon":                
            polygon = alphashape.alphashape(just_points,0)
        inverted_contours = np.flip(np.array(polygon.exterior.coords))
        
        #move to top left corner            
        minX = inverted_contours[:,0].min()
        minY = inverted_contours[:,1].min()
        inverted_contours[:,0]-=minX
        inverted_contours[:,1]-=minY
        # else:
        #     import pdb; pdb.set_trace()
        #     nodes_to_remove.append(symbol_id)
        #     print(f"symbol id = {symbol_id} has no contours")
        #     continue
        verts, poly, length = get_verts(inverted_contours)
        # rows, cols = np.array(verts, dtype=int).T
        # image[rows, cols] = 200                        
        new_verts = [[verts[0][0]+box[0],verts[0][1]+box[1]],[verts[1][0]+box[0],verts[1][1]+box[1]]]
        
        poly = translate(poly,xoff=box[0],yoff=box[1])
                        
        lbl_cls = symtags[index_of_primitive0].split("_")[0] if symtags[index_of_primitive0].split("_")[0] in LINES else "Char"
        if lbl_cls == 'Char' and '_Ann' in classLabels[index_of_primitive0]:
            lbl_cls = 'Annotation'
            lbl_type = classLabels[index_of_primitive0].split("_")[0]
        else:
            lbl_type = 'line' if isLine else classLabels[index_of_primitive0]
        midpoint = np.mean(new_verts, axis=0)
        pos = midpoint
        bbox_object = [box[1],box[0],box[3],box[2]]
        if not isLine:
            max_side = get_max_side_bbox(bbox_object)
            if max_side > highest_side:
                highest_side = max_side


        vertexes = [[new_verts[0][1],new_verts[0][0]],[new_verts[1][1],new_verts[1][0]]]
        
        angle = get_angle(vertexes)
        
        info_mapping.append((symbol_id,{
            "box": bbox_object,
            "isLine": isLine,
            "isChar": not isLine and not lbl_cls == 'Annotation',
            "lbl_cls":lbl_cls,
            "lbl_type":lbl_type,
            "midpoint":[midpoint[1],midpoint[0]],
            "pos": [pos[1],pos[0]],
            "verts": vertexes,
            "classLabel": classLabels[index_of_primitive0],
            "floating":False,
            "angle": angle,
            "shape_obj": poly,
            "length": length
        }))
    
    graph = nx.Graph()  
    graph.add_nodes_from(info_mapping)
    
    if highest_side>0:        
        # print(highest_side)
        nx.set_node_attributes(graph, round(highest_side/12)+1, "font_size")        
    
    filename = os.path.basename(file_path)[:-3]
    graph.add_edges_from([(rel[0],rel[1],{"weight":1, "connection":rel[2], "added_edge":True }) 
                          for rel in relations])
    # graph.remove_nodes_from(nodes_to_remove)
    
    if visualize:        
        # gdraw(graph, pdf_obj=os.path.join(visualize_path,filename + ".pdf"),font_size=3)
        draw_graph(graph, save_path=visualize_path, file_name=filename, save=True,
                    title=filename, node_size=20, hide_enum=True)
    characters = nx_merge.node_attr_match( graph, 'isChar', True )
    edge_selector = nx_merge.edge_attr_match(graph, 'connection', "CONCATENATED")
    target = nx_merge.select_left_topmost(graph)
    
    merged_characters = nx_merge.select_and_contract_nodes( graph, characters, f_edge=edge_selector, btselect=target)
    
    new = parser.label_merged_chars(merged_characters, graph, graph, avoid_assertion=True)
    
    for node in new.nodes(data=True):
        label,data = node
        if "classLabel" in data and (data["classLabel"] == "Double" or data["classLabel"] == "Triple"):
            nx.set_node_attributes(new, {label:"Ln||2" if data["classLabel"] == "Double" else "Ln||3"}, 'contraction_label')            
                
    # import pdb; pdb.set_trace()
    # import matplotlib; matplotlib.use('TkAgg')
    X = parser.merge_parallel_lines(new, graph) 
    I = parser.annotate_and_merge_brackets(X)
    # W = parser.fixing_wedge( I )
    W = geo.annotate_hash_lines( I )
    W = geo.annotate_hash_verts( W )

    ML = handle_bond_split(W)
    
    B = parser.fixing_line_pos(ML)
    
    #handle floating
    floating_nodes = list(filter(lambda x:len(x)==1, list(nx.connected_components(B))))
    
    if len(floating_nodes):
        B.graph["floating"] = True
    for node in floating_nodes:
        B.nodes[node.pop()]["floating"] = True

    J = parser.create_final_graph(ML)
    J.graph["scale_factor"] = get_scale_factor(J, bboxes,
                                               output_resolution=svg_resolution)
    J.graph["yolo_bbox"] = yolo_bbox

    J_dict = {}
    J_dict[filename] = {0: [J]}
    return J_dict

def get_node_data_from_line(line):
    # numpy_cords = np.array(line.coords)
    numpy_cords = np.flip(np.array(line.coords), 1)
    box = numpy_cords.min(0).tolist() + numpy_cords.max(0).tolist()
    midpoint  = numpy_cords.mean(0)
    
    return {
        "angle": get_angle(numpy_cords),
        "box":box,
        "length": line.length,
        "verts":numpy_cords.tolist(),
        "midpoint":midpoint.tolist(),
        "pos":midpoint.tolist(),
        "shape_obj":line,


        'classLabel': 'Single',
        'floating': False,
        'font_size': 11,
        'isChar': False,
        'isLine': True,
        'lbl_cls': 'Ln',
        'lbl_type': 'line',
    }

def handle_bond_split(graph):    
    parallel_lines = [key for (key,val) in dict(graph.nodes(data=True)).items() if ("contraction_label" in val and "||" in val["contraction_label"])]
    for parallel_group_id in parallel_lines:
        parallel_group_data = graph.nodes()[parallel_group_id].copy()
        lines = [parallel_group_data["shape_obj"]]
        for val in parallel_group_data["contraction"].values():
            lines.append(val["shape_obj"])
        sorted_lines = sorted(lines,key=lambda x:-x.length) #longest first
        main = sorted_lines[0]
        secondary = sorted_lines[1]

        if needs_split(main, secondary):

            new_bonds = split_bond(main, secondary)

            main_split = None
            dist_main = math.inf

            subgraph = nx.Graph()
            new_ids = []

            for idx, line in enumerate(new_bonds):
                new_id = f"Ln_{graph.number_of_nodes()+idx+100}"                
                new_data=get_node_data_from_line(line)
                subgraph.add_node(new_id,**new_data)

                if len(new_ids)>0:
                    data_edge = {'added_edge': True,
                        'connection': 'CONNECTED',
                        'parallel': False,
                        'weight': 1}
                    
                    subgraph.add_edge(new_ids[-1], new_id, **data_edge)
                    
                new_ids.append(new_id)
                distance_val = get_line_midpoint(secondary).distance(get_line_midpoint(line))
                if distance_val < dist_main:
                    main_split = new_id
                    dist_main = distance_val            
            data_new = subgraph.nodes(data=True)[main_split].copy()
            updated_parent_data = parallel_group_data.copy()
            updated_parent_data.update(data_new)
            nx.set_node_attributes(subgraph,{main_split:updated_parent_data})

            #remove old edges
            old_neighbords = [i for i in graph.neighbors(parallel_group_id)]
            graph.remove_node(parallel_group_id)

            new_graph = nx.compose(graph, subgraph)

            for i in old_neighbords:
                current_distance=math.inf
                closest = None
                for node in subgraph.nodes:                    
                    distance = Point(subgraph.nodes[node]["midpoint"]).distance(Point(graph.nodes[i]["midpoint"]))                    
                    if distance<current_distance:
                        current_distance = distance
                        closest = node
                data_edge = {'added_edge': True,
                        'connection': 'CONNECTED',
                        'parallel': False,
                        'weight': current_distance}                
                new_graph.add_edge(i, closest, **data_edge)                
            return new_graph
    return graph

def get_scale_factor(G, bboxes, output_resolution=720):
    all_bboxes = np.array(list(bboxes.values()))
    min_bbox = all_bboxes.min(0)[:2]
    max_bbox = all_bboxes.max(0)[2:]
    size = max_bbox - min_bbox + 1
    mean_size = size.mean()
    scale_factor = output_resolution / mean_size
    return scale_factor

def scale_points(G, bboxes, output_resolution=720):
    scale_factor = get_scale_factor(G, bboxes, output_resolution)
    
    # Scale node positions using a dictionary comprehension
    scaled_positions = {node: (x * scale_factor, y * scale_factor)
                        for node, (x, y) in nx.get_node_attributes(G, 'pos').items()}
    # Update the positions in the graph
    nx.set_node_attributes(G, scaled_positions, 'pos')
    # G.graph["scale_factor"] = scale_factor
    return G
    

def graph_to_cdxml(graph, filename, out_dir):
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    dict_final_graph={filename:{0:[graph]}}    
    visual2cdxml(dpr_vgraph_dict=dict_final_graph, 
                                out_dir=out_dir,
                                write_pages_out=1)

def write_svg(params):
    pdf_svg_dir, (file, svg) = params
    svg_file = os.path.basename(os.path.dirname(file))[:-4] + ".svg"
    with open(os.path.join(pdf_svg_dir, svg_file), 'w') as f:
        f.write(svg)
    return []
    
def cdxml2svg_bulk(out_path, selected_files=None, parallel=True):
    print('\n [ RENDERING SVGs from CDXMLs (Molconvert) ]')
    # Routing Java exceptions for font sizes in points to /dev/null
    stderr_file_name = 'STDERR_Molconvert_SVG'
    print('* Redirecting standard error to:  ' +  stderr_file_name)
    pattern = os.path.join(os.getcwd(), out_path, "*_SVG", 'Page_*.cdxml')
    svg_cdxml_paths = sorted(glob(pattern), key=lambda x:os.path.basename(os.path.dirname(x)))

    err_file = open(stderr_file_name, 'w')
    svgs = subprocess.run('molconvert "svg:scale28" ' + pattern,
                        shell=True, stdout=subprocess.PIPE, stderr=err_file)
    svg_string = svgs.stdout.decode('utf-8')
    err_file.close()

    pattern = r'(<\?xml version="1\.0"\?>.*?<\/svg\s*>)'
    pdf_svgs = re.findall(pattern, svg_string, re.DOTALL)
    if not len(pdf_svgs) or len(pdf_svgs) != len(svg_cdxml_paths):
        raise Exception('Error in SVG conversion. Please check CDXML Conversion')

    file2svg = dict(zip(svg_cdxml_paths, pdf_svgs))

    pdf_svg_dir = os.path.join(out_path, "svgs")

    print('\n* CDXML dir: ', os.path.join(out_path))
    print('  SVG dir: ', pdf_svg_dir )
    os.makedirs(pdf_svg_dir, exist_ok=True)

    if parallel:
        params_list = list(zip(repeat(pdf_svg_dir), list(file2svg.items())))
        _ = map_concat(write_svg, params_list)
    else:
        for file, svg in file2svg.items():
            write_svg((pdf_svg_dir, (file, svg)))
    print('\n  SVG generation complete\n')


def cdxml2svg(cdxml_dir):
    print('* CDXML/SVG dir: ', (cdxml_dir))
    # print('  SVG dir: ', pdf_svg_dir )
    pattern = os.path.join(os.getcwd(), cdxml_dir, 'Page_*.cdxml')
    # cdxmls = [f for f in cdxmls if f.endswith('.cdxml')]

    # Routing Java exceptions for font sizes in points to /dev/null
    stderr_file_name = 'STDERR_Molconvert_SVG'
    err_file = open(stderr_file_name, 'w')
    # NOTE: Molconvert will only resize atoms based on bond length (atsiz)
    #       if font sizes cannot be interpreted (e.g., 'pt' in sizes)
    svgs = subprocess.run('molconvert "svg:scale28" ' + pattern,
                        shell=True, stdout=subprocess.PIPE, stderr=err_file)
    svg_string = svgs.stdout.decode('utf-8')
    err_file.close()

    pattern = r'(<\?xml version="1\.0"\?>.*?<\/svg\s*>)'
    pdf_svgs = re.findall(pattern, svg_string, re.DOTALL)

    f = open(os.path.join(cdxml_dir, "Page_001_No001.svg"), 'w')
    svg = pdf_svgs[0]
    f.write(svg)
    f.close()
    print('  SVG generation complete\n')
    return []


def get_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--input_lg', '-i', type=str,
                        default="modules/qdgga-parser/data/generated-dataset/chem/train/lgs/line/")
    parser.add_argument('--output_path', '-o', type=str,
                        default="modules/qdgga-parser/data/generated-dataset/chem/train/cdxml_svg/line/")
    parser.add_argument( "-p", "--parallel", type=int, default=1, 
                        help="Parallel on/off")
    parser.add_argument( "-v", "--visualize", type=int, default=1, 
                        help="Saved graph from gdraw")
    parser.add_argument("-t", "--thresholds",
                        default="modules/chemscraper/data/thresholds-indigo.ini",
                        type=str)
    parser.add_argument('--filelist', '-f', 
                        help='File list to select', type=str,
                        default="")
    args = parser.parse_args()
    thresholds = ConfigParser(interpolation=ExtendedInterpolation())
    thresholds.read(args.thresholds)
    args.thresholds = thresholds

    os.makedirs(args.output_path, exist_ok=True)
    return args

def main():    
    args = get_args()
    input_path = args.input_lg
    out_path = args.output_path
    visualize = args.visualize
    
    thresholds_dict = {
        "REMOVE_ALPHA":args.thresholds.getfloat("thresholds", "REMOVE_ALPHA"),
        "NEG_CHARGE_Y_POSITION":args.thresholds.getfloat("thresholds", "NEG_CHARGE_Y_POSITION"),
        "NEG_CHARGE_LENGTH_TOLERANCE":args.thresholds.getfloat("thresholds", "NEG_CHARGE_LENGTH_TOLERANCE"),
        "Z_TOLERANCE":args.thresholds.getfloat("thresholds", "Z_TOLERANCE"),
        "CLOSE_NONPARALLEL_ALPHA":args.thresholds.getfloat("thresholds", "CLOSE_NONPARALLEL_ALPHA"),
        "CLOSE_CHAR_LINE_ALPHA":args.thresholds.getfloat("thresholds", "CLOSE_CHAR_LINE_ALPHA"),

        "LONGEST_LENGTHS_DIFF_RATIO":args.thresholds.getfloat("thresholds", "LONGEST_LENGTHS_DIFF_RATIO"),
        "PARALLEL_TOLERANCE":args.thresholds.getfloat("thresholds", "PARALLEL_TOLERANCE"),        
        "COS_PRUNE":args.thresholds.getfloat("thresholds", "COS_PRUNE"),
        "HASHED_LENGTH_DIFF_THRESHOLD":args.thresholds.getfloat("thresholds", "HASHED_LENGTH_DIFF_THRESHOLD"),
    }

    lg_files = glob(os.path.join(input_path, "*.lg"))
    filelist_file = args.filelist
    selected_files = set()
    if filelist_file:
        with open(filelist_file, "r") as f:
            selected_files = set(filter(None, f.read().strip("").split("\n")))
    if selected_files:
        lg_files = list(filter(lambda x: os.path.basename(x).split(".")[0] in
                            selected_files, lg_files))
    
    viz_path = os.path.join(out_path, "graphs")
    os.makedirs(viz_path, exist_ok=True)
    if args.parallel:
        graphs_dict = map_reduce(lg_eval_to_nx, combine_dict_pair,
                                 list(zip(lg_files, repeat(visualize), repeat(out_path),
                                          repeat(thresholds_dict), repeat(False), repeat(None),
                                          repeat(None), repeat(None))))
    else:
        graphs_dict = dict()
        for file in lg_files:            
            graph_dict = lg_eval_to_nx((file, visualize, out_path,
                                        thresholds_dict, False, None, None, None))
            graphs_dict.update(graph_dict)

    visual2cdxml(dpr_vgraph_dict=graphs_dict, out_dir=out_path, svg=False)
    visual2cdxml(dpr_vgraph_dict=graphs_dict, out_dir=out_path, svg=True)

    cdxml2svg_bulk(out_path, selected_files, parallel=args.parallel)


if __name__ == "__main__":    
    main()
