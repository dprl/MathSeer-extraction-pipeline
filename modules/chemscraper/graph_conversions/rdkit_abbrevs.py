import os
from rdkit.Chem import rdAbbreviations, rdchem
from bs4 import BeautifulSoup as bsoup
import networkx as nx
import pickle

from modules.chemscraper.nx_debug_fns import gdescr, vgdescr
from debug_fns import *

def abbrev_lookup():
    mod_dict = {'OiBu':'i-BuO', 'nDec':'n-Dec', 'nNon':'n-Non', 'nOct':'n-Oct',
                'nHept':'n-Hept', 'nHex':'n-Hex', 'nPent':'n-Pent', 'iPent':'i-Pent',
                'tBu':'t-Bu', 'iBu':'i-Bu', 'nBu':'n-Bu', 'iPr':'i-Pr', 'nPr':'n-Pr',
                'OMe':'MeO', 'SMe':'MeS'}
    bond_dict = {'SINGLE':'Single', 'DOUBLE':'Double', 'TRIPLE':'Triple', 
                 'Single':'Single', 'Double':'Double', 'Triple':'Triple'}
    abbrevs = rdAbbreviations.GetDefaultAbbreviations()
    abbrev_structs = {}
    for abbrev in abbrevs:
        abbrev_mol = abbrev.mol
        abbrev_label = abbrev.label

        #if abbrev_label == "OMe":
            #check("OMe found in rdkit",'')
            #pcheck('Molecule', abbrev_mol)
        abbrev_graph = nx.DiGraph()
        #for atom_id in range(len(abbrev_mol.GetAtoms())):
        #    atom_label = abbrev_mol.GetAtomWithIdx(atom_id).GetSymbol() 
        #    abbrev_graph.nodes[atom_label]['p'] = abbrev_mol.GetAtomPosition(atom_id)

        for bond_id in range(len(abbrev_mol.GetBonds())):
            bond_type = str(abbrev_mol.GetBondWithIdx(bond_id).GetBondType())
            strt_atom_idx = abbrev_mol.GetBondWithIdx(bond_id).GetBeginAtomIdx()
            end_atom_idx = abbrev_mol.GetBondWithIdx(bond_id).GetEndAtomIdx()
            strt_atom_lbl = abbrev_mol.GetAtomWithIdx(strt_atom_idx).GetSymbol()
            end_atom_lbl = abbrev_mol.GetAtomWithIdx(end_atom_idx).GetSymbol()
            strt_atom_lbl = strt_atom_lbl+'_'+str(strt_atom_idx)
            end_atom_lbl = end_atom_lbl+'_'+str(end_atom_idx)
            abbrev_graph.add_edge(strt_atom_lbl, end_atom_lbl, type=bond_dict[bond_type])
        abbrev_structs[abbrev_label] = abbrev_graph
        if abbrev_label in mod_dict.keys():
            abbrev_structs[mod_dict[abbrev_label]] = abbrev_graph
    
        
    # Add Ph block (Missing in RDKit abbreviations)
    #abbrev_structs['Ph'] = ph_block()
    # Add additional abbreviations from Blake
    # RZ: These abbreviations are labeled edge lists; 2d coordinates are not provided.
    new_abbrev_structs = 'modules/chemscraper/bin/blake_abb_map.pkl' #modules/chemscraper/
    new_abbrev_mod_dict = 'modules/chemscraper/bin/blake_same_abb_dict.pkl' #modules/chemscraper
    ext_abbrev_cdxmls = 'modules/chemscraper/bin/Nicknames'
    with open(new_abbrev_structs, 'rb') as f:
        new_structs = pickle.load(f)

    f.close()
    with open(new_abbrev_mod_dict, 'rb') as f:
        new_mod_dict = pickle.load(f)
    f.close()
    
    for abbrev in new_structs.keys():
        new_graph = nx.DiGraph()
        for bond in new_structs[abbrev]:
            if bond[0][0] == '[':
                split = bond[0].split(']')
                new_bond0 = split[0][1:] + split[1]
            else:
                new_bond0 = bond[0]
            if bond[1][0] == '[':
                split = bond[1].split(']')
                new_bond1 = split[0][1:] + split[1]
            else:
                new_bond1 = bond[1]
            new_graph.add_edge(new_bond0, new_bond1, type=bond_dict[bond[2]['type']])
        abbrev_structs[abbrev] = new_graph

    for abbrev_mod in new_mod_dict.keys():
        abbrev_structs[abbrev_mod] = abbrev_structs[new_mod_dict[abbrev_mod]]

    # RZ: External/additional abbreviations are inserted into existing dictionary
    ext_abbrev_structs = get_ext_abbrev(ext_abbrev_cdxmls, abbrev_structs)
    return ext_abbrev_structs

    

def ph_block():
    ph_graph = nx.DiGraph()
    ph_graph.add_edge('*_0','C_1',type='Single')
    ph_graph.add_edge('C_1','C_2',type='Single')
    ph_graph.add_edge('C_2','C_3',type='Double')
    ph_graph.add_edge('C_3','C_4',type='Single')
    ph_graph.add_edge('C_4','C_5',type='Double')
    ph_graph.add_edge('C_5','C_6',type='Single')
    ph_graph.add_edge('C_6','C_1',type='Double')
    return ph_graph

def get_ext_abbrev(cdxml_dir, abbrev_structs):
    order_dict = {'2': 'Double', '3': 'Triple'}
    cdxmls = os.listdir(cdxml_dir)
    for cdxml in cdxmls:
        abbrev = cdxml.split('.')[0]
        in_file = open(os.path.join(cdxml_dir, cdxml), 'r')
        contents = in_file.read()
        in_file.close()
        soup = bsoup(contents, 'xml')

        page_data = soup.find('page')
        abbrev_frag = page_data.find('fragment')

        c_count = 1
        ext_pt = 0
        node2id = {}
        nodes = abbrev_frag.find_all('n', recursive=False)
        node_pos = {}
        for n in nodes:
            idx = n['id']
            t_box = n.find('t')

            if t_box is None:
                if n.has_attr('NodeType'):
                    nd_type = n['NodeType']
                    if nd_type == 'ExternalConnectionPoint':
                        node2id[idx] = '*_'+str(ext_pt)
                        ext_pt += 1
                    else:
                        raise Exception(f'Weird NodeType value found {nd_type} for abbrev {cdxml} for No T-box case')
                else:
                    node2id[idx] = 'C_'+str(c_count)
                    c_count += 1
            else: # Element / R group case
                txt_blk = t_box.find('s')
                txt = txt_blk.get_text()
                txt = txt.split('H')[0]
                node2id[idx] = txt

            #point = [ float(c) for c in n['p'].split() ]
            point = n['p']
            node_pos[ node2id[idx] ] = point

        bonds = abbrev_frag.find_all('b', recursive=False)
        g = nx.DiGraph()
        for b_tag in bonds:
            beg = b_tag['B']
            end = b_tag['E']
            if b_tag.has_attr('Order'):
                order = b_tag['Order']
                g.add_edge(node2id[beg], node2id[end], type=order_dict[order])
            else:
                g.add_edge(node2id[beg], node2id[end], type='Single Bond')

        # RZ: Storing positions
        for new_id in node_pos:
            g.nodes[new_id]['p'] = node_pos[ new_id ]
        
        # DEBUG
        #if 'Ph' == abbrev:
        #    pcheck(cdxml + 'Graph', vgdescr(g))

        # Now add the structs to the existing dict
        #if abbrev not in abbrev_structs:
        abbrev_structs[abbrev] = g
    return abbrev_structs


if __name__=="__main__":
    # abbrev_lookup()
    # atm_no = rdchem.Atom('Br').GetAtomicNum()
    # print(atm_no)
    cd_dir = 'modules/chemscraper/bin/Nicknames'
    dummy_structs = {}
    get_ext_abbrev(cd_dir, dummy_structs)
