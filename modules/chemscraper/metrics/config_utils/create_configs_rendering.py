'''


'''

run_name = ''

REMOVE_PERCENT=2.6
NEG_CHARGE_Y_THRESHOLD=0.3
NEG_CHARGE_LENGTH_THRESHOLD=0.5
Z_THRESHOLD=1.6
CLOSE_THRESHOLD_NONPARALLEL_ALPHA=1.8
CLOSE_THRESHOLD_CHARLINE_ALPHA=1.5

for label_mode in ["hetero","terminal-hetero","all"]:
    for implicit_hydrogens_visible in [True,False]:
        for relative_thickness in [0.5,1,1.5]:
            run_name = f"grid_indigo_{label_mode}_{implicit_hydrogens_visible}_{relative_thickness}"
            template=f'''
[input]
input_path = ./modules/chemscraper/data/synthetic/indigo.csv
id_index_in_file=0
smiles_index_in_file=2

[output]
output_path = ./outputs/FINAL_{run_name}

gen_smiles=true
gen_images=true
gen_dot=true
gen_lg=true
parallel=true

[thresholds]
#DEFAULT: 2.5
REMOVE_PERCENT={REMOVE_PERCENT}
#DEFAULT: 0.3
NEG_CHARGE_Y_THRESHOLD={NEG_CHARGE_Y_THRESHOLD}
#DEFAULT: 0.5
NEG_CHARGE_LENGTH_THRESHOLD={NEG_CHARGE_LENGTH_THRESHOLD}
#DEFAULT: 1.6
Z_THRESHOLD={Z_THRESHOLD}
#DEFAULT: 1.8
CLOSE_THRESHOLD_NONPARALLEL_ALPHA={CLOSE_THRESHOLD_NONPARALLEL_ALPHA}
#DEFAULT: 1.5
CLOSE_THRESHOLD_CHARLINE_ALPHA={CLOSE_THRESHOLD_CHARLINE_ALPHA}

[rendering]
#default:terminal-hetero
LABEL_MODE={label_mode}
#default:True
IMPLICIT_HYDROGENS_VISIBLE={implicit_hydrogens_visible}
#default:1.0
RELATIVE_THICKNESS={relative_thickness}
'''
            print(f"./run-indigo-grid.sh -c modules/chemscraper/metrics/render_params_configs/configs_final_results/{run_name}.ini")
            #f = open(f"../render_params_configs/configs_final_results/{run_name}.ini", "w+")
            #f.write(template)
            #f.close()
