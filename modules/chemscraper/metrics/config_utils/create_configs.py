
from glob import glob

def get_file(REMOVE_PERCENT, NEG_CHARGE_Y_THRESHOLD, NEG_CHARGE_LENGTH_THRESHOLD,
    Z_THRESHOLD, CLOSE_THRESHOLD_NONPARALLEL_ALPHA, CLOSE_THRESHOLD_CHARLINE_ALPHA):
    run_name = f"tunning_{REMOVE_PERCENT}_{NEG_CHARGE_Y_THRESHOLD}_{NEG_CHARGE_LENGTH_THRESHOLD}_{Z_THRESHOLD}_{CLOSE_THRESHOLD_NONPARALLEL_ALPHA}_{CLOSE_THRESHOLD_CHARLINE_ALPHA}"
    template=f'''
[input]
input_path = ./modules/chemscraper/data/pubchem/train_1k.csv
id_index_in_file=2
smiles_index_in_file=4

[output]
output_path = ./outputs/{run_name}

gen_smiles=true
gen_images=true
gen_dot=true
gen_lg=true
parallel=true

[thresholds]
#DEFAULT: 2.5
REMOVE_PERCENT={REMOVE_PERCENT}
#DEFAULT: 0.3
NEG_CHARGE_Y_THRESHOLD={NEG_CHARGE_Y_THRESHOLD}
#DEFAULT: 0.5
NEG_CHARGE_LENGTH_THRESHOLD={NEG_CHARGE_LENGTH_THRESHOLD}
#DEFAULT: 1.6
Z_THRESHOLD={Z_THRESHOLD}
#DEFAULT: 1.8
CLOSE_THRESHOLD_NONPARALLEL_ALPHA={CLOSE_THRESHOLD_NONPARALLEL_ALPHA}
#DEFAULT: 1.5
CLOSE_THRESHOLD_CHARLINE_ALPHA={CLOSE_THRESHOLD_CHARLINE_ALPHA}
'''
    return template


#DEFAULTS
REMOVE_PERCENT=2.5
NEG_CHARGE_Y_THRESHOLD=0.3
NEG_CHARGE_LENGTH_THRESHOLD=0.5
Z_THRESHOLD=1.6
CLOSE_THRESHOLD_NONPARALLEL_ALPHA=1.8
CLOSE_THRESHOLD_CHARLINE_ALPHA=1.5

for REMOVE_PERCENT in [2.0,2.2,2.4,2.6,2.8,3.0]:
    run_name = f"tunning_{REMOVE_PERCENT}_{NEG_CHARGE_Y_THRESHOLD}_{NEG_CHARGE_LENGTH_THRESHOLD}_{Z_THRESHOLD}_{CLOSE_THRESHOLD_NONPARALLEL_ALPHA}_{CLOSE_THRESHOLD_CHARLINE_ALPHA}"
    template = get_file(REMOVE_PERCENT, NEG_CHARGE_Y_THRESHOLD, NEG_CHARGE_LENGTH_THRESHOLD, Z_THRESHOLD, CLOSE_THRESHOLD_NONPARALLEL_ALPHA, CLOSE_THRESHOLD_CHARLINE_ALPHA)
    #print(f"./run-indigo -c modules/chemscraper/metrics/render_params._configs_thresholds./configs_thresholds/{run_name}.ini")
    f = open(f"../configs_thresholds/{run_name}.ini", "w+")
    f.write(template)
    f.close()
for NEG_CHARGE_Y_THRESHOLD in [1.1,1.2,1.3,1.4,1.5]:
    run_name = f"tunning_{REMOVE_PERCENT}_{NEG_CHARGE_Y_THRESHOLD}_{NEG_CHARGE_LENGTH_THRESHOLD}_{Z_THRESHOLD}_{CLOSE_THRESHOLD_NONPARALLEL_ALPHA}_{CLOSE_THRESHOLD_CHARLINE_ALPHA}"
    template = get_file(REMOVE_PERCENT, NEG_CHARGE_Y_THRESHOLD, NEG_CHARGE_LENGTH_THRESHOLD, Z_THRESHOLD, CLOSE_THRESHOLD_NONPARALLEL_ALPHA, CLOSE_THRESHOLD_CHARLINE_ALPHA)
    #print(f"./run-indigo -c modules/chemscraper/metrics/render_params._configs_thresholds./configs_thresholds/{run_name}.ini")
    f = open(f"../configs_thresholds/{run_name}.ini", "w+")
    f.write(template)
    f.close()
for NEG_CHARGE_LENGTH_THRESHOLD in [0.3,0.4,0.5,0.6]:
    run_name = f"tunning_{REMOVE_PERCENT}_{NEG_CHARGE_Y_THRESHOLD}_{NEG_CHARGE_LENGTH_THRESHOLD}_{Z_THRESHOLD}_{CLOSE_THRESHOLD_NONPARALLEL_ALPHA}_{CLOSE_THRESHOLD_CHARLINE_ALPHA}"
    template = get_file(REMOVE_PERCENT, NEG_CHARGE_Y_THRESHOLD, NEG_CHARGE_LENGTH_THRESHOLD, Z_THRESHOLD, CLOSE_THRESHOLD_NONPARALLEL_ALPHA, CLOSE_THRESHOLD_CHARLINE_ALPHA)
    #print(f"./run-indigo -c modules/chemscraper/metrics/render_params._configs_thresholds./configs_thresholds/{run_name}.ini")
    f = open(f"../configs_thresholds/{run_name}.ini", "w+")
    f.write(template)
    f.close()
for Z_THRESHOLD in [1.5,1.6,1.7,1.8,1.9,2.0]:
    run_name = f"tunning_{REMOVE_PERCENT}_{NEG_CHARGE_Y_THRESHOLD}_{NEG_CHARGE_LENGTH_THRESHOLD}_{Z_THRESHOLD}_{CLOSE_THRESHOLD_NONPARALLEL_ALPHA}_{CLOSE_THRESHOLD_CHARLINE_ALPHA}"
    template = get_file(REMOVE_PERCENT, NEG_CHARGE_Y_THRESHOLD, NEG_CHARGE_LENGTH_THRESHOLD, Z_THRESHOLD, CLOSE_THRESHOLD_NONPARALLEL_ALPHA, CLOSE_THRESHOLD_CHARLINE_ALPHA)
    #print(f"./run-indigo -c modules/chemscraper/metrics/render_params._configs_thresholds./configs_thresholds/{run_name}.ini")
    f = open(f"../configs_thresholds/{run_name}.ini", "w+")
    f.write(template)
    f.close()
for CLOSE_THRESHOLD_NONPARALLEL_ALPHA in [1.5,1.6,1.7,1.8,1.9,2.0]:
    run_name = f"tunning_{REMOVE_PERCENT}_{NEG_CHARGE_Y_THRESHOLD}_{NEG_CHARGE_LENGTH_THRESHOLD}_{Z_THRESHOLD}_{CLOSE_THRESHOLD_NONPARALLEL_ALPHA}_{CLOSE_THRESHOLD_CHARLINE_ALPHA}"
    template = get_file(REMOVE_PERCENT, NEG_CHARGE_Y_THRESHOLD, NEG_CHARGE_LENGTH_THRESHOLD, Z_THRESHOLD, CLOSE_THRESHOLD_NONPARALLEL_ALPHA, CLOSE_THRESHOLD_CHARLINE_ALPHA)
    #print(f"./run-indigo -c modules/chemscraper/metrics/render_params._configs_thresholds./configs_thresholds/{run_name}.ini")
    f = open(f"../configs_thresholds/{run_name}.ini", "w+")
    f.write(template)
    f.close()
for CLOSE_THRESHOLD_CHARLINE_ALPHA in [1.5,1.6,1.7,1.8,1.9,2.0]:
    run_name = f"tunning_{REMOVE_PERCENT}_{NEG_CHARGE_Y_THRESHOLD}_{NEG_CHARGE_LENGTH_THRESHOLD}_{Z_THRESHOLD}_{CLOSE_THRESHOLD_NONPARALLEL_ALPHA}_{CLOSE_THRESHOLD_CHARLINE_ALPHA}"
    template = get_file(REMOVE_PERCENT, NEG_CHARGE_Y_THRESHOLD, NEG_CHARGE_LENGTH_THRESHOLD, Z_THRESHOLD, CLOSE_THRESHOLD_NONPARALLEL_ALPHA, CLOSE_THRESHOLD_CHARLINE_ALPHA)
    #print(f"./run-indigo -c modules/chemscraper/metrics/render_params._configs_thresholds./configs_thresholds/{run_name}.ini")
    f = open(f"../configs_thresholds/{run_name}.ini", "w+")
    f.write(template)
    f.close()

