from operator import itemgetter
from multiprocessing import Value
import modules.protables.debug_fns as debug_fns
from modules.protables.debug_fns import *
dTimer = DebugTimer("ChemScraper: Parse (born-digital or visual) and Evaluate Smiles")
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.
import copy
import os
import networkx as nx
import csv
import random
import numpy as np
import cv2
from modules.pipeline.pipeline_run_chemx import process_sscraper_one_pdf, process_combine_one_file, gen_smiles, gen_svg
from modules.chemscraper.utils.indigo import Indigo
from modules.chemscraper.utils.indigo.renderer import IndigoRenderer
from modules.chemscraper.graph_conversions.CDXML_Converter import visual2cdxml
import io
import re
import subprocess
import json
import argparse
from glob import glob
from configparser import ConfigParser, ExtendedInterpolation
from SmilesPE.pretokenizer import atomwise_tokenizer
from rdkit import Chem, DataStructs
from Levenshtein import distance, ratio
from tqdm import tqdm
from itertools import repeat
from modules.utils.dprl_map_reduce import combine_dict_pair, map_reduce
from modules.chemscraper.nx_debug_fns import vgdescr, str_remove_suffix
from modules.chemscraper.utils.viz_utils import imshow
from modules.chemscraper.parser_v2 import Parser_v2
import resource
import pandas as pd
import math
from modules.chemscraper.utils.lgeval_to_nx import lg_eval_to_nx, cdxml2svg, cdxml2svg_bulk
from modules.generate_data.src.constants import SUBSTITUTIONS, ABBREVIATIONS
from rdkit import RDLogger                                                                                                                                                               
RDLogger.DisableLog('rdApp.*')                                                                                                                                                           
from modules.generate_data.src.render_smiles import render_rdkit_image


bond_int2str = {1: "Single", 2: "Double", 3: "Triple", 5: "S Wedge", 6: "H Wedge"}
bond_str2int = {v:k for k, v in bond_int2str.items()}

# HACK: Global variables for map-reduce progress
CURR_COUNT=Value('i',0)
MAX_COUNT=None

def tanimoto_similarity(smiles1, smiles2):
    try:
        mol1 = Chem.MolFromSmiles(smiles1)
        mol2 = Chem.MolFromSmiles(smiles2)
        fp1 = Chem.RDKFingerprint(mol1)
        fp2 = Chem.RDKFingerprint(mol2)
        tanimoto = DataStructs.FingerprintSimilarity(fp1, fp2)
        return tanimoto
    except:
        return 0

def levenshtein_similarity(smiles1, smiles2):            
    return distance(smiles1, smiles2)

def canonicalize_smiles_using_mol(smiles):
    return Chem.MolToSmiles(Chem.MolFromSmiles(smiles),True)

def canonicalize_smiles(smiles, ignore_chiral=False, ignore_cistrans=True, replace_rgroup=True):
    if type(smiles) is not str or smiles == '':
        return '', False
    if ignore_cistrans:
        smiles = smiles.replace('/', '').replace('\\', '')
    if replace_rgroup:
        tokens = atomwise_tokenizer(smiles)
        for j, token in enumerate(tokens):
            if token[0] == '[' and token[-1] == ']':
                symbol = token[1:-1]
                # if symbol[0] == 'R' and symbol[1:].isdigit():
                #     tokens[j] = f'[{symbol[1:]}*]'
                # elif Chem.AtomFromSmiles(token) is None:
                #     tokens[j] = '*'
                if symbol in ABBREVIATIONS:
                    tokens[j] = ABBREVIATIONS[symbol].smiles
                elif Chem.AtomFromSmiles(token) is None:
                    tokens[j] = '*'
        smiles = ''.join(tokens)
    try:
        canon_smiles = Chem.CanonSmiles(smiles, useChiral=(not ignore_chiral))
        success = True
    except:
        canon_smiles = smiles
        success = False
    return canon_smiles, success


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config",
                        default="modules/chemscraper/metrics/test_config.ini",
                        type=str)
    parser.add_argument("-t", "--thresholds",
                        default="modules/chemscraper/data/thresholds-indigo.ini",
                        type=str)
    parser.add_argument("-i", "--input_path", default="", type=str)
    parser.add_argument("-l", "--lg_path", 
                        default= "modules/qdgga-parser/data/generated-dataset/chem/train/lgs/line", 
                        type=str)
    parser.add_argument("-id", "--id_index_in_file", default="", type=str)
    parser.add_argument("-sm", "--smiles_index_in_file", default="", type=str)
    parser.add_argument("-o", "--output_path", default="", type=str)
    parser.add_argument("-gs", "--gen_smiles", default="", type=str)
    parser.add_argument("-gi", "--gen_images", default="", type=str)
    parser.add_argument("-gd", "--gen_dot", default="", type=str)
    parser.add_argument("-gl", "--gen_lg", default="", type=str)
    parser.add_argument("-svg", "--gen_svg", default="", type=str)
    parser.add_argument("-v", "--visualize", default="", type=str, 
                        help="Save visual graph from gdraw")
    
    parser.add_argument("-p", "--parallel", default="", type=str)
    parser.add_argument("-n", "--run_name", default="", type=str)
    parser.add_argument("-pm", "--parser_mode", default="BORN_DIGITAL", type=str)
    parser.add_argument('-an', '--atom_numbers', type=int, default=0, help="1 to use the RDKit renderer to test with atom numbers.")
    parsed_args = parser.parse_args()
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(parsed_args.config)
    thresholds = ConfigParser(interpolation=ExtendedInterpolation())
    thresholds.read(parsed_args.thresholds)
    parsed_args.config = config
    parsed_args.thresholds = thresholds
    return parsed_args


def get_pdf_from_smiles_rdkit(smiles, outpath, id):
    pdf_buffer, _, _ = render_rdkit_image(smiles, outpath, id, "pdf", None, None, None)
    return pdf_buffer, None

def get_pdf_from_smiles(smiles, outpath, id, write_image, label_mode, implicit_hydrogens_visible, relative_thickness):
    ext = "pdf"
    indigo = Indigo()    
    renderer = IndigoRenderer(indigo)
    
    indigo.setOption('render-background-color', '1,1,1')
    indigo.setOption('render-stereo-style', 'none')    
    indigo.setOption('render-label-mode', 'all')
    indigo.setOption('render-font-family', 'Arial')
    
    indigo.setOption('render-label-mode', label_mode)    
    indigo.setOption('render-implicit-hydrogens-visible', implicit_hydrogens_visible)
    indigo.setOption('render-relative-thickness', relative_thickness)
    
    
    indigo.setOption('render-output-format', ext)
    molecule = indigo.loadMolecule(smiles)                
    
    pdf_buffer = renderer.renderToBuffer(molecule)
    # Weird: coordinates can be accessed inside get_graph only after the above line
    graph = get_graph(molecule)
    if write_image:
        with open(os.path.join(outpath,id,"visualize_ground_truth.pdf"),"wb+") as f:
            f.write(pdf_buffer) 

    # indigo.setOption('render-output-format', "png")
    # buf = renderer.renderToBuffer(molecule)
    # img = cv2.imdecode(np.asarray(bytearray(buf), dtype=np.uint8), 1)  # decode buffer to image
    # return pdf_buffer, graph, img
    return pdf_buffer, graph

def get_graph(mol):
    mol.layout()
    coords, symbols = [], []
    index_map = {}
    atoms = [atom for atom in mol.iterateAtoms()]
    for i, atom in enumerate(atoms):
        x, y = atom.coords()
        coords.append([x, y])
        symbols.append(atom.symbol())
        index_map[atom.index()] = i
    n = len(symbols)
    edges = np.zeros((n, n), dtype=int)
    for bond in mol.iterateBonds():
        s = index_map[bond.source().index()]
        t = index_map[bond.destination().index()]
        # 1/2/3/4 : single/double/triple/aromatic
        edges[s, t] = bond.bondOrder()
        edges[t, s] = bond.bondOrder()
        if bond.bondStereo() in [5, 6]:
            edges[s, t] = bond.bondStereo()
            edges[t, s] = 11 - bond.bondStereo()
    graph = {
        'coords': coords,
        'symbols': symbols,
        'edges': edges,
        'num_atoms': len(symbols)
    }
    return graph

def predict_born_digital(params,id, smiles_gt):
    row, id_index_in_file, smiles_index_in_file, output_path, gen_dot,\
    gen_images, gen_lg, thresholds, label_mode, implicit_hydrogens_visible,\
        relative_thickness, PARSER_MODE, gen_svg_param, path_lg, _, use_rdkit = params
    if use_rdkit: # NOTE: when using rdkit `gt_graph` is `None`. Certain visualizations may not work.
        pdf_buffer, gt_graph = get_pdf_from_smiles_rdkit(smiles_gt, output_path, id)
    else:
        pdf_buffer, gt_graph = get_pdf_from_smiles(smiles_gt, output_path, id, gen_images, label_mode, implicit_hydrogens_visible, relative_thickness)
    pdf_io = io.BytesIO(pdf_buffer)
    json_content = process_sscraper_one_pdf(id, pdf_io) 
    json_dict = json.loads(json_content)
    #print("json_dict",json_dict)
    height = json_dict["pages"][0]["BBOX"]["height"]
    width = json_dict["pages"][0]["BBOX"]["width"]
    page_size_map = {
        "0":(height,width)
    }
    bbox = [[(0,0,width,height)]]
    
        
    all_pdf_msts, _, mst_stats = process_combine_one_file(io.BytesIO(json_content),
                                                page_size_map, bbox, id,
                                                thresholds)  
    return all_pdf_msts,gt_graph,mst_stats 

def predict_visual(lg_path, id, thresholds, smiles_gt, params, gen_lg,
                   visualize, output_path):    
    file = os.path.join(lg_path, f"{id}.lg")
    all_pdf_msts = lg_eval_to_nx((file, visualize, output_path, thresholds,
                                  False, None, None, 2500))

    if gen_lg:
        _, _, _, output_path, _,\
        gen_images, gen_lg, thresholds, label_mode, implicit_hydrogens_visible,\
            relative_thickness, _, _, _, _, _ = params
        # PP: For now, ignore the "-an" flag on the visual side.
        _, gt_graph = get_pdf_from_smiles(smiles_gt, output_path, id, gen_images, 
                                          label_mode, implicit_hydrogens_visible,
                                          relative_thickness)        
    else:
        gt_graph = None
    return all_pdf_msts, gt_graph


def generate_smiles_dict(params):
    global CURR_COUNT, MAX_COUNT
    row, id_index_in_file, smiles_index_in_file, output_path, gen_dot,\
    gen_images, gen_lg, thresholds, label_mode, implicit_hydrogens_visible,\
    relative_thickness, PARSER_MODE, gen_svg, lg_path, visualize, use_rdkit = params
    #PARSER_MODE is VISUAL or BORN_DIGITAL
    smiles_data = {}
    id = f"{row[id_index_in_file]}"
    smiles_data[id] = {}
    mst_stats = {}
    if not os.path.exists(os.path.join(output_path, id)):
        os.makedirs(os.path.join(output_path, id))

    smiles_gt = row[smiles_index_in_file]
    smiles_gt_can = canonicalize_smiles(smiles_gt)[0]

    smiles_data[id]["ground_truth"] = smiles_gt
    smiles_data[id]["ground_truth_canonicalized"] = smiles_gt_can

    if not smiles_gt:
        print(f"\n>> {id}: Empty SMILES String. Skipping pdf {id}")
        smiles_data[id]["error"] = "Input error: Empty smiles string"
        return smiles_data        

    try:
        if PARSER_MODE == "VISUAL":
            all_pdf_msts, gt_graph = predict_visual(lg_path, id, thresholds,
                                                    smiles_gt, params, gen_lg,
                                                    visualize, output_path)
        elif PARSER_MODE == "BORN_DIGITAL":
            all_pdf_msts, gt_graph, mst_stats = predict_born_digital(params,id, smiles_gt)
            mst_stats = mst_stats[id][0][0]
        else:
            raise Exception(f"[generate_smiles_dict]: Invalid PARSER_MODE={PARSER_MODE}")

                                        
        pred_graph = all_pdf_msts[id][0][0]
        if gen_dot:
            nx.drawing.nx_pydot.write_dot(pred_graph, os.path.join(output_path, id, "graph.dot"))
        # pcheck('Final graph:', vgdescr(pred_graph))
        # import pdb; pdb.set_trace()

        gt_nx_graph = None
        if gen_lg:
            if (len(gt_graph["symbols"]) != len(pred_graph.nodes())):
                # print(f"\n>> {id}: Different number of nodes in ground truth and"
                #       f" predicted graph")
                smiles_data[id]["lg"] = "Graph to lg error: Different number of nodes"
                if "+" in smiles_gt or "-" in smiles_gt:
                    smiles_data[id]["lg"] += " (has charge)"
                      # \nLg file not generated")
                # smiles_data[id]["hasError"] = True
                # return smiles_data
            gt_graph, pred_graph = annotate_gt_pred_graphs(gt_graph, pred_graph)
            gt_nx_graph = create_nx_gt_graph(gt_graph)
            # else:
            gt_lg_path = os.path.join(output_path, "gt_lg")
            pred_lg_path = os.path.join(output_path, "pred_lg")
            generate_lg(gt_nx_graph, gt_lg_path, id)
            generate_lg(pred_graph, pred_lg_path, id)
           
        
        errors = {}
        visual2cdxml(all_pdf_msts, out_dir=output_path,
                     parallel=False, errors_dict=errors, verbose=False)
        if gen_svg:
            svg_cdxml_path = os.path.join(output_path, "svg_cdxmls")
            os.makedirs(svg_cdxml_path, exist_ok=True)
            visual2cdxml(dpr_vgraph_dict=all_pdf_msts, out_dir=svg_cdxml_path, 
                         parallel=False, svg=True, verbose=False)                        
            

        smiles_data[id].update(mst_stats)
        if errors:
            # smiles_data[id]["hasError"] = True
            smiles_data[id]["error"] = f"Graph2cdxml error: {errors[id]}"
            print(f"\n>> {id}: Graph to CDXML error")
    except Exception as e:
        # smiles_data[id]["hasError"] = True
        smiles_data[id]["error"] = f"Smiles2cdxml error: {str(e)}"
        print(f"\n>> {id}: SMILES to CDXML error")
        import traceback
        traceback.print_exc()
        # import pdb; pdb.set_trace()

        # print(e)

    # HACK: Track progress using global thread-locked variable CURR_COUNT
    with CURR_COUNT.get_lock():
        CURR_COUNT.value += 1
        percentage = CURR_COUNT.value / MAX_COUNT 
        if CURR_COUNT.value < MAX_COUNT:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} molecules processed [ {percentage:3.2%} ]                    \r', end='')
        else:
            print(f'>>> {CURR_COUNT.value} / {MAX_COUNT} molecuels processed [ {percentage:3.2%} ]                    \n\n')
        #time.sleep(0.25) # for debugging
    return smiles_data

def generate_lg(graph, output_path, filename):
    if nx.is_directed(graph):
        directed = True
    else:
        directed = False

    output_file = os.path.join(output_path, filename + ".lg")
#    Objects (O): 9
    outString = f"# IUD, {filename}\n"

    outString += f"# [ OBJECTS ]\n"
    outString += f"#    Objects (O): {len(graph.nodes())}\n"
    outString += "### Format: O, objId, class, 1.0 (weight), [ primitiveId list ]\n"

    prefix = "O, "
    middle = ", 1.0, "

    boxString = "\n# [OBJECT FEATURES]\n"
    boxPrefix = "#sym, "
    # Sort left-right top-down
    sorted_nodes = sorted(sorted(graph.nodes(data=True), key=lambda x:x[1]['new_pos'][1]), 
                          key=lambda x:x[1]['new_pos'][0])
    for node, data in sorted_nodes:
        label = data["label"]
        # prims = ", ".join(str(x) for x in sorted(obj_id, key=lambda x: int(x)))

        prims = str(data["obj_id"])
        outString += prefix + node + ", " + label + middle + prims + "\n"

        x, y = data["new_pos"]
        boxString += boxPrefix + prims + f", {x}, {y}" + "\n"

    # Write relation lines
    num_edges = len(graph.edges())
    if not directed:
        num_edges *= 2

    if num_edges:
        outString += "\n### Relations\n"
        outString += f"#    Objects Relationships (R): {num_edges}\n"
        outString += "### Format: R, parentId, childId, class, 1.0 (weight)\n"
        prefix = "R, "
        suffix = ", 1.0"
        for p, c, data in sorted(graph.edges(data=True)):
            label = data["type"]
            # For undirected graphs (predicted), write the reverse edge too
            if not directed:
                # For wedge bonds, use the updated direction and label
                if "dir_edge" in data:
                    p, c = data["dir_edge"]
                    # Stereo chemistry rule: Hash(6) + Solid(5) = 11
                    label2 = bond_int2str[11 - bond_str2int[label]]
                else:
                    label2 = label
                outString += prefix + str(p) + ", " + str(c) + ", " + label + suffix + "\n"
                outString += prefix + str(c) + ", " + str(p) + ", " + label2 + \
                        suffix + "\n"
            else:
                outString += prefix + str(p) + ", " + str(c) + ", " + label + suffix + "\n"

    outString += boxString

    with open(output_file, "w") as f:
        f.write(outString)

def get_label(label):
    label = str_remove_suffix(label)
    final_label = Parser_v2.remove_hydrogen(label)
    if not final_label:
        return label[0]
    return final_label

def annotate_gt_pred_graphs(gt_graph, pred_graph):
    # Ground truth graph - sort and rearrange attributes
    gt_nodes = gt_graph["coords"]
    # Sort left-right, top-down order
    gt_nodes = sorted(sorted(enumerate(gt_nodes), key=lambda x:x[1][1]), key=lambda x:x[1][0])
    gt_nodes_idx = list(map(itemgetter(0), gt_nodes))
    
    gt_graph["symbols"] = list(map(lambda idx:gt_graph["symbols"][idx], gt_nodes_idx))
    gt_graph["edges"] = gt_graph["edges"][gt_nodes_idx][:, gt_nodes_idx]
    gt_nodes_points = np.array(list(map(itemgetter(1), gt_nodes)))
    gt_graph["coords"] = gt_nodes_points.tolist()
    # import pdb; pdb.set_trace()
    
    # Predicted graph - sort, annotate positions 
    pred_graph = create_pos_from_bbox(pred_graph)
    pred_nodes = list(nx.get_node_attributes(pred_graph, 'new_pos').items())
    pred_nodes = sorted(sorted(pred_nodes, key=lambda x:x[1][1]), key=lambda
                      x:x[1][0])
    # Compute and Annotate atom labels for predicted graph
    pred_nodes_labels = list(map(itemgetter(0), pred_nodes))
    
    pred_atom_labels = list(map(get_label, pred_nodes_labels))
    pred_atom_labels = dict(zip(pred_nodes_labels, pred_atom_labels))
    nx.set_node_attributes(pred_graph, pred_atom_labels, "label")

    # Annotate obj ids for predicted graph
    obj_ids = dict(zip(pred_nodes_labels, range(len(pred_nodes))))
    nx.set_node_attributes(pred_graph, obj_ids, "obj_id")
    pred_nodes_points = np.array(list(map(itemgetter(1), pred_nodes)))

    # Get corresponding nodes from predicted graph for gt graph and update gt
    # graph node symbols
    _, closest_indices = get_closest_points(gt_nodes_points, pred_nodes_points)
    gt_obj_ids = closest_indices[:, 0]

    num_unique_gt_nodes = len(set(gt_obj_ids))
    new_id = max(gt_obj_ids) + 1
    # If more than one gt nodes assigned the same pred graph node
    # usually, wehn gt gtaph has fewer nodes than pred graph,
    # we add extra nodes for gt graph, so that fair evaluation
    if len(gt_obj_ids) > num_unique_gt_nodes:
        # Find repeated values and their indices
        unique_values, counts = np.unique(gt_obj_ids, return_counts=True)
        repeated_values = unique_values[counts > 1]
        for value in repeated_values:
            indices = np.where(gt_obj_ids == value)[0]
            for idx in indices[1:]:
                gt_obj_ids[idx] = new_id
                new_label = str_remove_suffix(pred_nodes_labels[value]) +\
                                f"_{new_id+1}"
                pred_nodes_labels.append(new_label)
                new_id += 1

    gt_graph["obj_ids"] = gt_obj_ids
    gt_new_symbols = list(map(lambda idx:pred_nodes_labels[idx],
                              gt_graph["obj_ids"]))
    gt_graph["new_symbols"] = gt_new_symbols
    return gt_graph, pred_graph
        
def create_nx_gt_graph(gt_graph):
    nx_graph = nx.DiGraph()
    # Nodes
    symbols = gt_graph["new_symbols"]
    nx_graph.add_nodes_from(symbols)
    pos = dict(zip(symbols, gt_graph["coords"]))
    obj_ids = dict(zip(symbols, gt_graph["obj_ids"]))
    # labels = dict(zip(symbols, gt_graph["symbols"]))
    new_labels = dict(zip(symbols, map(get_label, symbols)))
    
    nx.set_node_attributes(nx_graph, pos, "new_pos")
    nx.set_node_attributes(nx_graph, obj_ids, "obj_id")
    # nx.set_node_attributes(nx_graph, labels, "label")
    nx.set_node_attributes(nx_graph, new_labels, "label")

    # nodes = dict(zip(gt_graph["new_symbols"], gt_graph["coords"]))

    # Edges
    # Annotate gt graph: convert adjacency matrx to bond types
    adj_matrix = gt_graph["edges"]
    # Extract the pairs of edges
    edges = np.argwhere(adj_matrix >= 1)
    bond_types = adj_matrix[edges[:, 0], edges[:, 1]].reshape(-1, 1).flatten()
    for i, (p, c) in enumerate(edges):
        nx_graph.add_edge(symbols[p], symbols[c], type=bond_int2str[bond_types[i]])

    return nx_graph

get_mid = lambda x: [(x[0] + x[2]) * 0.5, (x[1] + x[3]) * 0.5]

def create_pos_from_bbox( G ):
    # Use mid point for nodes with bbox (char), pos for others
    for (node_id, attr_dict) in G.nodes.data():
        if 'bbox' in G.nodes[node_id]:
            bbox = attr_dict['bbox']
            new_pos = get_mid(bbox)
        else:
            new_pos = G.nodes[node_id]['pos']
        G.nodes[node_id]['new_pos'] = new_pos
    return G

def get_closest_points(points1, points2):
    """
    Get distances of points and closest points of one source for each point of
    another source
    """
    # Calculate the distances between each pair of points
    # Reshape the arrays to make use of broadcasting
    # Calculate the squared Euclidean distances
    distances = np.sum((points1[:, np.newaxis] - points2) ** 2, axis=2)
    # Take the square root to get the actual distances
    closest_distances = np.sqrt(distances)
    # closest_distances[i, j] contains the distance between points1[i] and points2[j]
    
    closest_points2_indices_per_points1 = np.argsort(closest_distances, axis=1)
    return closest_distances, closest_points2_indices_per_points1

def main():
    global CURR_COUNT, MAX_COUNT
    parsed_args = parse_args()
    config = parsed_args.config
    lg_path = parsed_args.lg_path
            
    label_mode = config.get("rendering", "LABEL_MODE")
    implicit_hydrogens_visible = config.getboolean("rendering", "IMPLICIT_HYDROGENS_VISIBLE")
    relative_thickness = config.getfloat("rendering", "RELATIVE_THICKNESS")


    thresholds_dict = {
        "REMOVE_ALPHA":config.getfloat("thresholds", "REMOVE_ALPHA"),
        "NEG_CHARGE_Y_POSITION":config.getfloat("thresholds", "NEG_CHARGE_Y_POSITION"),
        "NEG_CHARGE_LENGTH_TOLERANCE":config.getfloat("thresholds", "NEG_CHARGE_LENGTH_TOLERANCE"),
        "Z_TOLERANCE":config.getfloat("thresholds", "Z_TOLERANCE"),
        "CLOSE_NONPARALLEL_ALPHA":config.getfloat("thresholds", "CLOSE_NONPARALLEL_ALPHA"),
        "CLOSE_CHAR_LINE_ALPHA":config.getfloat("thresholds", "CLOSE_CHAR_LINE_ALPHA"),

        "LONGEST_LENGTHS_DIFF_RATIO":config.getfloat("thresholds", "LONGEST_LENGTHS_DIFF_RATIO"),
        "PARALLEL_TOLERANCE":config.getfloat("thresholds", "PARALLEL_TOLERANCE"),        
        "COS_PRUNE":config.getfloat("thresholds", "COS_PRUNE"),
        "HASHED_LENGTH_DIFF_THRESHOLD":config.getfloat("thresholds", "HASHED_LENGTH_DIFF_THRESHOLD"),
    }
        
    input_path = config.get("input","input_path")
    id_index_in_file = config.getint("input","id_index_in_file")
    smiles_index_in_file = config.getint("input","smiles_index_in_file")
    run_name = config.get("output","run_name")
    output_path = config.get("output","output_path")
    gen_smiles = config.getboolean("output","gen_smiles")
    gen_images = config.getboolean("output","gen_images")
    gen_dot = config.getboolean("output","gen_dot")
    parallel = config.getboolean("output","parallel")
    gen_lg = config.getboolean("output","gen_lg")    
    gen_svg = config.getboolean("output", "gen_svg")   
    visualize = config.getboolean("output", "visualize")

    # Read from command line args if available
    if parsed_args.input_path:
        input_path = parsed_args.input_path
    if parsed_args.id_index_in_file:
        id_index_in_file = int(parsed_args.id_index_in_file)
    if parsed_args.smiles_index_in_file:
        smiles_index_in_file = int(parsed_args.smiles_index_in_file)
    if parsed_args.output_path:
        output_path = parsed_args.output_path
    if parsed_args.gen_smiles:
        gen_smiles = bool(int(parsed_args.gen_smiles))
    if parsed_args.gen_images:
        gen_images = bool(int(parsed_args.gen_images))
    if parsed_args.gen_lg:
        gen_lg = bool(int(parsed_args.gen_lg))
    if parsed_args.gen_svg:
        gen_svg = bool(int(parsed_args.gen_svg))
    if parsed_args.parallel:
        parallel = bool(int(parsed_args.parallel))
    if parsed_args.visualize:
        visualize = bool(int(parsed_args.visualize))
    if parsed_args.parser_mode:
        PARSER_MODE = parsed_args.parser_mode
    if parsed_args.run_name:
        run_name = parsed_args.run_name            
    use_rdkit = False # Default
    if parsed_args.atom_numbers:
        use_rdkit = bool(int(parsed_args.atom_numbers))


    acum_lev = 0
    acum_tani = 0
    acum_lev_normalized = 0
    count = 0
    count_errors = 0
    exact_matches = 0
    not_exact_matches_ids = []
    smiles_data = {}
    dTimer.qcheck("Initial module load")
        
    with open(input_path) as csv_file:
        file_content = csv.reader(csv_file, delimiter=',')
        next(file_content, None) #skip header        
        file_content = list(file_content)
        print("\nTotal molecules (files):",len(file_content))

        gt_lg_path = os.path.join(output_path, "gt_lg")
        pred_lg_path = os.path.join(output_path, "pred_lg")
        os.makedirs(gt_lg_path, exist_ok=True)
        os.makedirs(pred_lg_path, exist_ok=True)

        # Collect metrics for files (map-reduce), and labels
        # HACK: Set global count
        MAX_COUNT=len(file_content)
        if parallel:
            input_list = list(zip(file_content, repeat(id_index_in_file),
                                repeat(smiles_index_in_file),
                                repeat(output_path), repeat(gen_dot),
                                repeat(gen_images), repeat(gen_lg),
                                repeat(thresholds_dict),
                                repeat(label_mode),repeat(implicit_hydrogens_visible),
                                repeat(relative_thickness), repeat(PARSER_MODE),
                                  repeat(gen_svg), repeat(lg_path), repeat(visualize), repeat(use_rdkit)))
            smiles_data_ = map_reduce( generate_smiles_dict, combine_dict_pair, input_list )
        else:
            smiles_data_ = {}
            for row in file_content:
                smiles_data.update(generate_smiles_dict((row, id_index_in_file, smiles_index_in_file,
                                                        output_path, gen_dot,
                                                        gen_images, gen_lg,
                                                        thresholds_dict, label_mode, 
                                                        implicit_hydrogens_visible,
                                                         relative_thickness,
                                                         PARSER_MODE,gen_svg,
                                                         lg_path, visualize, use_rdkit)))
        smiles_data = {**smiles_data, **smiles_data_}
        
    dTimer.qcheck("Parsing done")
    
    if gen_smiles:    
        cdxmls_paths_dict = generate_smiles_molconvert(output_path)
        dTimer.qcheck("Molconvert done")
        
        for id, value in cdxmls_paths_dict.items():        
            smiles_data[id]["predicted"] = value
            smiles_data[id]["predicted_canonicalized"] = canonicalize_smiles(value)[0]
            tanimoto = tanimoto_similarity(smiles_data[id]["predicted_canonicalized"], smiles_data[id]["ground_truth_canonicalized"])
            levenshtein_normalized = levenshtein_similarity_normalized(smiles_data[id]["predicted_canonicalized"], smiles_data[id]["ground_truth_canonicalized"])
            levenshtein = levenshtein_similarity(smiles_data[id]["predicted_canonicalized"], smiles_data[id]["ground_truth_canonicalized"])
            smiles_data[id]["tanimoto"] = tanimoto
            smiles_data[id]["levenshtein_normalized"] = levenshtein_normalized
            smiles_data[id]["levenshtein"] = levenshtein
            # if levenshtein_normalized == 1:
            if levenshtein == 0:
                exact_matches += 1
            else:
                not_exact_matches_ids.append(id)    
            acum_lev += levenshtein
            acum_lev_normalized += levenshtein_normalized
            acum_tani += tanimoto
            count += 1

        dTimer.qcheck("Canonicalization done")

    if gen_svg:        
        svg_cdxml_path = os.path.join(output_path, "svg_cdxmls")
        cdxml2svg_bulk(svg_cdxml_path, parallel=parallel)
        dTimer.qcheck("SVG Generation done")

    print()
    print(dTimer)

    
    pandas_smiles_meta = pd.DataFrame.from_dict(smiles_data, orient="index")    
    pandas_smiles_meta.to_csv(os.path.join(output_path, f"smiles_meta_{run_name}.csv"), index=True)  

    # with open(os.path.join(output_path, "smiles_meta.json"),"w+") as outfile:
    #     outfile.write(json.dumps({run_name:smiles_data}))  
    error_files = list(filter(lambda x: "error" in x[1], smiles_data.items()))
    
    # error_files = list(map(itemgetter(0), error_files))
    count_errors = len(error_files)
    print("count: ",count)
    print("Total molecules (files): ",str((count+count_errors)))
    print(f"Tanimoto {acum_tani/(count+count_errors)}")
    print(f"Lev {acum_lev/(count+count_errors)}")
    print(f"Lev normalized {acum_lev_normalized/(count+count_errors)}")
    print("Exact matches:", exact_matches)
    print("Incorrectly parsed:", count - exact_matches)
    print(f"Fatal errors (files failed): {count_errors}")


    memory_KB = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    head = dTimer.entries[0]
    tail = dTimer.entries[-1]
    metrics_object = {        
        "total_files": count+count_errors,
        "levenshtein": acum_lev/(count+count_errors),
        "levenshtein_normalized": acum_lev_normalized/(count+count_errors),
        "tanimoto":acum_tani/(count+count_errors),
        "exact_matches": exact_matches,
        "time":tail[1] - head[1],
        "memory_KB": memory_KB,      

        "fatal_errors": count_errors,
        "Incorrectly parsed": (count - exact_matches)
    }
    metadata = {}
    metadata[run_name]=metrics_object

    print(f"Memory usage: {memory_KB} KB")

    lg_errors = list(filter(lambda x: "lg" in x[1], smiles_data.items()))
    with open(os.path.join(output_path,"exact_matches_errors_ids.json"), "w") as f:
        f.write(json.dumps({run_name:not_exact_matches_ids}, indent=4))
    with open(os.path.join(output_path,"fatal_errors_ids.json"), "w") as f:
        f.write(json.dumps(dict({run_name:error_files}, indent=4)))
    with open(os.path.join(output_path,"different_no_of_nodes.json"), "w") as f:
        f.write(json.dumps(dict({run_name:lg_errors}, indent=4)))        
    with open(os.path.join(output_path,"metadata.json"), "w") as f:
        f.write(json.dumps(dict(metadata), indent=4))
    print(f"\nOuptut results path: {output_path}")
    
def levenshtein_similarity_normalized(smiles1, smiles2):
    lower_bound = abs(len(smiles1) - len(smiles2))
    upper_bound = max(len(smiles1), len(smiles2))
    range_val = upper_bound - lower_bound
    # TODO: To avoid crash currently, update this by better solution
    if range_val == 0:
        return 1.0 if smiles1 == smiles2 else 0.0
    distance_val = distance(smiles1, smiles2)
    normalized_val = (distance_val - lower_bound) / range_val
    return 1 - normalized_val

def run_one_pdf(pdf_path):
    parsed_args = parse_args()
    config = parsed_args.config


    thresholds_dict = {
        "REMOVE_ALPHA":config.getfloat("thresholds", "REMOVE_ALPHA"),
        "NEG_CHARGE_Y_POSITION":config.getfloat("thresholds", "NEG_CHARGE_Y_POSITION"),
        "NEG_CHARGE_LENGTH_TOLERANCE":config.getfloat("thresholds", "NEG_CHARGE_LENGTH_TOLERANCE"),
        "Z_TOLERANCE":config.getfloat("thresholds", "Z_TOLERANCE"),
        "CLOSE_NONPARALLEL_ALPHA":config.getfloat("thresholds", "CLOSE_NONPARALLEL_ALPHA"),
        "CLOSE_CHAR_LINE_ALPHA":config.getfloat("thresholds", "CLOSE_CHAR_LINE_ALPHA"),

        "LONGEST_LENGTHS_DIFF_RATIO":config.getfloat("thresholds", "LONGEST_LENGTHS_DIFF_RATIO"),
        "PARALLEL_TOLERANCE":config.getfloat("thresholds", "PARALLEL_TOLERANCE"),        
        "COS_PRUNE":config.getfloat("thresholds", "COS_PRUNE"),
        "HASHED_LENGTH_DIFF_THRESHOLD":config.getfloat("thresholds", "HASHED_LENGTH_DIFF_THRESHOLD"),
    }    

    with open(pdf_path) as f:
    #    pdf_io = io.BytesIO(f)
        pdf_io = f

    id = os.path.basename(pdf_path)[:-4]
    
    with open(pdf_path,"rb") as pdf_io:
        json_content = process_sscraper_one_pdf(id, pdf_io.read()) 
        json_dict = json.loads(json_content)
        #print("json_dict",json_dict)
        height = json_dict["pages"][0]["BBOX"]["height"]
        width = json_dict["pages"][0]["BBOX"]["width"]
        page_size_map = {
            "0":(height,width)
        }
        bbox = [[(0,0,width,height)]]
        all_pdf_msts, _, mst_stats = process_combine_one_file(io.BytesIO(json_content),
                                                    page_size_map, bbox, id, thresholds_dict)
        msts_copy = copy.deepcopy(all_pdf_msts)
         
        pred_graph = all_pdf_msts[id][0][0]        
        out_path = "DEBUG/original_parser"
        converter = visual2cdxml(dpr_vgraph_dict=all_pdf_msts, 
                                out_dir=out_path, metrics={},
                                 write_pages_out=1)
        visual2cdxml(dpr_vgraph_dict=msts_copy, out_dir=out_path, svg=True)
        out_svg_dirs = list(glob(os.path.join(out_path, "*_SVG")))

        for out_svg_dir in out_svg_dirs:
            cdxml2svg(out_svg_dir)

def get_smiles_from_paths(params):
    paths,id = params    
    pdf_smiles = []
    if len(paths) == 0:
        return {}
    try:
        #smiles = subprocess.run('molconvert smiles ' + os.path.join(output_path,"*","*.cdxml"),
        #                        shell=True, stdout=subprocess.PIPE)
        stderr_file_name = 'STDERR_Molconvert_SMILES'
        err_file = open(stderr_file_name, 'w')
        smiles = subprocess.run('molconvert smiles ' + ' '.join(paths),
                                shell=True, stdout=subprocess.PIPE, stderr=err_file)
        err_file.close()
    except Exception as e:        
        import traceback
        traceback.print_exc()
        return {id: ""}
    _pdf_smiles = smiles.stdout.decode('utf-8').split('\n')
    if not len(_pdf_smiles):
        raise Exception('Error in SMILES conversion. Please check CDXML Conversion')            
    
    pdf_smiles = _pdf_smiles[:-1]
    return {id: pdf_smiles}

def generate_smiles_molconvert(output_path, chunk_size=100):
    print('\n [ CONVERTING CDXMLs to SMILES (Molconvert) ... ]')
    stderr_file_name = 'STDERR_Molconvert_SMILES'
    print('* Redirecting standard error to:  ' +  stderr_file_name)
    cdxmls_paths = sorted(glob(os.path.join(output_path,"*","*.cdxml")), key=lambda x:os.path.basename(os.path.dirname(x)))
    
    chunks = math.ceil(len(cdxmls_paths)/chunk_size)
    chunks_list = [cdxmls_paths[i*chunk_size : (i+1)*chunk_size] for i in range(chunks+1)]
    print(f"* Number of CDXMLs to convert: {len(cdxmls_paths)}")
    print(f"* Total number of Chunks (chumk size = 100): {chunks}")    
        
    input_list = list(zip(chunks_list, list(range(chunks+1))))
        
    pdf_smiles_dict = map_reduce(get_smiles_from_paths, combine_dict_pair,
                                 input_list)
    pdf_smiles = [item for row in [x[1] for x in sorted(list(pdf_smiles_dict.items()), key=lambda x:x[0])] for item in row]

    cdxmls_paths_dict = dict(zip( map(lambda x: os.path.basename(os.path.dirname(x)), cdxmls_paths),pdf_smiles))
    return cdxmls_paths_dict


def cdxml2svg(cdxml_dir):
    print('* CDXML/SVG dir: ', (cdxml_dir))
    # print('  SVG dir: ', pdf_svg_dir )
    pattern = os.path.join(os.getcwd(), cdxml_dir, 'Page_*.cdxml')
    # cdxmls = [f for f in cdxmls if f.endswith('.cdxml')]

    # Routing Java exceptions for font sizes in points to /dev/null
    stderr_file_name = 'STDERR_Molconvert_SVG'
    err_file = open(stderr_file_name, 'w')
    # NOTE: Molconvert will only resize atoms based on bond length (atsiz)
    #       if font sizes cannot be interpreted (e.g., 'pt' in sizes)
    svgs = subprocess.run('molconvert "svg:scale28" ' + pattern,
                        shell=True, stdout=subprocess.PIPE, stderr=err_file)
    svg_string = svgs.stdout.decode('utf-8')
    err_file.close()

    pattern = r'(<\?xml version="1\.0"\?>.*?<\/svg\s*>)'
    pdf_svgs = re.findall(pattern, svg_string, re.DOTALL)

    f = open(os.path.join(cdxml_dir, "Page_001_No001.svg"), 'w')
    svg = pdf_svgs[0]
    f.write(svg)
    f.close()
    print('  SVG generation complete\n')
    return []


if __name__ == "__main__":
    main()
    # run_one_pdf("DEBUG/test_pdfs/US07314883-20080101-C0055_FALSE.pdf")
    # run_one_pdf("DEBUG/test_pdfs/US07314883-20080101-C0055_TRUE.pdf")

    
