
import pandas as pd
import argparse
import numpy as np
import bisect

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_file",
                        default="outputs/indigo_pubchem_small_BORN_DIGITAL/smiles_meta_indigo_pubchem_small_BORN_DIGITAL.csv",
                        type=str)
    parser.add_argument("-o", "--output_file",
                        default="outputs/indigo_pubchem_small_BORN_DIGITAL/quartiles_meta_data.json",
                        type=str)
    parser.add_argument("-n", "--num_percentiles",
                        default=8,
                        type=int)
    parsed_args = parser.parse_args()
    return parsed_args

def compute_confidence(graph_size, percentiles):
    confidence = 100-((np.searchsorted(percentiles, graph_size["graph_size"]))*100/len(percentiles))
    return confidence


def main():
    pd.options.mode.chained_assignment = None
    args = parse_args()
    pd_data = pd.read_csv(args.input_file)
    # pd_data_original = pd.read_csv(args.input_file)
    # pd_data = pd_data_original[(pd_data_original["levenshtein"]==0)]
    pd_data["graph_size"] = pd_data["num_nodes"]+pd_data["num_edges"]
    graph_sizes = np.array(pd_data.loc[:, 'graph_size'].tolist())
    
    indexes = np.arange(1,args.num_percentiles+1)
    percentiles_indexes = indexes*100/np.max(indexes)
    percentiles = np.percentile(graph_sizes,percentiles_indexes)
    
    pd_data["percentile"] = np.searchsorted(percentiles, pd_data["graph_size"])
    pd_data["confidence (%)"] = 100-((np.searchsorted(percentiles, pd_data["graph_size"]))*100/len(percentiles))

    percentiles_data = pd_data[["ID", "graph_size", "percentile","confidence (%)"]]
    percentiles_data= percentiles_data.sort_values(by=["confidence (%)"], ascending=False)
    import pdb;pdb.set_trace()
    
    

if __name__ == "__main__":
    main()