
import pandas as pd
import matplotlib.pyplot as plt
from modules.chemscraper.utils.indigo import Indigo
from modules.chemscraper.utils.indigo.renderer import IndigoRenderer
import os

def main():
    file_path = "DEBUG/GRID_PARAMS_CORRECTED/all_data.csv"
    

    data = pd.read_csv(file_path)
    data.rename(columns={'Unnamed: 0': 'id_render'}, inplace=True)
    data.set_index('id_render')

    # data2 = data[["id_render","id_molecule","label_mode","implicit_hydrogens_visible","relative_thickness","levenshtein_normalized"]]
    # correct = data2[(data2.label_mode=="terminal-hetero")&(data2.implicit_hydrogens_visible==True)&(data2.relative_thickness==1.0)&(data2.levenshtein_normalized == 1)]
    # correct = correct[["id_molecule"]].set_index('id_molecule')
    
    # incorrect = data2[(data2.label_mode=="terminal-hetero")&(data2.implicit_hydrogens_visible==False)&(data2.relative_thickness==1.0)&(data2.levenshtein_normalized != 1)]
    # incorrect = incorrect[["id_molecule"]].set_index('id_molecule')
    
    # import pdb;pdb.set_trace()
    # caca = incorrect.join(correct, on='id_molecule', how="inner")
    exact_matches = data[data.levenshtein_normalized == 1]
    grouped_exact_matches = exact_matches.groupby(["label_mode","implicit_hydrogens_visible","relative_thickness"])\
        .agg({'levenshtein_normalized':'count'})
    grouped_exact_matches = grouped_exact_matches.reset_index()
    
    grouped = data.groupby(["label_mode","implicit_hydrogens_visible","relative_thickness"])\
        .agg({'levenshtein_normalized':'mean','tanimoto':'mean'})
    grouped = grouped.reset_index()
    

    

    
    #dataset[dataset.levenshtein == 0]
    label_types = grouped[(grouped.implicit_hydrogens_visible==True) & (grouped.relative_thickness==1.0)]
    label_types = label_types[["label_mode","levenshtein_normalized","tanimoto"]]

    relative_thickness = grouped[(grouped.implicit_hydrogens_visible==True) & (grouped.label_mode=="terminal-hetero")]
    relative_thickness = relative_thickness[["relative_thickness","levenshtein_normalized","tanimoto"]]

    implicit_hydrogens_visible = grouped[(grouped.relative_thickness==1.0) & (grouped.label_mode=="terminal-hetero")]
    implicit_hydrogens_visible = implicit_hydrogens_visible[["implicit_hydrogens_visible","levenshtein_normalized","tanimoto"]]


    #grouped_exact_matches
    label_types_exact = grouped_exact_matches[(grouped_exact_matches.implicit_hydrogens_visible==True) & (grouped_exact_matches.relative_thickness==1.0)]
    label_types_exact = label_types_exact[["label_mode","levenshtein_normalized"]]

    relative_thickness_exact = grouped_exact_matches[(grouped_exact_matches.implicit_hydrogens_visible==True) & (grouped_exact_matches.label_mode=="terminal-hetero")]
    relative_thickness_exact = relative_thickness_exact[["relative_thickness","levenshtein_normalized"]]

    implicit_hydrogens_visible_exact = grouped_exact_matches[(grouped_exact_matches.relative_thickness==1.0) & (grouped_exact_matches.label_mode=="terminal-hetero")]
    implicit_hydrogens_visible_exact = implicit_hydrogens_visible_exact[["implicit_hydrogens_visible","levenshtein_normalized"]]
        
    import pdb;pdb.set_trace()

def get_pdf_from_smiles(smiles, label_mode, implicit_hydrogens_visible, relative_thickness, id):
    ext = "pdf"
    indigo = Indigo()    
    renderer = IndigoRenderer(indigo)
    
    indigo.setOption('render-background-color', '1,1,1')
    indigo.setOption('render-stereo-style', 'none')    
    indigo.setOption('render-label-mode', 'all')
    indigo.setOption('render-font-family', 'Arial')
    
    indigo.setOption('render-label-mode', label_mode)    
    indigo.setOption('render-implicit-hydrogens-visible', implicit_hydrogens_visible)
    indigo.setOption('render-relative-thickness', relative_thickness)
    
    
    indigo.setOption('render-output-format', ext)
    try:
        molecule = indigo.loadMolecule(smiles)
        # pdf_buffer = renderer.renderToBuffer(molecule)
        renderer.renderToFile(molecule, os.path.join("DEBUG/test_pdfs", f"{id}.{ext}"))
        return None
    except Exception as e:
        print(f"[get_pdf_from_smiles] SMILES:{smiles}. Error: {e}")
        return None  
    


if __name__ == "__main__":
    main()

    
    # smiles = "CC1=CC(CC(NC(=O)N2CCC(N3CC4=CC=CC=C4NC3=O)CC2)C(=O)OC2(C)CCCCC2)=CC2=C1NN=C2"
    # id = "US07314883-20080101-C0055_FALSE"
    # get_pdf_from_smiles(smiles,"terminal-hetero",False,1, id)



