
from operator import itemgetter
import modules.protables.debug_fns as debug_fns
from modules.protables.debug_fns import *
dTimer = DebugTimer("ChemScraper pipeline: synthetic dataset (indigo)")
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.

import os
import networkx as nx
import csv
import random
import numpy as np
import cv2
from modules.pipeline.pipeline_run_chemx import process_sscraper_one_pdf, process_combine_one_file, gen_smiles
from modules.chemscraper.utils.indigo import Indigo
from modules.chemscraper.utils.indigo.renderer import IndigoRenderer
from modules.chemscraper.graph_conversions.CDXML_Converter import visual2cdxml
import io
import re
import subprocess
import json
import argparse
from glob import glob
from configparser import ConfigParser, ExtendedInterpolation
from rdkit import Chem
from SmilesPE.pretokenizer import atomwise_tokenizer
from rdkit import Chem, DataStructs
from Levenshtein import distance
from tqdm import tqdm
from itertools import repeat
from modules.utils.dprl_map_reduce import combine_dict_pair, map_reduce
from modules.chemscraper.nx_debug_fns import vgdescr, str_remove_suffix
from modules.chemscraper.utils.viz_utils import imshow
import resource
import pandas as pd
import math


def parse_args():    
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--input_file", default="modules/chemscraper/data/indigo_pubchem_small.csv", type=str)
    parser.add_argument("-id", "--id_index_in_file", default=1, type=int)
    parser.add_argument("-sm", "--smiles_index_in_file", default=3, type=int)
    parser.add_argument("-o", "--output_path", default="TESTING_GRID", type=str)    
    parser.add_argument("-p", "--parallel", default=1, type=int)

    parser.add_argument("-f", "--index_from", default=0, type=int) #inclusive
    parser.add_argument("-t", "--index_to", default=5000, type=int) #inclusive

    parser.add_argument("-g", "--group", default=1, type=int)
    parsed_args = parser.parse_args()
    return parsed_args

def generate_all_molecule_instances(file_content, id_index, smiles_index, out_path, group=1): 
    #FIRST GROUP   
    PARALLEL_TOLERANCE = {
        "range":[1, 3, 5, 10, 15 ],
        "default":3
    } ### #ANGLE TOLERANCE in paper
    CLOSE_NONPARALLEL_ALPHA = {
        "range":[1, 1.25,1.5, 1.75, 2.0],
        "default":1.75
    } 
    CLOSE_CHAR_LINE_ALPHA = {
        "range":[1, 1.25,1.5, 1.75, 2.0],
        "default":1.5
    } 

    #SECOND GROUP
    LONGEST_LENGTHS_DIFF_RATIO = {
        "range":[0.70,0.85,0.90,0.95],
        "default":0.70
    } ### #S-WEDGE LENGTHS DIFF RATIO in paper
    NEG_CHARGE_Y_POSITION = {
        "range":[0, 0.25, 0.5],
        "default":0.5
    } 
    NEG_CHARGE_LENGTH_TOLERANCE = {
        "range":[0.33, 0.5, 0.66],
        "default":0.5
    }

    #THIRD GROUP
    REMOVE_ALPHA = {
        "range":[2.0,2.5,3.0],
        "default":2
    }
    Z_TOLERANCE = {
        "range":[1.0,1.5,2.0],
        "default":1.5
    }
    COS_PRUNE = {
        "range":[0.10,0.15,0.20],
        "default":0.1
    }    
        

    
    
    data= []
    counter=0
    if group == 1:
        for row in file_content:            
            id_molecule = row[id_index]
            smiles = row[smiles_index]
            label_mode = random.choice(["hetero","terminal-hetero","all"])
            implicit_hydrogens_visible = random.choice([True, False])
            relative_thickness = random.choice([0.5, 1, 1.5])
            pdf_buffer = get_pdf_from_smiles(smiles, label_mode, implicit_hydrogens_visible, relative_thickness)
            for PARALLEL_TOLERANCE_val in PARALLEL_TOLERANCE["range"]:
                for CLOSE_NONPARALLEL_ALPHA_val in CLOSE_NONPARALLEL_ALPHA["range"]:
                    for CLOSE_CHAR_LINE_ALPHA_val in CLOSE_CHAR_LINE_ALPHA["range"]:
                            data.append([counter,id_molecule, smiles, \
                                         PARALLEL_TOLERANCE_val,CLOSE_NONPARALLEL_ALPHA_val,CLOSE_CHAR_LINE_ALPHA_val,\
                                         LONGEST_LENGTHS_DIFF_RATIO["default"],NEG_CHARGE_Y_POSITION["default"],NEG_CHARGE_LENGTH_TOLERANCE["default"],\
                                         REMOVE_ALPHA["default"],Z_TOLERANCE["default"],COS_PRUNE["default"],\
                                        label_mode,implicit_hydrogens_visible,relative_thickness, out_path,pdf_buffer])
                            counter += 1
    elif group == 2:
        for row in file_content:
            id_molecule = row[id_index]
            smiles = row[smiles_index]
            label_mode = random.choice(["hetero","terminal-hetero","all"])
            implicit_hydrogens_visible = random.choice([True, False])
            relative_thickness = random.choice([0.5, 1, 1.5])
            pdf_buffer = get_pdf_from_smiles(smiles, label_mode, implicit_hydrogens_visible, relative_thickness)
            for LONGEST_LENGTHS_DIFF_RATIO_val in LONGEST_LENGTHS_DIFF_RATIO["range"]:
                for NEG_CHARGE_Y_POSITION_val in NEG_CHARGE_Y_POSITION["range"]:
                    for NEG_CHARGE_LENGTH_TOLERANCE_val in NEG_CHARGE_LENGTH_TOLERANCE["range"]:
                            data.append([counter,id_molecule, smiles, \
                                         PARALLEL_TOLERANCE["default"],CLOSE_NONPARALLEL_ALPHA["default"],CLOSE_CHAR_LINE_ALPHA["default"],\
                                         LONGEST_LENGTHS_DIFF_RATIO_val,NEG_CHARGE_Y_POSITION_val,NEG_CHARGE_LENGTH_TOLERANCE_val,\
                                         REMOVE_ALPHA["default"],Z_TOLERANCE["default"],COS_PRUNE["default"],\
                                        label_mode,implicit_hydrogens_visible,relative_thickness, out_path,pdf_buffer])
                            counter += 1
    elif group == 3:
        for row in file_content:
            id_molecule = row[id_index]
            smiles = row[smiles_index]
            label_mode = random.choice(["hetero","terminal-hetero","all"])
            implicit_hydrogens_visible = random.choice([True, False])
            relative_thickness = random.choice([0.5, 1, 1.5])
            pdf_buffer = get_pdf_from_smiles(smiles, label_mode, implicit_hydrogens_visible, relative_thickness)
            for REMOVE_ALPHA_val in REMOVE_ALPHA["range"]:
                for Z_TOLERANCE_val in Z_TOLERANCE["range"]:
                    for COS_PRUNE_val in COS_PRUNE["range"]:
                            data.append([counter,id_molecule, smiles, \
                                         PARALLEL_TOLERANCE["default"],CLOSE_NONPARALLEL_ALPHA["default"],CLOSE_CHAR_LINE_ALPHA["default"],\
                                         LONGEST_LENGTHS_DIFF_RATIO["default"],NEG_CHARGE_Y_POSITION["default"],NEG_CHARGE_LENGTH_TOLERANCE["default"],\
                                         REMOVE_ALPHA_val,Z_TOLERANCE_val,COS_PRUNE_val,\
                                        label_mode,implicit_hydrogens_visible,relative_thickness, out_path,pdf_buffer])
                            counter += 1
    else:
        #playing with parameters
        for row in file_content:
            id_molecule = row[id_index]
            smiles = row[smiles_index]
            

            label_mode_array = ["hetero","terminal-hetero","all"]
            implicit_hydrogens_visible_array = [True, False]
            relative_thickness_array = [0.5, 1, 1.5]
            for label_mode in label_mode_array:
                for implicit_hydrogens_visible in implicit_hydrogens_visible_array:
                    for relative_thickness in relative_thickness_array:
                        pdf_buffer = get_pdf_from_smiles(smiles, label_mode, implicit_hydrogens_visible, relative_thickness)
                        data.append([counter,id_molecule, smiles, \
                                            PARALLEL_TOLERANCE["default"],CLOSE_NONPARALLEL_ALPHA["default"],CLOSE_CHAR_LINE_ALPHA["default"],\
                                            LONGEST_LENGTHS_DIFF_RATIO["default"],NEG_CHARGE_Y_POSITION["default"],NEG_CHARGE_LENGTH_TOLERANCE["default"],\
                                            REMOVE_ALPHA["default"],Z_TOLERANCE["default"],COS_PRUNE["default"],\
                                            label_mode,implicit_hydrogens_visible,relative_thickness, out_path,pdf_buffer])
                        counter += 1



    return data
        
def canonicalize_smiles(smiles, ignore_chiral=False, ignore_cistrans=False, replace_rgroup=True):
    if type(smiles) is not str or smiles == '':
        return '', False
    if ignore_cistrans:
        smiles = smiles.replace('/', '').replace('\\', '')
    if replace_rgroup:
        tokens = atomwise_tokenizer(smiles)
        for j, token in enumerate(tokens):
            if token[0] == '[' and token[-1] == ']':
                symbol = token[1:-1]
                if symbol[0] == 'R' and symbol[1:].isdigit():
                    tokens[j] = f'[{symbol[1:]}*]'
                elif Chem.AtomFromSmiles(token) is None:
                    tokens[j] = '*'
        smiles = ''.join(tokens)
    try:
        canon_smiles = Chem.CanonSmiles(smiles, useChiral=(not ignore_chiral))
        success = True
    except:
        canon_smiles = smiles
        success = False
    return canon_smiles, success


def get_pdf_from_smiles(smiles, label_mode, implicit_hydrogens_visible, relative_thickness):
    ext = "pdf"
    indigo = Indigo()    
    renderer = IndigoRenderer(indigo)
    
    indigo.setOption('render-background-color', '1,1,1')
    indigo.setOption('render-stereo-style', 'none')    
    indigo.setOption('render-label-mode', 'all')
    indigo.setOption('render-font-family', 'Arial')
    
    indigo.setOption('render-label-mode', label_mode)    
    indigo.setOption('render-implicit-hydrogens-visible', implicit_hydrogens_visible)
    indigo.setOption('render-relative-thickness', relative_thickness)
    
    
    indigo.setOption('render-output-format', ext)
    try:
        molecule = indigo.loadMolecule(smiles)
        pdf_buffer = renderer.renderToBuffer(molecule)
        return pdf_buffer
    except Exception as e:
        print(f"[get_pdf_from_smiles] SMILES:{smiles}. Error: {e}")
        return None

    
    
    

def generate_smiles_dict(params):
    id, id_molecule,smiles_gt,\
        PARALLEL_TOLERANCE,CLOSE_NONPARALLEL_ALPHA,CLOSE_CHAR_LINE_ALPHA,\
        LONGEST_LENGTHS_DIFF_RATIO,NEG_CHARGE_Y_POSITION,NEG_CHARGE_LENGTH_TOLERANCE,\
        REMOVE_ALPHA,Z_TOLERANCE,COS_PRUNE,\
            label_mode, implicit_hydrogens_visible,\
        relative_thickness, output_path, pdf_buffer = params    
    # thresholds = [CLOSE_NONPARALLEL_ALPHA, 0, CLOSE_CHAR_LINE_ALPHA, 0, REMOVE_ALPHA, NEG_CHARGE_Y_POSITION, NEG_CHARGE_LENGTH_THRESHOLD, Z_TOLERANCE]
    thresholds_dict = {
        "REMOVE_ALPHA":REMOVE_ALPHA,
        "NEG_CHARGE_Y_POSITION":NEG_CHARGE_Y_POSITION,
        "NEG_CHARGE_LENGTH_TOLERANCE":NEG_CHARGE_LENGTH_TOLERANCE,
        "Z_TOLERANCE":Z_TOLERANCE,
        "CLOSE_NONPARALLEL_ALPHA":CLOSE_NONPARALLEL_ALPHA,
        "CLOSE_CHAR_LINE_ALPHA":CLOSE_CHAR_LINE_ALPHA,

        "LONGEST_LENGTHS_DIFF_RATIO":LONGEST_LENGTHS_DIFF_RATIO,
        "PARALLEL_TOLERANCE":PARALLEL_TOLERANCE,
        "COS_PRUNE":COS_PRUNE,
    }
    id = str(id)
    smiles_data = {}    
    smiles_data[id] = {}

    try:
        if not os.path.exists(os.path.join(output_path, id)):
            os.makedirs(os.path.join(output_path, id))
        
        if not smiles_gt:
            print(f"\n>> {id}: Empty SMILES String. Skipping pdf {id}")            
            smiles_data[id]["error"] = "Input error: Empty smiles string"
            return smiles_data        
        # pdf_buffer = get_pdf_from_smiles(smiles_gt, label_mode, implicit_hydrogens_visible, relative_thickness)
        
        
        pdf_io = io.BytesIO(pdf_buffer)
        json_content = process_sscraper_one_pdf(id, pdf_io) 
        json_dict = json.loads(json_content)        
        height = json_dict["pages"][0]["BBOX"]["height"]
        width = json_dict["pages"][0]["BBOX"]["width"]
        page_size_map = {
            "0":(height,width)
        }
        bbox = [[(0,0,width,height)]]        
        all_pdf_msts, _, mst_stats = process_combine_one_file(io.BytesIO(json_content),
                                                   page_size_map, bbox, id, thresholds=thresholds_dict)
                        
        
        errors = {}
        visual2cdxml(all_pdf_msts, out_dir=output_path,
                     parallel=False, errors_dict=errors, verbose=False)  
        
        smiles_gt_can = canonicalize_smiles(smiles_gt)[0]
                
        smiles_data[id].update({
            "id_molecule":id_molecule,
            "ground_truth":smiles_gt,
            "ground_truth_canonicalized":smiles_gt_can,

            "PARALLEL_TOLERANCE":PARALLEL_TOLERANCE,
            "CLOSE_NONPARALLEL_ALPHA":CLOSE_NONPARALLEL_ALPHA,
            "CLOSE_CHAR_LINE_ALPHA":CLOSE_CHAR_LINE_ALPHA,
            "LONGEST_LENGTHS_DIFF_RATIO":LONGEST_LENGTHS_DIFF_RATIO,
            "NEG_CHARGE_Y_POSITION":NEG_CHARGE_Y_POSITION,
            "NEG_CHARGE_LENGTH_TOLERANCE":NEG_CHARGE_LENGTH_TOLERANCE,
            
            "REMOVE_ALPHA":REMOVE_ALPHA,
            "Z_TOLERANCE":Z_TOLERANCE,
            "COS_PRUNE":COS_PRUNE,

            "label_mode":label_mode,
            "implicit_hydrogens_visible":implicit_hydrogens_visible,
            "relative_thickness":relative_thickness,            
        })
        if errors:
            # smiles_data[id]["hasError"] = True
            smiles_data[id]["error"] = f"Graph2cdxml error: {errors[id]}"
            print(f"\n>> {id}: Graph to CDXML error")
    except Exception as e:
        # smiles_data[id]["hasError"] = True
        smiles_data[id]["error"] = f"Smiles2cdxml error: {str(e)}"
        print(f"\n>> {id}: SMILES to CDXML error")
        import traceback
        traceback.print_exc()
        # import pdb; pdb.set_trace()

        # print(e)
    return smiles_data

def tanimoto_similarity(smiles1, smiles2):
    try:
        mol1 = Chem.MolFromSmiles(smiles1)
        mol2 = Chem.MolFromSmiles(smiles2)
        fp1 = Chem.RDKFingerprint(mol1)
        fp2 = Chem.RDKFingerprint(mol2)
        tanimoto = DataStructs.FingerprintSimilarity(fp1, fp2)
        return tanimoto
    except:
        return 0

def levenshtein_similarity(smiles1, smiles2):            
    return distance(smiles1, smiles2)

def levenshtein_similarity_normalized(smiles1, smiles2):
    lower_bound = abs(len(smiles1) - len(smiles2))
    upper_bound = max(len(smiles1), len(smiles2))
    range_val = upper_bound - lower_bound
    distance_val = distance(smiles1, smiles2) 
    normalized_val = (distance_val-lower_bound)/range_val    
    return 1-normalized_val

def get_smiles_from_paths(params):
    paths,id = params    
    pdf_smiles = []
    if len(paths) == 0:
        return {}
    try:
        #smiles = subprocess.run('molconvert smiles ' + os.path.join(output_path,"*","*.cdxml"),
        #                        shell=True, stdout=subprocess.PIPE)
        smiles = subprocess.run('molconvert smiles ' + ' '.join(paths),
                                shell=True, stdout=subprocess.PIPE)
    except Exception as e:
        import traceback
        traceback.print_exc()
    _pdf_smiles = smiles.stdout.decode('utf-8').split('\n')
    if not len(_pdf_smiles):
        raise Exception('Error in SMILES conversion. Please check CDXML Conversion')            
    
    pdf_smiles = _pdf_smiles[:-1]
    return {id: pdf_smiles}

      
def main():
    parsed_args = parse_args()

    input_file = parsed_args.input_file
    id_index_in_file = parsed_args.id_index_in_file
    smiles_index_in_file = parsed_args.smiles_index_in_file
    output_path = parsed_args.output_path
    parallel = parsed_args.parallel    
    group = parsed_args.group

    index_from = parsed_args.index_from
    index_to = parsed_args.index_to

    # output_path_cdxml = os.path.join(output_path,"cdxml")

    if not os.path.exists(output_path):
        os.makedirs(output_path)

    acum_lev = 0
    acum_tani = 0
    acum_lev_normalized = 0
    count = 0
    count_errors = 0
    exact_matches = 0
    not_exact_matches_ids = []
    smiles_data = {}
    dTimer.qcheck("Initial module load")
    
    with open(input_file) as csv_file:
        file_content = csv.reader(csv_file, delimiter=',')
        next(file_content, None) #skip header        
        file_content = list(file_content)[index_from:index_to+1]
        print("\nMolecules (files):",len(file_content)) 
        print(f"Indexes interval: [{index_from}, {index_to}] inclusive") 

    data_table = generate_all_molecule_instances(file_content, id_index_in_file, smiles_index_in_file, output_path,group)
    print("PDFs already rendered.")
    print("Total molecules combination:",len(data_table)) 
    # import pdb;pdb.set_trace()
    print("Parsing...")
    if parallel:
        smiles_data_ = map_reduce( generate_smiles_dict, combine_dict_pair, data_table, stages=10, progress=tqdm, cpu_count=20 )
    else:
        smiles_data_ = {}
        for row in data_table:
            smiles_data.update(generate_smiles_dict(row))
    smiles_data = {**smiles_data, **smiles_data_}
        
    dTimer.qcheck("Parsing done")
    print("Parsing done")    
    gap=1900
    cdxmls_paths = sorted(glob(os.path.join(output_path,"*","*.cdxml")), key=lambda x:os.path.basename(os.path.dirname(x)))
    
    chunks = math.ceil(len(cdxmls_paths)/gap)
    chunks_list = [cdxmls_paths[i*gap : (i+1)*gap] for i in range(chunks+1)]
    print(f"CDXMLs to get SMILES from: {len(cdxmls_paths)}. Chunks: {chunks}...")    
        
    input_list = list(zip(chunks_list, list(range(chunks+1))))
        
    pdf_smiles_dict = map_reduce( get_smiles_from_paths, combine_dict_pair, input_list,stages=10, progress=tqdm, cpu_count=20 )
    pdf_smiles = [item for row in [x[1] for x in sorted(list(pdf_smiles_dict.items()), key=lambda x:x[0])] for item in row]

    cdxmls_paths_dict = dict(zip( map(lambda x: os.path.basename(os.path.dirname(x)), cdxmls_paths),pdf_smiles))
    

    dTimer.qcheck("Molconvert done")
    print("Molconvert done...")
    
    for id, value in tqdm(cdxmls_paths_dict.items()):
        smiles_data[id]["predicted"] = value
        smiles_data[id]["predicted_canonicalized"] = canonicalize_smiles(value)[0]
        tanimoto = tanimoto_similarity(smiles_data[id]["predicted_canonicalized"], smiles_data[id]["ground_truth_canonicalized"])
        levenshtein_normalized = levenshtein_similarity_normalized(smiles_data[id]["predicted_canonicalized"], smiles_data[id]["ground_truth_canonicalized"])
        levenshtein = levenshtein_similarity(smiles_data[id]["predicted_canonicalized"], smiles_data[id]["ground_truth_canonicalized"])
        smiles_data[id]["tanimoto"] = tanimoto
        smiles_data[id]["levenshtein_normalized"] = levenshtein_normalized
        smiles_data[id]["levenshtein"] = levenshtein
        if levenshtein_normalized == 1:
            exact_matches += 1
        else:
            not_exact_matches_ids.append(id)    
        acum_lev += levenshtein
        acum_lev_normalized += levenshtein_normalized
        acum_tani += tanimoto
        count += 1

    dTimer.qcheck("Canonicalization done")
    print("Canonicalization done")
    print(dTimer)
    
    pandas_smiles_meta = pd.DataFrame.from_dict(smiles_data, orient="index")    
    pandas_smiles_meta.to_csv(os.path.join(output_path, "smiles_meta.csv"), index=True)  

    # with open(os.path.join(output_path, "smiles_meta.json"),"w+") as outfile:
    #     outfile.write(json.dumps(smiles_data))  
    error_files = list(filter(lambda x: "error" in x[1], smiles_data.items()))
    
    # error_files = list(map(itemgetter(0), error_files))
    count_errors = len(error_files)
    print("count: ",count)
    print(f"Molecules in file: {len(file_content)}")
    print("Total molecules (files): ",str((count+count_errors)))
    print(f"Tanimoto {acum_tani/(count+count_errors)}")
    print(f"Lev {acum_lev/(count+count_errors)}")
    print(f"Lev normalized {acum_lev_normalized/(count+count_errors)}")
    print("Exact matches:", exact_matches)
    print("Incorrectly parsed:", count - exact_matches)
    print(f"Fatal errors (files failed): {count_errors}")


    memory_KB = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
    head = dTimer.entries[0]
    tail = dTimer.entries[-1]
    metrics_object = {        
        "total_files": count+count_errors,
        "levenshtein": acum_lev/(count+count_errors),
        "levenshtein_normalized": acum_lev_normalized/(count+count_errors),
        "tanimoto":acum_tani/(count+count_errors),
        "exact_matches": exact_matches,
        "time":tail[1] - head[1],
        "memory_KB": memory_KB,         
    }
    metadata = metrics_object
    
    print(f"Memory usage: {memory_KB} KB")
    
    with open(os.path.join(output_path,"exact_matches_errors_ids.json"), "w") as f:
        f.write(json.dumps(not_exact_matches_ids)) 
    with open(os.path.join(output_path,"fatal_errors_ids.json"), "w") as f:
        f.write(json.dumps(dict(error_files)))    
    with open(os.path.join(output_path,"metadata.json"), "w") as f:
        f.write(json.dumps(dict(metadata)))
    print(f"\nOuptut results path: {output_path}")

if __name__ == "__main__":
    main()
