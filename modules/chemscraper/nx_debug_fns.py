###########################################################################
# nx_debug_fns.py
#   - convenience / debug methods for networkx graphs
#
# Note: needed to update 'decorator' package to 5.1.1 to work on macos
#       laptop (MacOs 13.4)
###########################################################################
# Created by Richard Zanibbi, Jun 10 2023 16:57:26
###########################################################################

# Importing to make all debug ops available, visible
# with *one* import statement
import os
from importlib import reload
from debug_fns import *
import debug_fns
debug_fns.DEBUG = True

import networkx as nx
import matplotlib.pyplot as plt
# Allows multiple PDF pages in output
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
from tqdm import tqdm
from modules.chemscraper.utils.viz_utils import imshow
import matplotlib

##################################
# Formatting (pretty print and
#   suffix removal)
##################################
# Initial versions are best attempts to edit Python-generated strings
# directly, to avoid assuming node identifiers are one type or another.


def str_remove_suffix( node_id ):
    return str( node_id ).split('_')[0]


MAX_LENGTH = 80
def pp_list( l, indent_size=4, max_length_split=MAX_LENGTH):
    indent = ' '  * indent_size
    list_string = str(l)   
    out_string = ''

    if len(list_string) > max_length_split:
        line = 1
        for idx in range(len(list_string)):
            if idx == 1 or idx == len(list_string) - 1:
                out_string += '  '

            if list_string[idx] == ',' and idx / max_length_split > line:
                out_string += ',\n' + indent
                line += 1
            
            else:
                out_string += list_string[idx]
    else:
        out_string = list_string

    return out_string

def pp_attr_list( al, indent_size=4, max_length_split=MAX_LENGTH ):
    indent = ' ' * indent_size
    tuple_string = str(al).replace('{','\n' + indent + '  { ') \
        .replace(", '", ',\n' + 2 * indent + "'") \
        .replace("(","\n" + indent + "(") \
        .replace('}',' }')
    return tuple_string 

def pp_edge_attr_list( al, indent_size=4, max_length_split=MAX_LENGTH ):
    # NOTE: for time, currently assumes a short attribute dict is used.
    indent = ' ' * indent_size
    list_string = str(al)
    out_string = ''

    if len(list_string) > max_length_split:
        line = 1
        for idx in range(len(list_string)):
            if idx == 1:
                out_string += '  ' + list_string[idx]

            #if list_string[idx] == ',' and idx / max_length_split > line:
            #    out_string += list_string[idx]
            #    out_string += ',\n' + indent
            #    line += 1
            
            else:
                out_string += list_string[idx]
    else:
        out_string = list_string

    out_string = out_string.replace('(', '\n' + indent + '(') \
            .replace('{', '\n' + indent + '  { ')

    return out_string 


##################################
# Structure
##################################

def gdescr(G, verbose=False, title=None, adj_lists=False ):

    out_string = ''

    # Show graph structure properties
    # Also attributes is 'verbose' output requested
    if title:
        out_string += '\n[ Graph summary: ' + title + ' ]'
    else:
        out_string += '\n[ Graph summary ]'
    
    out_string += "\nType: " + str( type(G) ) 
    out_string += "\nStructure\n============"
    out_string += "\nNodes (" + str(len(G.nodes)) + "):\n  " + pp_list( sorted( G.nodes ) )
    out_string += "\nEdges (" + str(len(G.edges)) + "):\n  " + pp_list( sorted( G.edges ) )

    if adj_lists:
        out_string += "\nAdjacency lists:\n  " + pp_list( sorted( G.adj ) )

    if verbose:
        out_string += "\nAttributes\n============\n"
        out_string += a_attrs(G)
    out_string +="\n++ ----- ++"

    return out_string

def vgdescr( G, title=None ):
    # Command for 'verbose' graph print output
    return gdescr(G, verbose=True, title=title)

def show_gdescr( G, title=None ):
    print(gdescr(G, title=title ))

def show_vgdescr( G, title=None ):
    print(gdescr(G, verbose=True, title=title))

##################################
# Attributes
##################################

def g_attrs( G ):
    return "Graph attributes:\n  " + str( G.graph )

def n_attrs( G ):
    return "Node attributes:\n  " + pp_attr_list( sorted( G.nodes.data() ) )

def e_attrs( G ):
    return "Edge atributes:\n  " +  pp_edge_attr_list( sorted( G.edges.data() ) )

def a_attrs( G ):
    # Print graph, node, and edge attributes
    return '\n'.join( (g_attrs( G ), n_attrs( G ), e_attrs( G )) )

##################################
# Draw graph + display or to file
##################################

# Chemscraper-specific
def create_pos_from_box( G ):
    # Add center points to nodes for plotting
    for (node_id, attr_dict) in G.nodes.data():
        if 'pos' in G.nodes[node_id]:
            continue
        bbox = attr_dict['box']

        # Appends attribute to node data in-place
        center_point = np.array( [ (bbox[0] + bbox [2]) /2, (bbox[1] + bbox[3]) / 2 ] )
        G.nodes[node_id]['pos'] = center_point
        
        #pcheck("Updated node atrributes", ( node_id, G.nodes[node_id]) )

    return G

def get_cmap(n, name='hsv'):
    '''Returns a function that maps each index in 0, 1, ..., n-1 to a distinct 
    RGB color; the keyword argument name must be a standard mpl colormap name.'''
    return plt.cm.get_cmap(name, n)

def shapesdraw( shapes, title=None, invert_y=True, save_path=None):
    #colors = ['blue','red','green', 'orange', 'purple']
    colors = np.random.randint(low=0, high=255, size=(len(shapes), 3))/255
    #opacity = np.ones(colors_rgb.shape[0]).reshape((-1,1))
    #colors = np.concatenate((colors_rgb, opacity),axis=1)
    #cmap = get_cmap(len(shapes))
    # Create plot
    # ax = plt.gca()
    plt.figure()
    if invert_y:
        # ax.set_ylim(ax.get_ylim()[::-1])
        plt.gca().invert_yaxis()
        if title:
            title += '\n(*Y axis inverted)'
        else:
            title = '(*Y axis inverted)'

    # plt.axis("off")
    # Include title if requested
    if title:
        plt.title( title )
    for i, shape in enumerate(shapes):
        if shape.geom_type == "LineString":
            plt.plot(*shape.coords.xy, color=colors[i])
            #plt.plot(*shape.coords.xy, color=cmap(i))
        elif shape.geom_type == "MultiPolygon":
            for poly in list(shape):
                plt.plot(*poly.exterior.coords.xy, color=colors[i])
        else:
            plt.plot(*shape.exterior.coords.xy, color=colors[i])
    
    # Display or save plot as requested
    if not save_path:
        plt.show(block=False)
        plt.pause(0.001)
    else:
        plt.savefig( save_path, format="pdf")
        plt.close()

def shapedraw( shape, pdf_obj=None, title=None, invert_y=True ):
    # Create plot
    # ax = plt.gca()
    plt.figure()
    if invert_y:
        # ax.set_ylim(ax.get_ylim()[::-1])
        plt.gca().invert_yaxis()
        if title:
            title += '\n(*Y axis inverted)'
        else:
            title = '(*Y axis inverted)'

    # plt.axis("off")
    # Include title if requested
    if title:
        plt.title( title )

    if shape.geom_type == "LineString":
        plt.plot(*shape.coords.xy)
    else:
        plt.plot(*shape.exterior.coords.xy)
    
    # Display or save plot as requested
    if not pdf_obj:
        plt.show(block=False)
        plt.pause(0.001)
    else:
        plt.savefig( pdf_obj, format="pdf")

def draw_points(points, shape):
    # Create plot
    image = np.zeros(shape)
    cols, rows = points.T
    image[rows, cols] = 255
    imshow(image)

def draw_graph(graph, save_path=None, file_name=None, save=False, invert_y=True,
               title=None, hide_enum=False, font_size=5, node_size=40):
    matplotlib.use("Agg")
    options = {
        "font_size": font_size,
        "node_size": node_size,
        # "font_size": 7,
        # "node_size": 180,
        "node_color": "white",
        "edgecolors": "blue",
        # "linewidths": 1,
        "linewidths": 0,
        "width": 0.5,
        # 'arrowstyle': "-",
        # 'style': "-.",
        # 'connectionstyle': 'arc3,rad=0.15'
    }
    # node_label_dictionary = {node: data['label'] + "_" + node for node, data in graph.nodes(data=True)}
    graph = create_pos_from_box( graph )
    pos_dict = nx.get_node_attributes(graph, 'pos')

    if len(pos_dict) < len(graph.nodes):
        graph = create_pos_from_box( graph )
        pos_dict = nx.get_node_attributes(graph, 'pos')

    plt.figure()
    label_dict =  {}
    for node in graph.nodes:
        if 'contraction_label' in graph.nodes[node]:
            # All merged labels ('contraction_label' attributes) replace node label
            if graph.nodes[node]['lbl_cls'] != "Char":
                # label_dict[node] = f"{node}:{graph.nodes[node]['contraction_label']}"
                label_dict[node] = f"{graph.nodes[node]['contraction_label']}"
            else:
                if (hide_enum):
                    label_dict[node] = str_remove_suffix(node)
                else:
                    label_dict[node] = str(node)

        else:
            # If asked, remove the numeric id in symbol labels
            if (hide_enum):
                label_dict[node] = str_remove_suffix(node)
            else:
                label_dict[node] = str(node)

    nx.draw(graph, pos_dict, with_labels=False, **options)
    # nx.draw(graph, pos, **options)
    # Add node labels
    nx.draw_networkx_labels(graph, pos_dict, labels=label_dict,
                            font_size=options["font_size"], font_color='black', 
                            verticalalignment='center')

    edge_labels = nx.get_edge_attributes(graph, 'label')
    edge_label_map = {"CONNECTED": "+", "CONCATENATED": "||"}
    edge_labels = dict(list(map(lambda x: (x[0], edge_label_map[x[1]]),
                                      edge_labels.items())))
    if edge_labels:
        nx.draw_networkx_edge_labels(graph, pos_dict, edge_labels=edge_labels,
                                     font_size=options["font_size"],
                                     font_color='black', verticalalignment='center')

    # Create plot
    if invert_y:
        # ax.set_ylim(ax.get_ylim()[::-1])
        plt.gca().invert_yaxis()
        # if title:
        #     title += '\n(*Y axis inverted)'
        # else:
        #     title = '(*Y axis inverted)'

    plt.axis("off")

    # Include title if requested
    if title:
        plt.title( title )

    if save:
        graphs_path = os.path.join(save_path, 'graphs')
        os.makedirs(graphs_path, exist_ok=True)
        plt.savefig(os.path.join(graphs_path, file_name + ".pdf"),
                    transparent=True, bbox_inches='tight', pad_inches=0)
        # plt.savefig(os.path.join(graphs_path, f'{idx}.png'), transparent=True)
        plt.clf()
        plt.close()
    else:
        plt.show(block=False)
        plt.pause(0.001)

def gdraw( G, pdf_obj=None, title=None, hide_enum=False, invert_y=True, font_size=11 ):
    # Rendering parameters
    options = {
        "font_size" : font_size,
        "style" : "dotted",
        #"font_weight" : "bold",
        #"node_shape" : "s"
        "node_color" : "white",
        "edge_color" : "blue",
        "linewidths" : 0,
        "width" : 0.5,
    }

    plt.figure()
    # Query node positions, use if available, ignore otherwise
    # 'pos' attributes (added using, e.g., G.nodes[node_id]['field'] = value
    pos_dict = nx.get_node_attributes(G, 'pos')
    if len(pos_dict) < len(G.nodes):
        G = create_pos_from_box( G )
        pos_dict = nx.get_node_attributes(G, 'pos')

    label_dict =  {}
    for node in G.nodes:
        if 'contraction_label' in G.nodes[node]:
            # All merged labels ('contraction_label' attributes) replace node label
            label_dict[node] = G.nodes[node]['contraction_label']
        else:
            # If asked, remove the numeric id in symbol labels
            if (hide_enum):
                label_dict[node] = str_remove_suffix(node)
            else:
                label_dict[node] = str(node)

    options['labels'] = label_dict
    nx.draw_networkx(G, pos = pos_dict, **options)

    # Create plot
    if invert_y:
        # ax.set_ylim(ax.get_ylim()[::-1])
        plt.gca().invert_yaxis()
        if title:
            title += '\n(*Y axis inverted)'
        else:
            title = '(*Y axis inverted)'

    plt.axis("off")

    # Include title if requested
    if title:
        plt.title( title )

    # Display or save plot as requested
    if not pdf_obj:
        plt.show(block=False)
        plt.pause(0.001)
    else:
        plt.savefig( pdf_obj, format="pdf")
        # Clear current plot -- without this plots in pdf are overlaid
        plt.clf()


##################################
# View series of graphs
##################################

def draw_gtlist( g_title_pair_list, file_name = None, descr=None, hide_enum=False, invert_y=False ):
    # Create PDF multi-page object for a list of (graph, title) pairs
    # Optional command-line output of graph properties
    pdf_obj = None
    if file_name:
        pdf_obj = PdfPages( file_name )

    check("* Graphs to draw", len(g_title_pair_list))

    for ( g, title ) in tqdm( g_title_pair_list ):
        if descr:
            descr( g, title )
        gdraw( g, pdf_obj, title, hide_enum, invert_y )
    plt.show(block=True)

    if file_name:
        pdf_obj.close()
        check("* Wrote PDF file", file_name + '\n')

def draw_dgtlist( g_title_pair_list, file_name=None, hide_enum=False, invert_y=False ):
    draw_gtlist( g_title_pair_list, file_name=file_name, descr=show_gdescr, hide_enum=hide_enum, invert_y=invert_y )

def draw_vgtlist( g_title_pair_list, file_name=None, hide_enum=False, invert_y=False ):
    draw_gtlist( g_title_pair_list, file_name=file_name, descr=show_vgdescr, hide_enum=hide_enum, invert_y=invert_y )
    


