import os
import subprocess
import numpy as np
from bs4 import BeautifulSoup as bsoup
import networkx as nx
from rdkit import Chem
from Levenshtein import distance, ratio
from modules.chemscraper.graph_conversions.CDXML_Converter import visual2cdxml


from modules.protables.debug_fns import *

def gen_alt_metrics(pred_smi_dir, regions,
            source_cdxml_dir='modules/chemscraper/Table_cdxml_files/Table_cdxml_files',
            eval_dir='outputs/chemxtest/eval_tsv', pdf_name='or100.09.tables'):
    
    pdf_regions = regions[pdf_name]
    num_pdf_regions = 0
    for pg in pdf_regions:
        num_pdf_regions += len(pdf_regions[pg])
    # Generate the GT molecular graphs
    source_dict = gen_mol_graphs(source_cdxml_dir, pdf_name=pdf_name)


    # pdf_dict = source_dict[pdf_name]
    # print(pdf_dict[1][4].nodes())
    # exit(0)

    # Convert graphs to CDXML
    gt_cdxml_path = os.path.join(eval_dir,'gt_cdxmls')
    if not os.path.exists(gt_cdxml_path):
        os.makedirs(gt_cdxml_path)


    #print('\n [ CONVERTING GT VISUAL GRAPHS TO CDXMLS... ]')
    print(f'[ WRITING GT CDXMLS TO {gt_cdxml_path} ... ]')

    visual2cdxml(dpr_vgraph_dict=source_dict, out_dir=gt_cdxml_path, fixed_attach=True)

    # Convert the GT CDXMLs to SMILES
    gt_smi_path = os.path.join(eval_dir, 'gt_smiles')
    if not os.path.exists(gt_smi_path):
        os.makedirs(gt_smi_path)
    
    gt_cdxml_dir = os.path.join(gt_cdxml_path,pdf_name)
    subprocess.run(['./modules/chemscraper/bin/par_cd_to_smi.sh', gt_cdxml_dir, gt_smi_path])

    
    # Start structuring Pred and GT to generate metrics
    all_tot_mols = 0
    tot_mols = 0
    exact_matches = 0
    tot_norm_score = 0
    tot_unnorm_score = 0
    all_source_lens = []

    # Get all the pred smiles and strings
    pred_smi_file = os.path.join(pred_smi_dir, pdf_name, 'smiles_out.txt')
    pred_smi_paths = []
    pred_smi_strs = []
    with open(pred_smi_file, 'r') as f:
        for data_line in f.readlines():
            file_name = data_line.split(',')[0].split('/')[-1]
            smi_str = data_line.split(',')[1].lstrip().rstrip('\n')
            if smi_str != 'ERROR':
                pred_smi_paths.append(file_name)
                pred_smi_strs.append(smi_str)
    
    # Convert all the pred smiles to RDKit canonicalization
    print('\n CONVERTING PREDS TO CANONICAL FORM \n')
    can_pred_smis = []
    for idx_smi, smi_str in enumerate(pred_smi_strs):
        try:
            mol = Chem.MolFromSmiles(smi_str)
            can_smi = Chem.MolToSmiles(mol, canonical=True)
            can_pred_smis.append(can_smi)
        except:
            print(f'For cdxml: {pred_smi_paths[idx_smi]}')
            can_pred_smis.append(smi_str)

    # Get all the GT smiles strings
    gt_smi_file = os.path.join(gt_smi_path, 'smiles_out.txt')
    gt_smi_paths = []
    gt_smi_strs = []
    with open(gt_smi_file, 'r') as f:
        for data_line in f.readlines():
            file_name = data_line.split(',')[0].split('/')[-1]
            smi_str = data_line.split(',')[1].lstrip().rstrip('\n')
            if smi_str != 'ERROR':
                gt_smi_paths.append(file_name)
                gt_smi_strs.append(smi_str)

    # Convert all the GT smiles to RDKit canonicalization
    print('\n CONVERTING GTS TO CANONICAL FORM \n')
    can_gt_smis = []
    for idx_smi, smi_str in enumerate(gt_smi_strs):
        try:
            mol = Chem.MolFromSmiles(smi_str)
            can_smi = Chem.MolToSmiles(mol, canonical=True)
            can_gt_smis.append(can_smi)
        except:
            print(f'For cdxml: {gt_smi_paths[idx_smi]}')
            can_gt_smis.append(smi_str)
    
    # Get unique Page IDs and create a dict to store matches
    page_ids = []
    for path in gt_smi_paths:
        page_id = int(path.split('_')[1])
        if page_id not in page_ids:
            page_ids.append(page_id)
    matches = {p:[] for p in page_ids}
    
    for page_id in page_ids:
        
        # Get the page-wise GT and Pred Canonical SMILES
        pg_gt_smis = [can_gt_smis[idx] for idx,path in enumerate(gt_smi_paths) if 
                    int(path.split('_')[1]) == page_id]
        pg_pred_smis = [can_pred_smis[idx] for idx,path in enumerate(pred_smi_paths) if 
                    int(path.split('_')[1]) == page_id]
        pg_pred_paths = [path for path in pred_smi_paths if int(path.split('_')[1]) == page_id]
        
        # if page_id == 2:
        #     print(pg_gt_smis)
        #     print(pg_pred_smis)
        #     exit(0)

        for page_idx, smi in enumerate(pg_pred_smis):
            all_tot_mols += 1
            cur_unnorm_scores = []
            cur_unnorm_idxs = []
            for idx_tab_smi, tab_smi in enumerate(pg_gt_smis):
                score = distance(tab_smi, smi)
                cur_unnorm_scores.append(score)
                cur_unnorm_idxs.append(idx_tab_smi)
            min_idx = cur_unnorm_scores.index(min(cur_unnorm_scores))
            min_score = min(cur_unnorm_scores)
            # Get the formula region of the smis
            region = regions[pdf_name][page_id-1][
                        int(pg_pred_paths[page_idx].split('_')[2].lstrip('No').rstrip('.cdxml'))-1]
            
            if min_score == 0:
                exact_matches += 1
            if min_score <= 15: # Somewhat valid match, take it
                # Get the normalized scores
                norm_score = min_score/max(len(smi),len(pg_gt_smis[min_idx]))
                matches[page_id].append([pg_gt_smis[min_idx], smi, region, min_score, norm_score])
                tot_mols += 1
                tot_unnorm_score += min_score
                tot_norm_score += norm_score
                all_source_lens.append(len(pg_gt_smis[min_idx]))
            else: # No Source PDF match found
                matches[page_id].append(['--', smi, region, len(smi), 1])
    
    # Now compute final metrics
    avg_norm_score = tot_norm_score/tot_mols
    avg_unnorm_score = tot_unnorm_score/tot_mols
    min_smi_len = min(all_source_lens)
    max_smi_len = max(all_source_lens)
    avg_smi_len = sum(all_source_lens)/len(all_source_lens)

    print("Average Norm Score:", avg_norm_score)
    print("Average Unnorm Score:", avg_unnorm_score)
    print("Min Smile Length:", min_smi_len, "\tMax Smile Length:", max_smi_len, "\tAverage Smile Length:", avg_smi_len)
    # Use num of YOLO detections rather than num of smiles
    print("Exact matches:", exact_matches, num_pdf_regions, "\tPercent:", exact_matches/num_pdf_regions)

    # Now write the final detections
    print(f"Eval Dir: {os.path.join(eval_dir,pdf_name+'_eval.tsv')}")
    with open(os.path.join(eval_dir,pdf_name+'_eval.tsv'), 'w') as f:
        f.write('PDF\t'+'1\t'+pdf_name+'\n')
        for page_id in sorted(matches.keys()):
            f.write('P\t'+str(page_id)+'\n')
            f.write('Source SMI\t'+'Pred SMI\t'+'PDF Region x1\t'+'PDF Region y1\t'+'PDF Region x2\t'+
                'PDF Region y2\t'+'Act. Leven.\t'+'Norm Leven.\n')
            # Order all the values by pdf region
            pdf_box = []
            for meta in matches[page_id]:
                pdf_box.append(meta[2])
            pdf_box = np.array(pdf_box).reshape((-1,4))
            sort_idx = np.lexsort((pdf_box[:,0],pdf_box[:,1]))
            np_meta = np.array(matches[page_id], dtype=object)
            np_meta = np_meta[sort_idx]

            for meta in np_meta:
                f.write(str(meta[0])+'\t'+str(meta[1])+'\t'+str(meta[2][0])+'\t'+str(meta[2][1])+
                                             '\t'+str(meta[2][2])+'\t'+str(meta[2][3])+'\t'+
                                             str(meta[3])+'\t'+str(meta[4])+'\n')
    f.close()


def gen_mol_graphs(cdxml_dir, pdf_name='or100.09.tables'):    
    cdxmls = os.listdir(cdxml_dir)
    
    # Get unique tables IDs
    tabs = []
    for cdxml in cdxmls:
        tid = int(cdxml.split('-')[2].lstrip('Tab'))-1
        if tid not in tabs:
            tabs.append(tid)
    tabs = sorted(tabs)

    tab_graphs = {t:[] for t in tabs}
    for cdxml in cdxmls:
        tid = int(cdxml.split('-')[2].lstrip('Tab'))-1
        in_file = open(os.path.join(cdxml_dir,cdxml), 'r')
        contents = in_file.read()
        in_file.close()
        soup = bsoup(contents, 'xml')

        page_data = soup.find('page')
        mol_frags = page_data.find_all('fragment', recursive=False)
        
        for idx_mol, mol in enumerate(mol_frags):
            c_count = 1
            node2id = {}
            nodes = mol.find_all('n', recursive=False)

            for n in nodes:
                idx = n['id']
                t_box = n.find('t', recursive=False)

                if t_box is None:
                    if n.has_attr('NodeType'):
                        nd_type = n['NodeType']
                        if nd_type == 'ExternalConnectionPoint':
                            pass
                        else:
                            raise Exception(f'Weird NodeType value found {nd_type} for abbrev {cdxml} for No T-box case')
                    else:
                        node2id[idx] = 'C_'+str(c_count)
                        c_count += 1
                else: # Element / R group / Abbrev case
                    full_txt = ''
                    s_boxes = t_box.find_all('s', recursive=False)
                    for s_tag in s_boxes:
                        txt = s_tag.get_text()
                        full_txt += txt
                    # full_txt = full_txt.split('H')[0]
                    node2id[idx] = full_txt
            
            # Set dummy node positions for the nodes
            g = nx.DiGraph()
            for key in node2id.keys():
                g.add_node(node2id[key], pos=[100,100])
            
            # Get the bonds and complete the graph
            order_dict = {'2': 'Double', '3': 'Triple', 'WedgeBegin': 'S Wedge', 
                          'WedgedHashBegin': 'H Wedge'}
            bonds = mol.find_all('b', recursive=False)
            
            for b_tag in bonds:
                beg = b_tag['B']
                end = b_tag['E']
                if b_tag.has_attr('Order'):
                    order = b_tag['Order']
                    g.add_edge(node2id[beg], node2id[end], type=order_dict[order])
                else:
                    if b_tag.has_attr('Display'):
                        if b_tag['Display'] == 'WedgeBegin':
                            g.add_edge(node2id[beg], node2id[end], type=order_dict['WedgeBegin'])
                        else:
                            g.add_edge(node2id[beg], node2id[end], type=order_dict['WedgedHashBegin'])
                    else:
                        g.add_edge(node2id[beg], node2id[end], type='Single')
            tab_graphs[tid].append(g)
    final_graphs = {pdf_name:tab_graphs}
    return final_graphs


if __name__=='__main__':
    gen_alt_metrics('outputs/chemxtest/generated_smiles', [])
