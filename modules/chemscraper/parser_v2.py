###########################################################################
#
# modules/chemscraper/parser_v2.py
#
###########################################################################
# Created by: Richard Zanibbi, Jun 08 2023 04:56:40 PM
# Last edited by: Ming Creekmore July 28 2023
#                 R. Zanibbi, Oct 16, 2023
#                 Ayush Kumar Shah, Nov 5, 2023
###########################################################################

from collections import defaultdict
import os
import statistics
from networkx import has_path, subgraph_view
from scipy.stats import zscore
from nx_debug_fns import *   # Includes networkx
import nx_merge
import sys
import re
import math
from copy import deepcopy
import markush
from geomtric_utils import GeometricToolbox
import parser_debug
from operator import itemgetter
from scipy.spatial.distance import cdist
from itertools import repeat
from shapely.geometry import Point
from shapely.ops import nearest_points

# RZ Addition
from modules.utils.dprl_map_reduce import map_reduce, combine_dict_pair
from modules.chemscraper.extract_mst import ExtractMST
from enum import Enum

# Enuemrate bond attachment positions
class Side(Enum):
    LEFT = 1
    RIGHT = 2
    TOP = 3
    BOTTOM =4


class Parser_v2:
    def __init__(self, thresholds:dict):        
        self.thresholds_ref = thresholds

        self.PARALLEL_TOLERANCE = thresholds["PARALLEL_TOLERANCE"]
        self.CLOSE_CHAR_LINE_ALPHA = thresholds["CLOSE_CHAR_LINE_ALPHA"]
        self.REMOVE_ALPHA = thresholds["REMOVE_ALPHA"]
        self.Z_TOLERANCE = thresholds["Z_TOLERANCE"]
        self.NEG_CHARGE_LENGTH_TOLERANCE = thresholds["NEG_CHARGE_LENGTH_TOLERANCE"]
        self.NEG_CHARGE_Y_POSITION = thresholds["NEG_CHARGE_Y_POSITION"]
        self.CLOSE_NONPARALLEL_ALPHA = thresholds["CLOSE_NONPARALLEL_ALPHA"]
        
        
        self.SCALE_Y = 720 / (11*300)  #RZ: Hack (see cdxml_conversion.py)
        self.get_mid = lambda x: [(x[0] + x[2]) * 0.5, (x[1] + x[3]) * 0.5]
        self.REGION_DBG_ID = -1  # OFF: Change to non-negative integer id for target region
        self.geo = GeometricToolbox(thresholds)
        self.mst_extractor = ExtractMST(thresholds)
    
    def close_parallel_lines(self, I, G):
        G_updated = G.copy()

        # line_selector = nx_merge.node_attr_match( I, 'isLine', True )
        line_selector = nx_merge.node_attr_match( I, 'lbl_cls', 'Ln' )
        
        nonintersecting_lines_selector = nx_merge.edge_attr_match(I, 'ln_intersect', False)
        parallel_edge_selector_I = nx_merge.edge_attr_match( I, 'parallel', True )

        I_parallel_lines = nx.subgraph_view(I, filter_edge=parallel_edge_selector_I, 
                                    filter_node=line_selector) 
        I_parallel_nonintersecting = nx.subgraph_view(I_parallel_lines, filter_edge=nonintersecting_lines_selector)

        for edge in I_parallel_nonintersecting.edges(data=True):
            node1, node2, data = edge
            if G_updated.has_edge(node1, node2):
                continue

            # One of the bond lines must not intersect with any other lines/objects
            if  (list(filter(lambda x:x[2].get('ln_intersect', False),
                    G.edges(node1, data=True))) and
                list(filter(lambda x:x[2].get('ln_intersect', False),
                            G.edges(node2, data=True))) ):
                continue

            degree_node1 = G.degree(node1)
            degree_node2 = G.degree(node2)
            # One of the bond lines must have degree of 1 (floating)
            if not (degree_node1 == 1 or degree_node2 == 1):
                continue

            line1 = G.nodes[node1]['shape_obj']
            line2 = G.nodes[node2]['shape_obj']
            distance = data["weight"]

            # Two parallel lines must be equidistant
            # TODO fix the offset distance
            if not self.geo.is_equidistance_parallel(line1, line2, offset=distance*1.5):
                continue

            boundary_points1 = list(self.mst_extractor.get_coords(line1))
            boundary_points2 = list(self.mst_extractor.get_coords(line2))
            # If both lines are floating (degree = 1), choose the farther line as floating
            if degree_node1 == 1 and degree_node2 == 1:
                connected_node1 = list(G.neighbors(node1))[0]
                connected_node2 = list(G.neighbors(node2))[0]
                distance_node1_connected1 = I[node1][connected_node1]['weight']
                distance_node2_connected2 = I[node2][connected_node2]['weight']
                if distance_node1_connected1 > distance_node2_connected2:
                    boundary_points_floating = boundary_points1
                    node_floating = node1
                    node_parallel_candidate = node2
                else:
                    boundary_points_floating = boundary_points2
                    node_floating = node2
                    node_parallel_candidate = node1

            elif degree_node1 == 1:
                boundary_points_floating = boundary_points1
                node_floating = node1
                node_parallel_candidate = node2
            else:
                boundary_points_floating = boundary_points2
                node_floating = node2
                node_parallel_candidate = node1
            
            if not nx.has_path(G, node1, node2):
                continue
            # shortest_path_length = nx.shortest_path_length(G, node1, node2)
            # if shortest_path_length > 3:
            #     continue

            # Check if parallel line candidate is within 5 nearest neighbors of
            # floating line
            nearest_neighbors = sorted(I.edges(node_floating, data=True), key=lambda x: x[2]['weight'])
            nearest_neighbors = list(map(itemgetter(1), nearest_neighbors))
            if node_parallel_candidate not in nearest_neighbors[:5]:
                continue

            # Re-assign (correct) edge in MST so floating line connected to it's
            # parallel pair
            if not G.has_edge(node1, node2):
                endpoint_dist_parallel = cdist(boundary_points1,
                                    boundary_points2).min(axis=0).mean()
                connected_node = list(G.neighbors(node_floating))[0]
                
                # If floating line connected to a non-line object, re-assign without check
                if not G.nodes[connected_node]['isLine'] :
                    G_updated.add_edge(node_floating, node_parallel_candidate, weight=distance,
                                    added_edge=True, parallel=True)
                    if G_updated.has_edge(node_floating, connected_node):
                        G_updated.remove_edge(node_floating, connected_node)
                    # print(node1, node2)
                    
                # If floating line connected to a line object, re-assign 
                else:
                    boundary_points_connected = list(self.mst_extractor.get_coords(G.nodes[connected_node]['shape_obj']))
                    endpoint_dist_connected = cdist(boundary_points_floating,
                                                    boundary_points_connected).min(axis=0).mean()
                    # if mean of min endpoint distance between the floating line and
                    # the candidate parallel line is less that between floating and the currently connected line
                    if endpoint_dist_parallel < endpoint_dist_connected:
                        G_updated.add_edge(node_floating, node_parallel_candidate, weight=distance,
                                        added_edge=True, parallel=True)
                        if G_updated.has_edge(node_floating, connected_node):
                            G_updated.remove_edge(node_floating, connected_node)
                        # print(node1, node2)
        return G_updated, I_parallel_nonintersecting

    def compute_parallel_threshold(self, G):
        close_tolerance_parallel = 0
        parallel_line_distances = []
        parallel_edge_selector_G = lambda n1, n2: G.edges[(n1, n2)]['parallel'] and not G.edges[(n1, n2)].get('hash')
        line_selector_G = nx_merge.node_attr_match(G, 'isLine', True) 
        G_parallel = nx.subgraph_view(G, filter_edge=parallel_edge_selector_G,
                                    filter_node=line_selector_G)
        if G_parallel.number_of_edges():
            parallel_line_distances = list(nx.get_edge_attributes(G_parallel,
                                                            'weight').values())
            max_parallel_lines_mst_dist = max(parallel_line_distances, default=0)
            close_tolerance_parallel = max_parallel_lines_mst_dist
        return close_tolerance_parallel, parallel_line_distances

    def compute_nonparallel_threshold(self, G, non_parallel_alpha, close_tolerance_char_line):
        close_tolerance_non_parallel_lines = 0
        max_nonparallel_lines_dist = 0
        nonparallel_edge_selector = nx_merge.edge_attr_match( G, 'parallel', False )
        line_selector = nx_merge.select_all_lines(G)
        G_nonparallel = nx.subgraph_view(G, filter_edge=nonparallel_edge_selector,
                                        filter_node=line_selector)
        if G_nonparallel.number_of_edges():
            max_nonparallel_lines_dist = max(nx.get_edge_attributes(G_nonparallel, 'weight').values())
            # Threshold for closing two non-parallel (intersecting) lines
            close_tolerance_non_parallel_lines = max_nonparallel_lines_dist * non_parallel_alpha

        # Re-use thresholds if one not available
        if close_tolerance_char_line and not close_tolerance_non_parallel_lines:
            close_tolerance_non_parallel_lines = close_tolerance_char_line
        return close_tolerance_non_parallel_lines, max_nonparallel_lines_dist


    def compute_char_line_threshold(self, G, parallel_line_distances, char_line_alpha,
                                    z_threshold):
        # np.seterr(all='raise')
        char_line_distances = G.graph["char_line_distances"]
        parallel_line_distances = sorted(parallel_line_distances)
        char_line_parallel_distances = np.concatenate((char_line_distances,
                                                    np.array(parallel_line_distances[:len(char_line_distances)])))
        # char_line_parallel_distances = np.concatenate((char_line_distances,
        #                                               np.array(parallel_line_distances)))
        
        if len(char_line_distances) >= 2:
            # avoid a divide by zero
            if np.all(char_line_parallel_distances==char_line_parallel_distances[0]):
                z_scores = np.zeros_like(char_line_parallel_distances)
            else:
                z_scores = np.abs(zscore(char_line_parallel_distances))
            selected_idx = (np.where(z_scores < z_threshold)[0])
            selected_idx = selected_idx[selected_idx < len(char_line_distances)]
            char_line_distances = char_line_distances[selected_idx]
            # try:
            # except Exception as e:
            #     print(traceback.print_exc())
            #     import pdb; pdb.set_trace()
        # char_line_distances = list(filter(lambda dis: dis<G.graph['max_length'] *
        #                                   REMOVE_PERCENT, char_line_distances.values()))

        char_line_distances = char_line_distances.tolist()
        G.graph["char_line_distances"] = char_line_distances

        if char_line_distances:
            G.graph["min_char_line_distances"] = min(char_line_distances, default=0)
            G.graph["max_char_line_distances"] = max(char_line_distances, default=0)

        max_char_line_distances = G.graph.get('max_char_line_distances', 0)
        close_tolerance_char_line = max_char_line_distances * char_line_alpha
        return G, close_tolerance_char_line, max_char_line_distances

    ################################################################
    # Identify bond line intersections, group parallel lines
    ################################################################

    def add_close_edges(self,  G, I):
        '''
        Find "close" edges and parallel edges from the original complete graph and add to graph
        Inputs: G - MST, I - Input complete graph
        Output: G with closed edges
        '''        
        char_line_alpha = self.CLOSE_CHAR_LINE_ALPHA
        z_threshold = self.Z_TOLERANCE
        non_parallel_alpha = self.CLOSE_NONPARALLEL_ALPHA
        remove_percent = self.REMOVE_ALPHA
                
        close_tolerance_non_parallel_lines = 0
        close_tolerance_char_line = 0
        close_tolerance_parallel = 0


        # 1. Closing parallel lines (without threshold)
        # I. Correct floating parallel line to correct parallel pair in MST using complete graph
        G, I_parallel_nonintersecting = self.close_parallel_lines(I, G.copy())

        # II. Compute close parallel threshold based on parallel lines in updated MST
        close_tolerance_parallel, parallel_line_distances = self.compute_parallel_threshold(G)

        # 2. Closing char and line nodes
        # Threshold for closing characters with lines 
        G, close_tolerance_char_line, max_char_line_distances = self.compute_char_line_threshold(G,
                                                                parallel_line_distances,                                                            
                                                                char_line_alpha, z_threshold)
        
        # 3. Computing threshold for non-parallel intersecting lines based on
        # updated MST
        close_tolerance_non_parallel_lines, max_nonparallel_lines_dist = self.compute_nonparallel_threshold(G, non_parallel_alpha,
                                                                        close_tolerance_char_line)

        if close_tolerance_char_line:
            remove_tolerance = max_char_line_distances * remove_percent
        elif close_tolerance_parallel:
            remove_tolerance = close_tolerance_parallel * remove_percent
        else:
            remove_tolerance = close_tolerance_non_parallel_lines * remove_percent

        # Re-use thresholds if one not available
        if close_tolerance_non_parallel_lines and not close_tolerance_char_line:
            close_tolerance_char_line = close_tolerance_non_parallel_lines

        # print("---------------------------")
        # print(f"max_char_line = {max_char_line_distances}, tolerance = {close_tolerance_char_line}")
        # print(f"max non parallel parallel = {max_nonparallel_lines_dist}, tolerance = {close_tolerance_non_parallel_lines}")
        # print(f"max_length = {G.graph['max_length']}, tolerance = {remove_tolerance}")
        # print("---------------------------")
        # print()

        node_lst = G.nodes.data()
        used_ends = set()
        char_line_merges = {}
        bond_classes = {'Ln', 'HW', 'SW'}
        G_chars_only = subgraph_view(G, filter_node=lambda n: G.nodes[n]['lbl_cls'] not in bond_classes)
        for node1 in node_lst:
            for node2 in node_lst:
                # Skip comparing if edge already exists or itself
                if node1 == node2:
                    continue
                # Skip comparing if we already compared this char-line merge.
                if (node1[0], node2[0]) in char_line_merges or (node2[0], node1[0]) in char_line_merges:
                    continue
                # dis = node1_shape.distance(node2_shape)
                dis = I[node1[0]][node2[0]]["weight"]
                
                # this is for "floating" molecules 
                if G.has_edge(node1[0], node2[0]):
                    if len(node1[0].split("_")[0]) == 1 or len(node2[0].split("_")[0]) == 1:
                        if dis > remove_tolerance:
                            # import pdb; pdb.set_trace()
                            G.remove_edge(node1[0], node2[0])
                            
                # Adding edge if it is close enough (closing loops and multi-intersecting lines)
                else:
                    node1_lblcls = node1[1]['lbl_cls']
                    node2_lblcls = node2[1]['lbl_cls']

                    if node1_lblcls in bond_classes or node2_lblcls in bond_classes:
                        # if dis < close_tolerance:
                        #     G.add_edge(node1[0], node2[0], weight=dis, added_edge=True)
                        if I[node1[0]][node2[0]]['parallel']:
                            # pass
                            if dis < close_tolerance_parallel:
                                G.add_edge(node1[0], node2[0], weight=dis, added_edge=True)
                        else:
                            # Non-parallel lines
                            if node1_lblcls in bond_classes and node2_lblcls in bond_classes:
                                # this is just for debugging
                                # if node1[1]['poly'] is None or node2[1]['poly'] is None:
                                #     print(f'poly is none! for {node1[0]} and {node2[0]}')
                                # if node1[1]['linestring'] is None or node2[1]['linestring'] is None:
                                #     print(f'linestring is none! for {node1[0]} and {node2[0]}')
                                
                                # HACK fallback to old method
                                if node1[1]['poly'] is None or node2[1]['poly'] is None or node1[1]['linestring'] is None or node2[1]['linestring'] is None :
                                    if dis < close_tolerance_non_parallel_lines:
                                        G.add_edge(node1[0], node2[0], weight=dis, added_edge=True)
                                    continue
                                # print(f'Using new intersection for {node1[0]} and {node2[0]}')
                                ipoly = node1[1]['poly'].intersection(node2[1]['poly'])
                                if not ipoly:
                                    continue

                                for (n1, n2) in ((node1, node2), (node2, node1)):
                                    line1 = n1[1]['linestring']
                                    p1 = Point(line1.coords[0])
                                    p2 = Point(line1.coords[1])

                                    new_dis = max(0, min(p1.distance(ipoly), p2.distance(ipoly)) - n2[1]['lineWidth'])
                                    if new_dis == 0:
                                        # PP: sign of L1 is a good comparison of line endedness *unless* the lines are nearly parallel or identical.
                                        line2 = n2[1]['linestring']
                                        nearest_points_on_lines = nearest_points(line1, line2)
                                        # Compute which "end" (-1 or 1) of each line is occupied by this connection.
                                        # If either "end" is 0, the line is vertical, and we must compare y instead.
                                        which_end_1 = np.sign(line1.centroid.x - nearest_points_on_lines[0].x) or np.sign(line1.centroid.y - nearest_points_on_lines[0].y)
                                        if which_end_1 != 0:
                                            used_ends.add((n1[0], which_end_1))
                                        else:
                                            raise ValueError # comparison is degenerate. Either line1 is a Point, or these lines have the same slope.
                                        which_end_2 = np.sign(line2.centroid.x - nearest_points_on_lines[1].x) or np.sign(line2.centroid.y - nearest_points_on_lines[1].y)
                                        if which_end_2 != 0:
                                            used_ends.add((n1[0], which_end_2))
                                        else:
                                            raise ValueError # comparison is degenerate. Either line2 is a Point, or these lines have the same slope.

                                        G.add_edge(node1[0], node2[0], weight=dis, added_edge=True)
                                        break

                            else:
                                # Character and line
                                if dis < close_tolerance_char_line:
                                    # G.add_edge(node1[0], node2[0], weight=dis, added_edge=True)
                                    # continue
                                    # PP: Hold off on merging chars to lines until all line-line mergers
                                    if node1_lblcls in bond_classes:
                                        char_line_merges[(node2[0], node1[0])] = (node2, node1)
                                    elif node2_lblcls in bond_classes:
                                        char_line_merges[(node2[0], node1[0])] = (node1, node2)
                    else:
                        if dis < close_tolerance_char_line:
                            # PP: Rare edge case that arises when two characters are not adjacent in the MST
                            # This didn't come before because: 1) rare in Indigo diagrams 2) previous add_close_edges strategy would cover this up
                            # We need to check if a path exists to prevent cyclic character clusters which would break `merge_chars`
                            if not has_path(G_chars_only, node1[0], node2[0]):
                                G.add_edge(node1[0], node2[0], weight=dis, added_edge=True)
        for char, line in char_line_merges.values():
            if G.has_edge(char[0], line[0]): # Prevent redundant computation
                continue
            dis = I[char[0]][line[0]]["weight"]
            if dis > close_tolerance_char_line:
                continue # Redundant sanity check; should never happen.
            line_shape = line[1]['shape_obj']
            line_centroid = line_shape.centroid
            nearest_endpoint = nearest_points(line_shape, char[1]['shape_obj'])[0]
            # Sign of L1 between endpoint and centroid is used to determined "endedness"
            which_end = np.sign(line_centroid.x - nearest_endpoint.x) or np.sign(line_centroid.y - nearest_endpoint.y)
            added_edge_on_this_end = (line[0], which_end) in used_ends
            if not added_edge_on_this_end:
                added_edge_on_this_end = list(
                        map(lambda right_end_for_nei: which_end == right_end_for_nei,
                            map(lambda endpoint: np.sign(line_centroid.x - endpoint.x) or np.sign(line_centroid.y - endpoint.y),
                                map(lambda shape: nearest_points(line_shape, shape)[0],
                                    map(lambda nei: G.nodes[nei]['shape_obj'],
                                        # Ignore neighbors that are parallel to `line` OR connected to `char` OR HACK: have a parallel neighbor connected to `char`
                                        # The last condition is only necessary because we have not merged parallel lines at this point.
                                        filter(lambda nei: not (I[line[0]][nei]['parallel'] or G.has_edge(char[0], nei) or
                                                                any(
                                                                    filter(lambda nei_nei: I[nei][nei_nei]['parallel'] and G.has_edge(char[0], nei_nei), G.neighbors(nei)))),
                                               G.neighbors(line[0]))))))
                            )
                # for nei, val in zip(filter(lambda nei: not I[line[0]][nei]['parallel'], G.neighbors(line[0])), added_edge_on_this_end):
                #     print(char[0], line[0], nei, val)
                added_edge_on_this_end = any(added_edge_on_this_end)
            if not added_edge_on_this_end:
                G.add_edge(char[0], line[0], weight=dis, added_edge=True)


        # gdraw(G, invert_y=True)
        # import pdb; pdb.set_trace()
        return G

    ################################################################
    # Functions to Label and Merge Nodes
    ################################################################

    def label_char_nodes(self, G):
        # Initial definition: Chemical characters are letters, numbers, or dashes 
        # (including minus and en-dash)
        chem_token_punc = set( [ '-', '(', ')', '+', '–', '•' ] )
        charges = {'-', '+'}

        for node in G.nodes:
            label = node.split('_')[0]
            if ( label.isalnum() or label in chem_token_punc ) and len(label) == 1 and G.nodes[node]['lbl_cls'] != 'Annotation':
                if label in charges:
                    G.nodes[node]['lbl_cls'] = 'Charge'
                else:
                    G.nodes[node]['lbl_cls'] = 'Char'


                # Add font size attribute
                (_, minY, _, maxY) = G.nodes[node]['box']
                #G.nodes[node]['font_size'] = round( (maxY-minY) * SCALE_Y )
                # HACK
                G.nodes[node]['font_size'] = max(6, math.ceil( (maxY-minY) * self.SCALE_Y ))

        return G

    def label_graph_info(self, G):
        # Labels the max line length as an attribute on the graph
        line_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'Ln' )
        graph_view = nx.subgraph_view(G, filter_node=line_selector)
        G.graph["max_length"] = max(nx.get_node_attributes(graph_view, 'length').values(),
                                    default=0)
        line_char_edge_selector = nx_merge.edge_2attr_match( G, 'obj_type', {'Char'},
                                                            'lbl_cls', {'Ln'})
        G_char_line = nx.subgraph_view(G, filter_edge=line_char_edge_selector)
        char_line_distances = nx.get_edge_attributes(G_char_line, 'weight')
        char_line_distances = np.array(list(char_line_distances.values()))
        G.graph["char_line_distances"] = char_line_distances

        return G

    def label_merged_chars(self,  G, M, G_unmerged, avoid_assertion=False ):
        for node in G.nodes:
            # Initialize (char, min_x) pair list with target character/node that
            # other nodes were contracted to
            if 'contraction' in  G.nodes[node]:
                start_node = node
                dict_contracted_nodes = G.nodes[node]['contraction']
                all_nodes = [node, *dict_contracted_nodes.keys()]
                # all_nodes_labels = list(map(str_remove_suffix, all_nodes))
                # digit_nodes = list(filter(lambda x:x.isdigit(), all_nodes_labels))
                subgraph = G_unmerged.subgraph(all_nodes)
                # End node is the node with degree 1 other than the start node
                end_node = list(filter(lambda n:subgraph.degree(n) == 1 and n !=
                                    start_node, subgraph.nodes()))
                # If the path from main atom has a unique path to its atom group end
                # if digit_nodes and (len(end_node) == 1):
                if len(end_node) == 1:
                    end_node = end_node[0]
                    subgraph_path = nx.shortest_path(subgraph, start_node, end_node)
                    # If graph order and characters order different, reverse the order
                    if subgraph.nodes[start_node]['box'][0] > subgraph.nodes[end_node]['box'][0]:
                        subgraph_path = subgraph_path[::-1]
                    if not avoid_assertion:
                        assert(len(subgraph_path) == len(subgraph.nodes()))
                    char_list = list(map(str_remove_suffix, subgraph_path))
                    # print(''.join(char_list))

                else:
                    # Sort by left-right top-down order
                    label_list = [ ( str_remove_suffix(node), G.nodes[node]['box'][:2] ) ]
                    for contract_node in dict_contracted_nodes:
                        label_list.append( ( str_remove_suffix(contract_node),  \
                                            dict_contracted_nodes[contract_node]['box'][:2] ) )
                    label_list = sorted( label_list, key=lambda k: k[1])
                    char_list, _ = zip( *label_list )

                    
                G_subgraph = G_unmerged.subgraph(all_nodes)
                # PP: don't fail on missing `'font_size'` in case we need to label merged chars before font size is determined.
                font_sizes = nx.get_node_attributes(G_subgraph, 'font_size').values()
                font_size = None
                if not len(font_sizes) == 0:
                    font_size = max(font_sizes)
                G.nodes[node]['contraction_label'] = ''.join(char_list)
                G.nodes[node]['lbl_type'] = ''.join(char_list)
                G.nodes[node]['font_size'] = font_size  # RZ addition

        return G

    def label_merged_lines(self,  G ):
        for node in G.nodes:
            if G.nodes[node]['lbl_cls'] == 'Ln' and 'contraction' in G.nodes[node]:
                dict_contracted_lines = G.nodes[node]['contraction']

                # Label number of lines
                line_count = 1 + len(dict_contracted_lines)

                G.nodes[node]['contraction_label'] = 'Ln||' + str(line_count)
                # G.nodes[node]['contraction_label'] = 'Ln||' + str(line_count) +' (og:' + node + ')'
                
    
        return G
    
    def label_merged_collinear_lines(self,  G ):
        for node in G.nodes:
            if G.nodes[node]['lbl_cls'] == 'Ln' and 'contraction' in G.nodes[node]:
                if 'contraction_label' not in G.nodes[node]:
                    G.nodes[node]['contraction_label'] = 'Dash'
                
        return G

    def merge_brackets(self,  G ):
        bracket_selector = nx_merge.node_attr_match( G, 'bracket', True )
        bracket_edge_selector = nx_merge.edge_attr_match( G, 'bracket', True )

        # Binary selector that I may need as of right now
        target = nx_merge.select_leftmost(G)

        # Merge related bracket nodes
        G_merged = nx_merge.select_and_contract_nodes( G, bracket_selector, bracket_edge_selector, btselect=target)
        G_merged.remove_edges_from(nx.selfloop_edges(G_merged))

        return G_merged

    ################################################################
    # Main Graph Transformation Functions
    ################################################################

    def close_edges(self,  G, I):
        # Adding edges for nodes that are close enough to "touch" or be parallel
        # Do before merging chars because merged char polygons will mess up pos attribute
        M, I = self.detect_negative_charge(G, I)
        M, I, annotations = self.identify_and_remove_annotations(M, I)
        I = self.geo.annotate_line_geometry(I)
        # Add annotations for parallel lines to edges     

        I = self.geo.annotate_edges_parallel_lines( I, M )

        G = self.geo.annotate_line_geometry( M.copy() )
        # Add annotations for parallel lines to edges 
        G = self.geo.annotate_edges_parallel_lines( G, M)
        G = self.label_graph_info(G)  # Gets the maximum line length, stores as graph attribute

        G = self.add_close_edges(G, I)
        G, M = self.add_annotations_to_graph(G, M, annotations)
        return G, M

    def identify_and_remove_annotations(self, G, I):
        NEG_CHARGE_Y_POSITION = self.NEG_CHARGE_Y_POSITION
        digit_selector = lambda n: G.nodes[n]['lbl_cls'].isdigit()
        G_merged = nx_merge.select_and_contract_nodes(G.copy(), digit_selector, btselect=nx_merge.select_leftmost(G))
        # PP: HACK reuse logic from `merge_chars` for digit merging
        G_merged = self.label_merged_chars(G_merged, I, G)
        G_merged.remove_edges_from(nx.selfloop_edges(G_merged))
        digits_view = nx.subgraph_view(G_merged, filter_node=digit_selector)
        annotations = {}
        nodes_to_remove = []
        # Sort left to right for now
        digit_nodes = sorted(digits_view.nodes(data=True), key=lambda n: n[1]['box'][0])
        for node, data in digit_nodes:
            box = data['box']
            pos = self.get_mid(box)
            height = box[3] - box[1]
            width = box[2] - box[0]
            is_annotation = True
            all_nodes_sorted = sorted(G_merged.nodes(data=True), key=lambda nei: np.linalg.norm(np.subtract(self.get_mid(nei[1]['box']), pos)))
            del all_nodes_sorted[0]
            # neis = sorted(I.neighbors(node), key=lambda nei: I[nei][node]['weight'])
            chars_checked = 0
            for node2, node2_data in all_nodes_sorted:
                if chars_checked > 3:
                    break # PP: HACK check only 3 nearest characters.
                # nei_data = I.nodes[nei]
                if node2_data['obj_type'] != 'Char' or node2_data['lbl_cls'].isdigit():
                    continue
                node2_box = node2_data['box']
                node2_pos = self.get_mid(node2_box)
                node2_height = node2_box[3] - node2_box[1]
                nei_width = node2_box[2] - node2_box[0]
                # if `node` can be a subscript of `nei`
                is_close_enough_x = abs(node2_box[2] - box[0]) < width
                is_right_of = node2_pos[0] < pos[0]
                is_bottom = pos[1] > (node2_pos[1] - NEG_CHARGE_Y_POSITION * node2_height)
                overlaps_y = box[1] <= node2_box[3]
                small_enough = node2_height / height > 1.2 # MAGIC NUMBER: char should be 20% bigger than a subscript.
                # print(node, nei, is_close_enough_x, is_right_of, is_bottom_left, overlaps_y, small_enough)
                if ( is_close_enough_x and is_right_of and is_bottom and overlaps_y and small_enough ):
                    is_annotation = False
                    break
                chars_checked = chars_checked + 1
            if is_annotation:
                G_merged.nodes[node]['obj_type'] = "Annotation"
                G_merged.nodes[node]['lbl_cls'] = "Annotation"
                nodes_to_remove.append(node)
                contraction = data.get('contraction')
                if contraction:
                    for contraction_child_data in contraction.values():
                        contraction_child_data['obj_type'] = "Annotation"
                        contraction_child_data['lbl_cls'] = "Annotation"
                for nei in G_merged.neighbors(node):
                    if G_merged.degree[nei] == 1:
                        nei_pos = self.get_mid(G_merged.nodes[nei]['box'])
                        connect_to = sorted(filter(lambda nei_node2: nei_node2[1]['lbl_cls'] != 'Annotation', G_merged.nodes(data=True)), key=lambda nei_node2: np.linalg.norm(np.subtract(self.get_mid(nei_node2[1]['box']), nei_pos)))[1]
                        G_merged.add_edge(nei, connect_to[0], weight=np.linalg.norm(np.subtract(self.get_mid(connect_to[1]['box']), nei_pos)))
                for node2, node2_data in all_nodes_sorted:
                    # nei_data = I.nodes[nei]
                    if node2_data['lbl_cls'].isdigit():
                        continue
                    if not annotations.get(node2):
                        annotations[node2] = (node, data)
                        break
        G_merged.remove_nodes_from(nodes_to_remove)

        return G_merged, I, annotations

    def add_annotations_to_graph(self, G, M, annotations):
        for atom, annotation in annotations.items():
            annotation, data = annotation
            G.add_node(annotation, **data)
            G.add_edge(atom, annotation)
            M.add_node(annotation, **data)
            M.add_edge(atom, annotation)

        return G, M

    def detect_negative_charge(self, G, I):
        G_relabeled = G.copy()
                        
        NEG_CHARGE_LENGTH_TOLERANCE = self.NEG_CHARGE_LENGTH_TOLERANCE
        NEG_CHARGE_Y_POSITION = self.NEG_CHARGE_Y_POSITION
        # Do close_edges before this so that we can remove chars that are too far
        # Node selector for Line nodes with single lines only
        line_selector = nx_merge.node_attr_match(G, 'isLine', True)
        graph_view = nx.subgraph_view(G, filter_node=line_selector)
        lengths = list(nx.get_node_attributes(graph_view, 'length').values())
        
        if lengths:
            mean_line_length = np.mean(lengths)
        else:
            return G_relabeled, I
                                    
        node_labels = {}
        for node, data in graph_view.nodes(data=True):
            # ne
            angle = data["angle"]
            length = data["length"]
            # if isinstance(angle, list) or isinstance(length, list):
            #     continue
            if angle > 1 and angle < 179:
                continue
            
            if length >= NEG_CHARGE_LENGTH_TOLERANCE * mean_line_length:
                continue
            neis = G.neighbors(node)
            pos_node = self.get_mid(data["box"])
            for nei in neis:
                obj_type = G.nodes[nei]["obj_type"]
                lbl_cls = G.nodes[nei]["lbl_cls"]
                if obj_type != "Char":
                    continue
                if lbl_cls in {"+", "-"}:            
                    continue

                box_nei = G.nodes[nei]['box']
                pos_nei = self.get_mid(box_nei)
                height_nei = box_nei[3] - box_nei[1]
                
                if pos_nei[0] < pos_node[0] and pos_node[1] < \
                (pos_nei[1] - NEG_CHARGE_Y_POSITION * height_nei):
                    G_relabeled.nodes[node]['obj_type'] = "Charge"
                    G_relabeled.nodes[node]['lbl_cls'] = "-"
                    G_relabeled.nodes[node]['lbl_type'] = "-"
                    G_relabeled.nodes[node]['isLine'] = False
                    node_labels[node] = "-_" + node.split("_")[1]
                    break
        if node_labels:
            G_relabeled = nx.relabel_nodes(G_relabeled, node_labels)
            I = nx.relabel_nodes(I, node_labels)

        return G_relabeled, I

    def merge_chars(self, M, G ):
        # Do close_edges before this so that we can remove chars that are too far
        # Node selector for character nodes
        G = self.label_char_nodes(G)
        f_node = nx_merge.node_attrs_match(G, 'lbl_cls', 'Char', 'Charge')

        # Binary selector for leftmost node in a connected component as node to hold CC node set
        # gdraw(M)
        target_alt = nx_merge.select_main_atom(M)
        target = nx_merge.select_left_topmost(G)

        # Merge character nodes, add 'contraction_label' with full string 
        # to node holding CC node set
        G_merged = nx_merge.select_and_contract_nodes( G, f_node, btselect=target,
                                                    btselect_alt=target_alt )
        G_merged_labeled = self.label_merged_chars( G_merged, M, G )
        G_merged_labeled.remove_edges_from(nx.selfloop_edges(G_merged_labeled))
        G_merged_labeled = self.geo.annotate_char_pos(G_merged_labeled)
        # gdraw(G_merged_labeled)
        # import pdb; pdb.set_trace()
        

        return G_merged_labeled

    def label_possible_annotations(self, G):
        char_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'Char' )
        graph_view = nx.subgraph_view(G, filter_node=char_selector)
        
        text_sizes = np.array([graph_view.nodes[g]['font_size'] for g in graph_view.nodes])
        if len(text_sizes) == 0:
            return G
        
        min_text_size = np.min(text_sizes)
        max_text_size = np.max(text_sizes)
        
        guide_lines = []
        for node in graph_view.nodes:
            min_delta = abs(min_text_size - G.nodes[node]['font_size'])
            max_delta = abs(max_text_size - G.nodes[node]['font_size'])
            if min_delta < max_delta:
                G.nodes[node]['annotation'] = True
                
                # remove the "guide lines"
                # even if it's just a thing in these examples it cleans up the output a lot
                G, l = self.geo.get_guide_line(G, node)
                if l is not None:
                    guide_lines.append(l)
            
        for l in guide_lines:
            if l in G.nodes:
                G.remove_node(l)

        return G
    
    def merge_parallel_lines(self,  G, mst_graph):
        # TODO Fix vertical double bond positioning
        # Add annotations for parallel lines to edges 
        # ASSUME annotate_line_geometry or close_edges was done before this
        max_font_size = max(filter(lambda font_size: font_size is not None, map(lambda node: node[1].get('font_size'), G.nodes(data=True))), default=0)
        G = self.geo.annotate_edges_parallel_lines( G, mst_graph)

        # Node select for line nodes, edge selector for edges between parallel lines
        line_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'Ln' )
        parallel_edge_selector = nx_merge.edge_attr_match( G, 'parallel', True )
        # HACK: This "selector" mutates edges. might result in non-determinism depending on graph traversal.
        def small_next_to_hash(n1, n2):
            has_match = any(any(G.nodes[n1]['length'] < 20 and G.edges[edge].get('hash') for edge in G.edges(n2)) for n1, n2 in [(n1, n2), (n2, n1)])
            G.edges[(n1, n2)]['hash'] = has_match
            return has_match
        def parallel_or_hash_selector(n1, n2):
            return parallel_edge_selector(n1, n2) or small_next_to_hash(n1, n2)
        old_edge_selector = nx_merge.edge_attr_exclude(G, 'added_edge', True)
        graph = nx.subgraph_view(G, filter_edge=old_edge_selector)

        # Binary selector for base parallel node
        target = nx_merge.select_max_degree( graph )

        # Merge parallel lines, add 'contraction_label' to hold line node set
        G_merged = nx_merge.select_and_contract_nodes( G, line_selector, f_edge=parallel_or_hash_selector, btselect=target)
        G_merged_labeled = self.label_merged_lines( G_merged )
        G_merged_labeled.remove_edges_from(nx.selfloop_edges(G_merged_labeled))

        return G_merged_labeled

    def merge_collinear_lines(self, G ):
        G = self.geo.annotate_edges_collinear_lines( G )

        # Node select for line nodes, edge selector for edges between collinear lines
        line_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'Ln' )
        collinear_edge_selector = nx_merge.edge_attr_match( G, 'collinear', True )
        graph = nx.subgraph_view(G, filter_edge=collinear_edge_selector)

        target = nx_merge.select_max_degree( graph )

        # gdraw(G)
        # show_vgdescr(G)
        # Merge collinear lines, add 'contraction_label' to hold line node set
        G_merged = nx_merge.select_and_contract_nodes( G, line_selector, f_edge=collinear_edge_selector, btselect=target)
        # gdraw(G_merged)
        # show_vgdescr(G_merged)
        # import pdb; pdb.set_trace()
        G_merged_labeled = self.label_merged_collinear_lines( G_merged )
        G_merged_labeled.remove_edges_from(nx.selfloop_edges(G_merged_labeled))

        return G_merged_labeled

    def fixing_wedge(self,  G ):
        # Add annotations to identify hash wedges and wedge vertices
        G = self.geo.annotate_solid_wedge_verts( G )
        G = self.geo.annotate_hash_lines( G )
        G = self.geo.annotate_hash_verts( G )
        return G

    def annotate_and_merge_brackets(self,  G ):
        # label bracket pairs to merge
        G = markush.pair_bracket_nodes(G)

        # merge bracket pairs first, then label how neighbors are related
        first_merge = self.merge_brackets(G)
        post_first_merge = markush.sort_bracket_neis(first_merge)

        # merge related bracket character annotation second
        second_merge = self.merge_brackets(post_first_merge)

        return second_merge

    def label_circles(self, G):
        G = self.geo.annotate_circles(G)

        return G

    def fixing_line_pos(self, G, merge_parallel_line_endpoints=True):
        # Redoing vertex positions so that vertexes that intersect
        # have the same position
        G = self.geo.annotate_line_edge_intersect( G )
        G = self.geo.label_correlate_adj_verts( G )
        G = self.geo.merge_collinear_line_endpoints( G )

        # RZ: Debug -- correct vertex end points for merged lines.
        # Assuming that each line has a verts attribute:
        #  * Estimate end points as average of closest end points across lines.
        if merge_parallel_line_endpoints:
            G = self.geo.merge_parallel_line_endpoints( G )
        

        return G

    @staticmethod
    def remove_hydrogen(string):
        elements = [s for s in re.split(r"H\d*", string) if s]            
        if elements:
            return elements[0]
        else:
            return ""

    def add_hidden_carbon(self,  c_int_id, F, pos_final ):
        # Create new carbon with int identifer, store vertices
        end_node_name = 'C_' + str(c_int_id)
        F.add_nodes_from( [ (end_node_name, {'pos': pos_final}) ] )

        return (F, end_node_name, c_int_id + 1)

    def add_visible_node(self, G, adj_lst, F, g_node_id, end_node_name, pos_final):
        # Generate font size and alignment
        (bbox, font_size, label_alignment) = (None, None, None)
        adj_node = G.nodes[g_node_id]

        # RZ: 'cbox' for merges ('content box' --?)
        if 'cbox' in adj_node:  
            bbox = adj_node['cbox'] 
            font_size = adj_node['font_size'] #round( adj_node['font_size'], ndigits=2 )
        else:
            bbox = adj_node['box'] 
            font_size = adj_node['font_size']

        # Bounding box coordinates and font size
        (xmin, ymin, xmax, ymax) = bbox
        (px, py) = pos_final

        # RZ: Add alignment based on nearest endpoint location around bbox
        # Subtlety -- do not right-align if chemical name is vertical
        label_alignment = None
        tolerance = 18 # pts, ~1/4 inch
        vertical = (ymax-ymin) > (xmax-xmin)
        if not vertical and px > xmax - tolerance:
                label_alignment = 'Right'

        # RZ: Record attachment side (bond connection to label -- **may be imperfect
        #     for labels with multiple connection points)
        attach_side = None
        if px < xmin + tolerance:
            attach_side = Side.LEFT
        elif px > xmax - tolerance:
            attach_side = Side.RIGHT
        elif py < ymin + tolerance:
            attach_side = Side.TOP
        else:
            attach_side = Side.BOTTOM

        if not label_alignment:
            F.add_nodes_from( [
                (end_node_name, {'pos': pos_final, 'bbox' : bbox, 'font_size' : font_size,
                    'np_side' : attach_side })] )
        else:
            F.add_nodes_from([
                (end_node_name, {'pos': pos_final, 'bbox' : bbox, 'font_size' : font_size, 
                    'LabelDisplay' : label_alignment, 'np_side': attach_side})])

        return F 

    def update_visible_node_label(self,  c_id, G, node_id, lbl_idx_dict):
        if G.nodes[node_id]['lbl_type'].startswith('C_'):                     # VISIBLE CARBON
            name = 'C_' + str(c_id) 
            c_id += 1
        else:
            lbl_type = G.nodes[node_id]['lbl_type']
            lbl_idx = int(node_id.split('_')[1])
            # PP: Prevent name collisions due to contraction
            idx_dict = lbl_idx_dict[lbl_type]
            while lbl_idx in idx_dict and idx_dict[lbl_idx] != node_id:
                lbl_idx += 1
            idx_dict[lbl_idx] = node_id
            name = lbl_type + '_' + str(lbl_idx)

        return (c_id, name)

    def bond_type(self,  G, line_subgraph_node):
        ln_type =                           'UNKNOWN'  # Default
        
        # Check for merged/contracted parallel lines 
        if 'contraction_label' in G.nodes[line_subgraph_node]:
            lbl = G.nodes[line_subgraph_node]['contraction_label']
            if lbl == 'Hash':               ln_type = 'H Wedge'
            elif lbl == 'Dash':             ln_type = 'Dash'
            else:
                lbl = lbl.split('||')[1].strip(" ")
                if lbl == '2':              ln_type = 'Double'
                elif lbl == '3':            ln_type = 'Triple'
        
        # Check node label text for isolated line nodes
        elif 'Ln' in line_subgraph_node:    ln_type = 'Single'
        elif 'Wave' in line_subgraph_node:  ln_type = 'Wave'
        elif 'SW' in line_subgraph_node:    ln_type = 'S Wedge'
        elif 'HW' in line_subgraph_node:    ln_type = 'H Wedge'

        return ln_type

    # DEBUG routines -- filter for specific graph
    def check_gid(self, label, value, gid, gid_match):
        if gid==gid_match:
            check(label, value)

    def pcheck_gid(self, label, value, gid, gid_match):
        if gid==gid_match:
            pcheck(label, value)

    ##################################
    # Final Steps to Dual Graph
    ##################################
    
    def create_final_graph(self, G, gid=None):
        # Create/add nodes at line intersections and ends for final graph
        # C_nodes = filter(lambda x:"C" in x, list(G.nodes())) 
        # mapping = {node: node.split("_")[0]+"*_" + node.split("_")[1] for node in C_nodes}

        # G = nx.relabel_nodes(G, mapping)
        F = nx.Graph()              # Output graph
        position_node_dict = dict() # Dictionary to record node positions ( vertex -> node_id )
        # TODO: CUrrently set to high value otherwise confused with already existing C_cid nodes
        c_id = G.number_of_nodes() + 1                  # Carbon integer id

        circle_dict = {}
        if 'circle' in G.graph:
            circle_selector = nx_merge.node_attr_match( G, 'lbl_cls', 'Circle' )
            graph_view = nx.subgraph_view(G, filter_node=circle_selector)
            for node in graph_view.nodes:
                x,y = G.nodes[node]['poly'].centroid.xy
                center_point = [x[0],y[0]]
                name = "circle_" + str(c_id)    # hack, copied from floating/bracket code
                circle_dict[node] = name    # hack
                c_id += 1
                
                F.add_nodes_from([(name, {'pos': center_point, 'box': G.nodes[node]['box']})])

        graph_view = nx.subgraph_view(G, filter_node=nx_merge.select_all_lines(G))
        line_subgraph_node_list = list(graph_view.nodes)  

        # Iterate over line nodes in the graph
        lbl_idx_dict = defaultdict(dict)
        for line_subgraph_node in line_subgraph_node_list:
            line_end_nodes = []                           # Nodes at line end points 
            adj = G.nodes[line_subgraph_node]['adj']      # End-point neighbors (0+ at each end)
            line_vertices = G.nodes[line_subgraph_node]['verts']  # Line endpoint vertices

            merged_verts = None                           # Merged vertices (if present)
            if 'merged_verts' in G.nodes[line_subgraph_node]:  
                merged_verts = G.nodes[line_subgraph_node]['merged_verts']  

            # Iterate over endpoints (i.e., neighbors at 2 line endpointpoints)
            num_line_endpoints = 2
            self.check_gid('-- line_vertices', line_vertices, gid, self.REGION_DBG_ID)
            self.check_gid('-- adj',adj,gid, self.REGION_DBG_ID)
            for i in range(num_line_endpoints):   
                # Check for existing node for new graph at line end i (0 or 1)
                end_node_name = position_node_dict.get(tuple(line_vertices[i])) 
                pos_final = line_vertices[i]

                # Add node at endpoint IF node not already located there
                # NOTE: A visible node may have connections on multiple sides/line endpoints
                if end_node_name is None:
                    adj_lst = adj[i]
                    g_node_id = None
                    node_label_class = None

                    for nid in adj_lst:
                        g_node_id = nid
                        node_label_class = G.nodes[nid]['lbl_cls']
                        if not nid.startswith('Ln_'):
                            break

                    self.check_gid("++ g_node_id",g_node_id,gid, self.REGION_DBG_ID)
                    if node_label_class in {'Char', 'Charge'}:
                        # VISIBLE NODE (incl. visible CARBON)
                        #g_node_id = adj_lst[0] 
                        #node_label_class = G.nodes[nid]['lbl_cls']
                        (c_id, end_node_name) = self.update_visible_node_label(c_id, G, g_node_id, lbl_idx_dict)
                        self.check_gid('  * (line_subgraph_node, i, adj)', (line_subgraph_node, i, adj), gid, self.REGION_DBG_ID)
                        self.check_gid('  *  end_node_name', end_node_name,gid, self.REGION_DBG_ID)
                        # RZ: DEBUG Use merged positions ONLY for label endpoints 
                        if merged_verts and G.degree(g_node_id) < 2: pos_final = merged_verts[i]
                        F = self.add_visible_node(G, adj_lst, F, g_node_id, end_node_name, pos_final)

                    else:
                        # HIDDEN CARBON
                        if len(adj_lst) == 0 and merged_verts: pos_final = merged_verts[i]  # LINE END
                        (F, end_node_name, c_id) = self.add_hidden_carbon(c_id, F, pos_final)

                        # Index new node name at the current line endpoint (vertex)
                        position_node_dict[tuple(line_vertices[i])] = end_node_name
                    # PP: Attach the atom number or annotation to the node
                    if g_node_id:
                        annotation = next(filter(lambda nei: G.nodes[nei]['lbl_cls'] == 'Annotation', G.neighbors(g_node_id)), None)
                        if annotation:
                            F.nodes[end_node_name]['annotation'] = (annotation, G.nodes[annotation])
                else:
                    self.check_gid('!! Found existing node:',end_node_name,gid, self.REGION_DBG_ID)

                # if 'annotation' not in F.nodes[end_node_name]:    
                line_end_nodes.append(end_node_name)
            
            if 'circle_connection' in G.nodes[line_subgraph_node]:
                F.add_edge(line_end_nodes[0], circle_dict[G.nodes[line_subgraph_node]['circle_connection']], type='circle_connection')

            # Inserting Edges with Bond Type; Annoated wedge bonds to preserve direction
            self.check_gid('line_end_nodes',(line_subgraph_node, line_end_nodes), gid, self.REGION_DBG_ID)
            line_bond_type =  self.bond_type( G, line_subgraph_node)
            if "Wedge" in line_bond_type:
                F.add_edge(line_end_nodes[0], line_end_nodes[1], type=line_bond_type)
                F[line_end_nodes[0]][line_end_nodes[1]]["dir_edge"] = line_end_nodes 
            else:
                F.add_edge(line_end_nodes[0], line_end_nodes[1], type=line_bond_type)

        # Floating molecules
        if 'floating' in G.graph:
            floating_selector = nx_merge.node_attr_match(G, 'floating', True)
            graph_view = nx.subgraph_view(G, filter_node=floating_selector)
            for node in graph_view.nodes:
                name = G.nodes[node]['lbl_type'] + "_" + str(c_id)  # Hack
                c_id += 1

                F.add_nodes_from([(name, {'pos': G.nodes[node]['pos']})])

        # Bracket
        if 'bracket' in G.graph:
            bracket_list = markush.find_bracket_inner_nodes(G, F, position_node_dict)
            for bracket in bracket_list:
                b = G.nodes[bracket]
                x,y = b['poly'].centroid.xy
                center_point = [x[0],y[0]]
                name = "bracket_" + str(c_id)   # hack
                c_id += 1

                # Note 'pos' is needed for debugging, attach box also for debugging
                try:
                    F.add_nodes_from([(name, {'pos': center_point, 'box1': b['box'], 'box2': b['box2'], 
                        'bracket_type': b['bracket_type'], 'bracket_usage': b['bracket_usage'], 
                        'attach': b['attach'], 'attach_box': b['attach_box'],
                        'inner_objects': b['inner_objects']})])
                except:
                    pass

        return F


    ################################################################
    # Top-level Functions to Parse MSTs
    ################################################################

    def transform_graph(self,  mst_graph, in_graph):
        # RZ: Added for multiprocessing
        if mst_graph is None:
            return None
        G, mst_graph = self.close_edges(mst_graph.copy(), in_graph)
        A = self.merge_chars(mst_graph.copy(), G )
        # H = self.merge_chars(mst_graph.copy(), G )
        # A = self.label_possible_annotations(H)
        I = self.merge_parallel_lines( A , mst_graph)
        C = self.merge_collinear_lines( I )
        I = self.annotate_and_merge_brackets(C)
        O = self.label_circles(I)
        J = self.fixing_line_pos(self.fixing_wedge( O ))
        J = self.create_final_graph(J)
        return J



    def post_process_msts(self,  dict_msts, dict_input_graphs, dict_formula_regions,
                        pdf_dir, debug=False, parallel=False ):
        """
        Merges neighboring characters in MST graphs for detected formula regions
        with associated PDF graphics objects.

        All dictionaries use integer page identifiers as keys.  NOTE: page
        identifiers correspond to pages with regions in dict_formula_regions, and
        NOT actual page numbers.

        Args:
            @param dict_msts: dict of networkX MST graphs over graphic objects
            @param dict_input_graphs: dict of networkX complete graphs 
            @param dict_formula_regions: dict of formula region detection on each page

        Returns:
            Updated dict_final_graph: dict of visual graphs used in cdxml_conversion

        """
        # change if you want the output for carbons
        # Otherwise just get OG MST, Merged MST, Final Graph
        carbons = True
        if debug: # for debugging
            # RZ: PDF generation currenty not working.

            # Create the directory for saving MST PDF 
            mst_dir = os.path.join(pdf_dir, 'MST_PDF')
            if not os.path.exists(mst_dir):
                os.makedirs(mst_dir)

            # RZ: Checking on available data types and keys
            check("TYPES", [ type(ds) for ds in [ dict_msts, dict_input_graphs, dict_formula_regions ] ])
            check("KEYS (mst)", dict_msts.keys())
            check("KEYS (input_graphs)", dict_input_graphs.keys())
            check("KEYS (formula_regions)", dict_formula_regions.keys())
            #pcheck('regions', dict_formula_regions )

            # Making Debug MSTs: Initial, Merged, Carbons(optional), Final Graph
            # Input MSTs
            parser_debug.output_mst_PDF("Initial MSTs", dict_msts, mst_dir)

            # RZ addition: Full parsing pipeline -- in order used in pipeline
            fns_lst_all = [self.close_edges, self.merge_chars, self.merge_parallel_lines, self.annotate_and_merge_brackets,
                    self.fixing_wedge, self.fixing_line_pos, self.create_final_graph] 
            mst_dict = parser_debug.output_mst_PDF("Final MSTs (SAME ORDER as Pipeline)", dict_msts, mst_dir, 
                    fns_lst_all, in_graphs=dict_input_graphs)

            # individual carbon atom graphs
            fns_lst_2 = [self.close_edges, self.merge_chars, self.merge_parallel_lines, self.fixing_wedge, self.annotate_and_merge_brackets]
            mst_dict = parser_debug.output_mst_PDF("Partial Step MSTs", dict_msts, mst_dir, fns_lst_2, in_graphs=dict_input_graphs)

            fns_lst = [parser_debug.adding_all_carbons]
            if carbons:
                parser_debug.output_mst_PDF("Carbon Graph Before Correlate Verts", deepcopy(mst_dict), mst_dir, fns_lst,
                                            selector=nx_merge.select_no_lines, hide_enum=True)
                fns_lst = [self.fixing_line_pos, parser_debug.adding_all_carbons]
                parser_debug.output_mst_PDF("Carbon Graph After Correlate Verts", deepcopy(mst_dict), mst_dir, fns_lst, 
                                            selector=nx_merge.select_no_lines, hide_enum=True)
            fns_lst = [self.fixing_line_pos, self.create_final_graph]
            parser_debug.output_mst_PDF("Final Graph", mst_dict, mst_dir, fns_lst, in_graphs=dict_input_graphs)

            # Exit after PDFs written
            sys.exit(0)

        ##################################
        # Pipeline execution
        # Parallel *if* parallel = true
        ##################################
        dict_final_graph = {}
        if parallel:
            # Map-reduce execution (using mrmpr library)
            mst_lists = sorted(list(dict_msts.items()), key=itemgetter(0))
            in_graph_lists = sorted(list(dict_input_graphs.items()), key=itemgetter(0))            
            thresholds_copy = self.thresholds_ref.copy()
            input_list = list(zip(mst_lists, in_graph_lists, repeat(thresholds_copy)))

            if len(input_list) > 0:
                dict_final_graph = map_reduce( map_parse_graph, combine_dict_pair, input_list)

        else:
            # Sequential execution (for testing / debugging )
            for page_id in dict_msts:
                page_msts = []
                input_graphs = dict_input_graphs[page_id]
                    
                for i, mst_graph in enumerate(dict_msts[ page_id ]):                    
                    # print(gdescr(mst_graph), '\n')
                    in_graph = input_graphs[i]
                    F = self.transform_graph(mst_graph, in_graph)
                    page_msts.append(F)

                    # if i == self.REGION_DBG_ID:
                    #     check('MST graph (region ' + str(i) + ')', gdescr(mst_graph))
                    #     check('Line pos fixed (region ' + str(i) + ')', gdescr(J))
                    #     check('Final graph (region ' + str(i) + ')', gdescr(F))

                dict_final_graph[page_id] = page_msts

            
        return dict_final_graph

def map_parse_graph(pair):
    # Deconstruct dictionary item pair for a page into its parts.
    
    msts, in_graphs, thresholds = pair
    parser = Parser_v2(thresholds)
    ( page_id, mst_list ) = msts
    ( page_id2, in_graph_list ) = in_graphs
    assert(page_id == page_id2)
    output_dict = dict()
    output_graph_list = []
    for next_mst, next_in_graph in zip(mst_list, in_graph_list):
        output_graph_list.append( parser.transform_graph( next_mst, next_in_graph) )

    # Instantiate as dict for use in dict-based merge in reduce step (pages are spread across processes)
    output_dict[ page_id ] = output_graph_list
    return output_dict
