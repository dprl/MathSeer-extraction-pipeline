## Installation of OpenBabel (For Ubuntu with Conda for Python 3.6)
### Install OBabel Binary first (With Python Bindings enabled)
* Extract the binary of one of latest OBabel releases from here: https://github.com/openbabel/openbabel/releases (tar.bz2 file)
* Extract the tar ball and cd into it:
* `mkdir build` 
* `cd build`
* Install cmake if you have not already - `sudo apt-get install cmake`
* Make your own conda environment and activate it
* Make sure to find the python lib locations of your conda environment. Most common location is `/home/<username>/anaconda3/envs/<env name>/lib`
* **Python bindings with your Anaconda distro -** Make sure to change the paths as appropriate for your system for the (For e.g. my conda environment is named `cext`.  
* **Build config -** ```cmake ../../openbabel-3.1.1 .. -DPYTHON_BINDINGS=ON -DCMAKE_INSTALL_PREFIX=~/anaconda3 -DPYTHON_INCLUDE_DIR=~/anaconda3/envs/cext/include/python3.6m -DCMAKE_LIBRARY_PATH=~/anaconda3/envs/cext/lib -DPYTHON_LIBRARY=~/anaconda3/envs/cext/lib/libpython3.6m.so -DCMAKE_BUILD_TYPE=DEBUG```
* `make -j4` (To build with 4 processors)
* `make install`

### Installing the PyPi python package (In your Conda Environment)
* pip install openbabel
* Try importing openbabel from a python shell: `import openbabel`
* If you get an error like (as shown here: https://open-babel.readthedocs.io/en/latest/UseTheLibrary/PythonInstall.html#linux-and-macosx):
```  
python
>>> from openbabel import openbabel
Traceback (most recent call last):
  File "<stdin>", line 1, in
  File "/usr/lib/python2.4/site-packages/openbabel.py", line 9, in
   import _openbabel
ImportError: libopenbabel.so.7: cannot open shared object file: No such file or directory
```
* Create a new config file in your ld library
    * `cd /etc/ld.so.conf.d`
    * Create a file (e.g. `open-babel.conf`) and add the path to the lib `libopenbabel.so` (This was installed during the OpenBabel Binary installation from source). For me it was installed in my anaconda distro: `/home/<username>/anaconda3/lib/libopenbabel.so`
    * Save the file
    * Activate your new path: `sudo ldconfig`
    * You should now be able to import openbabel in a python shell with your conda env
* Good Luck!

