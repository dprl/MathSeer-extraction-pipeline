import time
import os
import shutil
import tempfile
from io import StringIO, BytesIO
from typing import Dict
import json
import requests
import zipfile
import copy
from pdf_annotate import PdfAnnotator, Location, Appearance
from modules.chemscraper.extract_mst import ExtractMST
from modules.chemscraper.graph_conversions.CDXML_Converter import visual2cdxml
from modules.pipeline.pipeline_run_chemx import gen_smiles, gen_svg, gen_tsv, gen_full_page_cdxml
from modules.protables.ProTables import read_region_csv_from_io, region_intersection,\
    read_sscraper_json, region_intersection_with_visual, read_pro_tsv_from_io,\
    read_sscraper_json_from_io, grow_for_contents, merge_pro_tables,\
    transform_pr_table, region_bb_list, apply_obj_within_region,\
    separate_visual_regions, crop_contents, obj_keep_types, filter_pr_table,\
    sort_page_regions
from modules.protables.util import get_cc_objects_original, get_cc_objects_original_v2

from modules.pipeline.pipeline_run_chemx import convert2image_pymupdf
from pprint import pprint

from modules.chemscraper import parser_v2

from modules.pipeline.pipeline_run_chemx import parse_images_from_visual
from configparser import ConfigParser, ExtendedInterpolation
from modules.chemscraper.utils.merging import merge_page_boxes

from modules.protables.debug_fns import DebugTimer, check
import modules.protables.debug_fns as debug_fns
dTimer = DebugTimer("ChemScraper pipeline")
debug_fns.DEBUG = True # Run all debug commands and produce output for now. Set to False to hide output/prevent.

# TODO: Later, read from config file
non_parallel_alpha = 1.8
char_line_alpha = 2.0
remove_percent = 2.5
neg_charge_y_threshold = 0.3
neg_charge_length_threshold = 0.5
z_threshold = 1.4



def get_yolo_version(service_name):
    r = (f"http://{service_name}:8005/get_version")    
    resp = requests.get(r)    
    if resp.status_code != 200:
        print("request failed!")
        return None
    return resp.content

def get_symbolscraper_version(service_name):
    r = (f"http://{service_name}:7002/get-version")    
    resp = requests.get(r)    
    if resp.status_code != 200:
        print("request failed!")
        return None
    return resp.content


def process_pdf(pdf_bytes, service_name):

    print('\n [ SYMBOL SCRAPER -- SERVICE PROCESSING PDF... ]')
    start = time.time()
    # r = f"http://{service_name}:7002/process-pdf?writePageBorderBBOX=true&writePoints=true"
    r = (f"http://{service_name}:7002/explain-geometry?processors=ExplainGeometry&"
         "writePageBorderBBOX=true&writePoints=true")

    file = {"pdf": ("test", pdf_bytes, "application/pdf")}
    resp = requests.post(r, files=file)
    if resp.status_code != 200:
        print("request failed!")
        return None
    print(f'SymScraper took: {time.time()-start} secs')
    return resp.content


def process_yolo_from_images(img_dir, service_name):
    print("[START YOLOV8]")
    start = time.time()        
    endpoint = f"http://{service_name}:8005/yolo_from_images"    
    
    yolo_img_dir = os.path.abspath(img_dir)
    
    for (A, _, _filenames) in os.walk(yolo_img_dir):
        array_files = []        
        pdf_name = os.path.basename(A)
        for i in _filenames:            
            f = open(f"{A}/{i}", 'rb')
            array_files.append(('images',(i,f,'image/png')))
        if len(array_files) != 0:
            files_sorted = sorted(array_files, key=lambda x: int(os.path.basename(x[1][0])[:-4]))        
            resp = requests.post(endpoint, files=files_sorted, timeout=None)
            if resp.status_code != 200:
                print(f"Request failed! Yolov8 to: {endpoint}. File: {pdf_name}")
                print("resp.reason",resp.reason)
                return None
            else:                        
                print(f'YOLO detections took: {time.time()-start} secs')
                print("[END YOLOV8]")
                data = json.loads(resp.content.decode())
            map(lambda x: x[1][1].close(),array_files)
    return data
            
def process_yolo(pdf_bytes, dpi, service_name):
    start = time.time()        
    r = f"http://{service_name}:8005/yolo"

    file = {"pdf": ("test", pdf_bytes, "application/pdf")}
    resp = requests.post(r, files=file, params={"dpi": dpi})
    if resp.status_code != 200:
        print("request failed!")
        return None
    print(f'YOLO detections took: {time.time()-start} secs')
    return json.loads(resp.content.decode())

def zipfiles(filenames,fpath):
    s = BytesIO()
    zf = zipfile.ZipFile(s, "w")

    for fname in filenames:                
        zf.write(fpath+fname,arcname=fname, compress_type=zipfile.ZIP_DEFLATED)
    
    zf.close()
    return s

def convert_pdf2image(pdf_bytes, dpi=300, pdf_image_dir="temp/pdf_img_dir"):
    if os.path.exists(pdf_image_dir):
        shutil.rmtree(pdf_image_dir)
    with tempfile.TemporaryDirectory() as pdf_dir:
        with open(os.path.join(pdf_dir, "temp.pdf"),"wb") as f:
            f.write(pdf_bytes)            
        class temp_args():
            def __init__(self):
                self.in_pdf_dir = pdf_dir
                self.dpi = dpi
        temp_args_instance = temp_args()
        convert2image_pymupdf(temp_args_instance, pdf_image_dir)

def server_combine(json: str, generate_svg: bool, csv_dict: Dict, pdf=None,
                   lgap_server_name="lgap", dTimer=dTimer, pdf_img_dir="temp/pdf_img_dir/temp"):

    print('\n [ COMBINING SYMBOL SCRAPER AND YOLO OUTPUTS... ]')
    all_pdf_msts = {}
    all_pdf_frs = {}
    metrics = {}

    # Extract SymbolScraper Information
    num_pages, page_sizes, _, pageWordBBs,  _, _, pageCharGeometryBBs = \
        read_sscraper_json_from_io(BytesIO(json), "server", pdf_img_dir, extract_graphics=True)
        # read_sscraper_json_from_io(BytesIO(json), "server", None, extract_graphics=True,
        #                            page_size_map= csv_dict["size_dict"])
    csv_IO = StringIO(csv_dict["csv"])
    dTimer.qcheck("Reading Symbol Scraper Info (JSON)")

    non_blank = len(list(filter(lambda x: len(x) > 0, pageCharGeometryBBs)))
    check('* PDF pages',num_pages)
    check('*     non-blank', non_blank)

    filename = "molecules"
    metrics[filename] = {}
    metrics[filename]['PDF-pages'] = num_pages
    metrics[filename]['PDF-non-blank'] = non_blank

    # Extract YOLO detections
    all_chem_bbs = read_region_csv_from_io(csv_IO, num_pages)
    check('* Initial YOLO detections', sum(map(len, all_chem_bbs)))

    merged_chem_bbs = list( map(merge_page_boxes, enumerate(all_chem_bbs)) )
    num_regions = sum(map(len, merged_chem_bbs))
    metrics[ filename ][ 'YOLO-detections' ] = num_regions

    dTimer.qcheck("Reading YOLO regions (CSV)")

    # Merge formula regions from YOLO and primitives from SymbolScraper
    print(f'\n [ Creating merged ProTables: SymbolScraper and YOLO ... ]')
    regionCharGeometryPROTable, metrics = \
        region_intersection(merged_chem_bbs, 'FR', pageCharGeometryBBs,
                            metrics=metrics, filename=filename, 
                            sort_obj_types=False)
    # dTimer.check("Region intersection")
    regionBBs = region_bb_list(regionCharGeometryPROTable)
    # dTimer.check("Region bb list")
    # regionCharGeometryWordPROTable = \
    #     merge_pro_tables( grownRegionWordPROTable, regionCharGeometryPROTable  )

    # print('\n [ Converting PDF to images ... ]')
    # with tempfile.TemporaryDirectory() as pdf_img_dir:
    #     with tempfile.TemporaryDirectory() as pdf_dir:
    #         with open(os.path.join(pdf_dir, "temp.pdf"),"wb") as f:
    #             f.write(pdf)            
    #         class temp_args():
    #             def __init__(self):
    #                 self.in_pdf_dir = pdf_dir
    #                 self.dpi = 300
    #         temp_args_instance = temp_args()
    #         convert2image_pymupdf(temp_args_instance, pdf_img_dir)

    #         dTimer.qcheck("Converting PDF to images")
            
            ##################################
            # Get CCs for generated regions
            ##################################
    ccGrownProTable, page_imgs = get_cc_objects_original_v2(pdf_img_dir, regionBBs, "FR")
    shutil.rmtree(pdf_img_dir)
    # dTimer.check("Get ccs")
    # Merge CCs, chars and graphics into one complete PRO table
    mergedTable = merge_pro_tables(regionCharGeometryPROTable,
                                   ccGrownProTable)
    # dTimer.check("Merge protables")

    # Filter out objects larger than the formula regions
    filteredPROTable = apply_obj_within_region(mergedTable)
    # dTimer.check("Filter large objects")
    # AKS: Grow the region based on objects (CCs, symbol scraper objects)
    grownFilteredPROTable = transform_pr_table(grow_for_contents(page_sizes), 
                                               None, filteredPROTable)
    # dTimer.check("Grow regions")

    objTypeFilter = obj_keep_types( [ 'S', 'GC', 'G' ] )
    regionProTable = filter_pr_table( None, objTypeFilter, grownFilteredPROTable )

    regionProTable = sort_page_regions(regionProTable, sort_obj_types=False)
    # dTimer.check("Filter object entries")
    dTimer.qcheck("Creating merged ProTables: SymbolScraper and YOLO ...")

    visual_imgs, metrics, mapping, digital_table = separate_visual_regions(regionProTable, page_imgs,
                                                                            metrics, filename)

    thresholds = ConfigParser(interpolation=ExtendedInterpolation())
    thresholds.read("modules/chemscraper/data/thresholds-scott.ini")

    thresholds = {
        "REMOVE_ALPHA":thresholds.getfloat("thresholds", "REMOVE_ALPHA"),
        "NEG_CHARGE_Y_POSITION":thresholds.getfloat("thresholds", "NEG_CHARGE_Y_POSITION"),
        "NEG_CHARGE_LENGTH_TOLERANCE":thresholds.getfloat("thresholds", "NEG_CHARGE_LENGTH_TOLERANCE"),
        "Z_TOLERANCE":thresholds.getfloat("thresholds", "Z_TOLERANCE"),
        "CLOSE_NONPARALLEL_ALPHA":thresholds.getfloat("thresholds", "CLOSE_NONPARALLEL_ALPHA"),
        "CLOSE_CHAR_LINE_ALPHA":thresholds.getfloat("thresholds", "CLOSE_CHAR_LINE_ALPHA"),
        "LONGEST_LENGTHS_DIFF_RATIO":thresholds.getfloat("thresholds", "LONGEST_LENGTHS_DIFF_RATIO"),
        "PARALLEL_TOLERANCE":thresholds.getfloat("thresholds", "PARALLEL_TOLERANCE"),        
        "COS_PRUNE":thresholds.getfloat("thresholds", "COS_PRUNE"),
        "HASHED_LENGTH_DIFF_THRESHOLD":thresholds.getfloat("thresholds", "HASHED_LENGTH_DIFF_THRESHOLD"),
    }

    graphs_from_visual = {}
    if len(visual_imgs):
        print(f'\n [ Parsing images using visual parser (LGAP) ]')
        check("* Number of visual regions", len(visual_imgs))
        msts_from_visual = parse_images_from_visual(visual_imgs, thresholds, 
                                                    lgap_server_name=lgap_server_name,
                                                    parallel=True)
        dTimer.qcheck("Parsing images to chemical graphs using visual parser (LGAP)")
    else:
        print(f'\n [ No visual regions detected ]')
        msts_from_visual = None

    graphs_from_visual[filename] = msts_from_visual
    all_mapping = {}
    all_mapping[filename] = mapping

    # print("-----------------")
    # print("MAPPING OF REGION TYPES")
    # pprint(all_mapping)
    # print("-----------------")
    
    mst_generator = ExtractMST(thresholds)
    msts, in_graph, frs, _, _ = mst_generator.extract_mst_poly(digital_table)

    check('* PDF pages with YOLO detections',len(msts))
    all_mst = sum(msts.values(), [])
    check('* Initial MST count (all pages)', len(all_mst))
    print('')

    metrics[filename]['Pages-with-YOLO-detections'] = len(msts)
    metrics[filename]['Initial-MST-count'] = len(all_mst)

    # Get the visual graphs from post-processed graphs

    #pp_msts = post_process_mst(msts, in_graph, frs, mode='pipeline')
    # thresholds = [non_parallel_alpha, 0, char_line_alpha, 0, remove_percent, neg_charge_y_threshold, neg_charge_length_threshold, z_threshold]
    with tempfile.TemporaryDirectory() as img_dir:
        parser = parser_v2.Parser_v2(thresholds=thresholds)
        pp_msts = parser.post_process_msts( msts, in_graph, frs, img_dir,
                                           debug=False, parallel=True)          

    all_pdf_msts[filename] = pp_msts
    all_pdf_frs[filename] = frs
    zip_io = ""
    all_pdf_page_region_bboxes = {}
    all_pdf_page_region_bboxes[filename] = frs

    counter_visual_regions = len(graphs_from_visual[filename] if graphs_from_visual[filename] else [])        
    counter = 0
    counter_digital=0
    if graphs_from_visual[filename]:
        for page_num, regions in all_mapping[filename].items():
            for region_num, type_region in regions.items():
                if type_region == "visual":
                    all_pdf_msts[filename][page_num][region_num] = graphs_from_visual[filename][counter]["graph"]
                    all_pdf_page_region_bboxes[filename][page_num][region_num] = graphs_from_visual[filename][counter]["bbox"]
                    counter+=1
                else:
                    counter_digital+=1
    else:
        counter_digital = num_regions

    check('* Number of Visual regions', counter)
    check('* Number of Born-digital regions', counter_digital)
    # print("VISUAL REGIONS: ",counter)
    # print("BORN DIGITAL REGIONS: ",counter_digital)
    if counter_digital:
        dTimer.qcheck("Parsing PDF objects to chemical graphs using born-digital parser")    

    print(f'\n [ CONVERTING VISUAL GRAPHS TO CDXMLS ... ]')

    msts_copy = copy.deepcopy(all_pdf_msts)
    with tempfile.TemporaryDirectory() as cdxml_td:
        converter = visual2cdxml(dpr_vgraph_dict=all_pdf_msts, 
                                out_dir=cdxml_td, metrics=metrics,
                                write_pages_out=True, svg=False)
        
        metrics = converter.metrics        

        dTimer.qcheck("Converting visual graphs to CDXML (page + doc files)")

        with tempfile.TemporaryDirectory() as smiles_td:
            print('\n [ CONVERTING CDXMLs to SMILES... ]')
            start = time.time()
            metrics = gen_smiles(cdxml_dir=cdxml_td, smi_dir=smiles_td, png=0, metrics=metrics)            
            dTimer.qcheck("CDXML -> SMILES translation (molconvert)")
            print(f'  Converting CDXMLs to SMILES took: {time.time()-start} secs')


            print('* Writing final TSV file for ECFP')
            start = time.time()
            with tempfile.TemporaryDirectory() as tsv_td:
                gen_tsv(smi_dir=smiles_td, frs=all_pdf_frs, tsv_dir=tsv_td, metrics=metrics)
                f = os.listdir(tsv_td)[0]
                with open(os.path.join(tsv_td, f)) as tsv_f:
                    content = StringIO(tsv_f.read())
            print(f'  Writing SMILES TSV took: {time.time()-start} secs')
            dTimer.qcheck("TSV generation")
        
        if generate_svg:
            svg_converted = visual2cdxml(dpr_vgraph_dict=msts_copy, out_dir=cdxml_td, svg=True)            
            dTimer.qcheck("CDXML generation for molconvert SVG rendering")
            in_dir_name = "molecules"
            cdxml_path = os.path.join(cdxml_td, in_dir_name, cdxml_td )
            gen_svg( cdxml_path, out_path=os.path.join(cdxml_td, "molecules") )
            dTimer.qcheck("molconvert SVG image generation")

        with open(f'{cdxml_td}/locations.tsv', mode='w') as f:
            print(content.getvalue(), file=f)
        f = []

        
        for (_, _, filenames) in os.walk(cdxml_td+"/molecules"):            
            f.extend([f"/molecules/{f}" for f in filenames])
            break 
        
        for (_, _, filenames) in os.walk(cdxml_td+"/molecules_all_pages"):                   
            f.extend([f"/molecules_all_pages/{f}" for f in filenames])
            break 

        for (_, _, filenames) in os.walk(cdxml_td+"/molecules_full_cdxml"):            
            f.extend([f"/molecules_full_cdxml/{f}" for f in filenames])
            break 
        
        
        
        f.append("/locations.tsv")
        zip_io = zipfiles(f, cdxml_td)

        ##################################
        # Report times and metrics
        ##################################
        print("\n")
        print(">>----------\n")
        print(dTimer)
        print(">>----------\n")
        print("[ Metrics: ChemScraper pipeline ]")
        pprint(metrics)
        print(">>----------\n")

    return content,zip_io


def annotate_whole_pdf(pdf: bytes, tsv: StringIO, outfile):
    with tempfile.TemporaryDirectory() as td:
        pdf_file_path = os.path.join(td, "test.pdf")
        with open(pdf_file_path, "wb") as f:
            f.write(pdf)

        a = PdfAnnotator(pdf_file_path)
        pro_table = read_pro_tsv_from_io(tsv)
        for doc in pro_table[0]:
            for page_n, page in enumerate(doc[1]):
                for fr in page:
                    for smi in fr[1]:
                        if smi[5] == 'smi' or smi[5] == 'SMI':
                            bbox = [smi[0], smi[1], smi[2], smi[3]]
                            bbox = [b * 72 / 300 for b in bbox]
                            smile_str = smi[4]
                            a.add_annotation(
                                'square',
                                Location(x1=bbox[0], y1=792 - bbox[3], x2=bbox[2], y2=792 - bbox[1],
                                         page=page_n + 1),
                                Appearance(stroke_color=(1, 0, 0), stroke_width=1),
                            )
                            a.add_annotation(
                                'text',
                                Location(x1=bbox[0], y1=792 - bbox[3], x2=bbox[2], y2=792 - bbox[1],
                                         page=page_n + 1),
                                Appearance(content=smile_str, font_size=7, fill=(0, 0, 0)),
                            )
        a.write(outfile)

