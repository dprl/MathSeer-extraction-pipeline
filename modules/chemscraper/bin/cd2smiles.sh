#!/bin/bash

X=`echo $1`
Y=`molconvert smiles $1`

if [ $? -eq 0 ]
then
    echo "$X, $Y"
else
    echo "$X, ERROR"
fi