#!/bin/bash

printf "Now Converting Molecule CDXMLs to SMILES...\n"
# Check if Mol CDXML directory argument is provided
if [ $# -lt 1 ]
then
    echo "Directory path for Mol CDXMLs must be provided!"
    exit 0
fi

# Check if path for saving SMILES is provided, else use default
if [ $# -eq 1 ]
then
    SMI_PATH='Table_cdxml_files/molecule_smiles'
else
    SMI_PATH=$2
fi

# Create the new SMILES directory if it does not exist
if [ ! -d $SMI_PATH ]
then
    mkdir -p $SMI_PATH
fi

# Get all Molecule CDXML files
EXT='*.cdxml'
CD_PATHS="$1/$EXT"
FILE_LIST_NAME='cdxml_list.txt'

FILE_LIST="$SMI_PATH/$FILE_LIST_NAME"

# Save the cdxml list to path
if [ -d $FILE_LIST ]
then
    rm $FILE_LIST
fi
ls $CD_PATHS > $FILE_LIST

# Convert cdxmls to smiles parallely (8 processes)
cat $FILE_LIST | xargs -n 1 -P 8 ./modules/chemscraper/bin/cd2smiles.sh $3 > "$SMI_PATH/smiles_error.txt" > "$SMI_PATH/smiles_out.txt" 2> STDERR_SMILES_AltMetrics

# COUNTER=1
# for f in $CD_PATHS
# do
#     printf "Processing Molecule: $COUNTER\n"
#     F_NAME=$(basename $f .cdxml)
#     SMI_OUT_PATH="$SMI_PATH/$F_NAME.smi"
#     molconvert smiles $f -o $SMI_OUT_PATH | 
#     COUNTER=$[ COUNTER + 1 ]
# done
