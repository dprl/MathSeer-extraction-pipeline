#!/bin/bash
printf ""
printf "Now Converting Molecule CDXMLs to PNGs...\n"
# Check if Mol CDXML directory argument is provided
if [ $# -lt 1 ]
then
    echo "Directory path for Mol CDXMLs must be provided!"
    exit 0
fi

# Check if path for saving SMILES is provided, else use default
if [ $# -eq 1 ]
then
    SMI_PATH='Table_cdxml_files/molecule_png'
else
    SMI_PATH=$2
fi

# Create the new SMILES directory if it does not exist
if [ ! -d $SMI_PATH ]
then
    mkdir -p $SMI_PATH
fi

# Get all Molecule CDXML files
EXT='*.cdxml'
CD_PATHS="$1/$EXT"

COUNTER=1
for f in $CD_PATHS
do
    printf "Processing Molecule: $COUNTER\n"
    F_NAME=$(basename $f .cdxml)
    SMI_OUT_PATH="$SMI_PATH/$F_NAME.png"
    molconvert png:w3000,h4000,b32 $f -o $SMI_OUT_PATH
    COUNTER=$[ COUNTER + 1 ]
done
