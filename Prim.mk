
# Run either of the medium tests normally
generate-chem-medium:
	./generate-data -c 1 -v 0 -p 1 --data_file modules/chemscraper/data/medium_test.csv \
		-id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/medium_test -lgs 0

generate-chem-medium-2:
	./generate-data -c 1 -v 0 -p 1 --data_file modules/chemscraper/data/medium_test2.csv \
		-id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/medium_test2 -lgs 0



# Run either of the medium tests with visualizations
generate-chem-medium-vis:
	./generate-data -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/medium_test.csv \
		-id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/medium_test -lgs 0

generate-chem-medium-2-vis:
	./generate-data -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/medium_test2.csv \
		-id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/medium_test2 -lgs 0



# Run either of the medium tests with visualizations and images showing primitive errors
generate-chem-medium-debug:
	./generate-data -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/medium_test.csv \
		-id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/medium_test -lgs 0 --debug 1

generate-chem-medium-2-debug:
	./generate-data -c 1 -v 1 -p 1 --data_file modules/chemscraper/data/medium_test2.csv \
		-id 0 -sm 2 -o modules/lgap-parser/data/generated-dataset/chem/medium_test2 -lgs 0 --debug 1
