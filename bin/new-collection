#!/bin/bash

################################################################
# new-collection
#
# Create a new collection for graphics extraction
# (pdf files and lists)
#
# Author: R. Zanibbi, June 2022
################################################################

if [ $# -lt 2 ]
then
	echo "Usage: new-collection <name> <pdf-dir> [pdf-txt-list]"
	echo ""
	echo "Takes a collection name and a directory of source pdfs"
	echo "*  optional file list (one per line, no pdf extensions)"
	echo "*  if no file list is provided, one is generated automatically"
	exit 0
fi

# Check directories
ERROR=0
if [ -d "inputs/$1" ]
then
	echo ">>> Collection name error"
	echo "    inputs/$1 already exists."
	ERROR=1
elif [ ! -d $2 ]
then
	echo ">>> PDF source directory error"
	echo "    $2 does not exist."
	ERROR=2
elif [ $# -gt 2 ]
then
	if [ ! -f $3 ]
	then
		echo ">>> File list error"
		echo "    $3 does not exist."
		ERROR=3
	fi
fi

if [ $ERROR -gt 0 ]
then
	exit $ERROR
fi

# Create new collection
mkdir inputs/$1
mkdir inputs/$1/images
ln -s `realpath $2` inputs/$1/pdf

if [ $# -gt 2 ]
then
	cp $3 inputs/$1/pdf_list
else
	bin/pdf-file-list $2 > inputs/$1/pdf_list
fi

# Share details of output
echo ""
echo "PDF collection created:"
tree inputs/$1
echo "** Note: pdf directory is a symbolic link"
