FROM dprl/chemscraperbase:latest

COPY modules/chemscraper modules/chemscraper
COPY modules/pipeline modules/pipeline
COPY modules/protables modules/protables
COPY modules/lgeval modules/lgeval
COPY modules/utils modules/utils
COPY modules/generate_data modules/generate_data
COPY .cz.json .cz.json

RUN apt update
RUN apt install poppler-utils -y

COPY server-requirements.txt server-requirements.txt
# COPY requirements.txt requirements.txt

ENV PYTHONPATH "${PYTHONPATH}:/modules:/modules/pipeline:/modules/protables:/modules/lgeval:/modules/chemscraper:/modules/generate_data:/modules/utils"

RUN pip install -r server-requirements.txt
RUN pip uninstall typing_extensions -y
RUN pip uninstall fastapi -y
RUN pip install --no-cache fastapi==0.83.0
RUN pip install --no-cache pydantic==1.9.2

RUN apt install -y libenchant-2-dev

EXPOSE 8000

CMD uvicorn modules.chemscraper.server.server:app --host 0.0.0.0 --port 8000 --workers 4

